package main

import (
	"fmt"
	"go/ast"
	"go/types"

	"github.com/golangci/plugin-module-register/register"
	"golang.org/x/tools/go/analysis"
)

func init() {
	const name = "jsonrpcretval"
	const doc = "Checks for values returned incorrectly by a JSON-RPC method"

	register.Plugin(name, func(any) (register.LinterPlugin, error) {
		return &customLinter{
			LoadMode: register.LoadModeTypesInfo,
			Analyzer: &analysis.Analyzer{
				Name: name,
				Doc:  doc,
				Run:  jsonRpcRetVal,
			},
		}, nil
	})
}

func jsonRpcRetVal(pass *analysis.Pass) (interface{}, error) {
	for _, f := range pass.Files {
		ast.Inspect(f, func(n ast.Node) bool {
			return findJrpcMethod(pass, n)
		})
	}
	return nil, nil
}

func findJrpcMethod(pass *analysis.Pass, node ast.Node) bool {
	if _, ok := node.(*ast.File); ok {
		return true
	}

	fn, ok := node.(*ast.FuncDecl)
	if !ok {
		return false
	}

	sig, ok := pass.TypesInfo.TypeOf(fn.Name).(*types.Signature)
	if !ok {
		return false
	}

	// Matches func(x X, y Y) (z Z)?
	if sig.Params().Len() != 2 ||
		sig.Results().Len() != 1 {
		return false
	}

	if sig.Params().At(0).Type().String() != "context.Context" ||
		sig.Params().At(1).Type().String() != "encoding/json.RawMessage" ||
		sig.Results().At(0).Type().String() != "interface{}" {
		return false
	}

	jsonRawMsg := sig.Params().At(1).Type()
	jsonPkg := jsonRawMsg.(*types.Named).Obj().Pkg()
	marshaler := jsonPkg.Scope().Lookup("Marshaler").Type().Underlying().(*types.Interface)
	ast.Inspect(fn.Body, func(n ast.Node) bool {
		return findJrpcReturn(pass, n, marshaler)
	})
	return false
}

func findJrpcReturn(pass *analysis.Pass, node ast.Node, jsonMarshaler *types.Interface) bool {
	switch node := node.(type) {
	case *ast.ReturnStmt:
		// Get the type of the value being returned
		expr := node.Results[0]
		typ := pass.TypesInfo.TypeOf(expr)

		// If it is an interface, ignore it
		if _, ok := typ.Underlying().(*types.Interface); ok {
			return false
		}

		// If it implements json.Marshaler, we're good
		if types.Implements(typ, jsonMarshaler) {
			return false
		}

		// The return value appears to not implement json.Marshaler at all, so
		// let it pass
		if !types.Implements(types.NewPointer(typ), jsonMarshaler) {
			return false
		}

		// Returning a non-pointer when MarshalJSON takes a pointer
		d := analysis.Diagnostic{
			Pos:     expr.Pos(),
			End:     expr.End(),
			Message: fmt.Sprintf("%v is not a json.Marshaler (pointer receiver)", typ),
		}

		switch expr := expr.(type) {
		case *ast.CompositeLit, *ast.BasicLit, *ast.Ident, *ast.IndexExpr, *ast.IndexListExpr, *ast.SelectorExpr, *ast.SliceExpr:
			// Add an ampersand
			d.SuggestedFixes = []analysis.SuggestedFix{{
				Message: "Take the address of the expression",
				TextEdits: []analysis.TextEdit{{
					Pos:     expr.Pos(),
					NewText: []byte("&"),
				}},
			}}
		case *ast.StarExpr:
			// Remove the star
			d.SuggestedFixes = []analysis.SuggestedFix{{
				Message: "Do not dereference the pointer",
				TextEdits: []analysis.TextEdit{{
					Pos:     expr.Pos(),
					End:     expr.X.Pos(),
					NewText: []byte(""),
				}},
			}}
		}

		pass.Report(d)
		return false

	case *ast.FuncLit:
		// Do not recurse into function literals
		return false

	default:
		return true
	}
}
