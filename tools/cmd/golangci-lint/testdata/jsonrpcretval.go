package pkg

import (
	"context"
	"encoding/json"
)

type Pointer struct{}

func (*Pointer) MarshalJSON() ([]byte, error) { return nil, nil }

type Value struct{}

func (Value) MarshalJSON() ([]byte, error) { return nil, nil }

type Handler struct{}

func (h *Handler) Interface(ctx context.Context, params json.RawMessage) interface{} {
	var v any
	return v
}

func (h *Handler) TypeWithoutCustomMarshaller(ctx context.Context, params json.RawMessage) interface{} {
	var v struct{}
	return v
}

func (h *Handler) PointerAsPointer(ctx context.Context, params json.RawMessage) interface{} {
	return &Pointer{}
}

func (h *Handler) PointerAsValue(ctx context.Context, params json.RawMessage) interface{} {
	return Pointer{} // want `command-line-arguments\.Pointer is not a json\.Marshaler \(pointer receiver\)`
}

func (h *Handler) ValueAsValue(ctx context.Context, params json.RawMessage) interface{} {
	return Value{}
}

func (h *Handler) ValueAsPointer(ctx context.Context, params json.RawMessage) interface{} {
	return &Value{} // This is safe because it is assignable to json.Marshaler
}
