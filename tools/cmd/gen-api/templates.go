package main

import (
	"bytes"
	_ "embed"
	"go/format"
	"go/parser"
	"go/token"
	"html/template"
	"os"
	"os/exec"
	"regexp"
	"strings"
)

var reNeedsSpace = regexp.MustCompile(`\p{Ll}\p{Lu}`)

var Server = mustParseTemplate("server", serverSrc)
var Client = mustParseTemplate("client", clientSrc)
var Docs = mustParseTemplate("docs", docsSrc)

//go:embed server.go.tmpl
var serverSrc string

//go:embed client.go.tmpl
var clientSrc string

//go:embed docs.md.tmpl
var docsSrc string

func mustParseTemplate(name, src string) *template.Template {
	tmpl, err := template.New(name).Funcs(template.FuncMap{
		"service": func(svc *Service) string {
			s := strings.TrimSuffix(svc.Name, "Service")
			return reNeedsSpace.ReplaceAllStringFunc(s, func(s string) string {
				return s[:1] + " " + s[1:]
			})
		},
	}).Parse(src)
	checkf(err, "bad template")
	return tmpl
}

func GoFmt(filePath string, buf *bytes.Buffer) error {
	f, err := os.Create(filePath)
	if err != nil {
		return err
	}
	defer f.Close()

	fset := token.NewFileSet()
	file, err := parser.ParseFile(fset, filePath, buf, parser.ParseComments)
	if err != nil {
		// If parsing fails, write out the unformatted code. Without this,
		// debugging the generator is a pain.
		_, _ = buf.WriteTo(f)
	}
	if err != nil {
		return err
	}

	err = format.Node(f, fset, file)
	if err != nil {
		return err
	}

	err = exec.Command("go", "run", "github.com/rinchsan/gosimports/cmd/gosimports", "-w", filePath).Run()
	if err != nil {
		return err
	}

	return nil
}
