package main

import (
	"bytes"
	"fmt"
	"go/ast"
	"go/doc"
	"go/token"
	"go/types"
	"os"
	"path/filepath"
	"reflect"
	"strings"

	"github.com/spf13/cobra"
	"golang.org/x/tools/go/packages"
)

func main() {
	_ = cmd.Execute()
}

func fatalf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, "Error: "+format+"\n", args...)
	os.Exit(1)
}

func check(err error) {
	if err != nil {
		fatalf("%v", err)
	}
}

func checkf(err error, format string, otherArgs ...interface{}) {
	if err != nil {
		fatalf(format+": %v", append(otherArgs, err)...)
	}
}

var cmd = &cobra.Command{
	Use: "gen-api",
}

var cmdServer = &cobra.Command{
	Use:   "server [services]",
	Short: "Generate a JSON-RPC server",
	Args:  cobra.MinimumNArgs(1),
	Run:   genServer,
}

var cmdClient = &cobra.Command{
	Use:   "client [services]",
	Short: "Generate a JSON-RPC client",
	Args:  cobra.MinimumNArgs(1),
	Run:   genClient,
}

var cmdDocs = &cobra.Command{
	Use:   "docs [services]",
	Short: "Generate a JSON-RPC docs",
	Args:  cobra.MinimumNArgs(1),
	Run:   genDocs,
}

var flag = struct {
	Dir string
	Out string
}{}

var flagClient = struct {
	Dir string
	Out string
}{}

var flagDocs = struct {
	Dir          string
	HeaderIndent uint
}{}

func init() {
	cmd.AddCommand(
		cmdServer,
		cmdClient,
		cmdDocs,
	)

	cmdServer.Flags().StringVarP(&flag.Dir, "dir", "d", ".", "Package to load services from")
	cmdServer.Flags().StringVarP(&flag.Out, "out", "o", "server_gen.go", "Output file name")

	cmdClient.Flags().StringVarP(&flagClient.Dir, "dir", "d", ".", "Package to load services from")
	cmdClient.Flags().StringVarP(&flagClient.Out, "out", "o", "client_gen.go", "Output file name")

	cmdDocs.Flags().StringVarP(&flagDocs.Dir, "dir", "d", ".", "Package to load services from")
	cmdDocs.Flags().UintVarP(&flagDocs.HeaderIndent, "indent", "i", 2, "Base level of indentation for headers")
}

func genServer(_ *cobra.Command, args []string) {
	checkOutput(&flag.Out, "server_gen.go")

	w := new(bytes.Buffer)
	check(Server.Execute(w, load(flag.Dir, flag.Out, args)))
	check(GoFmt(flag.Out, w))
}

func genClient(_ *cobra.Command, args []string) {
	checkOutput(&flagClient.Out, "client_gen.go")

	w := new(bytes.Buffer)
	check(Client.Execute(w, load(flagClient.Dir, flagClient.Out, args)))
	check(GoFmt(flagClient.Out, w))
}

func genDocs(_ *cobra.Command, args []string) {
	check(Docs.Execute(os.Stdout, load(flagDocs.Dir, flagDocs.Dir, args)))
}

func checkOutput(s *string, def string) {
	if strings.HasSuffix(*s, string(filepath.Separator)) {
		*s = filepath.Join(*s, def)
	}
}

func load(dir, out string, names []string) *Data {
	// Load source package
	srcPkg := loadSinglePackage(&packages.Config{
		Mode: packages.NeedName |
			packages.NeedTypes |
			packages.NeedTypesInfo |
			packages.NeedSyntax,
	}, dir)

	// Parse services
	var services []*Service
	for _, name := range names {
		services = append(services, parseService(srcPkg, name))
	}

	// Load destination package
	dstPkg := loadSinglePackage(&packages.Config{
		Mode: packages.NeedName,
	}, filepath.Dir(out))

	data := new(Data)
	data.Package = dstPkg.Name
	data.Services = services
	data.MdBaseHeader = strings.Repeat("#", int(flagDocs.HeaderIndent))

	if srcPkg.PkgPath != dstPkg.PkgPath {
		data.Qualifier = srcPkg.Name + "."
	}

	return data
}

type Data struct {
	Package      string
	Qualifier    string
	MdBaseHeader string
	Services     []*Service
}

type Service struct {
	Name        string
	Description string
	Methods     []*Method
}

type Method struct {
	Name         string
	Description  string
	Input        string
	Output       string
	InputFields  []*Field
	OutputFields []*Field
}

type Field struct {
	Name     string
	Type     string
	Optional bool
}

func loadSinglePackage(cfg *packages.Config, dir string) *packages.Package {
	wd, err := os.Getwd()
	check(err)

	if filepath.IsAbs(dir) {
		dir, err = filepath.Rel(wd, dir)
		check(err)
	}
	if dir != "." && !strings.HasPrefix(dir, "./") {
		dir = "./" + dir
	}

	pkgs, err := packages.Load(cfg, dir)
	check(err)
	if len(pkgs) != 1 {
		fatalf("expected one package, got %d\n", len(pkgs))
	}
	pkg := pkgs[0]
	for _, err := range pkg.Errors {
		fmt.Println(err)
	}
	for _, err := range pkg.Errors {
		fmt.Println(err)
	}
	if len(pkg.Errors) != 0 {
		os.Exit(1)
	}
	return pkg
}

func parseService(pkg *packages.Package, name string) *Service {
	docPkg, err := doc.NewFromFiles(pkg.Fset, pkg.Syntax, pkg.Name, doc.PreserveAST)
	check(err)

	obj := pkg.Types.Scope().Lookup(name)
	if obj == nil {
		fatalf("cannot find %s", name)
	}
	tn, ok := obj.(*types.TypeName)
	if !ok {
		fatalf("%s is not a type", name)
	}
	iface, ok := tn.Type().Underlying().(*types.Interface)
	if !ok {
		fatalf("%s is not an interface", name)
	}

	service := new(Service)
	service.Name = tn.Name()

	if c := findComments(pkg, obj); c != "" {
		c = strings.TrimPrefix(c, service.Name+" ")
		c = strings.ToUpper(c[:1]) + c[1:]
		c = strings.TrimSpace(c)
		service.Description = string(docPkg.Markdown(c))
	}

	for i, n := 0, iface.NumMethods(); i < n; i++ {
		m := iface.Method(i)
		fun := m.Type().(*types.Signature)
		if n := fun.Params().Len(); n != 1 && n != 2 {
			fatalf("%s.%s: expected 1 or 2 parameters, got %d", name, m.Name(), fun.Params().Len())
		}
		if fun.Results().Len() != 2 {
			fatalf("%s.%s: expected 2 results, got %d", name, m.Name(), fun.Results().Len())
		}
		if typeName(fun.Params().At(0).Type()) != "context.Context" {
			fatalf("%s.%s: first parameter should be context.Context, got %s", name, m.Name(), fun.Params().At(0).Type())
		}
		if typeName(fun.Results().At(1).Type()) != "error" {
			fatalf("%s.%s: second result should be error, got %s", name, m.Name(), fun.Results().At(1).Type())
		}

		method := new(Method)
		method.Name = m.Name()
		if fun.Params().Len() > 1 {
			in := fun.Params().At(1).Type()
			method.Input = typeName(in)
			method.InputFields = fields(in)
		}
		out := fun.Results().At(0).Type()
		method.Output = typeName(out)
		method.OutputFields = fields(out)
		service.Methods = append(service.Methods, method)

		c := findComments(pkg, m)
		if c == "" {
			continue
		}

		c = strings.TrimPrefix(c, method.Name+" ")
		c = strings.ToUpper(c[:1]) + c[1:]
		c = strings.TrimSpace(c)
		method.Description = string(docPkg.Markdown(c))
	}

	return service
}

func findComments(pkg *packages.Package, obj types.Object) string {
	var file *ast.File
	for _, file = range pkg.Syntax {
		if file.Pos() <= obj.Pos() && obj.Pos() <= file.End() {
			goto foundFile
		}
	}
	return ""

foundFile:
	var comments *ast.CommentGroup
	ast.Walk(&astVisitor{pos: obj.Pos(), fn: func(nodes []ast.Node) bool {
		n := nodes[len(nodes)-1]
		if !(n.Pos() <= obj.Pos() && obj.Pos() <= n.End()) {
			return false
		}
		if _, ok := n.(*ast.Ident); !ok {
			return true
		}

		// For type definitions
		if len(nodes) > 2 {
			decl, ok := nodes[len(nodes)-3].(*ast.GenDecl)
			if ok {
				comments = decl.Doc
				return true
			}
		}

		// For interface methods
		if len(nodes) > 1 {
			field, ok := nodes[len(nodes)-2].(*ast.Field)
			if ok {
				comments = field.Doc
				return true
			}
		}
		return true
	}}, file)
	if comments == nil {
		return ""
	}
	var s []string
	for _, l := range strings.Split(comments.Text(), "\n") {
		l = strings.TrimPrefix(l, "//")
		l = strings.TrimSpace(l)
		s = append(s, l)
	}
	return strings.Join(s, "\n")
}

type astVisitor struct {
	pos token.Pos
	fn  func([]ast.Node) bool

	stack *[]ast.Node
	depth int
}

func (v *astVisitor) Visit(node ast.Node) ast.Visitor {
	if node == nil {
		return nil
	}
	if !(node.Pos() <= v.pos && v.pos <= node.End()) {
		return nil
	}
	if v.stack == nil {
		v.stack = new([]ast.Node)
	}
	if len(*v.stack) <= v.depth {
		*v.stack = append(*v.stack, make([]ast.Node, v.depth-len(*v.stack)+1)...)
	}
	(*v.stack)[v.depth] = node

	if !v.fn((*v.stack)[:v.depth+1]) {
		return nil
	}

	u := *v
	u.depth++
	return &u
}

func typeName(typ types.Type) string {
	switch typ := typ.(type) {
	case *types.Named:
		obj := typ.Obj()
		if obj.Pkg() == nil {
			return obj.Name()
		}
		return obj.Pkg().Name() + "." + obj.Name()
	case *types.Pointer:
		return "*" + typeName(typ.Elem())
	case *types.Slice:
		return "[]" + typeName(typ.Elem())
	case *types.Basic:
		return typ.Name()
	default:
		panic(fmt.Errorf("unsupported type %T", typ))
	}
}

func fields(typ types.Type) []*Field {
	if ptr, ok := typ.(*types.Pointer); ok {
		typ = ptr.Elem()
	}
	if named, ok := typ.(*types.Named); ok {
		typ = named.Underlying()
	}
	st, ok := typ.(*types.Struct)
	if !ok {
		return nil
	}

	var fields []*Field
	for i, n := 0, st.NumFields(); i < n; i++ {
		field := st.Field(i)
		_ = field
		tag := reflect.StructTag(st.Tag(i))
		name := strings.SplitN(tag.Get("json"), ",", 2)[0]
		if name == "-" {
			continue
		}
		if name == "" {
			name = field.Name()
		}
		tn := typeName(field.Type())
		switch tn {
		case "json.RawMessage":
			tn = "any"
		case "[]byte", "[32]byte":
			tn = "hex"
		}
		fields = append(fields, &Field{
			Name:     name,
			Type:     tn,
			Optional: !contains(strings.Split(tag.Get("validate"), ","), "required"),
		})
	}
	return fields
}

func contains[E comparable, T ~[]E](s T, e E) bool {
	for _, x := range s {
		if x == e {
			return true
		}
	}
	return false
}
