# Accumulate Core Wallet

## Command-line interface

The CLI lives in `./cmd/accumulate`. It can be run directly via `go run ./cmd/accumulate`, which builds to a
temporary directory and executes the binary in one go. It can be built via `go build ./cmd/accumulate`, which
creates `accumulate` or `accumulate.exe` in the current directory. It can be installed to `$GOPATH/bin/accumulate` (
GOPATH defaults to `$HOME/go`) via
`go install ./cmd/accumulate`.

## Graphical interface

To run in live development mode, run `wails dev` in the project directory. This will run a Vite development
server that will provide very fast hot reload of your frontend changes. If you want to develop in a browser
and have access to your Go methods, there is also a dev server that runs on http://localhost:34115. Connect
to this in your browser, and you can call your Go code from devtools.

To build a redistributable, production mode package, use `wails build`.

If you don't have [`wails`](https://wails.io) installed, you can run `go run
github.com/wailsapp/wails/v2/cmd/wails [X]` instead of `wails [X]`.