build 1/2:
  extends: [ .rules all ]
  needs: []
  script:
  - go build -v ./...

build 2/2:
  extends: [ .rules all ]
  needs: []
  script:
  - cd cmd/accumulate-app
  - apt-get update -qq && apt-get install -yqq npm libgtk-3-dev libwebkit2gtk-4.0-dev
  - go run github.com/wailsapp/wails/v2/cmd/wails build

test 1/2:
  extends: [ .rules all ]
  needs: []
  artifacts:
    when: always
    reports:
      junit: report.xml
  script:
  - go run gotest.tools/gotestsum --junitfile report.xml --format testname -- -tags testnet ./...

test 2/2:
  extends: [ .rules all, .windows go ]
  needs: []
  artifacts:
    when: always
    reports:
      junit: report.xml
  script:
  - go run gotest.tools/gotestsum --junitfile report.xml --format testname -- -tags testnet ./cmd/accumulate/...

lint 1/2:
  extends: [ .rules all ]
  needs: []
  variables:
    GIT_DEPTH: 0
  before_script:
    - function die { echo -e "${BOLD_RED}${1}${NO_COLOR}"; false; }
    - function check-diff { git diff --quiet "$@" && return 0; git --no-pager -c color.ui=always diff; return 1; }
  script:
    - echo -e "${SECTION}section_start:`date +%s`:generate\r${SECTION}Verify generated files are up to date"
    - go generate -x ./...
    - check-diff || die "Generated files are not up to date. Please run \`go generate ./...\`."
    - echo -e "${SECTION}section_end:`date +%s`:generate\r${SECTION}"

    - echo -e "${SECTION}section_start:`date +%s`:lint\r${SECTION}Lint"
    - go run ./tools/cmd/golangci-lint run --verbose --timeout=20m
    - echo -e "${SECTION}section_end:`date +%s`:lint\r${SECTION}"

    - echo -e "${SECTION}section_start:`date +%s`:tidy\r${SECTION}Verify go.mod is tidy"
    - go mod tidy
    - check-diff go.mod go.sum || die "Go mod files are not tidy. Please run \`go mod tidy\`."
    - echo -e "${SECTION}section_end:`date +%s`:tidy\r${SECTION}"

    - echo -e "${SECTION}section_start:`date +%s`:imports\r${SECTION}Verify code is correctly formatted"
    - go run github.com/rinchsan/gosimports/cmd/gosimports -l */ | tee fmt.log
    - test -s fmt.log && die "Code is incorrectly formatted. Please run \`gosimports -w .\` (or \`./scripts/imports.sh\`)."
    - echo -e "${SECTION}section_end:`date +%s`:imports\r${SECTION}"

lint 2/2: # Python
  needs: []
  image: python
  variables:
    PIP_ROOT_USER_ACTION: ignore
  before_script:
  - function die { echo -e "${BOLD_RED}${1}${NO_COLOR}"; false; }
  - pip install -U pip
  - pip install yamllint
  script:
  - echo -e "${SECTION}section_start:`date +%s`:yaml\r${SECTION}Lint YAML"
  - yamllint .
  - git diff --quiet || { git status; git --no-pager -c color.ui=always diff; die "YAML files have lint"; }
  - echo -e "${SECTION}section_end:`date +%s`:yaml\r${SECTION}"

### MERGE ###

mr bott:
  needs: []
  rules:
  - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
  variables:
    PROJECT: $CI_PROJECT_PATH
    MERGE_REQUEST: $CI_MERGE_REQUEST_IID
  trigger: accumulatenetwork/core/gitlab-bot

### RELEASE ###

build binaries 1/6:
  # Build CLI for Linux
  rules:
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG != null || $BUILD_BIN != null) && $CI_PIPELINE_SOURCE != 'merge_request_event'
  script:
    - git fetch --unshallow
    - go install github.com/go-task/task/v3/cmd/task@latest
    - task build-cli-linux
  artifacts:
    paths:
      - accumulate-*

build binaries 2/6:
  # Build CLI for Windows
  extends: [ .rules all, .windows go ]
  rules:
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG != null || $BUILD_BIN != null) && $CI_PIPELINE_SOURCE != 'merge_request_event'
  script:
    - git fetch --unshallow
    - go install github.com/go-task/task/v3/cmd/task@latest
    - task build-cli-windows
  artifacts:
    paths:
      - accumulate-*

build binaries 3/6:
  # Build CLI for macOS
  image: macos-14-xcode-15
  tags:
    - saas-macos-medium-m1
  rules:
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG != null || $BUILD_BIN != null) && $CI_PIPELINE_SOURCE != 'merge_request_event'
  variables:
    # It's a CI job, no reason to run cleanup
    HOMEBREW_NO_INSTALL_CLEANUP: 1
  script:
    # Node is already installed. Go is too but we need 1.22, and we should also
    # use a fixed version to stay consistent.
    - git fetch --unshallow
    - brew install go@1.22
    - brew link go@1.22
    - go install github.com/go-task/task/v3/cmd/task@latest
    - export PATH="$PATH:$(go env GOPATH)/bin"
    - task build-cli-darwin
  artifacts:
    paths:
      - accumulate-*

build binaries 4/6:
  # Build GUI for Linux and Windows
  rules:
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG != null || $BUILD_BIN != null) && $CI_PIPELINE_SOURCE != 'merge_request_event'
  script:
    - git fetch --unshallow
    - apt update -y && apt install -qqy npm pkg-config libgtk-3-dev libwebkit2gtk-4.0-dev
    - go install github.com/go-task/task/v3/cmd/task@latest
    - go install github.com/wailsapp/wails/v2/cmd/wails
    - task build-app-linux build-app-windows
  artifacts:
    paths:
      - accumulate-*

build binaries 5/6:
  # Build GUI for Windows
  rules:
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG != null || $BUILD_BIN != null) && $CI_PIPELINE_SOURCE != 'merge_request_event'
  script:
    - echo "Wails explodes so this job is disabled"
  variables:
    GIT_STRATEGY: none
  # tags:
  #   - saas-windows-medium-amd64
  # script:
  #   - git fetch --unshallow
  #   - choco upgrade -y golang --version=1.22
  #   - Import-Module $env:ChocolateyInstall\helpers\chocolateyProfile.psm1
  #   - refreshenv
  #   - go version
  #   - go install github.com/go-task/task/v3/cmd/task@latest
  #   - go install github.com/wailsapp/wails/v2/cmd/wails
  #   - task build-app-windows
  # artifacts:
  #   paths:
  #     - accumulate-*


build binaries 6/6:
  # Build GUI for macOS
  image: macos-14-xcode-15
  tags:
    - saas-macos-medium-m1
  rules:
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG != null || $BUILD_BIN != null) && $CI_PIPELINE_SOURCE != 'merge_request_event'
  variables:
    # It's a CI job, no reason to run cleanup
    HOMEBREW_NO_INSTALL_CLEANUP: 1
  script:
    # Node is already installed. Go is too but we need 1.22, and we should also
    # use a fixed version to stay consistent.
    - git fetch --unshallow
    - brew install go@1.22
    - brew link go@1.22
    - go install github.com/go-task/task/v3/cmd/task@latest
    - go install github.com/wailsapp/wails/v2/cmd/wails
    - export PATH="$PATH:$(go env GOPATH)/bin"
    - task build-app-darwin
  artifacts:
    paths:
      - accumulate-*

binaries:
  # Collect all the binaries in a single job
  rules:
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG != null || $BUILD_BIN != null) && $CI_PIPELINE_SOURCE != 'merge_request_event'
  needs:
    - build binaries 1/6
    - build binaries 2/6
    - build binaries 3/6
    - build binaries 4/6
    - build binaries 5/6
    - build binaries 6/6
  script: echo Binaries
  variables:
    GIT_STRATEGY: none
  artifacts:
    paths:
      - accumulate-*

### SETUP / DEFAULTS ###

image: golang:1.22

cache:
  key: go-mod-cache
  paths:
    - .go-mod-cache
    - .golangci-cache

variables:
  BOLD_RED: '\e[1;31m'
  NO_COLOR: '\e[0m'
  SECTION: '\e[0K'
  GOMODCACHE: ${CI_PROJECT_DIR}/.go-mod-cache
  GOLANGCI_LINT_CACHE: ${CI_PROJECT_DIR}/.golangci-cache

.rules all:
  rules:
  - if: $CI_PIPELINE_SOURCE != ''

.rules release:
  rules:
  - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH || $CI_COMMIT_TAG != null) && $CI_PIPELINE_SOURCE != 'merge_request_event'

.windows go:
  tags:
    - saas-windows-medium-amd64
  before_script:
    - choco upgrade -y golang --version=1.22
    - Import-Module $env:ChocolateyInstall\helpers\chocolateyProfile.psm1
    - refreshenv

### SCANNING ###

include:
- template: Jobs/Dependency-Scanning.gitlab-ci.yml
- template: Jobs/SAST.gitlab-ci.yml
- template: Jobs/Secret-Detection.gitlab-ci.yml

gemnasium-dependency_scanning:
  variables:
    DS_EXCLUDED_PATHS: .go-mod-cache
    DS_MAX_DEPTH: 5
  rules:
  - if: $CI_PIPELINE_SOURCE

semgrep-sast:
  variables:
    SAST_EXCLUDED_PATHS: .go-mod-cache,*_test.go
  rules:
  - if: $CI_PIPELINE_SOURCE

secret_detection:
  variables:
    SECRET_DETECTION_EXCLUDED_PATHS: .go-mod-cache
  rules:
  - if: $CI_PIPELINE_SOURCE
