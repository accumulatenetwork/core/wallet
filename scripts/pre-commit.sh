#!/bin/bash

set -e

if which yamllint &> /dev/null; then
    yamllint .
else
    echo "Skipping YAML: can't find yamllint. Run \`pip install yamllint\` to install."
fi

go mod tidy
go generate ./...
go run github.com/rinchsan/gosimports/cmd/gosimports -w .
go vet ./...
go run ./tools/cmd/golangci-lint run --verbose --timeout=10m
