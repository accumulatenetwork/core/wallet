#!/bin/bash

chico=acc://5eee79bf10e13d26532067ff93473aced45e68942f4cc58e
harpo=acc://8973bbaf6843279aea6a63d0a795abcfaf624f741b0a8722
groucho=acc://df8381728d3798253f5aff9755924a152ce12e9b7e694ac9

function accumulate { command accumulate --spawn-daemon=true --wallet ~/.accumulate/testnets --server https://fozzie.accumulatenetwork.io --vault main "$@"; }

while true; do
    for lite in $chico $harpo $groucho; do
        tokens=$(accumulate get $lite/ACME -j | jq -r '.data.balance // 0')
        if ((tokens/10**8 < 10)); then
            echo "    Getting tokens for $lite"
            txn=$(accumulate faucet $lite -j | grep '^{' | jq -r .transactionHash)
            accumulate tx get $txn --wait 10s --wait-synth 10s
        fi

        credits=$(accumulate get $lite -j | jq '.data.creditBalance // 0')
        if ((credits/10**2 < 3000)); then
            echo "    Buying credits for $lite"
            txn=$(accumulate credits $lite/ACME $lite $((3000 - credits/10**2)) -j | grep '^{' | jq -r .transactionHash)
            accumulate tx get $txn --wait 10s --wait-synth 10s
        fi
    done

    for ((i=1;i<=1000;i++)); do
        accumulate tx create $chico/ACME $harpo/ACME 0.001
        accumulate tx create $harpo/ACME $groucho/ACME 0.001
        accumulate tx create $groucho/ACME $chico/ACME 0.001
    done
done
