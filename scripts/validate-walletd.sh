#!/bin/bash

# Exit on error
set -e

tmpDir=$(mktemp -d -t accumulated-ci-XXXXXXXX)

# die <message> - Print an error message and exit
function die {
  >&2 echo -e '\033[1;31m'"$@"'\033[0m'
  exit 1
}

die "Moved to cmd/accumulate/walletd/validate_test.go"

# call <method> [<params>] - Call a wallet daemon method
function call {
    local METHOD=''
    local PARAMS='{}'
    local WITH_DB=false
    while [ $# -gt 0 ]; do
        case "$1" in
            --with-db)
                WITH_DB=true
                ;;
            *)
                if [ -z "$METHOD" ]; then
                    METHOD=$1
                else
                    PARAMS="$1"
                fi
                ;;
        esac
        shift
    done

    if $WITH_DB; then
        PARAMS=$(echo '{"vault":{"filePath":"'$tmpDir'"}}'$'\n'"$PARAMS" | jq -s add)
    fi

    curl -m 120 -k -s -X POST --data-binary '{"jsonrpc": "2.0", "id": 1, "method":"'$METHOD'", "params":'"$PARAMS"'}' https://localhost:26661/wallet
}

# call-success <method> [<params>] - Call a wallet daemon method and exit if it fails
function call-success {
    local RESPONSE=$(call "$@")
    if jq -e .error <<< $RESPONSE &> /dev/null ; then
        >&2 jq .error <<< $RESPONSE
        die Failed
    else
        jq -re .result <<< $RESPONSE
        return $?
    fi
}

echo "Start the server"
go build ./cmd/accumulate
echo $tmpDir
./accumulate wallet serve --pid-file $tmpDir/daemon.pid --listen https://127.0.0.1:26661 --pinentry disable &
# get the Process ID
declare -g ACCPID=$!
sleep 0.1

# Kill server on exit
function cleanup {
    trap - SIGTERM
    ./accumulate wallet stop --pid-file $tmpDir/daemon.pid
    rm -rf "$tmpDir"
}
trap cleanup SIGINT SIGTERM EXIT

echo "Get Version"
call-success version

echo "Verify wallet is locked"
RESPONSE=$(call key-list --with-db)
if jq -e .error <<< $RESPONSE &> /dev/null ; then
    MSG=$(jq -re .error.data.message <<< $RESPONSE)
    if [ "$MSG" != "no password specified for encrypted database" ]; then
        die "Expected 'no password specified for encrypted database', got '$MSG'"
    fi
else
    die Call did not fail
fi

echo "Unlock"
call-success unlock-vault --with-db '{"password": "foobarbaz", "timeout": "1h"}'

echo "Import mnemonic"
call-success import-mnemonic --with-db '{"mnemonic": ["yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"]}'

echo "Generate key"
call-success generate-key --with-db '{"label": "keytest"}'

echo "Reload the wallet"
# This needs to fully close the wallet to ensure it will be reloaded from
# scratch
call-success lock-vault '{"close": true}'
call-success unlock-vault --with-db '{"password": "foobarbaz", "timeout": "1h"}'

echo "List Keys"
RESULT=$(call-success key-list --with-db)
jq <<< $RESULT
if ! LABEL=$(jq -re .keyList[0].labels.names[0] <<< $RESULT); then
    die "Failed to get key"
elif [ "$LABEL" != "keytest" ]; then
    die "Expected 'keytest', got '$LABEL'"
fi

echo "Register ADI"
call-success register-adi --with-db '{"adi":"acc://RedWagon.acme"}'

echo "Get ADI List"
RESULT=$(call-success adi-list --with-db)
jq <<< $RESULT
RESULT=$(jq -re .adis[0].url <<< "$RESULT") || die "Response does not include the expected value"
[ "$RESULT" == "acc://RedWagon.acme" ] || die "Response does not include the expected value"

echo "Get Key List"
call-success key-list --with-db

echo "Create New Transaction"
call-success create-transaction --with-db '{"name":"test-tx8","principal":"acc//test"}'

echo "Add output to Transaction"
call-success add-output --with-db '{"name":"test-tx8","recipient":{"url":"acc://test/token","amount":"100"}}'

echo "Sign Transaction"
SIGNEDTX=$(call-success sign-transaction --with-db '{"name":"test-tx8","keyName":"keytest","signerVersion":1}')

echo "Compose Transaction"
call-success compose-transaction --with-db '{"name":"test-tx8"}'

echo "List Transactions"
RESULT=$(call-success list-transactions --with-db)
TXNAME=$(jq -r .names[0] <<< "${RESULT}")
if [ "$TXNAME" == "test-tx8" ]
then
    echo "success $TXNAME"
else
    die "Transaction not found in ${RESULT}"
fi

echo "Delete Transaction"
call-success delete-transaction --with-db '{"name":"test-tx8"}'


echo "No Transactions should be listed"
RESULT=$(call-success list-transactions --with-db)
TXNAME=$(jq -r .names[0] <<< "${RESULT}")
if [ "$TXNAME" == "test-tx8" ]
then
    die "transaction was not deleted"
else
    echo "success transaction was deleted"
fi

TXHASH=$(jq -r .envelope.signatures[0].transactionHash <<< "$SIGNEDTX")
SIGNER=$(jq -r .envelope.signatures[0].signer <<< "$SIGNEDTX")
TIMESTAMP=$(jq -r .envelope.signatures[0].timestamp <<< "$SIGNEDTX")
EXPECT_SIGNATURE=$(jq -r .envelope.signatures[0].signature <<< "$SIGNEDTX")
RESULT=$(call-success sign-message --with-db '{"keyName":"keytest","signerVersion":1,"timestamp":'$TIMESTAMP',"signer":"'"$SIGNER"'","message":"'"$TXHASH"'","isHash":true}')
jq <<< $RESULT
SIGNATURE=$(jq -r .signature.signature <<< "$RESULT")
[ "$EXPECT_SIGNATURE" == "$SIGNATURE" ] || die "expected signature does not match sign-message signature"$'\n'" Want: $EXPECT_SIGNATURE"$'\n'" Got:  $SIGNATURE"

echo "Export Vault"
RESULT=$(call-success export-vault --with-db)
jq <<< $RESULT
EXPECT="yellow yellow yellow yellow yellow yellow yellow yellow yellow yellow yellow yellow"
MNEMONIC=$( jq -r .vault.seedInfo.mnemonic <<< $RESULT)
[ "$MNEMONIC" == "$EXPECT" ] || die "Data mismatch:"$'\n'"  Want: $EXPECT"$'\n'"   Got: $MNEMONIC"

# https://gitlab.com/accumulatenetwork/sdk/test-data/-/blob/main/protocol.1.json
TEST_JSON='{ "signatures": [ { "type": "legacyED25519", "timestamp": 255288238, "publicKey": "79b2e21740c5dc73a59f580c6390cb93aebe58e158a4856264e0ba534f75bd05", "signature": "fa7668a581fc19b0ddb16b132e72c57e7029c5574b235c748a6c6ca7df40d747a8a9556c0f5e60c3ab464fd8ad73b8fcd7101df14b537e191c5e632a3a877301", "signer": "acc://lite-token-account.acme/ACME", "signerVersion": 1, "transactionHash": "c1e3541ab8ef58598131f8d2fc31af7bc45a977154482212dae3250df11fa730" } ], "transaction": [ { "header": { "principal": "acc://lite-token-account.acme/ACME", "initiator": "039b4ae4578e8c78ae2b1b316da136de40c63f6be3c480eeabb5091bbe09d225" }, "body": { "type": "createIdentity", "url": "acc://adi.acme", "keyHash": "79b2e21740c5dc73a59f580c6390cb93aebe58e158a4856264e0ba534f75bd05" } } ] }'
TEST_BIN='01b201010102aec7dd79032079b2e21740c5dc73a59f580c6390cb93aebe58e158a4856264e0ba534f75bd050440fa7668a581fc19b0ddb16b132e72c57e7029c5574b235c748a6c6ca7df40d747a8a9556c0f5e60c3ab464fd8ad73b8fcd7101df14b537e191c5e632a3a87730105226163633a2f2f6c6974652d746f6b656e2d6163636f756e742e61636d652f41434d45060108c1e3541ab8ef58598131f8d2fc31af7bc45a977154482212dae3250df11fa730037d014501226163633a2f2f6c6974652d746f6b656e2d6163636f756e742e61636d652f41434d4502039b4ae4578e8c78ae2b1b316da136de40c63f6be3c480eeabb5091bbe09d22502340101020e6163633a2f2f6164692e61636d65032079b2e21740c5dc73a59f580c6390cb93aebe58e158a4856264e0ba534f75bd05'

echo "Encode Transaction"
RESP=$(call-success encode '{ "dataJson": '"$TEST_JSON"' }')
BIN=$(jq -re .envelopeBinary <<< "$RESP") || die "Response does not include .envelopeBinary"
[ "$BIN" == "$TEST_BIN" ] || die "Response does not match expected value"
echo "$BIN"

echo "Decode Transaction"
RESP=$(call-success decode '{ "dataBinary": "'"$BIN"'" }')
JSON=$(jq -re .dataJson <<< "$RESP") || die "Response does not include .dataJson"
[ "$(jq <<< "$JSON")" == "$(jq <<< "$TEST_JSON")" ] || die "Response does not match expected value"
