package interactive

import (
	"fmt"
	"os"

	"github.com/fatih/color"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

func AuthenticateVault(v *api.VaultInfo, attempt int, lastErr error) ([]byte, error) {
	if v.OnePasswordRef != "" {
		pass, err := GetOnePassword(v.OnePasswordRef)
		if err != nil {
			return nil, err
		}
		if pass != nil {
			// Confirmation is unnecessary when retrieving the password from
			// 1Password
			return pass, nil
		}
	}

	name := v.Name
	if name == "" {
		name = v.FilePath
	}

	// Replace prompt with an error
	if lastErr != nil {
		color.Red(fmt.Sprintf("Error: %s", lastErr))
	}

	short := "Password: "
	prompt := fmt.Sprintf("Unlock %s vault", name)
	if attempt > 1 {
		prompt += fmt.Sprintf(" (attempt %d/3)", attempt)
	}
	pass, err := GetPassword(prompt, short, os.Stdin, os.Stderr, true)
	if err != nil {
		return nil, errors.NoPassword.With("no password specified for encrypted vault")
	}

	return []byte(pass), nil
}
