package interactive

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"os/exec"

	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

func isOnePassWarn(err error) bool {
	return errors.Is(err, errors.OnePassNotSupported)
}

func GetOnePassword(ref string) ([]byte, error) {
	b, err := readOnePassword(ref)
	switch {
	case err == nil:
		return b, nil
	case isOnePassWarn(err):
		// If 1Password integration is not available, warn the user and fall back
		fmt.Fprintf(os.Stderr, "Warning: %v\n", err)
		return nil, nil
	default:
		return nil, err
	}
}

func readOnePassword(ref string) ([]byte, error) {
	b, err := execOnePassword("read", ref)
	return bytes.TrimSpace(b), err
}

func execOnePassword(args ...string) ([]byte, error) {
	err := canExecOnePass()
	if err != nil {
		return nil, err
	}

	cmd := exec.Command("op", args...)
	b, err := cmd.Output()
	if err == nil {
		return b, nil
	}

	var ee *exec.ExitError
	if errors.As(err, &ee) {
		_, _ = os.Stderr.Write(ee.Stderr)
	}
	return nil, err
}

type OnePasswordField struct {
	ID        string `json:"id"`
	Type      string `json:"type"`
	Label     string `json:"label"`
	Value     string `json:"value"`
	Reference string `json:"reference"`
}

func ResolveOnePassword(itemName, fieldLabel string) (string, error) {
	b, err := execOnePassword("item", "get", itemName, "--field", fieldLabel, "--format", "json", "--debug")
	if err != nil {
		return "", err
	}

	field := new(OnePasswordField)
	err = json.Unmarshal(b, field)
	if err != nil {
		// DO NOT RETURN THE ERROR - it could contain a password or other sensitive info
		return "", fmt.Errorf("failed to unmarshal response from 1Password")
	}
	return field.Reference, nil
}
