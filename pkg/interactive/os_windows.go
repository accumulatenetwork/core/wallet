package interactive

import "gitlab.com/accumulatenetwork/core/wallet/internal/errors"

func canExecOnePass() error {
	return errors.OnePassNotSupported.With("1Password integration is not supported on this platform")
}
