package interactive

import (
	"fmt"
	"io"
	"os"
	"runtime/debug"
	"slices"
	"strings"

	"github.com/fatih/color"
	"github.com/twpayne/go-pinentry"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"golang.org/x/term"
)

var PinEntryMode string

// ValidateDependencies checks that security-critical dependencies (namely,
// go-pinentry) are audited when they are updated.
//
// The pinentry library is used to read secret data. Therefore it must be
// audited. If the pinentry library is updated without updating this function,
// the program will print a warning. This makes it harder to accidentally update
// the library without auditing it.
//
// I wanted to make this exit the program, but I couldn't figure out how to
// reliably detect if the program was running in a test environment.
func ValidateDependencies() {
	audited := map[string][]string{
		"github.com/twpayne/go-pinentry": {"v0.2.0"},
	}

	info, ok := debug.ReadBuildInfo()
	if !ok {
		color.Red("FATAL: cannot verify dependencies: binary does not include build info")
		os.Exit(1)
		return
	}
	for _, dep := range info.Deps {
		versions, ok := audited[dep.Path]
		if !ok {
			continue
		}
		if slices.Contains(versions, dep.Version) {
			// Audited
			delete(audited, dep.Path)
		} else {
			// Unknown
			color.Red("FATAL: cannot verify dependencies: %s@%s has not been audited", dep.Path, dep.Version)
			os.Exit(1)
		}
	}
	if len(audited) > 0 {
		color.Red("FATAL: cannot verify dependencies: could not locate dependency info")
		os.Exit(1)
		return
	}
}

func GetPassword(long, short string, rd io.Reader, wr io.Writer, mask bool) (string, error) {
	opts := []pinentry.ClientOption{
		pinentry.WithTitle("Accumulate CLI Wallet"),
		pinentry.WithDesc(long),
		pinentry.WithPrompt(short),
		pinentry.WithGPGTTY(),
	}

	switch strings.ToLower(PinEntryMode) {
	case "", "enable", "enabled":
		opts = append(opts, pinentry.WithBinaryNameFromGnuPGAgentConf())
	case "none", "disable", "disabled", "loopback":
		return ttyGetPass(long+". "+short, rd, wr)
		// loopback
	default:
		opts = append(opts, pinentry.WithBinaryName(PinEntryMode))
	}

	c, err := pinentry.NewClient(opts...)
	if err != nil {
		return "", err
	}
	defer c.Close()

	pin, fromCache, err := c.GetPIN()
	if err != nil {
		if pinentry.IsCancelled(err) {
			return "", errors.BadPassword.With("password request was canceled")
		}
		var err2 pinentry.UnexpectedResponseError
		if errors.As(err, &err2) && strings.HasPrefix(err2.Line, "S ERROR curses.isatty") {
			// Pinentry not available, fallback
			return ttyGetPass(short, rd, wr)
		}
		return "", err
	}
	if fromCache {
		fmt.Fprintf(os.Stderr, "Got password from cache\n")
	}

	return pin, nil
}

func GetNewPassword(label string) (string, error) {
	const confirm = true
	type interactiveRetriever struct {
		prompt         string
		confirmPrompt1 string
		confirmPrompt2 string
	}
	p := &interactiveRetriever{
		prompt:         fmt.Sprintf("Unlock %s", label),
		confirmPrompt1: fmt.Sprintf("Enter new password for %s", label),
		confirmPrompt2: fmt.Sprintf("Confirm password for %s", label),
	}
	short := "Password: "
	prompt := p.prompt
	if confirm {
		short = "New password: "
		if p.confirmPrompt1 != "" {
			prompt = p.confirmPrompt1
		}
	}
	pass, err := GetPassword(prompt, short, os.Stdin, os.Stderr, true)
	if err != nil {
		return "", errors.NoPassword.With("no password specified for encrypted vault")
	}

	if !confirm {
		return pass, nil
	}

	pass2, err := GetPassword(p.confirmPrompt2, "Confirm password: ", os.Stdin, os.Stderr, true)
	if err != nil {
		return "", errors.NoPassword.With("no password specified for encrypted vault")
	}
	if pass != pass2 {
		return "", errors.BadPassword.With("passwords do not match")
	}

	return pass, nil
}

func ttyGetPass(prompt string, r io.Reader, w io.Writer) (string, error) {
	// If the reader is a tty, put it in raw mode to prevent echoing
	restore, err := ttyMakeRaw(r)
	if err != nil {
		return "", err
	}
	defer restore()

	_, err = w.Write([]byte(prompt))
	if err != nil {
		return "", err
	}
	defer func() {
		_, _ = w.Write([]byte("\n"))
	}()

	var pass []byte
	for {
		var b [1]byte
		n, err := r.Read(b[:])
		if err != nil {
			return "", err
		} else if n == 0 {
			return "", io.EOF
		}

		switch b[0] {
		// Delete or backspace
		case 0x7F, 0x08:
			if len(pass) > 0 {
				pass = pass[:len(pass)-1]
			}

		// Enter or newline
		case 0x0A, 0x0D:
			return string(pass), nil

		// Ctrl-C
		case 0x03:
			return "", errors.BadPassword.With("password request was canceled")

		default:
			pass = append(pass, b[0])
		}
	}
}

func ttyMakeRaw(rd io.Reader) (func(), error) {
	fd, ok := rd.(interface{ Fd() uintptr })
	if !ok {
		return func() {}, nil
	}

	if !term.IsTerminal(int(fd.Fd())) {
		return func() {}, nil
	}

	oldState, err := term.MakeRaw(int(fd.Fd()))
	if err != nil {
		return nil, err
	}

	return func() { _ = term.Restore(int(fd.Fd()), oldState) }, nil
}
