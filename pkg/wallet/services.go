package wallet

import (
	"context"

	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
)

//go:generate go run gitlab.com/accumulatenetwork/core/wallet/tools/cmd/gen-api server -o jsonrpc/ GeneralService VaultService TransactionService KeyService LedgerService AccountService SigningService
//go:generate go run gitlab.com/accumulatenetwork/core/wallet/tools/cmd/gen-api client -o jsonrpc/ GeneralService VaultService TransactionService KeyService LedgerService AccountService SigningService

// GeneralService defines methods that do not require access to a vault.
type GeneralService interface {
	// Version returns the version of the wallet daemon.
	Version(context.Context) (*api.VersionResponse, error)

	// GenerateMnemonic generates a new, random mnemonic.
	GenerateMnemonic(context.Context, *api.GenerateMnemonicRequest) (*api.GenerateMnemonicResponse, error)

	// Encode encodes an object to binary.
	Encode(context.Context, *api.EncodeRequest) (*api.EncodeResponse, error)

	// Decode decodes an object from binary.
	Decode(context.Context, *api.DecodeRequest) (*api.DecodeResponse, error)

	// RefreshToken refreshes an existing token or generates a new token.
	RefreshToken(context.Context, *api.RefreshTokenRequest) (*api.RefreshTokenResponse, error)
}

// VaultService manages wallets and vaults.
type VaultService interface {
	// CreateWallet initializes the wallet.
	CreateWallet(context.Context, *api.CreateWalletRequest) (*api.CreateWalletResponse, error)

	// CreateVault creates a new vault.
	CreateVault(context.Context, *api.CreateVaultRequest) (*api.CreateVaultResponse, error)

	// AdoptVault adopts an existing vault into the wallet.
	AdoptVault(context.Context, *api.AdoptVaultRequest) (*api.AdoptVaultResponse, error)

	// ConvertWallet converts a single-vault wallet to a multi-vault wallet.
	ConvertWallet(context.Context, *api.ConvertWalletRequest) (*api.ConvertWalletResponse, error)

	// Status checks the status of the wallets.
	Status(context.Context, *api.StatusRequest) (*api.StatusResponse, error)

	// OpenVault opens a vault without unlocking it.
	OpenVault(context.Context, *api.OpenVaultRequest) (*api.OpenVaultResponse, error)

	// CopyVaultSanitized creates a copy of a vault, sanitized of keys and other
	// secure information.
	CopyVaultSanitized(context.Context, *api.CopyVaultSanitizedRequest) (*api.CopyVaultSanitizedResponse, error)

	// UnlockVault unlocks wallet for a certain time.
	UnlockVault(context.Context, *api.UnlockVaultRequest) (*api.UnlockVaultResponse, error)

	// LockVault locks the wallet.
	LockVault(context.Context, *api.LockVaultRequest) (*api.LockVaultResponse, error)

	// ListVaults lists vaults in the wallet.
	ListVaults(context.Context, *api.ListVaultsRequest) (*api.ListVaultsResponse, error)

	// ImportMnemonic imports an existing mnemonic.
	ImportMnemonic(context.Context, *api.ImportMnemonicRequest) (*api.ImportMnemonicResponse, error)

	// EncryptVault encrypts a previously unencrypted vault.
	EncryptVault(context.Context, *api.EncryptVaultRequest) (*api.EncryptVaultResponse, error)

	// ExportVault returns all the wallet data.
	ExportVault(context.Context, *api.ExportVaultRequest) (*api.ExportVaultResponse, error)

	// ImportVault imports an exported vault.
	ImportVault(context.Context, *api.ImportVaultRequest) (*api.ImportVaultResponse, error)

	// Set1PasswordRef updates the 1Password reference for a vault.
	Set1PasswordRef(context.Context, *api.Set1PasswordRefRequest) (*api.Set1PasswordRefResponse, error)
}

// TransactionService creates and updates stored transactions.
type TransactionService interface {
	// CreateEnvelope creates an envelope by name.
	CreateEnvelope(context.Context, *api.CreateEnvelopeRequest) (*api.CreateEnvelopeResponse, error)

	// CreateTransaction creates a transaction by name.
	CreateTransaction(context.Context, *api.CreateTransactionRequest) (*api.TransactionResponse, error)

	// GetTransaction composes a transaction from map to be executed.
	GetTransaction(context.Context, *api.GetTransactionRequest) (*api.TransactionResponse, error)

	// ListTransactions returns a list of transactions currently being composed in the wallet.
	ListTransactions(context.Context, *api.ListTransactionsRequest) (*api.ListTransactionsResponse, error)

	// DeleteTransaction deletes a transaction from map.
	DeleteTransaction(context.Context, *api.DeleteTransactionRequest) (*api.TransactionResponse, error)

	// WriteDataTransaction assigns a data entry to a transaction.
	WriteDataTransaction(context.Context, *api.WriteDataRequest) (*api.TransactionResponse, error)

	// AddMemoToTransaction adds a memo to the saved transaction.
	AddMemoToTransaction(context.Context, *api.AddMemoToTransactionRequest) (*api.TransactionResponse, error)

	// AddSendTokensOutput adds an output to the send token transaction.
	AddSendTokensOutput(context.Context, *api.AddSendTokensOutputRequest) (*api.TransactionResponse, error)
}

// SigningService signs transactions, messages, and hashes, and manages
// recorded timestamps.
type SigningService interface {
	// Sign signs a transaction or an arbitrary hash, using a partial signature
	// as a template.
	Sign(_ context.Context, req *api.SignRequest) (*api.SignResponse, error)

	// SignMessage signs the hash of arbitrary data passed in message.
	SignMessage(context.Context, *api.SignMessageRequest) (*api.SignResponse, error)

	// SignTransaction signs a transaction.
	SignTransaction(context.Context, *api.SignTransactionRequest) (*api.SignResponse, error)

	// ResetLastUsedOn resets the timestamp of a key.
	ResetLastUsedOn(_ context.Context, req *api.ResetLastUsedOnRequest) (*api.ResetLastUsedOnResponse, error)
}

// KeyService manages keys.
type KeyService interface {
	// GenerateAddress generates address based on the provided type.
	GenerateAddress(context.Context, *api.GenerateAddressRequest) (*api.GenerateAddressResponse, error)

	// ResolveKey returns a public key from either a label or keyhash.
	ResolveKey(context.Context, *api.ResolveKeyRequest) (*api.ResolveKeyResponse, error)

	// FindSigner checks the wallet for a key book and a key matching the given authority set.
	FindSigner(context.Context, *api.FindSignerRequest) (*api.FindSignerResponse, error)

	// ImportKey imports an existing key.
	ImportKey(context.Context, *api.ImportKeyRequest) (*api.ResolveKeyResponse, error)

	// KeyList returns a list of available keys in the wallet.
	KeyList(context.Context, *api.KeyListRequest) (*api.KeyListResponse, error)

	// KeyAssign assigns a new label to a public key.
	KeyAssign(context.Context, *api.KeyRotateRequest) (*api.KeyRotateResponse, error)

	// KeyRemove removes a label from a public key.
	KeyRemove(context.Context, *api.KeyRotateRequest) (*api.KeyRotateResponse, error)

	// KeyRename renames a label of a public key.
	KeyRename(context.Context, *api.KeyRotateRequest) (*api.KeyRotateResponse, error)

	// RestoreKeyCounter restores a corrupted key counter.
	RestoreKeyCounters(context.Context, *api.RestoreKeyCountersRequest) (*api.RestoreKeyCountersResponse, error)
}

// LedgerService manages interaction with Ledger hardware wallets.
type LedgerService interface {
	// LedgerGenerateKey generates a key on a ledger device.
	LedgerGenerateKey(context.Context, *api.GenerateLedgerKeyRequest) (*api.Key, error)

	// LedgerQueryWallets returns the version of an external ledger device.
	LedgerQueryWallets(context.Context) (*api.LedgerWalletResponse, error)
}

// AccountService manages registered accounts.
type AccountService interface {
	// AdiList returns a list of adi's managed by the wallet.
	AdiList(context.Context, *api.AdiListRequest) (*api.AdiListResponse, error)

	// ListAccounts lists accounts registered with the wallet.
	ListAccounts(context.Context, *api.ListAccountsRequest) (*api.ListAccountsResponse, error)

	// RegisterADI registers adi in walletd.
	RegisterADI(context.Context, *api.RegisterADIRequest) (*api.RegisterADIResponse, error)

	// RegisterBook registers a key book with the wallet.
	RegisterBook(context.Context, *api.RegisterBookRequest) (*api.RegisterBookResponse, error)

	// UnregisterBook registers a key book with the wallet.
	UnregisterBook(context.Context, *api.UnregisterBookRequest) (*api.UnregisterBookResponse, error)

	// RegisterTokenAccount registers a token account with the wallet.
	RegisterTokenAccount(context.Context, *api.RegisterTokenAccountRequest) (*api.RegisterTokenAccountResponse, error)

	// UnregisterTokenAccount unregisters a token account with the wallet.
	UnregisterTokenAccount(context.Context, *api.UnregisterTokenAccountRequest) (*api.UnregisterTokenAccountResponse, error)
}
