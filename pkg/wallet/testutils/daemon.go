package testutils

import (
	"crypto/sha256"

	"gitlab.com/accumulatenetwork/core/wallet/internal/daemon/services"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/vault"
)

var TestToken = sha256.Sum256([]byte("token"))
var Yellow = []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"}

func NewSimpleDaemon(wallets services.WalletManager, keys services.KeyStore, ledger services.LedgerManager) *services.SimpleDaemon {
	return services.NewSimpleDaemon(wallets, keys, ledger)
}

func NewWalletManager(memory bool, keys services.KeyStore) services.WalletManager {
	return &services.WalletManagerWithKeys{
		InMemory: memory,
		Keys:     keys,
	}
}

func NewKeyStore() services.KeyStore {
	return new(services.TokenKeyStore)
}

type SingleInMemoryWallet struct {
	keys   services.KeyStore
	wallet *vault.Wallet
}

var _ services.WalletManager = (*SingleInMemoryWallet)(nil)

func (m *SingleInMemoryWallet) UseMemDB() bool { return true }

func (m *SingleInMemoryWallet) Wallet(string) services.Wallet {
	if m.wallet == nil {
		m.keys = new(services.TokenKeyStore)
		m.wallet = new(vault.Wallet)
	}
	return &services.WalletWithKeys{Wallet: m.wallet, Keys: m.keys}
}

func (m *SingleInMemoryWallet) Each(fn func(services.Wallet) error) error {
	return fn(m.Wallet(""))
}
