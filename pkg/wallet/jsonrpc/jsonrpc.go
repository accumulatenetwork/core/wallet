package jsonrpc

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/AccumulateNetwork/jsonrpc2/v15"
)

// General Errors
const (
	ErrCodeInternal = -32800 - iota
	ErrCodeValidation
	ErrCodeGeneral
)

// Custom errors
const (
	ErrCodeProtocolBase = -33000 - iota
)

type Transport interface {
	Request(ctx context.Context, server, method string, params, result interface{}) error
	SetTimeout(time.Duration)
}

func NewHandler(services ...interface{ methods() jsonrpc2.MethodMap }) (http.Handler, error) {
	methods := jsonrpc2.MethodMap{}
	for _, service := range services {
		for name, method := range service.methods() {
			if _, ok := methods[name]; ok {
				return nil, fmt.Errorf("duplicate method %s", name)
			}
			methods[name] = method
		}
	}

	return jsonrpc2.HTTPRequestHandler(methods, log.New(os.Stdout, "", 0)), nil
}
