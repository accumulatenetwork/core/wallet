package jsonrpc

import (
	"context"
	"encoding/json"
	"io/fs"

	"github.com/AccumulateNetwork/jsonrpc2/v15"
	"github.com/go-playground/validator/v10"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

var val = func() *validator.Validate {
	val, err := protocol.NewValidator()
	if err != nil {
		panic(err)
	}
	return val
}()

func parseRequest(params json.RawMessage, target interface{}) error {
	err := json.Unmarshal(params, target)
	if err != nil {
		return jsonrpc2.NewError(ErrCodeValidation, "Validation Error", err)
	}

	switch target.(type) {
	case *api.SignRequest:
		// This is a hack
		err = val.StructExcept(target, "Signature", "Transaction")
	case *api.SignTransactionRequest:
		// This is a hack
		err = val.StructExcept(target, "Transaction")
	default:
		err = val.Struct(target)
	}
	if err != nil {
		return jsonrpc2.NewError(ErrCodeValidation, "Validation Error", err)
	}
	return nil
}

func processResponse(res any, err error) any {
	if err == nil {
		return res
	}

	if errors.Is(err, fs.ErrNotExist) {
		err = errors.NotFound.Wrap(err)
	}

	var perr *errors.Error
	var jerr jsonrpc2.Error
	switch {
	case errors.As(err, &perr):
		b, _ := json.Marshal(perr)
		return jsonrpc2.NewError(ErrCodeProtocolBase-jsonrpc2.ErrorCode(perr.Code), perr.Message, json.RawMessage(b))

	case errors.As(err, &jerr):
		return jerr

	default:
		return jsonrpc2.NewError(ErrCodeGeneral, "Unknown error", err)
	}
}

func makeMethodNoInput[V any](fn func(context.Context) (*V, error)) jsonrpc2.MethodFunc {
	return func(ctx context.Context, _ json.RawMessage) any {
		return processResponse(fn(ctx))
	}
}

func makeMethod[U, V any](fn func(context.Context, *U) (*V, error)) jsonrpc2.MethodFunc {
	return func(ctx context.Context, params json.RawMessage) any {
		req := new(U)
		err := parseRequest(params, req)
		if err != nil {
			return err
		}

		return processResponse(fn(ctx, req))
	}
}

func callClient[V ~*VS, VS any](c Transport, server, method string, ctx context.Context, req any) (V, error) {
	var resp VS
	err := c.Request(ctx, server, method, req, &resp)
	if err == nil {
		return &resp, nil
	}

	return nil, DecodeError(err)
}

func DecodeError(err error) error {
	var jerr jsonrpc2.Error
	switch {
	case !errors.As(err, &jerr):
		return err
	case jerr.Code <= ErrCodeProtocolBase:
		return convertError[*errors.Error](jerr)
	default:
		err := errors.UnknownRpcError.With(jerr.Message)
		err.Data, _ = json.Marshal(jerr)
		if s, ok := jerr.Data.(string); ok {
			err.Message = s
		}
		return err
	}
}

func convertError[V error](src jsonrpc2.Error) error {
	var dst V
	if b, e := json.Marshal(src.Data); e != nil {
		return src
	} else if json.Unmarshal(b, &dst) != nil {
		return src
	}
	return dst
}
