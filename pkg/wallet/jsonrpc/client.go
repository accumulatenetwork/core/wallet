package jsonrpc

import (
	"context"
	"encoding/json"
	"net"
	"net/http"
	"time"

	"github.com/AccumulateNetwork/jsonrpc2/v15"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"golang.org/x/crypto/ssh"
)

// New creates new API client with default config
func New(server string) (*Client, error) {
	c := new(Client)
	c.Transport = new(DefaultTransport)
	c.Transport.SetTimeout(15 * time.Second)
	c.Server = server
	return c, nil
}

// New creates new API client with default config
func NewWith(server string, transport Transport) (*Client, error) {
	c := new(Client)
	c.Server = server
	c.Transport = transport
	return c, nil
}

type DefaultTransport struct {
	jsonrpc2.Client
}

func (t *DefaultTransport) SetTimeout(timeout time.Duration) {
	t.Timeout = timeout
}

// DialTransport returns a Transport that uses the given function to dial connections.
func DialTransport(dial func(network, addr string) (net.Conn, error)) Transport {
	tr := new(DefaultTransport)
	tr.Transport = &http.Transport{
		Dial: dial,
	}
	return tr
}

// NetDialTransport uses net.Dial to dial the given network address.
func NetDialTransport(network, address string) Transport {
	return DialTransport(func(string, string) (net.Conn, error) {
		return net.Dial(network, address)
	})
}

// SSHDialTransport uses client.Dial to dial the given network address.
func SSHDialTransport(client *ssh.Client, network, address string) Transport {
	return DialTransport(func(string, string) (net.Conn, error) {
		return client.Dial(network, address)
	})
}

type InteractiveAuthnTransport struct {
	Transport
	Use1Password bool
	GetPassword  func(*api.VaultInfo, int, error) ([]byte, error)
}

func (t *InteractiveAuthnTransport) Request(ctx context.Context, url, method string, params, result interface{}) error {
	s := &interactiveAuthnTransportInner{t, map[string]int{}, map[string]error{}}
	return s.Request(ctx, url, method, params, result)
}

type interactiveAuthnTransportInner struct {
	*InteractiveAuthnTransport
	attempts map[string]int
	lastErr  map[string]error
}

func (t *interactiveAuthnTransportInner) Request(ctx context.Context, url, method string, params, result interface{}) error {
	for {
		// Make the request
		err := t.Transport.Request(ctx, url, method, params, result)
		if err == nil {
			return nil
		}

		// Check for an authentication error
		err2, ok := DecodeError(err).(*errors.Error)
		if !ok || err2.Code != errors.NoPassword {
			return err
		}

		var vault *api.VaultInfo
		if json.Unmarshal(err2.Data, &vault) != nil {
			return err
		}

		// Don't try to unlock a vault more than three times
		attemptID := vault.FilePath + "|" + vault.Name
		if t.attempts[attemptID] > 2 {
			if err, ok := t.lastErr[attemptID]; ok {
				return err
			}
			return err
		}
		t.attempts[attemptID]++

		// Attempt 1Password?
		use1Pass := t.Use1Password && vault.OnePasswordRef != ""

		// Prompt the user for the password
	unlock:
		var pw []byte
		if !use1Pass {
			pw, err = t.GetPassword(vault, t.attempts[attemptID], t.lastErr[attemptID])
			if err != nil {
				return err
			}
		}

		// Unlock the vault
		c := &Client{Transport: t, Server: url}
		res, err := c.UnlockVault(ctx, &api.UnlockVaultRequest{
			Use1Password: use1Pass,
			Wallet:       vault.FilePath,
			Vault:        vault.Name,
			Passphrase:   string(pw),
		})

		switch {
		case res != nil && res.Success:
			t.lastErr[attemptID] = errors.UnknownError.With("an unknown error occurred")
			continue

		case err == nil && use1Pass:
			// Try again without 1Password
			use1Pass = false
			goto unlock

		case err == nil:
			return errors.BadPassword.With("failed to unlock")

		case errors.Is(err, errors.BadPassword):
			t.lastErr[attemptID] = err
			continue

		default:
			return err
		}
	}
}

// AutoWalletTransport populates wallet request fields automatically.
//
// AutoWalletTransport does not automatically set fields for certain request
// types that probably should be populated manually.
type AutoWalletTransport struct {
	Transport
	SetWallet func(request any, field *string)
	SetVault  func(request any, field *string)
	SetToken  func(request any, field *[]byte)
}

func (t *AutoWalletTransport) Request(ctx context.Context, url, method string, params, result interface{}) error {
	var wptr *string
	var vptr *string
	var tptr *[]byte
	switch r := params.(type) {
	case *api.AddMemoToTransactionRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.AddSendTokensOutputRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.AdiListRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.AdoptVaultRequest:
		wptr, tptr = &r.Wallet, &r.Token
	case *api.ConvertWalletRequest:
		wptr, tptr = &r.Wallet, &r.Token
	case *api.CopyVaultSanitizedRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.CreateTransactionRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.CreateVaultRequest:
		tptr = &r.Token
	case *api.CreateWalletRequest:
		tptr = &r.Token
	case *api.DeleteTransactionRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.EncryptVaultRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.ExportVaultRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.FindSignerRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.GenerateAddressRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.GenerateLedgerKeyRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.GetTransactionRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.ImportKeyRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.ImportMnemonicRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.ImportVaultRequest:
		wptr, tptr = &r.Wallet, &r.Token
	case *api.KeyListRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.KeyRotateRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.ListAccountsRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.ListTransactionsRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.ListVaultsRequest:
		wptr, tptr = &r.Wallet, &r.Token
	case *api.RegisterADIRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.RegisterBookRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.RegisterTokenAccountRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.ResolveKeyRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.RestoreKeyCountersRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.Set1PasswordRefRequest:
		wptr, tptr = &r.Wallet, &r.Token
	case *api.SignRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.SignMessageRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.SignTransactionRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.StatusRequest:
		wptr, tptr = &r.Wallet, &r.Token
	case *api.UnlockVaultRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.UnregisterBookRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.UnregisterTokenAccountRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	case *api.WriteDataRequest:
		wptr, vptr, tptr = &r.Wallet, &r.Vault, &r.Token
	}

	if wptr != nil && *wptr == "" && t.SetWallet != nil {
		t.SetWallet(params, wptr)
	}
	if vptr != nil && *vptr == "" && t.SetVault != nil {
		t.SetVault(params, vptr)
	}
	if tptr != nil && *tptr == nil && t.SetToken != nil {
		t.SetToken(params, tptr)
	}

	return t.Transport.Request(ctx, url, method, params, result)
}
