package sshutil

import (
	"errors"
	"fmt"
	"io/fs"
	"net"
	"net/url"
	"os"
	"os/user"
	"path/filepath"
	"strings"

	"github.com/kevinburke/ssh_config"
	"golang.org/x/crypto/ssh"
	"golang.org/x/crypto/ssh/agent"
	"golang.org/x/crypto/ssh/knownhosts"
)

// Connect opens an SSH connection. Connect expects a URL in the format
// 'ssh[-scheme]://[user@[:identity]]alias[:port][/path]'.
//
// Connect will use ~/.ssh/config to resolve the alias and any unspecified
// settings. The user defaults to the current user and the port defaults to 22,
// if they are not specified directly and are not specified in ~/.ssh/config.
func Connect(u *url.URL) (*ssh.Client, error) {
	alias := u.Hostname()
	hostName := ssh_config.Get(alias, "HostName")
	if hostName == "" {
		hostName = alias
	}

	port := ssh_config.Get(alias, "Port")
	if port == "" {
		port = "22"
	}

	cu, err := user.Current()
	if err != nil {
		return nil, fmt.Errorf("resolve current user: %w", err)
	}

	config := new(ssh.ClientConfig)
	config.User = u.User.Username()
	if config.User == "" {
		config.User = ssh_config.Get(alias, "User")
		if config.User == "" {
			config.User = cu.Username
		}
	}

	var files []string
	for _, file := range strings.Split(ssh_config.Get(alias, "UserKnownHostsFile"), " ") {
		if strings.HasPrefix(file, "~/") {
			file = filepath.Join(cu.HomeDir, file[2:])
		}
		_, err := os.Stat(file)
		switch {
		case err == nil:
			files = append(files, file)
		case !errors.Is(err, fs.ErrNotExist):
			return nil, err
		}
	}
	config.HostKeyCallback, err = knownhosts.New(files...)
	if err != nil {
		return nil, fmt.Errorf("load known hosts: %w", err)
	}

	identityAgent := os.Getenv("SSH_AUTH_SOCK")
	if identityAgent == "" {
		identityAgent = ssh_config.Get(alias, "IdentityAgent")
	}
	if identityAgent != "" {
		if strings.HasPrefix(identityAgent, "~/") {
			identityAgent = filepath.Join(cu.HomeDir, identityAgent[2:])
		}
		sock, err := net.Dial("unix", identityAgent)
		if err != nil {
			return nil, fmt.Errorf("connect to identity agent: %w", err)
		}
		defer sock.Close() // Is it ok to close this here?
		config.Auth = append(config.Auth, ssh.PublicKeysCallback(agent.NewClient(sock).Signers))
	}

	identityFile, ok := u.User.Password()
	if !ok {
		identityFile = ssh_config.Get(alias, "IdentityFile")
	}
	if identityFile != "" {
		config.Auth = append(config.Auth, ssh.PublicKeysCallback(func() ([]ssh.Signer, error) {
			b, err := os.ReadFile(identityFile)
			switch {
			case err == nil:
				// Ok
			case errors.Is(err, fs.ErrNotExist):
				return nil, nil
			default:
				return nil, err
			}
			signer, err := ssh.ParsePrivateKey(b)
			if err != nil {
				return nil, err
			}
			return []ssh.Signer{signer}, nil
		}))
	}

	return ssh.Dial("tcp", hostName+":"+port, config)
}
