package main

import (
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"golang.org/x/sys/windows"
)

func isConnRefused(err error) bool {
	return errors.Is(err, windows.WSAECONNREFUSED)
}
