//go:build !windows

package main

import (
	"syscall"

	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

func isConnRefused(err error) bool {
	return errors.Is(err, syscall.ECONNREFUSED)
}
