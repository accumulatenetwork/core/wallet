package main

import (
	"context"
	"encoding/hex"
	"io/fs"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"time"

	"github.com/wailsapp/wails/v2/pkg/runtime"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/address"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"gitlab.com/accumulatenetwork/core/wallet/internal/util"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/jsonrpc"
	"golang.org/x/exp/slog"
)

const deadline = time.Second //* 3600 * 24

// App struct
type App struct {
	ctx    context.Context
	mu     *sync.Mutex
	client *jsonrpc.Client
	wallet string
}

// NewApp creates a new App application struct
func NewApp() *App {
	return &App{}
}

// startup is called when the app starts. The context is saved
// so we can call the runtime methods
func (a *App) startup(ctx context.Context) {
	a.ctx = ctx
	a.mu = new(sync.Mutex)
}

func (a *App) Started() string { return a.wallet }

func (a *App) Start(walletDir string) error {
	a.mu.Lock()
	defer a.mu.Unlock()

	if a.client != nil {
		if walletDir != a.wallet {
			return errors.Conflict.With("another wallet is already open")
		}
		return nil
	}

	a.wallet = walletDir
	err := os.MkdirAll(walletDir, 0700)
	if err != nil {
		return errors.UnknownError.WithFormat("create wallet directory: %w", err)
	}

	a.client, err = jsonrpc.New("http://socket/wallet")
	if err != nil {
		return errors.UnknownError.WithFormat("create client: %w", err)
	}

	home, err := os.UserHomeDir()
	if err != nil {
		return errors.UnknownError.WithFormat("get user home directory: %w", err)
	}

	socket := filepath.Join(home, ".accumulate", "wallet", "daemon.socket")
	tr := new(jsonrpc.DefaultTransport)
	tr.Transport = &autodaemon{a, socket, &http.Transport{
		Dial: func(network, addr string) (net.Conn, error) {
			return net.Dial("unix", socket)
		},
	}}
	a.client.Transport = tr

	return nil
}

func (a *App) Close() error {
	a.mu.Lock()
	defer a.mu.Unlock()

	if a.client == nil {
		return nil
	}

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()

	_, err := a.client.LockVault(ctx, &api.LockVaultRequest{Wallet: a.wallet, Close: true})
	if err != nil {
		return err
	}

	a.wallet = ""
	a.client = nil
	return nil
}

type autodaemon struct {
	app       *App
	socket    string
	transport http.RoundTripper
}

func (a *autodaemon) RoundTrip(req *http.Request) (*http.Response, error) {
	w, err := a.transport.RoundTrip(req)
	switch {
	case err == nil:
		return w, nil
	case errors.Is(err, fs.ErrNotExist), isConnRefused(err):
		// Start server
	default:
		return nil, err
	}

	// Use a custom transport that runs the daemon in-process
	jrpc, err := walletd.NewJrpc(walletd.Options{Logger: slog.Default()})
	if err != nil {
		return nil, err
	}
	a.transport = util.DirectHttpTransport(jrpc.NewMux())
	return a.transport.RoundTrip(req)
}

func (a *App) UserHomeDir() (string, error) {
	return os.UserHomeDir()
}

func (a *App) ChooseWallet() (string, error) {
	home, err := os.UserHomeDir()
	if err != nil {
		return "", errors.UnknownError.WithFormat("get user home directory: %w", err)
	}

	s, err := runtime.OpenDirectoryDialog(a.ctx, runtime.OpenDialogOptions{
		DefaultDirectory: filepath.Join(home, ".accumulate"),
		Title:            "Select a wallet directory",
	})
	if err != nil {
		return "", errors.UnknownError.Wrap(err)
	}

	return s, nil
}

func (a *App) Status(req *api.StatusRequest) (*api.StatusResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()
	return a.client.Status(ctx, req)
}

func (a *App) CreateWallet(req *api.CreateWalletRequest) (*api.CreateWalletResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()
	return a.client.CreateWallet(ctx, req)
}

func (a *App) ListVaults(req *api.ListVaultsRequest) (*api.ListVaultsResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()
	return a.client.ListVaults(ctx, req)
}

func (a *App) OpenVault(req *api.OpenVaultRequest) (*api.OpenVaultResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()
	return a.client.OpenVault(ctx, req)
}

func (a *App) UnlockVault(req *api.UnlockVaultRequest) (*api.UnlockVaultResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx := a.ctx

	// 1Password might take a while, don't use a deadline
	if !req.Use1Password {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(a.ctx, deadline)
		defer cancel()
	}

	return a.client.UnlockVault(ctx, req)
}

func (a *App) LockVault(req *api.LockVaultRequest) (*api.LockVaultResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()
	return a.client.LockVault(ctx, req)
}

func (a *App) KeyList(req *api.KeyListRequest) (*api.KeyListResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()
	return a.client.KeyList(ctx, req)
}

func (a *App) ResolveKey(req *api.ResolveKeyRequest) (*api.ResolveKeyResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()
	return a.client.ResolveKey(ctx, req)
}

func (a *App) GenerateMnemonic(req *api.GenerateMnemonicRequest) (*api.GenerateMnemonicResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()
	return a.client.GenerateMnemonic(ctx, req)
}

func (a *App) ImportMnemonic(req *api.ImportMnemonicRequest) (*api.ImportMnemonicResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()
	return a.client.ImportMnemonic(ctx, req)
}

type ParseKeyRequest struct {
	Address string `json:"address,omitempty"`
}

type ParseKeyResponse struct {
	PrivateKey string                 `json:"privateKey"`
	PublicKey  string                 `json:"publicKey"`
	Type       protocol.SignatureType `json:"type"`
}

func (a *App) ParseKey(req *ParseKeyRequest) (*ParseKeyResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	addr, err := address.Parse(req.Address)
	if err != nil {
		return nil, err
	}

	sk, ok := addr.GetPrivateKey()
	if !ok {
		return nil, errors.BadRequest.With("the value must be a private key address")
	}
	pk, _ := addr.GetPublicKey()
	return &ParseKeyResponse{
		PrivateKey: hex.EncodeToString(sk),
		PublicKey:  hex.EncodeToString(pk),
		Type:       addr.GetType(),
	}, nil
}

func (a *App) ImportKey(req *api.ImportKeyRequest) (*api.Key, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()
	resp, err := a.client.ImportKey(ctx, req)
	if err != nil {
		return nil, err
	}
	return resp.Key, nil
}

func (a *App) GenerateAddress(req *api.GenerateAddressRequest) (*api.Key, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()
	resp, err := a.client.GenerateAddress(ctx, req)
	if err != nil {
		return nil, err
	}
	return &resp.Key, nil
}

func (a *App) GenerateLedgerKey(req *api.GenerateLedgerKeyRequest) (*api.Key, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	return a.client.LedgerGenerateKey(a.ctx, req)
}

func (a *App) ListAccounts(req *api.ListAccountsRequest) (*api.ListAccountsResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()
	return a.client.ListAccounts(ctx, req)
}

func (a *App) RegisterTokenAccount(req *api.RegisterTokenAccountRequest) (*api.RegisterTokenAccountResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()
	return a.client.RegisterTokenAccount(ctx, req)
}

func (a *App) Sign(req *api.SignRequest, isLedger bool) (*api.SignResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	var ctx context.Context
	if isLedger {
		ctx = a.ctx
	} else {
		var cancel context.CancelFunc
		ctx, cancel = context.WithTimeout(a.ctx, deadline)
		defer cancel()
	}
	return a.client.Sign(ctx, req)
}

type FindSignerRequest struct {
	Wallet      string   `json:"wallet,omitempty" `
	Vault       string   `json:"vault,omitempty" `
	Authorities []string `json:"authorities,omitempty" `
}

func (a *App) FindSigner(req *FindSignerRequest) (*api.FindSignerResponse, error) {
	a.mu.Lock()
	defer a.mu.Unlock()

	ctx, cancel := context.WithTimeout(a.ctx, deadline)
	defer cancel()

	req2 := new(api.FindSignerRequest)
	req2.Wallet = req.Wallet
	req2.Vault = req.Vault
	for _, auth := range req.Authorities {
		u, err := url.Parse(auth)
		if err != nil {
			return nil, err
		}
		req2.Authorities = append(req2.Authorities, u)
	}

	return a.client.FindSigner(ctx, req2)
}
