package main

import (
	"errors"

	"github.com/AccumulateNetwork/jsonrpc2/v15"
	"github.com/wailsapp/wails/v2"
	"github.com/wailsapp/wails/v2/pkg/options"
	"github.com/wailsapp/wails/v2/pkg/options/assetserver"
	"gitlab.com/accumulatenetwork/core/wallet"
)

func main() {
	// Create an instance of the app structure
	app := NewApp()

	// Create application with options
	err := wails.Run(&options.App{
		Title:  "Accumulate Wallet",
		Width:  1024,
		Height: 768,
		AssetServer: &assetserver.Options{
			Assets: wallet.Assets,
		},
		BackgroundColour: &options.RGBA{R: 27, G: 38, B: 54, A: 1},
		OnStartup:        app.startup,
		Bind: []interface{}{
			app,
		},
		ErrorFormatter: func(err error) any {
			var jerr jsonrpc2.Error
			if errors.As(err, &jerr) {
				return jerr
			}
			return err.Error()
		},
	})

	if err != nil {
		println("Error:", err.Error())
	}
}
