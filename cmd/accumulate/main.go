package main

import (
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/interactive"
)

func main() {
	interactive.ValidateDependencies()
	cmd.Execute()
}
