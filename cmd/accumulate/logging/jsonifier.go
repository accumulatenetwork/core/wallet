package logging

import (
	"encoding/json"
	"fmt"
	"reflect"

	"golang.org/x/exp/slog"
)

func Jsonify(groups []string, a slog.Attr) slog.Attr {
	// TODO What I really want is output like zerolog - I want the text logger
	// to output structured data as `key={"foo": "bar"}` instead of
	// `key.foo=bar`. Also JSON marshalling and unmarshalling is not cheap. But
	// this is good enough for now I guess.

	v := a.Value.Resolve()
	if v.Kind() != slog.KindAny {
		return a
	}

	rv := reflect.ValueOf(v.Any())
	for rv.Kind() == reflect.Ptr {
		rv = rv.Elem()
	}
	if rv.Kind() != reflect.Struct {
		return a
	}

	b, err := json.Marshal(v.Any())
	if err != nil {
		return a
	}
	var jv any
	if json.Unmarshal(b, &jv) != nil {
		return a
	}
	m, ok := jv.(map[string]any)
	if !ok {
		return a
	}

	return jsonify(a.Key, m)
}

func jsonify(k string, v any) slog.Attr {
	switch v := v.(type) {
	case map[string]any:
		fields := make([]any, 0, len(v))
		for k, v := range v {
			fields = append(fields, jsonify(k, v))
		}
		return slog.Group(k, fields...)
	case []any:
		fields := make([]any, 0, len(v))
		for i, v := range v {
			k := fmt.Sprint(i)
			fields = append(fields, jsonify(k, v))
		}
		return slog.Group(k, fields...)
	default:
		return slog.Any(k, v)
	}
}
