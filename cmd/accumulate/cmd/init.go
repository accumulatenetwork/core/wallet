package cmd

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/fatih/color"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/interactive"
)

var initCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "init",
		Short: "An alias for `accumulate wallet init`",
		Args:  cobra.NoArgs,
		Run:   x.runCmdFunc2(x.initWallet),
	}
})

var _ = walletCmd.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "create [name]",
		Short: "Create a new vault (multi-vault mode)",
		Args:  cobra.ExactArgs(1),
		Run:   x.runCmdFunc2(x.createVault),
	}
})

var _ = walletCmd.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:     "migrate [path (optional)]",
		Aliases: []string{"adopt"},
		Short:   "Migrate a vault",
		Example: "" +
			"  accumulate migrate [path]       Convert an old wallet to a new wallet or vault\n" +
			"  accumulate migrate              Convert a single-vault wallet to a multi-vault wallet\n",
		Args: cobra.RangeArgs(0, 1),
		Run:  x.runCmdFunc2(x.migrateVault),
	}
})

var _ = walletCmd.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "restore [path]",
		Short: "Restore a vault from a backup",
		Args:  cobra.ExactArgs(1),
		Run:   x.runCmdFunc2(x.restoreVault),
	}
})

var _ = walletCmd.New(func(ctx *Context) *cobra.Command {
	return &cobra.Command{
		Use:    "copy-sanitized [path]",
		Short:  "Create a sanitized copy of a vault, with private keys and other secure information overwritten with zeros",
		Hidden: true,
		Args:   cobra.ExactArgs(1),
		Run:    ctx.runCmdFunc2(ctx.copyVaultSanitized),
	}
})

func (x *Context) initWallet(cmd *cobra.Command, _ []string) (string, error) {
	// Verify the wallet does not exist
	sr, err := x.getWallet().Status(cmd.Context(), &api.StatusRequest{})
	if err != nil {
		return "", fmt.Errorf("get status: %w", err)
	}
	if sr.Wallet.Exists {
		if x.gotConfig {
			return "", errors.New("wallet exists")
		}

		i, _, err := (&promptui.Select{
			Label: "Wallet exists; configure daemon?",
			Items: []string{
				"Yes",
				"No",
			},
		}).Run()
		if err != nil {
			return "", err
		}
		if i != 0 {
			return "", errors.New("wallet exists")
		}
		err = x.initConfigPrompt()
		return "", err
	}

	// Configure daemon mode
	if !x.gotConfig {
		err = x.initConfigPrompt()
		if err != nil {
			return "", err
		}
	}

	req := new(api.CreateWalletRequest)
	req.Path = WalletDir

	// Single- or multi-vault?
	i, _, err := (&promptui.Select{
		Label: "Wallet mode",
		Items: []string{
			"Single vault",
			"Multi vault",
		},
	}).Run()
	if err != nil {
		return "", err
	}
	req.MultiVault = i == 1

	// Encrypted?
	prompt := "Encrypt keys?"
	if req.MultiVault {
		prompt = "Encrypt vault list?"
	}
	i, _, err = (&promptui.Select{
		Label: prompt,
		Items: []string{
			"Yes",
			"No",
		},
	}).Run()
	if err != nil {
		return "", err
	}
	if i == 0 {
		b, err := interactive.GetNewPassword("wallet")
		if err != nil {
			return "", fmt.Errorf("set password: %w", err)
		}
		req.Passphrase = b
	}

	var mnemonic string
	if !req.MultiVault {
		// Mnemonic?
		i, _, err = (&promptui.Select{
			Label: "Mnemonic",
			Items: []string{
				"Create",
				"Import",
				"Skip",
			},
		}).Run()
		if err != nil {
			return "", err
		}
		switch i {
		case 0:
			mnemonic, err = x.newMnemonic(cmd)
		case 1:
			mnemonic, err = getPasswdPrompt(cmd, "Mnemonic seed phrase", "Enter mnemonic: ", true)
		}
		if err != nil {
			return "", err
		}
	}

	// Create it
	_, err = x.getWallet().CreateWallet(cmd.Context(), req)
	if err != nil {
		return "", fmt.Errorf("create wallet: %w", err)
	}

	// Import the mnemonic
	if mnemonic != "" {
		mnemonic := strings.Split(mnemonic, " ")
		_, err = x.getWallet().ImportMnemonic(cmd.Context(), &api.ImportMnemonicRequest{Wallet: WalletDir, Mnemonic: mnemonic})
		if err != nil {
			return "", err
		}
	}

	return "Wallet created", nil
}

func (x *Context) initConfigPrompt() error {
	i, _, err := (&promptui.Select{
		Label: "Automatically run the wallet daemon in the background when the CLI is used?",
		Items: []string{
			"Yes (password(s) will be remembered for 10 minutes)",
			"No (enter your password(s) each time)",
		},
	}).Run()
	if err != nil {
		return err
	}
	if i == 0 {
		x.Set("spawn-daemon", true)
		check(os.MkdirAll(filepath.Join(currentUser.HomeDir, ".accumulate", "wallet"), 0700))
		check(x.SafeWriteConfig())
	}
	return nil
}

func (x *Context) createVault(cmd *cobra.Command, args []string) (string, error) {
	// Verify the vault does not exist
	vaults, err := x.getWallet().ListVaults(cmd.Context(), &api.ListVaultsRequest{Wallet: WalletDir})
	if err != nil {
		return "", err
	}
	for _, v := range vaults.Vaults {
		if strings.EqualFold(v, args[0]) {
			return "", fmt.Errorf("vault %q exists", args[0])
		}
	}

	req := new(api.CreateVaultRequest)
	req.Wallet = WalletDir
	req.Vault = args[0]

	// Encrypted?
	i, _, err := (&promptui.Select{
		Label: "Encrypt?",
		Items: []string{
			"Yes",
			"No",
		},
	}).Run()
	if err != nil {
		return "", err
	}
	if i == 0 {
		b, err := interactive.GetNewPassword("wallet")
		if err != nil {
			return "", fmt.Errorf("set password: %w", err)
		}
		req.Passphrase = b
	}

	// Mnemonic?
	i, _, err = (&promptui.Select{
		Label: "Mnemonic",
		Items: []string{
			"Create",
			"Import",
			"Skip",
		},
	}).Run()
	if err != nil {
		return "", err
	}
	var mnemonic string
	switch i {
	case 0:
		mnemonic, err = x.newMnemonic(cmd)
	case 1:
		mnemonic, err = getPasswdPrompt(cmd, "Mnemonic seed phrase", "Enter mnemonic: ", true)
	}
	if err != nil {
		return "", err
	}

	// Create it
	_, err = x.getWallet().CreateVault(cmd.Context(), req)
	if err != nil {
		return "", fmt.Errorf("create vault: %w", err)
	}

	// Import the mnemonic
	if mnemonic != "" {
		mnemonic := strings.Split(mnemonic, " ")
		_, err = x.getWallet().ImportMnemonic(cmd.Context(), &api.ImportMnemonicRequest{Vault: req.Vault, Mnemonic: mnemonic})
		if err != nil {
			return "", err
		}
	}

	return "Created vault", nil
}

func (x *Context) migrateVault(cmd *cobra.Command, args []string) (string, error) {
	// Open the default vault and get its status
	status, err := x.getWallet().Status(cmd.Context(), &api.StatusRequest{})
	if err != nil {
		return "", err
	}
	if status.Wallet.Exists && !status.Wallet.Unlocked {
		_, err = x.getWallet().ListVaults(cmd.Context(), &api.ListVaultsRequest{})
		if err != nil && !strings.Contains(err.Error(), "not a multi-vault wallet") {
			return "", err
		}
		status, err = x.getWallet().Status(cmd.Context(), &api.StatusRequest{})
		if err != nil {
			return "", err
		}
	}

	// Single-to-multi mode?
	if len(args) == 0 {
		return x.convertSingleToMulti(cmd, status)
	}

	// Is the argument a database?
	path := args[0]
	st, err := os.Stat(path)
	if err != nil {
		return "", err
	}
	if st.IsDir() {
		path, err = findDb(path)
		if err != nil {
			return "", err
		}
		if path == "" {
			return "", fmt.Errorf("%s does not contain wallet.db or wallet_encrypted.db", args[0])
		}
	} else if base := filepath.Base(path); base != "wallet.db" && base != "wallet_encrypted.db" {
		return "", fmt.Errorf("%s is not a wallet database; expected wallet.db or wallet_encrypted.db", args[0])
	}

	// Verify the wallet is multi-vault
	if !status.Wallet.Multi {
		return "", fmt.Errorf("wallet %s must be converted to multi-vault before it can adopt %s", formatHome(WalletDir), formatHome(path))
	}

	// Confirm what we're about to do
	i, _, err := (&promptui.Select{
		Label: fmt.Sprintf("Adopt vault %s into wallet %s?", formatHome(path), formatHome(WalletDir)),
		Items: []string{
			"Yes",
			"No",
		},
	}).Run()
	if err != nil {
		return "", err
	}
	if i == 1 {
		return "", nil
	}

	// Prompt for the vault name
	name, err := (&promptui.Prompt{
		Label: "What should the vault be named?",
		Validate: func(s string) error {
			if s == "" {
				return errors.New("cannot be empty")
			}
			return nil
		},
	}).Run()
	if err != nil {
		return "", err
	}

	// Do it
	_, err = x.getWallet().AdoptVault(cmd.Context(), &api.AdoptVaultRequest{
		NewVaultName: name,
		OldVaultPath: path,
	})
	if err != nil {
		return "", err
	}

	return "Adopted", nil
}

func (x *Context) restoreVault(cmd *cobra.Command, args []string) (string, error) {
	return "", x.ImportAccounts(cmd, args[0])
}

func formatHome(s string) string {
	home := currentUser.HomeDir + string(filepath.Separator)
	if !strings.HasPrefix(s, home) {
		return s
	}
	return "~/" + s[len(home):]
}

func (x *Context) convertSingleToMulti(cmd *cobra.Command, status *api.StatusResponse) (string, error) {
	// Doesn't make sense if the wallet doesn't exist
	if !status.Wallet.Exists {
		fmt.Fprintf(os.Stderr, "If you wish to convert an old wallet, run `%s vault migrate [path]`\n", os.Args[0])
		os.Exit(1)
	}

	// Is it already multi-vault?
	if status.Wallet.Multi {
		return "", errors.New("wallet is already multi-vault")
	}

	i, _, err := (&promptui.Select{
		Label: "Convert wallet to a multi-vault wallet?",
		Items: []string{
			"Yes",
			"No",
		},
	}).Run()
	if err != nil {
		return "", err
	}
	if i == 1 {
		return "", nil
	}

	fmt.Println("The current wallet will be converted to a vault.")
	name, err := (&promptui.Prompt{
		Label: "What should it be named?",
		Validate: func(s string) error {
			if s == "" {
				return errors.New("cannot be empty")
			}
			return nil
		},
	}).Run()
	if err != nil {
		return "", err
	}

	// Encrypted?
	i, _, err = (&promptui.Select{
		Label: "Encrypt the vault list?",
		Items: []string{
			"Yes",
			"No",
		},
	}).Run()
	if err != nil {
		return "", err
	}
	var passphrase string
	if i == 0 {
		passphrase, err = interactive.GetNewPassword("wallet")
		if err != nil {
			return "", fmt.Errorf("set password: %w", err)
		}
	}

	_, err = x.getWallet().ConvertWallet(cmd.Context(), &api.ConvertWalletRequest{
		Vault:      name,
		Passphrase: passphrase,
	})
	if err != nil {
		return "", err
	}

	return "Conversion complete", nil
}

func (x *Context) copyVaultSanitized(cmd *cobra.Command, args []string) (string, error) {
	if !NonInteractive {
		i, _, err := (&promptui.Select{
			Label: "This will create an " + color.New(color.FgHiYellow, color.Bold).Sprint("unencrypted") + " copy of your vault with private keys etc replaced by zeros. Continue?",
			Items: []string{
				"Yes",
				"No",
			},
		}).Run()
		if err != nil || i != 0 {
			return "", err
		}
	}

	_, err := x.getWallet().CopyVaultSanitized(cmd.Context(), &api.CopyVaultSanitizedRequest{
		Destination: args[0],
	})
	if err != nil {
		return "", err
	}
	return "Success", nil
}
