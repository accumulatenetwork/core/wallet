package cmd

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/address"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/testutils"
)

func init() {
	testMatrix.addTest(testCase4_9)
	testMatrix.addTest(testCase4_10)
	testMatrix.addTest(testCase4_11)
	testMatrix.addTest(testCase4_12)
	testMatrix.addTest(testCase4_13)
	testMatrix.addTest(testCase4_14)
	testMatrix.addTest(testCase4_15)
	testMatrix.addTest(testCase4_16)
}

// testCase4_9 ED25519 test of ed25519 default signature type
func testCase4_9(t *testing.T, tc *testCmd) {

	sig := protocol.SignatureTypeED25519

	// generate protocol signature with default signature type
	r, err := tc.execute(t, "key generate ed25519")
	require.NoError(t, err)
	kr := KeyResponse{}
	require.NoError(t, json.Unmarshal([]byte(r), &kr))

	// verify signature type
	require.Equal(t, sig, kr.KeyInfo.Type)
}

// testCase4_10 deprecated Legacyed25519ED25519 in favor of ed25519 default signature type
func testCase4_10(t *testing.T, tc *testCmd) {
	// generate protocol signature with legacyed25519
	r, err := tc.execute(t, "key generate --sigtype legacyed25519 legacyed25519")
	//the cli has deprecated the legacyed25519, so the key should be converted by the cli
	// to a regular ed25519 type instead, so check for that.
	sig := protocol.SignatureTypeED25519
	require.NoError(t, err)
	kr := KeyResponse{}
	require.NoError(t, json.Unmarshal([]byte(r), &kr))

	// verify signature type is a regular ed25519
	require.Equal(t, sig, kr.KeyInfo.Type)
}

// testCase4_11 SignatureTypeRCD1 test of rcd1 default signature type
func testCase4_11(t *testing.T, tc *testCmd) {

	sig := protocol.SignatureTypeRCD1

	// generate protocol signature with rcd1
	r, err := tc.execute(t, "key generate --sigtype rcd1 rcd1")
	require.NoError(t, err)
	kr := KeyResponse{}
	require.NoError(t, json.Unmarshal([]byte(r), &kr))

	// verify signature type
	require.Equal(t, sig, kr.KeyInfo.Type)
}

// testCase4_12 BTCSignature test of btc default signature type
func testCase4_12(t *testing.T, tc *testCmd) {

	sig := protocol.SignatureTypeBTC

	// generate protocol signature with btc
	r, err := tc.execute(t, "key generate --sigtype btc btc")
	require.NoError(t, err)
	kr := KeyResponse{}
	require.NoError(t, json.Unmarshal([]byte(r), &kr))

	// verify signature type
	require.Equal(t, sig, kr.KeyInfo.Type)
}

// testCase4_13 BTCLegacySignature test of btclegacy signature type
func testCase4_13(t *testing.T, tc *testCmd) {

	sig := protocol.SignatureTypeBTCLegacy

	// generate protocol signature with btclegacy
	r, err := tc.execute(t, "key generate --sigtype btclegacy btclegacy")
	require.NoError(t, err)
	kr := KeyResponse{}
	require.NoError(t, json.Unmarshal([]byte(r), &kr))

	// verify signature type
	require.Equal(t, sig, kr.KeyInfo.Type)
}

// testCase4_14 SignatureTypeETH test of eth signature type
func testCase4_14(t *testing.T, tc *testCmd) {

	sig := protocol.SignatureTypeETH

	// generate protocol signature with eth
	r, err := tc.execute(t, "key generate --sigtype eth eth")
	require.NoError(t, err)
	kr := KeyResponse{}
	require.NoError(t, json.Unmarshal([]byte(r), &kr))

	// verify signature type
	require.Equal(t, sig, kr.KeyInfo.Type)
}

// Convert ADI Token Account to Staking account, should pass
func testCase4_15(t *testing.T, tc *testCmd) {
	//rename one of the lite account keys.
	origName1 := url.MustParse(liteAccounts[0]).Hostname()
	origName2 := url.MustParse(liteAccounts[1]).Hostname()
	renameTo := "renametest1"
	renameToTwo := "renametest2"
	_, err := tc.execute(t, "key label rename "+origName1+" "+renameTo)
	require.NoError(t, err)

	//if the rename worked, then the export should work
	_, err = tc.execute(t, "key export private "+renameTo)
	//must pass
	require.NoError(t, err)

	//this must fail.
	_, err = tc.execute(t, "key label rename "+origName1+" "+origName2)
	require.Error(t, err)

	//this must fail.
	_, err = tc.execute(t, "key label rename "+origName2+" "+renameTo)
	require.Error(t, err)

	//this should pass
	_, err = tc.execute(t, "key label rename "+origName2+" "+renameTo+" --force")
	require.NoError(t, err)

	//now assign, we should have the same key mapped to multiple names
	_, err = tc.execute(t, "key label assign "+renameTo+" "+renameToTwo)
	require.NoError(t, err)

	//now we can query by renameTo and renameToTwo
	r6, err := tc.execute(t, "key export private "+renameTo)
	require.NoError(t, err)

	r7, err := tc.execute(t, "key export private "+renameToTwo)
	require.NoError(t, err)

	type pvk struct {
		PrivateKey string `json:"privateKey"`
	}
	pvk1 := pvk{}
	pvk2 := pvk{}
	require.NoError(t, json.Unmarshal([]byte(r6), &pvk1))
	require.NoError(t, json.Unmarshal([]byte(r7), &pvk2))
	//now renameTo and renameToTwo must match
	require.Equal(t, pvk1.PrivateKey, pvk2.PrivateKey)
}

// testCase4_16 Verify Signing a transaction with a public key hash
func testCase4_16(t *testing.T, tc *testCmd) {

	_, err := tc.executeTx(t, "credits %s acc://RedWagon.acme/book/1 50000", liteAccounts[2])
	require.NoError(t, err)

	k, err := tc.execute(t, "key export private red1")
	require.NoError(t, err)
	var res KeyResponse
	require.NoError(t, json.Unmarshal([]byte(k), &res))

	addr, err := address.Parse(*res.Addresses[0].AsString())
	require.NoError(t, err)
	hash, ok := addr.GetPublicKeyHash()
	require.True(t, ok)
	hexhash := hex.EncodeToString(hash)

	_, err = tc.executeTx(t, "credits %s acc://RedWagon.acme/book/1 1000 10", liteAccounts[2])
	require.NoError(t, err)

	command := fmt.Sprintf("page key add acc://RedWagon.acme/book/1 --sign-with=%s red4", hexhash)

	_, err = tc.executeTx(t, command)
	require.NoError(t, err)
}

func TestRsaSha256(t *testing.T) {
	// Set up the wallet
	tc := newTestCmd(t, testCmdOpts{})
	_, err := _wallet.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:       tc.dir,
		Token:      testutils.TestToken[:],
		MultiVault: false,
		Passphrase: "",
	})
	require.NoError(t, err)

	_, err = _wallet.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	// Import a hard-coded key
	_, err = tc.executeWithInput(t, "key import private rsaKey", "AS3M5vNQ37taKs7v8wo9e6vop9NGJNwWvQd11uHnSbaJMbdiHJLyCD1p9iHJhRqbg3qtLSvfb8GTWgmh9yCjA2mpe3vpkxpqLEjpNVAAv4e6sYWP8Bwrp4DaQUqyccFiy5oT5CHhXopNtL26QFcS38rUHHY3mzpyPm4jmG1BRwdJWUGr6Q24tMcStaD9W1W9vK7wWwN2TvkwH4Dzcu6xfJcjFnszNQzwvLuSmDRfwC4tWgbN1uL6ajPJCnx6U1k8uaCYkJUyqjv37Q1GydfQ6d9NDuBpZ8YuFNKvRGT8t81zV4B4zqLCK7dLwtmkVX2mVkxe2YKi2M97AJMXTYu7GrhG9hUXLXxKtweXUQbBhELQPqUvGinch4TWuNh7n8RiSrjxuq3eeMSdpAdPLKaseYNfJEwexrk3E57BwLxzWWTBDppYVsZ7e3K1j58nXVdEXc8EeTu5mB5eaCoKaqtGvnVvCLW7a3dYjuBXJ9jqo3Bidb57L2juda6ywrpHCKUbD6BXNSnGeaf3ni8FGVx7omb3q8EDtH68HHE6tfDEgsw3MUAHVj5A37qQ993Ues96WKZqnZDRdbDcLcZiawfpstRQpmNminCtc94PJY3iZxL2SZU8ssjYBkY2tosnwgvucNpen5uaXuFEZcgEtNjF4KoppjW6Ybyfj4GkKaQiKkwbY7CcrwD6bPG8zjPkp1tCAVJ994hceAKDejQB96Lx2apeZ4jpaNeq2zY8QYikGqajigu3Eom3XvgD1zkAUpCVX1pBKRfczwbBg1D6TfbFJ2P48EM8sTxquPJRKueDq1K7WfqrbLPtrUrysFGHTLJcyXBPKSd7\n")
	require.NoError(t, err)

	// List the key
	keys, err := _wallet.KeyList(context.Background(), &api.KeyListRequest{})
	require.NoError(t, err)
	require.Len(t, keys.KeyList, 1)
	key := keys.KeyList[0]
	require.Equal(t, "rsaSha256", key.KeyInfo.Type.String())

	// Get some tokens
	_, err = tc.executeTx(t, "faucet acc://%s/ACME", key.Labels.Lite)
	require.NoError(t, err)

	// Do something
	_, err = tc.executeTx(t, "credits acc://%s/ACME acc://%[1]s 1000", key.Labels.Lite)
	require.NoError(t, err)
}

func TestEcdsaSha256(t *testing.T) {
	// Set up the wallet
	tc := newTestCmd(t, testCmdOpts{})
	_, err := _wallet.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:       tc.dir,
		Token:      testutils.TestToken[:],
		MultiVault: false,
		Passphrase: "",
	})
	require.NoError(t, err)

	_, err = _wallet.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	// Import a hard-coded key
	_, err = tc.executeWithInput(t, "key import private ecdsaKey", "AS24M2JMxB4uWnD2hsr16JbMrfJM1MbZsU2AumL9i3Yho9M26wKQpK3gyVATBH59rpK8AjkxMR3hRTmR6VPYWV9ZZDpj7YYs1wDg3QH56wS979TJYoRCbNTSVpwejVttDNQUd9qgMc5T8u5y1fvyXGpEBDPw1BBq6CscnYWwuxDSM4\n")
	require.NoError(t, err)

	// List the key
	keys, err := _wallet.KeyList(context.Background(), &api.KeyListRequest{})
	require.NoError(t, err)
	require.Len(t, keys.KeyList, 1)
	key := keys.KeyList[0]
	require.Equal(t, "ecdsaSha256", key.KeyInfo.Type.String())

	// Get some tokens
	_, err = tc.executeTx(t, "faucet acc://%s/ACME", key.Labels.Lite)
	require.NoError(t, err)

	// Do something
	_, err = tc.executeTx(t, "credits acc://%s/ACME acc://%[1]s 1000", key.Labels.Lite)
	require.NoError(t, err)
}
