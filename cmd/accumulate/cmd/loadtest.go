package cmd

import (
	"errors"
	"fmt"
	"log"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
)

var makeLoadCmd = rootCmd.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:    "make-load [account] [transaction]",
		Short:  "Generate load",
		Args:   cobra.ExactArgs(2),
		Run:    x.runCmdFunc2(x.makeLoad),
		Hidden: true,
	}
})

var makeLoadFlags = struct {
	TPS int
}{}

func init() {
	makeLoadCmd.Flags().IntVar(&makeLoadFlags.TPS, "tps", 1, "Transactions to issue per second")
}

func (x *Context) makeLoad(cmd *cobra.Command, args []string) (string, error) {
	account, err := url.Parse(args[0])
	if err != nil {
		return "", err
	}

	signer, err := x.getSigners(cmd, account, nil)
	if err != nil {
		return "", err
	}

	body, err := parseTx(cmd, args[1])
	if err != nil {
		return "", err
	}

	t := time.NewTicker(time.Second)
	defer t.Stop()

	env := new(messaging.Envelope)
	for range t.C {
		env.Signatures = env.Signatures[:0]
		env.Transaction = env.Transaction[:0]
		for range makeLoadFlags.TPS {
			txn := x.buildTransaction(protocol.CopyTransactionBody(body), account)

			for _, signer := range signer {
				signer.Transaction = txn
				r, err := x.getWallet().Sign(cmd.Context(), signer)
				if err != nil {
					return "", err
				}
				txn = r.Transaction
				env.Signatures = append(env.Signatures, r.Signature)
			}

			env.Transaction = append(env.Transaction, txn)
		}

		log.Printf("Submitting %d transactions", len(env.Transaction))
		for _, txn := range env.Transaction {
			fmt.Printf("  Txn %v\n", txn.ID().ShortString())
		}
		for _, sig := range env.Signatures {
			fmt.Printf("  Sig %v\n", sig.GetSigner().WithTxID([32]byte(sig.Hash())).ShortString())
		}

		sub, err := ClientV3.Submit(cmd.Context(), env, api.SubmitOptions{})
		if err != nil {
			return "", err
		}
		var ok = true
		for _, sub := range sub {
			if !sub.Success {
				ok = false
				fmt.Println(sub.Message)
			}
		}
		if !ok {
			return "", errors.New("failed")
		}
	}

	return "", nil
}
