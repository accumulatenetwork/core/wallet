package cmd

import (
	"context"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	url2 "gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
)

func init() {
	pageCmd.AddCommand(
		pageGetCmd,
		pageCreateCmd,
		pageKeyCmd,
		pageSetThresholdCmd,
		pageLockCmd,
		pageUnlockCmd)

	pageKeyCmd.AddCommand(
		pageKeyAddCmd,
		pageKeyUpdateCmd,
		pageKeyReplaceCmd,
		pageKeyRemoveCmd)
}

var pageCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "page",
		Short: "Create and manage Keys, Books, and Pages",
	}
})

var pageGetCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "get [url]",
		Short: "Get existing Key Page by URL",
		Args:  cobra.ExactArgs(1),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.GetAndPrintKeyPage(cmd, args[0])
		}),
	}
})

var pageCreateCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "create [origin key book url] [public key 1] ... [public key hex or name n + 1]",
		Short: "Create a key page",
		Args:  cobra.MinimumNArgs(2),
		Run:   x.runCmdFunc2(x.CreateKeyPage),
	}
}).With((*Context).submitFlags)

var pageKeyCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "key",
		Short: "Add, update, or remove keys from a key page",
	}
})

var pageKeyAddCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "add [key page url] [new key name]",
		Short: "Add a key to a key page",
		Args:  cobra.ExactArgs(2),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.KeyPageUpdate(cmd, args[0], protocol.KeyPageOperationTypeAdd, args[1:])
		}),
	}
}).With((*Context).submitFlags)

var pageKeyRemoveCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "remove [key page url]  [old key name]",
		Short: "Remove a key from a key page",
		Args:  cobra.ExactArgs(2),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.KeyPageUpdate(cmd, args[0], protocol.KeyPageOperationTypeRemove, args[1:])
		}),
	}
}).With((*Context).submitFlags)

var pageKeyUpdateCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "update [key page url] [old key name] [new public key or name]",
		Short: "Update a key on a key page",
		Args:  cobra.ExactArgs(3),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.KeyPageUpdate(cmd, args[0], protocol.KeyPageOperationTypeUpdate, args[1:])
		}),
	}
}).With((*Context).submitFlags)

var pageKeyReplaceCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "replace [key page url] [new public key or name]",
		Short: "Update a your key on a key page which bypasses threshold",
		Args:  cobra.ExactArgs(2),
		Run:   x.runCmdFunc2(x.ReplaceKey),
	}
}).With((*Context).submitFlags)

var pageSetThresholdCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "set-threshold [key page url] [threshold]",
		Short: "Set the M-of-N signature threshold for a key page",
		Args:  cobra.ExactArgs(2),
		Run:   x.runCmdFunc2(x.setKeyPageThreshold),
	}
}).With((*Context).submitFlags)

var pageLockCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "lock [key page url]",
		Short: "Lock a key page",
		Args:  cobra.ExactArgs(1),
		Run:   x.runCmdFunc2(x.lockKeyPage),
	}
}).With((*Context).submitFlags)

var pageUnlockCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "unlock [key page url]",
		Short: "Unlock a key page",
		Args:  cobra.ExactArgs(1),
		Run:   x.runCmdFunc2(x.unlockKeyPage),
	}
}).With((*Context).submitFlags)

func (x *Context) GetAndPrintKeyPage(cmd *cobra.Command, url string) (string, error) {
	res, _, err := GetKeyPage(url)
	if err != nil {
		return "", fmt.Errorf("error retrieving key page for %s, %v", url, err)
	}

	return x.PrintChainQueryResponseV2(cmd, res)
}

func GetKeyPage(url string) (*QueryResponse, *protocol.KeyPage, error) {
	res, err := GetUrl(url)
	if err != nil {
		return nil, nil, err
	}

	if res.Type != protocol.AccountTypeKeyPage.String() {
		return nil, nil, fmt.Errorf("expecting key page but received %v", res.Type)
	}

	kp := protocol.KeyPage{}
	err = Remarshal(res.Data, &kp)
	if err != nil {
		return nil, nil, err
	}
	return res, &kp, nil
}

// CreateKeyPage create a new key page
func (x *Context) CreateKeyPage(cmd *cobra.Command, args []string) (string, error) {
	bookUrlStr, args := args[0], args[1:]
	bookUrl, err := url2.Parse(bookUrlStr)
	if err != nil {
		return "", err
	}

	signer, err := x.getSigners(cmd, bookUrl, nil)
	if err != nil {
		return "", err
	}

	ckp := protocol.CreateKeyPage{}
	ckp.Keys = make([]*protocol.KeySpecParams, len(args))
	for i, s := range args {
		key, err := x.getWallet().ResolveKey(context.Background(), &api.ResolveKeyRequest{Value: s})
		if err != nil {
			return "", fmt.Errorf("key arg %d", i)
		}
		if key.PublicKeyHash == nil {
			return "", fmt.Errorf("cannot determine hash for key arg %d", i)
		}
		ckp.Keys[i] = &protocol.KeySpecParams{KeyHash: key.PublicKeyHash}
	}

	return x.dispatchTxAndPrintResponse(cmd, &ckp, bookUrl, signer)
}

func (x *Context) KeyPageUpdate(cmd *cobra.Command, origin string, op protocol.KeyPageOperationType, args []string) (string, error) {
	u, err := url2.Parse(origin)
	if err != nil {
		return "", err
	}

	signer, err := x.getSigners(cmd, u, nil)
	if err != nil {
		return "", err
	}

	ukp := protocol.UpdateKeyPage{}

	switch op {
	case protocol.KeyPageOperationTypeUpdate:
		if len(args) < 2 {
			return "", fmt.Errorf("invalid number of arguments")
		}
		oldKey, err := x.parseKeyPageEntry(cmd, args[0])
		if err != nil {
			return "", err
		}

		newKey, err := x.parseKeyPageEntry(cmd, args[1])
		if err != nil {
			return "", err
		}

		ukp.Operation = append(ukp.Operation, &protocol.UpdateKeyOperation{OldEntry: oldKey, NewEntry: newKey})
	case protocol.KeyPageOperationTypeAdd:
		if len(args) < 1 {
			return "", fmt.Errorf("invalid number of arguments")
		}
		newKey, err := x.parseKeyPageEntry(cmd, args[0])
		if err != nil {
			return "", err
		}
		ukp.Operation = append(ukp.Operation, &protocol.AddKeyOperation{Entry: newKey})
	case protocol.KeyPageOperationTypeRemove:
		if len(args) < 1 {
			return "", fmt.Errorf("invalid number of arguments")
		}
		oldKey, err := x.parseKeyPageEntry(cmd, args[0])
		if err != nil {
			return "", err
		}
		ukp.Operation = append(ukp.Operation, &protocol.RemoveKeyOperation{Entry: oldKey})
	}

	return x.dispatchTxAndPrintResponse(cmd, &ukp, u, signer)
}

func (x *Context) ReplaceKey(cmd *cobra.Command, args []string) (string, error) {
	principal, err := url2.Parse(args[0])
	if err != nil {
		return "", err
	}

	k, err := x.resolvePublicKey(cmd, args[1])
	if err != nil {
		return "", err
	}

	txn := new(protocol.UpdateKey)
	txn.NewKeyHash = k.PublicKeyHash()

	signer, err := x.getSigners(cmd, principal, &protocol.Transaction{Body: txn})
	if err != nil {
		return "", err
	}

	return x.dispatchTxAndPrintResponse(cmd, txn, principal, signer)
}

func (x *Context) setKeyPageThreshold(cmd *cobra.Command, args []string) (string, error) {
	args, principal, signer, err := x.parseArgsAndPrepareSigner(cmd, args)
	if err != nil {
		return "", err
	}

	value, err := strconv.ParseUint(args[0], 10, 16)
	if err != nil {
		return "", fmt.Errorf("invalid threshold: %v", err)
	}

	op := new(protocol.SetThresholdKeyPageOperation)
	op.Threshold = value
	txn := new(protocol.UpdateKeyPage)
	txn.Operation = append(txn.Operation, op)

	return x.dispatchTxAndPrintResponse(cmd, txn, principal, signer)
}

func (x *Context) lockKeyPage(cmd *cobra.Command, args []string) (string, error) {
	_, principal, signer, err := x.parseArgsAndPrepareSigner(cmd, args)
	if err != nil {
		return "", err
	}

	op := new(protocol.UpdateAllowedKeyPageOperation)
	op.Deny = append(op.Deny, protocol.TransactionTypeUpdateKeyPage)
	txn := new(protocol.UpdateKeyPage)
	txn.Operation = append(txn.Operation, op)

	return x.dispatchTxAndPrintResponse(cmd, txn, principal, signer)
}

func (x *Context) unlockKeyPage(cmd *cobra.Command, args []string) (string, error) {
	_, principal, signer, err := x.parseArgsAndPrepareSigner(cmd, args)
	if err != nil {
		return "", err
	}

	op := new(protocol.UpdateAllowedKeyPageOperation)
	op.Allow = append(op.Deny, protocol.TransactionTypeUpdateKeyPage)
	txn := new(protocol.UpdateKeyPage)
	txn.Operation = append(txn.Operation, op)

	return x.dispatchTxAndPrintResponse(cmd, txn, principal, signer)
}

func (x *Context) parseKeyPageEntry(cmd *cobra.Command, s string) (protocol.KeySpecParams, error) {
	key, err := x.getWallet().ResolveKey(cmd.Context(), &api.ResolveKeyRequest{
		Value: s,
	})
	if err == nil {
		return protocol.KeySpecParams{KeyHash: key.PublicKeyHash}, nil
	}

	if u, e := url.Parse(s); e == nil {
		return protocol.KeySpecParams{Delegate: u}, nil
	}
	return protocol.KeySpecParams{}, err
}
