package cmd

import (
	"context"

	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/interactive"
)

var configCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "config",
		Short: "Set configuration values",
	}
})

var configOpCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "1password [vault name] [item] [field]",
		Short: "Configure the 1Password item and field that the wallet encryption key is stored in",
		Args:  cobra.ExactArgs(3),
		Run:   x.runCmdFunc2(x.configure1Password),
	}
})

var configClearOpCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "clear-1password [vault name]",
		Short: "Remove the 1Password configuration",
		Args:  cobra.ExactArgs(1),
		Run:   x.runCmdFunc2(x.clear1Password),
	}
})

func init() {
	configCmd.AddCommand(
		configOpCmd,
		configClearOpCmd,
	)
}

func (x *Context) configure1Password(cmd *cobra.Command, args []string) (string, error) {
	ref, err := interactive.ResolveOnePassword(args[1], args[2])
	if err != nil {
		return "", err
	}
	_, err = x.getWallet().Set1PasswordRef(context.Background(), &api.Set1PasswordRefRequest{Vault: args[0], Value: ref})
	if err != nil {
		return "", err
	}
	return "Configured 1Password integration", nil
}

func (x *Context) clear1Password(cmd *cobra.Command, args []string) (string, error) {
	_, err := x.getWallet().Set1PasswordRef(context.Background(), &api.Set1PasswordRefRequest{Vault: args[0]})
	if err != nil {
		return "", err
	}
	return "Removed 1Password integration", nil
}
