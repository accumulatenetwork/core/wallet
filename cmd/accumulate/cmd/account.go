package cmd

import (
	"bufio"
	"bytes"
	"context"
	"crypto/sha256"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/manifoldco/promptui"
	"github.com/mdp/qrterminal"
	"github.com/olekukonko/tablewriter"
	"github.com/spf13/cobra"
	v3 "gitlab.com/accumulatenetwork/accumulate/pkg/api/v3"
	"gitlab.com/accumulatenetwork/accumulate/pkg/api/v3/jsonrpc"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/merkle"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	url2 "gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	api2 "gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
)

func init() {
	accountCmd.AddCommand(
		accountGetCmd,
		accountCreateCmd,
		accountQrCmd,
		accountGenerateCmd,
		accountListCmd,
		accountLockCmd,
		accountRegisterCmd,
		accountUnregisterCmd)

	accountCreateCmd.AddCommand(
		accountCreateTokenCmd,
		accountCreateDataCmd)

	accountCreateTokenCmd.Flags().BoolVar(&flagAccount.Lite, "lite", false, "Create a lite token account")
	accountGenerateCmd.Flags().StringVar(&SigType, "sigtype", "ed25519", "Specify the signature type use rcd1 for RCD1/Factoid(FCT) type ; ed25519 for accumulate ED25519 ; btc for Bitcoin ; btclegacy for LegacyBitcoin  ; eth for Ethereum ")
	accountGenerateCmd.Flags().BoolVarP(&flagKey.Force, "force", "f", false, "If there is an existing external key, overwrite it")
	accountLockCmd.Flags().BoolVarP(&flagAccount.Force, "force", "f", false, "Do not prompt the user")
}

var flagAccount = struct {
	Lite     bool
	LiteData string
	Force    bool
}{}

var accountCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "account",
		Short: "Create and get token accounts",
	}
})

var accountCreateCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "create",
		Short: "Create an account",
	}
})

var accountGetCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "get [url]",
		Short: "Get an account by URL",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.GetTokenAccount(cmd, args[0])
			x.printOutput(cmd, out, err)
		},
	}
})

var accountCreateTokenCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "token",
		Short: "Create an ADI token account",
		Example: "" +
			"accumulate account create token [actor adi] [new token account url] [tokenUrl]\n" +
			"accumulate account create token --lite [lite token account url]\n" +
			"",
		Args: cobra.RangeArgs(1, 3),
		Run:  x.runTxnCmdFunc(x.CreateTokenAccount),
	}
}).
	With((*Context).submitFlags).
	With((*Context).createFlags)

var accountQrCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "qr [url]",
		Short: "Display QR code for lite token account URL",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := QrAccount(args[0])
			x.printOutput(cmd, out, err)
		},
	}
})

var accountGenerateCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "generate",
		Short: "Generate a lite token address",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			// Generate a key with no lables
			out, err := x.GenerateKey(cmd)
			x.printOutput(cmd, out, err)
		},
	}
})

var accountListCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "list",
		Short: "Display all lite token accounts",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, _ []string) {
			out, err := x.ListAccounts(cmd)
			x.printOutput(cmd, out, err)
		},
	}
})

var _ = accountListCmd.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "registered",
		Short: "Display registered accounts",
		Args:  cobra.NoArgs,
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			r, err := x.getWallet().ListAccounts(cmd.Context(), &api2.ListAccountsRequest{})
			if err != nil {
				return "", err
			}
			buf := new(strings.Builder)
			tw := tablewriter.NewWriter(buf)
			tw.SetBorder(false)
			for _, token := range r.Tokens {
				tw.Append([]string{"Token account", token.Url.String()})
			}
			for _, book := range r.Books {
				tw.Append([]string{"Key book", book.Url.String()})
			}
			tw.Render()
			return buf.String(), nil
		}),
	}
})

var accountLockCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "lock [account url] [height]",
		Short: "Lock the account until the given block height",
		Args:  cobra.ExactArgs(2),
		Run:   x.runCmdFunc2(x.lockAccount),
	}
}).With((*Context).submitFlags)

var accountRegisterCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "register [account url]",
		Short: "Register a token account with the wallet",
		Args:  cobra.ExactArgs(1),
		Run:   x.runCmdFunc2(x.registerAccount),
	}
})

var accountUnregisterCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "unregister [account url]",
		Short: "Unregister a token account with the wallet",
		Args:  cobra.ExactArgs(1),
		Run:   x.runCmdFunc2(x.unregisterAccount),
	}
})

func (x *Context) GetTokenAccount(cmd *cobra.Command, url string) (string, error) {
	res, err := GetUrl(url)
	if err != nil {
		return "", err
	}

	if res.Type != protocol.AccountTypeTokenAccount.String() && res.Type != protocol.AccountTypeLiteTokenAccount.String() &&
		res.Type != protocol.AccountTypeLiteIdentity.String() &&
		res.Type != protocol.AccountTypeDataAccount.String() && res.Type != protocol.AccountTypeLiteDataAccount.String() {
		return "", fmt.Errorf("expecting token account or data account but received %v", res.Type)
	}

	return x.PrintChainQueryResponseV2(cmd, res)
}

func QrAccount(s string) (string, error) {
	u, err := url2.Parse(s)
	if err != nil {
		return "", fmt.Errorf("%q is not a valid Accumulate URL: %v", s, err)
	}

	b := bytes.NewBufferString("")
	qrterminal.GenerateWithConfig(u.String(), qrterminal.Config{
		Level:          qrterminal.M,
		Writer:         b,
		HalfBlocks:     true,
		BlackChar:      qrterminal.BLACK_BLACK,
		BlackWhiteChar: qrterminal.BLACK_WHITE,
		WhiteChar:      qrterminal.WHITE_WHITE,
		WhiteBlackChar: qrterminal.WHITE_BLACK,
		QuietZone:      2,
	})

	r, err := io.ReadAll(b)
	return string(r), err
}

// CreateTokenAccount account create url labelOrPubKeyHex height index tokenUrl keyBookUrl
func (x *Context) CreateTokenAccount(cmd *cobra.Command, principal *url2.URL, signers []*api.SignRequest, args []string) (string, error) {
	if flagAccount.Lite {
		return x.CreateLiteTokenAccount(cmd, principal, signers, args)
	}

	if len(args) < 2 {
		return "", fmt.Errorf("wrong number of arguments")
	}

	accountUrl, err := url2.Parse(args[0])
	if err != nil {
		return "", fmt.Errorf("invalid account url %s", args[0])
	}
	if principal.Authority != accountUrl.Authority {
		return "", fmt.Errorf("account url to create (%s) doesn't match the authority adi (%s)", accountUrl.Authority, principal.Authority)
	}
	tok, err := url2.Parse(args[1])
	if err != nil {
		return "", fmt.Errorf("invalid token url")
	}

	//make sure this is a valid token account
	req := new(client.GeneralQuery)
	req.Url = tok
	resp := new(client.ChainQueryResponse)
	token := protocol.TokenIssuer{}
	resp.Data = &token
	err = Client.RequestAPIv2(context.Background(), "query", req, resp)
	if err != nil || resp.Type != protocol.AccountTypeTokenIssuer.String() {
		return "", fmt.Errorf("invalid token type %v", err)
	}

	tac := protocol.CreateTokenAccount{}
	tac.Url = accountUrl
	tac.TokenUrl = tok
	tac.Authorities = x.Create.Authorities

	err = proveTokenIssuerExistence(cmd, &tac)
	if err != nil {
		return "", fmt.Errorf("unable to prove account state: %w", err)
	}

	return x.dispatchTxAndPrintResponse(cmd, &tac, principal, signers)
}

// CreateLiteTokenAccount usage is:
// accumulate account create token --lite ${LTA} --sign-with ${KEY}@${SIGNER}
func (x *Context) CreateLiteTokenAccount(cmd *cobra.Command, principal *url2.URL, signers []*api.SignRequest, args []string) (string, error) {
	if len(args) != 0 {
		return "", fmt.Errorf("wrong number of arguments")
	}

	if len(signers) == 0 || !signers[0].Signer.Equal(principal.RootIdentity()) {
		log.Fatal("Internal error: expected first signer to be the lite identity")
	}
	signers = signers[1:]
	if len(signers) == 0 {
		return "", fmt.Errorf("an additional signer must be specified by --sign-with")
	}

	key, tok, err := protocol.ParseLiteTokenAddress(principal)
	if err != nil {
		return "", fmt.Errorf("invalid lite token address: %w", err)
	} else if key == nil {
		return "", fmt.Errorf("not a lite token address: %v", principal)
	}

	if !protocol.AcmeUrl().Equal(tok) {
		return "", fmt.Errorf("create lite token account does not support creating non-ACME accounts")
	}

	body := new(protocol.CreateLiteTokenAccount)
	return x.dispatchTxAndPrintResponse(cmd, body, principal, signers)
}

var theStall = must(time.Parse(time.RFC3339, "2024-01-01T00:00:00Z"))

func proveTokenIssuerExistence(cmd *cobra.Command, body *protocol.CreateTokenAccount) error {
	if body.Url.LocalTo(body.TokenUrl) {
		return nil // Don't need a proof if the issuer is local
	}

	if protocol.AcmeUrl().Equal(body.TokenUrl) {
		return nil // Don't need a proof for ACME
	}

	// Set a longer timeout, otherwise queries tend to fail
	if c, ok := ClientV3.(*jsonrpc.Client); ok {
		orig := c.Client.Timeout
		c.Client.Timeout = max(orig, time.Minute)
		defer func() { c.Client.Timeout = orig }()
	}

	// Get the transaction
	txn, err := Q.QueryMainChainEntry(cmd.Context(), body.TokenUrl, &v3.ChainQuery{
		Name:  "main",
		Index: v3.Ptr[uint64](0),
		IncludeReceipt: &v3.ReceiptOptions{
			ForAny: true,
		},
	})
	if err != nil {
		return err
	}

	// The API defaults to providing a proof for the block containing a
	// transaction, but if that was before The Stall then SearchForAnchor may
	// fail. So instead, figure out which BVN the account is on and tell it to
	// provide a proof for a more recent block.
	if txn.Receipt.LocalBlockTime.Before(theStall) {
		bvn, _, err := routeAccount(body.TokenUrl)
		if err != nil {
			return err
		}

		block, err := Q.QueryMainChainEntries(cmd.Context(), protocol.PartitionUrl(bvn).JoinPath(protocol.AnchorPool), &v3.ChainQuery{
			Name: "anchor-sequence",
			Range: &v3.RangeOptions{
				Start:   100,
				Count:   v3.Ptr[uint64](1),
				FromEnd: true,
				Expand:  v3.Ptr(true),
			},
		})
		if err != nil {
			return err
		}

		anchor := block.Records[0].Value.Message.Transaction.Body.(protocol.AnchorBody)
		txn, err = Q.QueryMainChainEntry(cmd.Context(), body.TokenUrl, &v3.ChainQuery{
			Name:  "main",
			Index: v3.Ptr[uint64](0),
			IncludeReceipt: &v3.ReceiptOptions{
				ForHeight: anchor.GetPartitionAnchor().RootChainIndex,
			},
		})
		if err != nil {
			return err
		}
	}

	create, ok := txn.Value.Message.Transaction.Body.(*protocol.CreateToken)
	if !ok {
		return fmt.Errorf("first transaction of %v is %v, expected %v", body.TokenUrl, txn.Value.Message.Transaction.Body.Type(), protocol.TransactionTypeCreateToken)
	}

	// Start with a proof from the body hash to the transaction hash
	receipt := new(merkle.Receipt)

	b, err := txn.Value.Message.Transaction.Body.MarshalBinary()
	if err != nil {
		return fmt.Errorf("marshal transaction header: %w", err)
	}
	headerHash := sha256.Sum256(b)
	receipt.Start = headerHash[:]

	b, err = txn.Value.Message.Transaction.Header.MarshalBinary()
	if err != nil {
		return fmt.Errorf("marshal transaction header: %w", err)
	}
	bodyHash := sha256.Sum256(b)
	receipt.Entries = []*merkle.ReceiptEntry{{Hash: bodyHash[:]}}
	receipt.Anchor = txn.Value.Message.Transaction.GetHash()

	// Add the proof from the issuer's main chain
	receipt, err = receipt.Combine(&txn.Receipt.Receipt)
	if err != nil {
		return err
	}

	// Get a proof of the BVN anchor
	anchor, err := Q.SearchForAnchor(cmd.Context(), protocol.DnUrl().JoinPath(protocol.AnchorPool), &v3.AnchorSearchQuery{
		Anchor: receipt.Anchor,
		IncludeReceipt: &v3.ReceiptOptions{
			ForAny: true,
		},
	})
	if err != nil {
		return err
	}
	if len(anchor.Records) == 0 {
		return fmt.Errorf("unable to get proof of anchor %x", receipt.Anchor)
	}

	receipt, err = receipt.Combine(&anchor.Records[0].Receipt.Receipt)
	if err != nil {
		return err
	}

	body.Proof = new(protocol.TokenIssuerProof)
	body.Proof.Transaction = create
	body.Proof.Receipt = receipt
	return nil
}

func (x *Context) ListAccounts(cmd *cobra.Command) (string, error) {
	//this just lists the keys...
	return x.listKeys(cmd)
}

func (x *Context) lockAccount(cmd *cobra.Command, args []string) (string, error) {
	principal, err := url2.Parse(args[0])
	if err != nil {
		return "", err
	}

	signers, err := x.getSigners(cmd, principal, nil)
	if err != nil {
		return "", err
	}

	body := new(protocol.LockAccount)
	body.Height, err = strconv.ParseUint(args[1], 10, 64)
	if err != nil {
		return "", fmt.Errorf("invalid height argument: %v", err)
	}

	if flagAccount.Force {
		return x.dispatchTxAndPrintResponse(cmd, body, principal, signers)
	}

	req := new(client.MajorBlocksQuery)
	req.Url = protocol.DnUrl()
	req.Start = 0
	req.Count = 0
	res, err := Client.QueryMajorBlocks(context.Background(), req)
	if err != nil {
		return PrintJsonRpcError(err)
	}

	latest := new(client.MajorQueryResponse)
	if res.Total != 0 {
		req.Start = res.Total
		req.Count = 1
		res, err = Client.QueryMajorBlocks(context.Background(), req)
		if err != nil {
			return PrintJsonRpcError(err)
		}
		if len(res.Items) == 0 {
			return "", fmt.Errorf("failed to query latest major block: empty response")
		}
		err = Remarshal(res.Items[0], latest)
		if err != nil {
			return "", fmt.Errorf("failed to parse query response: %w", err)
		}

		if body.Height <= latest.MajorBlockIndex {
			return "", fmt.Errorf("specified height (%d) is before or the same as the current major block height (%d)", body.Height, latest.MajorBlockIndex)
		}
	}

	days := float64(body.Height-latest.MajorBlockIndex) / 2
	fmt.Printf("This will lock your account for %.1f days. Are you sure [yN]? ", days)
	answer, err := bufio.NewReader(cmd.InOrStdin()).ReadString('\n')
	if err != nil {
		return "", nil
	}
	answer = strings.ToLower(strings.TrimSpace(answer))
	if answer != "y" && answer != "yes" {
		return "", nil
	}

	return x.dispatchTxAndPrintResponse(cmd, body, principal, signers)
}

func (x *Context) registerAccount(cmd *cobra.Command, args []string) (string, error) {
	u, err := url.Parse(args[0])
	if err != nil {
		return "", err
	}

	var tokens protocol.AccountWithTokens
	_, err = Q.QueryAccountAs(cmd.Context(), u, nil, &tokens)
	if err != nil {
		return "", err
	}

	_, err = x.getWallet().RegisterTokenAccount(cmd.Context(), &api2.RegisterTokenAccountRequest{Url: u})
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("Registered %v", u), nil
}

func (x *Context) unregisterAccount(cmd *cobra.Command, args []string) (string, error) {
	u, err := url.Parse(args[0])
	if err != nil {
		return "", err
	}

	_, err = x.getWallet().UnregisterTokenAccount(cmd.Context(), &api2.UnregisterTokenAccountRequest{Url: u})
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("Unregistered %v", u), nil
}

func (x *Context) ExportAccounts(cmd *cobra.Command, filePath string) error {
	res, err := x.getWallet().ExportVault(context.Background(), &api2.ExportVaultRequest{})
	if err != nil {
		return err
	}

	bin, err := json.MarshalIndent(&res, "", "  ")
	if err != nil {
		return fmt.Errorf("cannot convert export to json for export, error: %v", err)
	}

	if filePath == "-" {
		fmt.Println(string(bin))
		return nil
	}

	if _, err := os.Stat(filePath); err == nil {
		opt, err := promptOverwrite()
		if err != nil {
			return err
		}
		if strings.EqualFold(opt, "yes") {
			file, err := os.OpenFile(filePath, os.O_RDWR|os.O_TRUNC, 0600)
			if err != nil {
				return err
			}
			defer file.Close()
			_, err = io.Copy(file, strings.NewReader(string(bin)))
			if err != nil {
				return err
			}
		} else {
			return fmt.Errorf("operation skipped")
		}

	} else if errors.Is(err, os.ErrNotExist) {
		out, err := os.Create(filePath)
		if err != nil {
			return err
		}
		defer out.Close()
		_, err = io.Copy(out, strings.NewReader(string(bin)))
		if err != nil {
			return err
		}
	} else {
		return err
	}
	return nil
}

func (x *Context) ImportAccounts(cmd *cobra.Command, filePath string) error {
	data, err := os.ReadFile(filePath)
	if err != nil {
		return fmt.Errorf("failed reading data from file: %s", err)
	}
	var req *api2.Vault
	if err := json.Unmarshal(data, &req); err != nil {
		return err
	}

	_, err = x.getWallet().ImportVault(context.Background(), &api2.ImportVaultRequest{Vault: req})
	if err != nil {
		return fmt.Errorf("%v", err)
	}

	return nil
}

func promptOverwrite() (string, error) {
	pc := promptContent{
		"",
		"File already exists. Do you want to overwrite?",
	}
	items := []string{"Yes", "No"}
	index := -1
	var result string
	var err error

	for index < 0 {
		prompt := promptui.SelectWithAdd{
			Label: pc.label,
			Items: items,
		}
		index, result, err = prompt.Run()
		if index == -1 {
			items = append(items, result)
		}
	}

	if err != nil {
		return "", err
	}

	return result, nil
}
