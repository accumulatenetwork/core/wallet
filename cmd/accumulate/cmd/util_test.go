package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	. "gitlab.com/accumulatenetwork/accumulate/test/helpers"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/testutils"
)

func TestParseAmount(t *testing.T) {
	cases := []string{"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"}

	for i, c := range cases {
		t.Run(fmt.Sprint(i), func(t *testing.T) {
			const precision = 18
			str := "10000000." + strings.Repeat(c, precision)
			v, err := parseAmount(str, precision)
			require.NoError(t, err)
			require.Equal(t, str, protocol.FormatBigAmount(v, precision))
		})
	}
}

func TestGetSigners(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	t.Cleanup(cancel)
	tc := newTestCmd(t, testCmdOpts{})
	NonInteractive = true
	Q = tc.sim.Query()

	// Create a wallet
	_, err := _wallet.CreateWallet(ctx, &api.CreateWalletRequest{
		Path:  tc.dir,
		Token: testutils.TestToken[:],
	})
	require.NoError(t, err)

	// Import a mnemonic
	_, err = _wallet.ImportMnemonic(ctx, &api.ImportMnemonicRequest{
		Wallet:   tc.dir,
		Token:    testutils.TestToken[:],
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	// Create keys
	aliceKey, err := _wallet.GenerateAddress(ctx, &api.GenerateAddressRequest{
		Wallet: tc.dir,
		Token:  testutils.TestToken[:],
		Labels: []string{"alice"},
	})
	require.NoError(t, err)

	bobKey, err := _wallet.GenerateAddress(ctx, &api.GenerateAddressRequest{
		Wallet: tc.dir,
		Token:  testutils.TestToken[:],
		Labels: []string{"bob"},
	})
	require.NoError(t, err)

	// Setup accounts
	db := tc.sim.Database("BVN0")
	MakeIdentity(t, db, url.MustParse("alice.acme"), aliceKey.Key.PublicKey)
	MakeIdentity(t, db, url.MustParse("bob.acme"), bobKey.Key.PublicKey)
	MakeIdentity(t, db, url.MustParse("charlie.acme"))
	MakeIdentity(t, db, url.MustParse("david.acme"))

	UpdateAccount(t, db, url.MustParse("bob.acme/book/1"), func(p *protocol.KeyPage) {
		p.AddKeySpec(&protocol.KeySpec{Delegate: url.MustParse("alice.acme/book")})
	})

	UpdateAccount(t, db, url.MustParse("charlie.acme/book/1"), func(p *protocol.KeyPage) {
		p.AddKeySpec(&protocol.KeySpec{Delegate: url.MustParse("alice.acme/book")})
	})

	MakeAccount(t, db, &protocol.DataAccount{
		Url: url.MustParse("david.acme/data"),
		AccountAuth: protocol.AccountAuth{Authorities: []protocol.AuthorityEntry{
			{Url: url.MustParse("bob.acme/book")},
			{Url: url.MustParse("charlie.acme/book")},
		}},
	})

	// Test
	sAliceBob := &api.SigningPath{
		Keys:    []*api.Key{&aliceKey.Key},
		Signers: []*url.URL{url.MustParse("alice.acme/book/1"), url.MustParse("bob.acme/book/1")},
	}
	sAliceCharlie := &api.SigningPath{
		Keys:    []*api.Key{&aliceKey.Key},
		Signers: []*url.URL{url.MustParse("alice.acme/book/1"), url.MustParse("charlie.acme/book/1")},
	}
	sBob := &api.SigningPath{
		Keys:    []*api.Key{&bobKey.Key},
		Signers: []*url.URL{url.MustParse("bob.acme/book/1")},
	}

	cases := []struct {
		SignWith []*url.URL
		Paths    []*api.SigningPath
	}{
		// This should cause the books to get registered
		{Paths: []*api.SigningPath{sAliceBob}, SignWith: []*url.URL{url.MustParse("alice.acme/book"), url.MustParse("bob.acme/book")}},
		{Paths: []*api.SigningPath{sAliceCharlie}, SignWith: []*url.URL{url.MustParse("alice.acme/book"), url.MustParse("charlie.acme/book")}},

		// Not specifying any signer should return all possible paths
		{Paths: []*api.SigningPath{sBob, sAliceBob, sAliceCharlie}, SignWith: nil},

		// Paths containing Alice's key and/or book
		{Paths: []*api.SigningPath{sAliceBob, sAliceCharlie}, SignWith: []*url.URL{url.MustParse("alice.acme/book")}},
		{Paths: []*api.SigningPath{sAliceBob, sAliceCharlie}, SignWith: []*url.URL{url.MustParse("alice@*")}},
		{Paths: []*api.SigningPath{sAliceBob, sAliceCharlie}, SignWith: []*url.URL{url.MustParse("alice@alice.acme/book")}},

		// Paths containing Bob's book
		{Paths: []*api.SigningPath{sBob, sAliceBob}, SignWith: []*url.URL{url.MustParse("bob.acme/book")}},

		// Paths containing Bob's key
		{Paths: []*api.SigningPath{sBob}, SignWith: []*url.URL{url.MustParse("bob@*")}},
		{Paths: []*api.SigningPath{sBob}, SignWith: []*url.URL{url.MustParse("bob@bob.acme/book")}},
	}

	for i, c := range cases {
		t.Run(fmt.Sprintf("Case %d", i), func(t *testing.T) {
			txn := new(protocol.Transaction)
			txn.Body = &protocol.UpdateAccountAuth{Operations: []protocol.AccountAuthOperation{
				&protocol.AddAccountAuthorityOperation{Authority: url.MustParse("bob.acme/book")},
				&protocol.AddAccountAuthorityOperation{Authority: url.MustParse("charlie.acme/book")},
			}}

			x := new(Context)
			cmd := rootCmd.Build(x)
			cmd.SetContext(ctx)
			x.Sign.With = c.SignWith
			paths, err := x.getSigners2(
				cmd,
				url.MustParse("david.acme/data"),
				txn)
			require.NoError(t, err)

			b, err := json.MarshalIndent(paths, "", "  ")
			require.NoError(t, err)
			c, err := json.MarshalIndent(c.Paths, "", "  ")
			require.NoError(t, err)
			require.Equal(t, string(c), string(b))
		})
	}
}

// TestGetSignersUpdateKey verifies that getSigners includes direct signing
// paths for page delegates for UpdateKey.
func TestGetSignersUpdateKey(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	t.Cleanup(cancel)
	tc := newTestCmd(t, testCmdOpts{})
	NonInteractive = true
	Q = tc.sim.Query()

	// Create a wallet
	_, err := _wallet.CreateWallet(ctx, &api.CreateWalletRequest{
		Path: tc.dir,
	})
	require.NoError(t, err)

	// Import a mnemonic
	_, err = _wallet.ImportMnemonic(ctx, &api.ImportMnemonicRequest{
		Wallet:   tc.dir,
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	// Create a key
	aliceKey, err := _wallet.GenerateAddress(ctx, &api.GenerateAddressRequest{
		Wallet: tc.dir,
		Labels: []string{"alice"},
	})
	require.NoError(t, err)

	// Setup accounts
	db := tc.sim.Database("BVN0")
	MakeIdentity(t, db, url.MustParse("alice.acme"), aliceKey.Key.PublicKey)
	MakeIdentity(t, db, url.MustParse("bob.acme"))

	UpdateAccount(t, db, url.MustParse("bob.acme/book/1"), func(p *protocol.KeyPage) {
		p.AddKeySpec(&protocol.KeySpec{Delegate: url.MustParse("alice.acme/book")})
	})

	txn := new(protocol.Transaction)
	txn.Body = new(protocol.UpdateKey)

	x := new(Context)
	cmd := rootCmd.Build(x)
	cmd.SetContext(ctx)

	paths, err := x.getSigners2(
		cmd,
		url.MustParse("bob.acme/book/1"),
		txn)
	require.NoError(t, err)

	expect := []*api.SigningPath{{
		Keys:    []*api.Key{&aliceKey.Key},
		Signers: []*url.URL{url.MustParse("alice.acme/book/1")},
	}}

	b, err := json.MarshalIndent(paths, "", "  ")
	require.NoError(t, err)
	c, err := json.MarshalIndent(expect, "", "  ")
	require.NoError(t, err)
	require.Equal(t, string(c), string(b))
}

func TestSignFor(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	t.Cleanup(cancel)
	tc := newTestCmd(t, testCmdOpts{})
	NonInteractive = true
	Q = tc.sim.Query()

	// Create a wallet
	_, err := _wallet.CreateWallet(ctx, &api.CreateWalletRequest{
		Path: tc.dir,
	})
	require.NoError(t, err)

	// Import a mnemonic
	_, err = _wallet.ImportMnemonic(ctx, &api.ImportMnemonicRequest{
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	// Create a key
	aliceKey, err := _wallet.GenerateAddress(ctx, &api.GenerateAddressRequest{
		Labels: []string{"alice"},
	})
	require.NoError(t, err)

	// Setup accounts
	db := tc.sim.Database("BVN0")
	MakeIdentity(t, db, url.MustParse("alice.acme"), aliceKey.Key.PublicKey)
	MakeIdentity(t, db, url.MustParse("bob.acme"))

	txn := new(protocol.Transaction)
	txn.Body = new(protocol.RemoteTransaction)

	// Verify that Alice can sign for Bob with --sign-for regardless of whether
	// Alice is a valid authority of Bob
	x := new(Context)
	x.Sign.For = url.MustParse("alice.acme/book")
	cmd := rootCmd.Build(x)
	cmd.SetContext(ctx)
	paths, err := x.getSigners2(
		cmd,
		url.MustParse("bob.acme"),
		txn)
	require.NoError(t, err)

	expect := []*api.SigningPath{{
		Keys:    []*api.Key{&aliceKey.Key},
		Signers: []*url.URL{url.MustParse("alice.acme/book/1")},
	}}

	b, err := json.MarshalIndent(paths, "", "  ")
	require.NoError(t, err)
	c, err := json.MarshalIndent(expect, "", "  ")
	require.NoError(t, err)
	require.Equal(t, string(c), string(b))
}

func TestSignUpdateKeyPage(t *testing.T) {
	ctx, cancel := context.WithCancel(context.Background())
	t.Cleanup(cancel)
	tc := newTestCmd(t, testCmdOpts{})
	NonInteractive = true
	Q = tc.sim.Query()

	// Create a wallet
	_, err := _wallet.CreateWallet(ctx, &api.CreateWalletRequest{
		Path: tc.dir,
	})
	require.NoError(t, err)

	// Import a mnemonic
	_, err = _wallet.ImportMnemonic(ctx, &api.ImportMnemonicRequest{
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	// Create keys
	key1, err := _wallet.GenerateAddress(ctx, &api.GenerateAddressRequest{
		Labels: []string{"key1"},
	})
	require.NoError(t, err)

	key2, err := _wallet.GenerateAddress(ctx, &api.GenerateAddressRequest{
		Labels: []string{"key2"},
	})
	require.NoError(t, err)

	key3, err := _wallet.GenerateAddress(ctx, &api.GenerateAddressRequest{
		Labels: []string{"key3"},
	})
	require.NoError(t, err)

	keyBob, err := _wallet.GenerateAddress(ctx, &api.GenerateAddressRequest{
		Labels: []string{"keyBob"},
	})
	require.NoError(t, err)

	// Setup accounts
	alice := protocol.AccountUrl("alice")
	bob := protocol.AccountUrl("bob")
	db := tc.sim.Database("BVN0")
	MakeIdentity(t, db, alice, key1.Key.PublicKey)
	MakeIdentity(t, db, bob, keyBob.Key.PublicKey)
	MakeKeyPage(t, db, alice.JoinPath("book"), key2.Key.PublicKey)
	MakeKeyPage(t, db, alice.JoinPath("book"), key3.Key.PublicKey)

	// Register the books
	x := new(Context)
	cmd := rootCmd.Build(x)
	cmd.SetContext(ctx)
	require.NoError(t, x.registerBook(cmd, alice.JoinPath("book")))
	require.NoError(t, x.registerBook(cmd, bob.JoinPath("book")))

	// Verify the paths (sanity check)
	r, err := _wallet.FindSigner(ctx, &api.FindSignerRequest{
		Authorities: []*url.URL{
			alice.JoinPath("book"),
			bob.JoinPath("book"),
		},
	})
	require.NoError(t, err)

	expect := []*api.SigningPath{
		{Keys: []*api.Key{&key1.Key}, Signers: []*url.URL{alice.JoinPath("book", "1")}},
		{Keys: []*api.Key{&key2.Key}, Signers: []*url.URL{alice.JoinPath("book", "2")}},
		{Keys: []*api.Key{&key3.Key}, Signers: []*url.URL{alice.JoinPath("book", "3")}},
		{Keys: []*api.Key{&keyBob.Key}, Signers: []*url.URL{bob.JoinPath("book", "1")}},
	}

	b, err := json.MarshalIndent(r.Paths, "", "  ")
	require.NoError(t, err)
	c, err := json.MarshalIndent(expect, "", "  ")
	require.NoError(t, err)
	require.Equal(t, string(c), string(b))

	// Verify that Alice page 1, Alice page 2, and Bob are the allowed signers
	// for a transaction adding Bob as a delegate to Alice page 2
	txn := &protocol.Transaction{
		Header: protocol.TransactionHeader{
			Principal: alice.JoinPath("book", "2"),
		},
		Body: &protocol.UpdateKeyPage{Operation: []protocol.KeyPageOperation{
			&protocol.AddKeyOperation{Entry: protocol.KeySpecParams{
				Delegate: bob.JoinPath("book"),
			}},
		}},
	}

	paths, err := x.getSigners2(
		cmd,
		txn.Header.Principal,
		txn)
	require.NoError(t, err)

	expect = []*api.SigningPath{
		{Keys: []*api.Key{&key1.Key}, Signers: []*url.URL{alice.JoinPath("book", "1")}},
		{Keys: []*api.Key{&key2.Key}, Signers: []*url.URL{alice.JoinPath("book", "2")}},
		{Keys: []*api.Key{&keyBob.Key}, Signers: []*url.URL{bob.JoinPath("book", "1")}},
	}

	b, err = json.MarshalIndent(paths, "", "  ")
	require.NoError(t, err)
	c, err = json.MarshalIndent(expect, "", "  ")
	require.NoError(t, err)
	require.Equal(t, string(c), string(b))
}
