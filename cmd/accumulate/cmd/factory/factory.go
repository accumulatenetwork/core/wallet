package factory

import (
	"net"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
)

type Factory[Ctx any] struct {
	build    func(Ctx) *cobra.Command
	children []*Factory[Ctx]
	flags    []func(*cobra.Command)
}

func New[Ctx any](build func(Ctx) *cobra.Command) *Factory[Ctx] {
	return &Factory[Ctx]{build: build}
}

func (f *Factory[Ctx]) With(fn func(Ctx, *cobra.Command)) *Factory[Ctx] {
	build := f.build
	f.build = func(ctx Ctx) *cobra.Command {
		cmd := build(ctx)
		fn(ctx, cmd)
		return cmd
	}
	return f
}

func (f *Factory[Ctx]) New(build func(Ctx) *cobra.Command) *Factory[Ctx] {
	c := New(build)
	f.children = append(f.children, c)
	return c
}

func (f *Factory[Ctx]) AddCommand(children ...*Factory[Ctx]) {
	f.children = append(f.children, children...)
}

func (f *Factory[Ctx]) Build(ctx Ctx) *cobra.Command {
	cmd := f.build(ctx)
	for _, f := range f.flags {
		f(cmd)
	}
	for _, c := range f.children {
		cmd.AddCommand(c.Build(ctx))
	}
	return cmd
}

func (f *Factory[Ctx]) Flags() *FlagSet[Ctx] {
	return &FlagSet[Ctx]{f, (*cobra.Command).Flags}
}

func (f *Factory[Ctx]) PersistentFlags() *FlagSet[Ctx] {
	return &FlagSet[Ctx]{f, (*cobra.Command).PersistentFlags}
}

func (f *Factory[Ctx]) MarkFlagRequired(name string) {
	f.flags = append(f.flags, func(c *cobra.Command) {
		_ = c.MarkFlagRequired(name)
	})
}

type FlagSet[Ctx any] struct {
	*Factory[Ctx]
	fn func(*cobra.Command) *pflag.FlagSet
}

func (f FlagSet[Ctx]) add(fn func(*pflag.FlagSet)) {
	f.flags = append(f.flags, func(c *cobra.Command) { fn(f.fn(c)) })
}

func (f FlagSet[Ctx]) BoolSliceVar(p *[]bool, name string, value []bool, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.BoolSliceVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) BoolSliceVarP(p *[]bool, name, shorthand string, value []bool, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.BoolSliceVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) BoolVar(p *bool, name string, value bool, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.BoolVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) BoolVarP(p *bool, name, shorthand string, value bool, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.BoolVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) BytesBase64Var(p *[]byte, name string, value []byte, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.BytesBase64Var(p, name, value, usage) })
}

func (f FlagSet[Ctx]) BytesBase64VarP(p *[]byte, name, shorthand string, value []byte, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.BytesBase64VarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) BytesHexVar(p *[]byte, name string, value []byte, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.BytesHexVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) BytesHexVarP(p *[]byte, name, shorthand string, value []byte, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.BytesHexVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) CountVar(p *int, name string, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.CountVar(p, name, usage) })
}

func (f FlagSet[Ctx]) CountVarP(p *int, name, shorthand string, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.CountVarP(p, name, shorthand, usage) })
}

func (f FlagSet[Ctx]) DurationSliceVar(p *[]time.Duration, name string, value []time.Duration, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.DurationSliceVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) DurationSliceVarP(p *[]time.Duration, name, shorthand string, value []time.Duration, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.DurationSliceVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) DurationVar(p *time.Duration, name string, value time.Duration, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.DurationVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) DurationVarP(p *time.Duration, name, shorthand string, value time.Duration, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.DurationVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Float32SliceVar(p *[]float32, name string, value []float32, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Float32SliceVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) Float32SliceVarP(p *[]float32, name, shorthand string, value []float32, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Float32SliceVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Float32Var(p *float32, name string, value float32, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Float32Var(p, name, value, usage) })
}

func (f FlagSet[Ctx]) Float32VarP(p *float32, name, shorthand string, value float32, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Float32VarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Float64SliceVar(p *[]float64, name string, value []float64, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Float64SliceVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) Float64SliceVarP(p *[]float64, name, shorthand string, value []float64, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Float64SliceVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Float64Var(p *float64, name string, value float64, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Float64Var(p, name, value, usage) })
}

func (f FlagSet[Ctx]) Float64VarP(p *float64, name, shorthand string, value float64, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Float64VarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) IPMaskVar(p *net.IPMask, name string, value net.IPMask, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.IPMaskVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) IPMaskVarP(p *net.IPMask, name, shorthand string, value net.IPMask, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.IPMaskVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) IPNetVar(p *net.IPNet, name string, value net.IPNet, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.IPNetVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) IPNetVarP(p *net.IPNet, name, shorthand string, value net.IPNet, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.IPNetVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) IPSliceVar(p *[]net.IP, name string, value []net.IP, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.IPSliceVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) IPSliceVarP(p *[]net.IP, name, shorthand string, value []net.IP, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.IPSliceVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) IPVar(p *net.IP, name string, value net.IP, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.IPVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) IPVarP(p *net.IP, name, shorthand string, value net.IP, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.IPVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Int16Var(p *int16, name string, value int16, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Int16Var(p, name, value, usage) })
}

func (f FlagSet[Ctx]) Int16VarP(p *int16, name, shorthand string, value int16, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Int16VarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Int32SliceVar(p *[]int32, name string, value []int32, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Int32SliceVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) Int32SliceVarP(p *[]int32, name, shorthand string, value []int32, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Int32SliceVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Int32Var(p *int32, name string, value int32, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Int32Var(p, name, value, usage) })
}

func (f FlagSet[Ctx]) Int32VarP(p *int32, name, shorthand string, value int32, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Int32VarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Int64SliceVar(p *[]int64, name string, value []int64, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Int64SliceVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) Int64SliceVarP(p *[]int64, name, shorthand string, value []int64, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Int64SliceVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Int64Var(p *int64, name string, value int64, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Int64Var(p, name, value, usage) })
}

func (f FlagSet[Ctx]) Int64VarP(p *int64, name, shorthand string, value int64, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Int64VarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Int8Var(p *int8, name string, value int8, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Int8Var(p, name, value, usage) })
}

func (f FlagSet[Ctx]) Int8VarP(p *int8, name, shorthand string, value int8, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Int8VarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) IntSliceVar(p *[]int, name string, value []int, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.IntSliceVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) IntSliceVarP(p *[]int, name, shorthand string, value []int, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.IntSliceVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) IntVar(p *int, name string, value int, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.IntVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) IntVarP(p *int, name, shorthand string, value int, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.IntVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) StringArrayVar(p *[]string, name string, value []string, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.StringArrayVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) StringArrayVarP(p *[]string, name, shorthand string, value []string, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.StringArrayVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) StringSliceVar(p *[]string, name string, value []string, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.StringSliceVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) StringSliceVarP(p *[]string, name, shorthand string, value []string, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.StringSliceVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) StringToInt64Var(p *map[string]int64, name string, value map[string]int64, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.StringToInt64Var(p, name, value, usage) })
}

func (f FlagSet[Ctx]) StringToInt64VarP(p *map[string]int64, name, shorthand string, value map[string]int64, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.StringToInt64VarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) StringToIntVar(p *map[string]int, name string, value map[string]int, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.StringToIntVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) StringToIntVarP(p *map[string]int, name, shorthand string, value map[string]int, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.StringToIntVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) StringToStringVar(p *map[string]string, name string, value map[string]string, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.StringToStringVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) StringToStringVarP(p *map[string]string, name, shorthand string, value map[string]string, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.StringToStringVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) StringVar(p *string, name string, value string, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.StringVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) StringVarP(p *string, name, shorthand string, value string, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.StringVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Uint16Var(p *uint16, name string, value uint16, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Uint16Var(p, name, value, usage) })
}

func (f FlagSet[Ctx]) Uint16VarP(p *uint16, name, shorthand string, value uint16, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Uint16VarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Uint32Var(p *uint32, name string, value uint32, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Uint32Var(p, name, value, usage) })
}

func (f FlagSet[Ctx]) Uint32VarP(p *uint32, name, shorthand string, value uint32, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Uint32VarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Uint64Var(p *uint64, name string, value uint64, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Uint64Var(p, name, value, usage) })
}

func (f FlagSet[Ctx]) Uint64VarP(p *uint64, name, shorthand string, value uint64, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Uint64VarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Uint8Var(p *uint8, name string, value uint8, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Uint8Var(p, name, value, usage) })
}

func (f FlagSet[Ctx]) Uint8VarP(p *uint8, name, shorthand string, value uint8, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Uint8VarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) UintSliceVar(p *[]uint, name string, value []uint, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.UintSliceVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) UintSliceVarP(p *[]uint, name, shorthand string, value []uint, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.UintSliceVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) UintVar(p *uint, name string, value uint, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.UintVar(p, name, value, usage) })
}

func (f FlagSet[Ctx]) UintVarP(p *uint, name, shorthand string, value uint, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.UintVarP(p, name, shorthand, value, usage) })
}

func (f FlagSet[Ctx]) Var(value pflag.Value, name string, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.Var(value, name, usage) })
}

func (f FlagSet[Ctx]) VarP(value pflag.Value, name, shorthand, usage string) {
	f.add(func(fs *pflag.FlagSet) { fs.VarP(value, name, shorthand, usage) })
}
