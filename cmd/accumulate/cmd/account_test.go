package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"math/big"
	"testing"

	"github.com/stretchr/testify/require"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	. "gitlab.com/accumulatenetwork/accumulate/test/helpers"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/daemon/services"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/testutils"
)

func init() {
	testMatrix.addTest(testCase1_1)
	testMatrix.addTest(testCase1_2)
	testMatrix.addTest(testCase3_1)
	testMatrix.addTest(testCase3_2)
	testMatrix.addTest(testCase3_3)
	testMatrix.addTest(testCase3_4)
	testMatrix.addTest(testCase3_5)
}

// testCase1_1 Generate 100 lite account addresses in cli
func testCase1_1(t *testing.T, tc *testCmd) {
	for i := 0; i < 100; i++ {
		r, err := tc.execute(t, "account generate")
		require.NoError(t, err)
		var out KeyResponse
		require.NoError(t, json.Unmarshal([]byte(r), &out))
		l, _ := services.LabelForLiteTokenAccount(liteAccounts[i])
		if string(out.Lite) != l {
			t.Fatalf("account generate error, expected %s, but got %s", liteAccounts[i], out.Lite)
		}
	}

	//now create a few FCT keys and test those against what is  expected.
	for i := range fctLiteAccounts {
		r, err := tc.execute(t, "account generate --sigtype rcd1")
		require.NoError(t, err)
		var out KeyResponse
		require.NoError(t, json.Unmarshal([]byte(r), &out))
		if out.LiteAccount.String() != fctLiteAccounts[i] {
			t.Fatalf("account generate error, expected %s, but got %s %v", fctLiteAccounts[i], out.LiteAccount, out.KeyInfo.Derivation)
		}
	}
}

// unitTest3_1
// Create ADI Token Account (URL), should pass
func testCase3_1(t *testing.T, tc *testCmd) {

	_, err := tc.executeTx(t, "account create token acc://RedWagon.acme --sign-with=red1 acc://RedWagon.acme/acct acc://acme")
	require.NoError(t, err)

}

// unitTest3_2
// Create ADI Token Account without parent ADI, should fail
func testCase3_2(t *testing.T, tc *testCmd) {

	_, err := tc.execute(t, "account create token acc://RedWagon.acme --sign-with=red1 acmeacct2 acc://acme")
	require.Error(t, err)

}

// unitTest3_3
// Create ADI Token Account with invalid token URL, should fail
func testCase3_3(t *testing.T, tc *testCmd) {

	_, err := tc.execute(t, "account create token acc://RedWagon.acme --sign-with=red1 acc://RedWagon.acme/acmeacct acc://factoid.acme")
	require.Error(t, err)

}

// unitTest3_4
// Credit amount with invalid lite address as sender, should fail
func testCase3_4(t *testing.T, tc *testCmd) {

	_, err := tc.execute(t, "accumulate credits acc://1a2d4a07f9cc525b43a63d8d89e32adca1194bc6e3bc4984 acc://ADIdoesntexist.acme 100")
	require.Error(t, err)

}

// unitTest3_5
// Send tokens to multiple receivers
func testCase3_5(t *testing.T, tc *testCmd) {
	//build up a list of the current balances, so we can confirm the deposits later
	balances := []string{}
	acctOffset := 25
	for i := 0; i < 10; i++ {
		bal, err := testGetBalance(t, tc, liteAccounts[acctOffset+i])
		require.NoError(t, err)
		balances = append(balances, bal)
	}

	cmd := "tx create " + liteAccounts[0]

	for i := range balances {
		cmd += fmt.Sprintf(" %s %f", liteAccounts[acctOffset+i], 0.01*float64(i))
	}
	_, err := tc.executeTx(t, cmd)
	require.NoError(t, err)

	//now confirm the balances
	for i := range balances {
		bal, err := testGetBalance(t, tc, liteAccounts[acctOffset+i])
		require.NoError(t, err)
		b, s := new(big.Int).SetString(bal, 10)
		require.True(t, s)
		f := big.NewInt(int64(protocol.AcmePrecision * 0.01 * float64(i)))
		require.Equal(t, balances[i], b.Sub(b, f).String())
	}
}

// unitTest1_2
// Create Lite Token Accounts based on RCD1-based factoid addresses
func testCase1_2(t *testing.T, tc *testCmd) {

	fs := "Fs1bhqin6z9rXBJDwnhJnRmGEs2NR4paFpsjzT4F8mgsK9t7MFug"
	fa := "FA3QCi34DugvxPAeRHBUmLFLAY8AS4qwFxCExL3Ae5jREpZsUroo"

	//quick check to make sure the factoid addresses are correct.
	fa2, rcdHash, _, err := protocol.GetFactoidAddressRcdHashPkeyFromPrivateFs(fs)
	require.NoError(t, err)
	_ = rcdHash
	require.Equal(t, fa, fa2)

	//quick protocol import check.
	r, err := executeCmd(tc,
		[]string{"-j", "key", "import", "factoid"},
		fmt.Sprintf("%v\n", fs))
	require.NoError(t, err)
	kr := KeyResponse{}
	require.NoError(t, json.Unmarshal([]byte(r), &kr))

	// make sure the right rcd account exists
	lt, err := protocol.GetLiteAccountFromFactoidAddress(fa)
	require.NoError(t, err)
	require.Equal(t, lt.String(), kr.LiteAccount.String())

	//now faucet the rcd account
	_, err = tc.executeTx(t, "faucet "+kr.LiteAccount.String())
	require.NoError(t, err)

	//now make sure rcd account has the funds
	bal, err := testGetBalance(t, tc, kr.LiteAccount.String())
	require.NoError(t, err)
	require.Equal(t, "1000000000", bal)

	_, err = tc.execute(t, "get "+kr.LiteAccount.String())
	require.NoError(t, err)

	_, err = tc.executeTx(t, "credits "+kr.LiteAccount.String()+" "+kr.LiteAccount.RootIdentity().String()+" 100")
	require.NoError(t, err)

	legacyAccount := KeyResponse{}
	r, err = tc.execute(t, "account generate")
	require.NoError(t, err)
	require.NoError(t, json.Unmarshal([]byte(r), &legacyAccount))

	//now transfer from an RCD based account to an ED25519 based account
	_, err = tc.executeTx(t, "tx create "+kr.LiteAccount.String()+" "+legacyAccount.LiteAccount.String()+" "+"1.00")
	require.NoError(t, err)

	//now make sure it transferred
	bal, err = testGetBalance(t, tc, legacyAccount.LiteAccount.String())
	require.NoError(t, err)
	require.Equal(t, "100000000", bal)
}

// testGetBalance helper function to get the balance of a token account
func testGetBalance(t *testing.T, tc *testCmd, accountUrl string) (string, error) {
	//now query the account to make sure each account has 10 acme.
	commandLine := fmt.Sprintf("account get %s", accountUrl)
	r, err := tc.execute(t, commandLine)
	if err != nil {
		return "", err
	}

	res := new(client.ChainQueryResponse)
	acc := new(protocol.LiteTokenAccount)
	res.Data = acc
	err = json.Unmarshal([]byte(r), &res)
	if err != nil {
		return "", err
	}

	return acc.Balance.String(), nil
}

func TestLockTokenAccount(t *testing.T) {
	// Setup
	tc := newTestCmd(t, testCmdOpts{})
	_, err := _wallet.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:       tc.dir,
		Token:      testutils.TestToken[:],
		MultiVault: false,
		Passphrase: "",
	})
	require.NoError(t, err)

	// Import the mnemonic
	_, err = _wallet.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	key := new(KeyResponse)
	out, err := tc.execute(t, "account generate")
	require.NoError(t, err)
	require.NoError(t, json.Unmarshal([]byte(out), key))
	lta := key.LiteAccount

	_, err = tc.executeTx(t, "faucet %s", lta)
	require.NoError(t, err)

	_, err = tc.executeTx(t, "credits %s %[1]s 10000", lta)
	require.NoError(t, err)

	_, err = executeCmd(tc, []string{"-j", "account", "lock", lta.String(), "10", "--wait=10s"}, "Y\n")
	require.NoError(t, err)

	// Fix weird test formatting issue
	fmt.Println()

	tokens := GetAccount[*protocol.LiteTokenAccount](t, tc.sim.DatabaseFor(lta), lta)
	require.NotZero(t, tokens.LockHeight, "Account must be locked")
}
