package cmd

import (
	"bytes"
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"reflect"
	"runtime"
	"sort"
	"strings"
	"testing"
	"time"

	"github.com/AccumulateNetwork/jsonrpc2/v15"
	"github.com/google/shlex"
	"github.com/spf13/cobra"
	"github.com/stretchr/testify/require"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/network"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/accumulate/test/harness"
	"gitlab.com/accumulatenetwork/accumulate/test/simulator"
	acctesting "gitlab.com/accumulatenetwork/accumulate/test/testing"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/util"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/interactive"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/jsonrpc"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/testutils"
	"golang.org/x/exp/slog"
)

func init() {
	acctesting.EnableDebugFeatures()
	jsonrpc2.DebugMethodFunc = true
}

type testCase func(t *testing.T, tc *testCmd)
type testMatrixTests []testCase

var testMatrix testMatrixTests

func bootstrap(t *testing.T, tc *testCmd) {
	_, err := _wallet.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:       tc.dir,
		Token:      testutils.TestToken[:],
		MultiVault: false,
		Passphrase: "",
	})
	require.NoError(t, err)

	// Import the mnemonic
	_, err = _wallet.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	// import eth private key.
	// res, err := tc.execute(t, "key import private 26b9b10aec1e75e68709689b446196a5235b26bb9d4c0fc91eaccc7d8b66ec16 ethKey --sigtype eth")
	res, err := executeCmd(tc,
		[]string{"-j", "key", "import", "private", "ethKey", "--sigtype", "eth"},
		"26b9b10aec1e75e68709689b446196a5235b26bb9d4c0fc91eaccc7d8b66ec16\n")
	require.NoError(t, err)
	var keyResponse KeyResponse
	err = json.Unmarshal([]byte(res), &keyResponse)
	require.NoError(t, err)

	//add the DN private key to our key list.
	_, err = executeCmd(tc,
		[]string{"-j", "key", "import", "private", "dnkey", "--sigtype", "ed25519"},
		fmt.Sprintf("%v\n", hex.EncodeToString(tc.privKey)))
	require.NoError(t, err)

	oracle := new(protocol.AcmeOracle)
	oracle.Price = 10_000 * protocol.AcmeOraclePrecision
	data, err := oracle.MarshalBinary()
	require.NoError(t, err)

	//set the oracle price to $10,000
	resp, err := tc.executeTx(t, "data write --write-state --wait 10s dn.acme/oracle --sign-with=dnkey %x", data)
	require.NoError(t, err)
	ar := new(ActionResponse)
	require.NoError(t, json.Unmarshal([]byte(resp), ar))
	for _, r := range ar.Flow {
		if r.Status.Error != nil {
			require.NoError(t, r.Status.Error)
		}
	}
}

func TestCli(t *testing.T) {
	testMatrix.execute(t, newTestCmd(t, testCmdOpts{doBootstrap: true}))
}

func GetFunctionName(i interface{}) string {
	return runtime.FuncForPC(reflect.ValueOf(i).Pointer()).Name()
}

func (tm *testMatrixTests) addTest(tc testCase) {
	*tm = append(*tm, tc)

}

func (tm *testMatrixTests) execute(t *testing.T, tc *testCmd) {

	// Sort by testCase number
	sort.SliceStable(*tm, func(i, j int) bool {
		return GetFunctionName((*tm)[i]) < GetFunctionName((*tm)[j])
	})

	//execute the tests
	var skip bool
	for _, f := range testMatrix {
		name := strings.Split(GetFunctionName(f), ".")
		ok := t.Run(name[len(name)-1], func(t *testing.T) {
			fmt.Println(name[len(name)-1])
			if skip {
				t.SkipNow()
			}
			f(t, tc)
		})
		if !ok {
			skip = true
		}
	}
}

type testCmd struct {
	sim     *harness.Sim
	step    chan chan struct{}
	privKey []byte
	dir     string
}

type testCmdOpts struct {
	doBootstrap bool
	dir         string
}

func newTestCmd(t *testing.T, opts testCmdOpts) *testCmd {
	t.Helper()

	c := new(testCmd)
	c.step = make(chan chan struct{})
	c.sim = newSim(t, c.step)
	c.privKey = c.sim.S.SignWithNode(protocol.Directory, 0).Key()

	if opts.dir == "" {
		c.dir = t.TempDir()
	} else {
		c.dir = opts.dir
	}

	jrpc, err := walletd.NewJrpc(walletd.Options{
		Logger:   slog.Default(),
		UseMemDB: opts.dir == "",
	})
	require.NoError(t, err)
	dt := new(jsonrpc.DefaultTransport)
	dt.Transport = util.DirectHttpTransport(jrpc.NewMux())
	_wallet = newWalletWithTransport(&jsonrpc.AutoWalletTransport{
		Transport: dt,
		SetWallet: func(_ any, v *string) { *v = c.dir },
		SetVault:  func(_ any, v *string) { *v = VaultName },
		SetToken:  func(_ any, v *[]byte) { *v = testutils.TestToken[:] },
	}, false)

	t.Cleanup(func() {
		_wallet = nil
	})

	if opts.doBootstrap {
		bootstrap(t, c)
	}
	return c
}

func newSim(t *testing.T, step chan chan struct{}) *harness.Sim {
	t.Helper()

	// Disable the sliding fee schedule
	values := new(network.GlobalValues)
	values.Globals = new(protocol.NetworkGlobals)
	values.Globals.FeeSchedule = new(protocol.FeeSchedule)
	values.Oracle = new(protocol.AcmeOracle)
	values.Oracle.Price = 10_000 * protocol.AcmeOraclePrecision
	values.ExecutorVersion = protocol.ExecutorVersionLatest

	// Initialize
	sim := harness.NewSim(t,
		simulator.SimpleNetwork("Simulator", 1, 1),
		simulator.GenesisWith(harness.GenesisTime, values),
	)

	// Step at 100 Hz
	tick := time.NewTicker(time.Second / 100)
	t.Cleanup(tick.Stop)
	go func() {
		ch := make(chan struct{})
		for {
			select {
			case <-tick.C:
				sim.Step()
			case step <- ch:
				sim.Step()
				close(ch)
				ch = make(chan struct{})
			}
		}
	}()

	return sim
}

func (c *testCmd) newSimClient() *client.Client {
	// Create a direct client with a hook
	return c.sim.S.NewDirectClientWithHook(func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			// Intercept API v2 requests
			if r.URL.Path != "/v2" {
				h.ServeHTTP(w, r)
				return
			}

			// Copy the body
			body, err := io.ReadAll(r.Body)
			require.NoError(c.sim.TB, err)
			r2 := *r
			r = &r2
			r.Body = io.NopCloser(bytes.NewBuffer(body))

			// Parse the request
			var req jsonrpc2.Request
			if err := json.Unmarshal(body, &req); err != nil {
				h.ServeHTTP(w, r)
				return
			}

			// Intercept query-tx requests
			if req.Method != "query-tx" {
				h.ServeHTTP(w, r)
				return
			}

			// Parse the parameters
			var params client.TxnQuery
			if err := json.Unmarshal(req.Params.(json.RawMessage), &params); err != nil {
				h.ServeHTTP(w, r)
				return
			}

			// Intercept wait requests
			if params.Wait == 0 {
				h.ServeHTTP(w, r)
				return
			}

			// Get the transaction ID
			id := params.TxIdUrl
			if id == nil {
				id = protocol.UnknownUrl().WithTxID(*(*[32]byte)(params.Txid))
			}

			// Step the simulator until the transaction exists
			for i := 0; i < 50; i++ {
				_, err := c.sim.Query().QueryTransaction(context.Background(), id, nil)
				if err == nil {
					break
				}
				<-<-c.step
			}

			// Reconstruct the request with wait=0 and resubmit it
			params.Wait = 0
			req.Params = &params
			body, err = json.Marshal(req)
			require.NoError(c.sim.TB, err)
			r.Body = io.NopCloser(bytes.NewBuffer(body))
			h.ServeHTTP(w, r)
		})
	})
}

func (c *testCmd) execute(t *testing.T, cmdLine string) (string, error) {
	return c.executeWithInput(t, cmdLine, "")
}

func (c *testCmd) executeWithInput(t *testing.T, cmdLine, input string) (string, error) {
	cmdLine = fmt.Sprintf("-j --non-interactive --server=sim://simulator %s", cmdLine)
	args, err := shlex.Split(cmdLine)
	require.NoError(t, err)
	return executeCmd(c, args, input)
}

func executeCmd(tc *testCmd, args []string, input string) (string, error) {
	// Reset flags
	Client = nil
	Q.Querier = nil
	ClientTimeout = 0
	ClientDebug = false
	WantJsonOutput = false
	Prove = false
	SigType = ""
	DaemonConnStr = ""
	VaultName = ""
	Entropy = 0
	WalletDir = tc.dir
	NonInteractive = false
	Server = ""

	flagAccount = struct {
		Lite     bool
		LiteData string
		Force    bool
	}{}

	TxWait = 0
	TxWaitSynth = 0
	TxIgnorePending = false
	TxLocal = false
	TxGetFor = nil

	DataSigningKeys = nil
	WriteState = false
	Scratch = false

	ctx := new(Context)
	cmd := rootCmd.Build(ctx)
	cmd.PersistentPreRunE = func(cmd *cobra.Command, args []string) error {
		err := ctx.rootPreRun(cmd, args)
		Client = tc.newSimClient()
		Client.Timeout = 1 * time.Hour
		ClientV3 = tc.sim.S.Services()
		interactive.PinEntryMode = "none"
		Q = tc.sim.Query()

		// The simulator's faucet implementation does not produce a real
		// transaction
		if cmd.Name() == "faucet" {
			TxWait = 0
		}

		// Never start the daemon as a separate process
		ctx.Set("spawn-daemon", false)
		return err
	}

	e := bytes.NewBufferString("")
	b := bytes.NewBufferString("")
	cmd.SetErr(e)
	cmd.SetOut(b)
	cmd.SetArgs(args)
	cmd.SetIn(strings.NewReader(input))
	err := cmd.Execute()
	if ctx.DidError != nil {
		return "", ctx.DidError
	}
	if err != nil {
		return "", err
	}

	ret, err := io.ReadAll(b)
	return string(ret), err
}

func (c *testCmd) executeTx(t *testing.T, cmdLine string, args ...interface{}) (string, error) {
	cmdLine = fmt.Sprintf(cmdLine, args...)
	return c.execute(t, "--wait 10s "+cmdLine)
}
