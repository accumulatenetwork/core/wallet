package cmd

import (
	"context"

	"github.com/spf13/cobra"
	url2 "gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
)

// faucetCmd represents the faucet command
var faucetCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "faucet [token account]",
		Short: "Get tokens from faucet",
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.Faucet(cmd, args[0])
		}),
	}
})

func (x *Context) Faucet(cmd *cobra.Command, url string) (string, error) {
	u, err := url2.Parse(url)
	if err != nil {
		return "", err
	}

	res, err := Client.Faucet(context.Background(), &protocol.AcmeFaucet{Url: u})
	if err != nil {
		return PrintJsonRpcError(err)
	}

	if TxWait != 0 {
		_, err = waitForTxnUsingHash(res.TransactionHash, TxWait, true)
		if err != nil {
			return PrintJsonRpcError(err)
		}
	}

	return ActionResponseFrom(res).Print(x)
}
