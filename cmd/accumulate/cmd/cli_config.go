package cmd

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd"
	"gitlab.com/accumulatenetwork/core/wallet/internal/flags"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/interactive"
	"golang.org/x/exp/slog"
)

type Context struct {
	viper.Viper
	gotConfig bool

	// Execution state
	DidError error

	// Flags for constructing a transaction
	Tx struct {
		Expires     flags.Time
		Authorities []*url.URL
		Memo        string
		Metadata    []byte
	}

	// Flags for submitting a transaction
	Submit struct {
		Suggest    bool
		Pretend    bool
		SkipReview bool // For `tx execute`
	}

	// Flags for constructing a signature
	Sig struct {
		Memo     string
		Metadata []byte
		Version  uint
	}

	// Flags for signing a transaction
	Sign struct {
		With []*url.URL
		For  *url.URL
	}

	// Flags for creating accounts
	Create struct {
		Authorities []*url.URL
	}
}

func (x *Context) SpawnDaemon() bool    { return x.GetBool("spawn-daemon") }
func (x *Context) DaemonSocket() string { return x.GetString("daemon-socket") }

func (x *Context) initConfig(cmd *cobra.Command) {
	x.Viper = *viper.New()
	x.SetConfigName("config")
	x.AddConfigPath(filepath.Join(currentUser.HomeDir, ".accumulate", "wallet"))
	x.SetConfigType("toml")

	x.initOptions(cmd)
	x.initOldFlags(cmd.PersistentFlags())
	x.readConfig()
}

func (x *Context) initOptions(cmd *cobra.Command) {
	cmd.PersistentFlags().Bool("spawn-daemon", false, "Spawn the daemon as a separate process")
	check(x.BindPFlag("spawn-daemon", cmd.PersistentFlags().Lookup("spawn-daemon")))
}

func (x *Context) submitFlags(cmd *cobra.Command) {
	cmd.PersistentFlags().BoolVar(&x.Submit.Suggest, "suggest", false, "Submit the transaction as a suggestion")
	cmd.PersistentFlags().Var(&x.Tx.Expires, "expires", "transaction expiration time")
	cmd.PersistentFlags().Var((*flags.UrlSlice)(&x.Tx.Authorities), "require", "additional authorities required to sign the transaction before it can be executed")
	cmd.PersistentFlags().BoolVarP(&x.Submit.Pretend, "pretend", "n", false, "Enables check-only mode for transactions")
	cmd.PersistentFlags().StringVarP(&x.Tx.Memo, "memo", "m", "", "Transaction memo")
	cmd.PersistentFlags().VarP((*flags.Bytes)(&x.Tx.Metadata), "metadata", "M", "Transaction metadata")

	cmd.PersistentFlags().Var((*signWithFlag)(&x.Sign.With), "sign-with", "Specifies a key and/or signer to sign the transaction with")
	cmd.PersistentFlags().Var(flags.Url{V: &x.Sign.For}, "sign-for", "Specifies the authority to sign on behalf of. Overrides the default behavior.")

	cmd.PersistentFlags().StringVar(&x.Sig.Memo, "signature-memo", "", "Signature memo")
	cmd.PersistentFlags().Var((*flags.Bytes)(&x.Sig.Metadata), "signature-metadata", "Signature metadata")
	cmd.PersistentFlags().UintVar(&x.Sig.Version, "signer-version", uint(0), "Specify the signer version. Overrides the default behavior of fetching the signer version.")

	// Renamed flags
	cmd.PersistentFlags().Var((*signWithFlag)(&x.Sign.With), "delegator", "Deprecated, use --sign-with")
}

func (x *Context) signFlags(cmd *cobra.Command) {
	cmd.PersistentFlags().Var((*signWithFlag)(&x.Sign.With), "with", "Specifies a key and/or signer to sign the transaction with")
	cmd.PersistentFlags().Var(flags.Url{V: &x.Sign.For}, "for", "Specifies the authority to sign on behalf of. Overrides the default behavior.")

	cmd.PersistentFlags().StringVarP(&x.Sig.Memo, "memo", "m", "", "Memo")
	cmd.PersistentFlags().VarP((*flags.Bytes)(&x.Sig.Metadata), "metadata", "M", "Transaction Metadata")
	cmd.PersistentFlags().UintVar(&x.Sig.Version, "signer-version", uint(0), "Specify the signer version. Overrides the default behavior of fetching the signer version.")

	// Renamed flags
	cmd.PersistentFlags().Var((*signWithFlag)(&x.Sign.With), "sign-with", "Deprecated, use --with")
	cmd.PersistentFlags().Var(flags.Url{V: &x.Sign.For}, "sign-for", "Deprecated, use --for")
}

func (x *Context) createFlags(cmd *cobra.Command) {
	cmd.PersistentFlags().Var((*flags.UrlSlice)(&x.Create.Authorities), "authority", "Additional authorities to add when creating an account")
}

func (x *Context) initOldFlags(flags *pflag.FlagSet) {
	Server = os.Getenv("ACC_API")
	if Server == "" {
		Server = "https://mainnet.accumulatenetwork.io/v2"
	}

	flags.StringVarP(&Server, "server", "s", Server, "Accumulated server")
	flags.DurationVarP(&ClientTimeout, "timeout", "t", 5*time.Second, "Timeout for all API requests (i.e. 10s, 1m)")
	flags.BoolVarP(&ClientDebug, "debug", "d", false, "Print accumulated API calls")
	flags.BoolVarP(&WantJsonOutput, "json", "j", false, "print outputs as json")
	flags.BoolVar(&Prove, "prove", false, "Request a receipt proving the transaction or account")
	flags.BoolVar(&TxIgnorePending, "ignore-pending", false, "Ignore pending transactions. Combined with --wait, this waits for transactions to be delivered.")
	flags.DurationVarP(&TxWait, "wait", "w", 0, "Wait for the transaction to complete")
	flags.StringVarP(&DaemonConnStr, "daemon", "D", "", "Specify the wallet daemon to connect to")
	flags.BoolVar(&NonInteractive, "non-interactive", false, "Disable interaction")

	var pinentryDefault string
	if runtime.GOOS != "linux" {
		pinentryDefault = "none"
	}

	flags.UintVar(&Entropy, "entropy", uint(128), "Specifies the size of the mnemonic entropy.")
	flags.StringVar(&WalletDir, "wallet", filepath.Join(currentUser.HomeDir, ".accumulate", "wallet"), "The wallet directory")
	flags.StringVar(&VaultName, "vault", "", "The vault to open")
	flags.BoolVar(&walletd.NoWalletVersionCheck, "no-wallet-version-check", false, "Bypass the check to prevent updating the wallet to the format supported by the cli")
	flags.StringVar(&interactive.PinEntryMode, "pinentry", pinentryDefault, "Override or disable pinentry ('disable' or 'none' to disable)")

	flags.BoolVar(new(bool), "use-unencrypted-wallet", false, "Use unencrypted wallet (strongly discouraged) stored at ~/.accumulate/wallet.db")
	check(flags.MarkDeprecated("use-unencrypted-wallet", "no longer has an effect (encryption is set when creating a wallet or vault)"))

	// Old --database flag
	flags.StringVar(new(string), "database", "", "Directory the database is stored in")
	check(flags.MarkDeprecated("database", fmt.Sprintf("migrate your vault with `%s vault migrate ...`", os.Args[0])))
}

func (x *Context) readConfig() {
	err := x.ReadInConfig()
	switch {
	case err == nil:
		x.gotConfig = true
	case errors.As(err, new(viper.ConfigFileNotFoundError)):
		// Ignore
	default:
		slog.Error("Unable to load CLI configuration", "error", err)
	}
}
