package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"math"
	"math/big"
	stdurl "net/url"
	"os"
	"strconv"
	"strings"
	"unicode"

	tmjson "github.com/cometbft/cometbft/libs/json"
	"github.com/cometbft/cometbft/privval"
	"github.com/spf13/cobra"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/address"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/daemon/services"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"gitlab.com/accumulatenetwork/core/wallet/internal/types"
)

func (x *Context) runCmdFunc(fn func(args []string) (string, error)) func(cmd *cobra.Command, args []string) {
	return func(cmd *cobra.Command, args []string) {
		out, err := fn(args)
		x.printOutput(cmd, out, err)
	}
}

func (x *Context) runCmdFunc2(fn func(cmd *cobra.Command, args []string) (string, error)) func(cmd *cobra.Command, args []string) {
	return func(cmd *cobra.Command, args []string) {
		out, err := fn(cmd, args)
		x.printOutput(cmd, out, err)
	}
}

func (x *Context) runTxnCmdFunc(fn func(cmd *cobra.Command, principal *url.URL, signers []*api.SignRequest, args []string) (string, error)) func(cmd *cobra.Command, args []string) {
	return x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
		principal, err := url.Parse(args[0])
		if err != nil {
			return "", err
		}

		signers, err := x.getSigners(cmd, principal, nil)
		if err != nil {
			return "", err
		}

		return fn(cmd, principal, signers, args[1:])
	})
}

func resolveAddress(s string) (address.Address, error) {
	if !strings.HasPrefix(s, "file:") {
		return address.Parse(s)
	}

	// If the address is prefixed with file:, assume it is a Tendermint private
	// validator key file
	b, err := os.ReadFile(s)
	if err != nil {
		return nil, err
	}

	var pvkey privval.FilePVKey
	err = tmjson.Unmarshal(b, &pvkey)
	if err != nil {
		return nil, err
	}

	if pvkey.PrivKey == nil {
		return nil, fmt.Errorf("invalid private key in %s", s)
	}
	// TODO Check the key type
	ret := new(services.Key)
	err = ret.InitializeFromSeed(pvkey.PrivKey.Bytes(), protocol.SignatureTypeED25519, "external")
	if err != nil {
		return nil, err
	}
	return ret, nil
}

func (x *Context) parseArgsAndPrepareSigner(cmd *cobra.Command, args []string) ([]string, *url.URL, []*api.SignRequest, error) {
	principal, err := url.Parse(args[0])
	if err != nil {
		return nil, nil, nil, err
	}

	signers, err := x.getSigners(cmd, principal, nil)
	if err != nil {
		return nil, nil, nil, err
	}

	return args, principal, signers, nil
}

func IsLiteTokenAccount(urlstr string) (bool, error) {
	u, err := url.Parse(strings.Trim(urlstr, " "))
	if err != nil {
		return false, err
	}
	if strings.Contains(u.Hostname(), ".") {
		return false, nil
	}
	key, _, err := protocol.ParseLiteTokenAddress(u)
	if err != nil {
		return false, fmt.Errorf("invalid lite token address : %s", u.String())
	}
	return key != nil, nil
}

func IsLiteIdentity(urlstr string) (bool, error) {
	u, err := url.Parse(strings.Trim(urlstr, " "))
	if err != nil {
		return false, err
	}
	if protocol.AcmeUrl().Equal(u) {
		return false, nil
	}
	if strings.Contains(u.Hostname(), ".") {
		return false, nil
	}
	key, err := protocol.ParseLiteIdentity(u)
	if err != nil {
		return false, fmt.Errorf("invalid lite identity : %s", u.String())
	}
	return key != nil, nil
}

// Remarshal uses mapstructure to convert a generic JSON-decoded map into a struct.
func Remarshal(src interface{}, dst interface{}) error {
	data, err := json.Marshal(src)
	if err != nil {
		return err
	}
	return json.Unmarshal(data, dst)
}

func RemarshalAs[T any](src any) (*T, error) {
	v := new(T)
	return v, Remarshal(src, v)
}

// This is a hack to reduce how much we have to change
type QueryResponse struct {
	Type           string                      `json:"type,omitempty"`
	MainChain      *client.MerkleState         `json:"mainChain,omitempty"`
	Data           interface{}                 `json:"data,omitempty"`
	ChainId        []byte                      `json:"chainId,omitempty"`
	Origin         string                      `json:"origin,omitempty"`
	KeyPage        *client.KeyPage             `json:"keyPage,omitempty"`
	Txid           []byte                      `json:"txid,omitempty"`
	Signatures     []protocol.Signature        `json:"signatures,omitempty"`
	Status         *protocol.TransactionStatus `json:"status,omitempty"`
	SyntheticTxids [][32]byte                  `json:"syntheticTxids,omitempty"`
}

func GetUrl(urlstr string) (*QueryResponse, error) {
	var res QueryResponse

	u, err := url.Parse(urlstr)
	if err != nil {
		return nil, err
	}
	params := client.UrlQuery{}
	params.Url = u

	err = queryAs("query", &params, &res)
	if err != nil {
		return nil, err
	}

	return &res, nil
}

func getAccount(url string) (protocol.Account, error) {
	qr, err := GetUrl(url)
	if err != nil {
		return nil, err
	}

	json, err := json.Marshal(qr.Data)
	if err != nil {
		return nil, err
	}

	return protocol.UnmarshalAccountJSON(json)
}

func queryAs(method string, input, output interface{}) error {
	err := Client.RequestAPIv2(context.Background(), method, input, output)
	if err == nil {
		return nil
	}

	_, err = PrintJsonRpcError(err)
	return err
}

func (x *Context) dispatchTxRequest(cmd *cobra.Command, payload interface{}, origin *url.URL, signers []*api.SignRequest) (*client.TxResponse, error) {
	// Convert the payload to an envelope
	env := new(messaging.Envelope)
	switch payload := payload.(type) {
	case *messaging.Envelope:
		env = payload

	case *protocol.Transaction:
		env.Transaction = []*protocol.Transaction{payload}

	case []byte:
		txn := x.buildTransaction(&protocol.RemoteTransaction{
			Hash: *(*[32]byte)(payload),
		}, origin)
		env.Transaction = []*protocol.Transaction{txn}

	case protocol.TransactionBody:
		txn := x.buildTransaction(payload, origin)
		env.Transaction = []*protocol.Transaction{txn}

	default:
		panic(fmt.Errorf("%T is not a supported payload type", payload))
	}

	// Resolve the transaction - check on the principal and every signer and delegator
	if remote, ok := env.Transaction[0].Body.(*protocol.RemoteTransaction); ok {
		r, err := Q.QueryTransaction(cmd.Context(), protocol.UnknownUrl().WithTxID(remote.Hash), nil)
		if err != nil {
			return nil, err
		}
		env.Transaction[0] = r.Message.Transaction
	}

	// Sign
	if len(env.Signatures) == 0 {
		for _, signer := range signers {
			signer.Transaction = env.Transaction[0]
			res, err := x.getWallet().Sign(cmd.Context(), signer)
			if err != nil {
				return nil, err
			}
			env.Transaction[0] = res.Transaction
			env.Signatures = append(env.Signatures, res.Signature)
		}
	}

	req := new(client.ExecuteRequest)
	req.Envelope = env
	if x.Submit.Pretend {
		req.CheckOnly = true
	}

	res, err := Client.ExecuteDirect(context.Background(), req)
	if err != nil {
		_, err := PrintJsonRpcError(err)
		return nil, err
	}
	if res.Code != 0 {
		result := new(protocol.TransactionStatus)
		if Remarshal(res.Result, result) != nil {
			return nil, errors.EncodingError.With(res.Message)
		}
		return nil, result.Error
	}

	return res, nil
}

func (x *Context) dispatchTxAndWait(cmd *cobra.Command, payload interface{}, origin *url.URL, signers []*api.SignRequest) (*client.TxResponse, []*client.TransactionQueryResponse, error) {
	res, err := x.dispatchTxRequest(cmd, payload, origin, signers)
	if err != nil {
		return nil, nil, err
	}

	if TxWait == 0 {
		return res, nil, nil
	}

	resps, err := waitForTxnUsingHash(res.TransactionHash, TxWait, TxIgnorePending)
	if err != nil {
		return nil, nil, err
	}

	return res, resps, nil
}

func (x *Context) dispatchTxAndPrintResponse(cmd *cobra.Command, payload interface{}, origin *url.URL, signers []*api.SignRequest) (string, error) {
	res, resps, err := x.dispatchTxAndWait(cmd, payload, origin, signers)
	if err != nil {
		return PrintJsonRpcError(err)
	}

	result, err := ActionResponseFrom(res).Print(x)
	if err != nil {
		return "", err
	}
	if res.Code == 0 {
		for _, response := range resps {
			str, err := x.PrintTransactionQueryResponseV2(response)
			if err != nil {
				return PrintJsonRpcError(err)
			}
			result = fmt.Sprint(result, "\n", str)
		}
	}
	return result, nil
}

func (x *Context) buildTransaction(payload protocol.TransactionBody, origin *url.URL) *protocol.Transaction {
	txn := new(protocol.Transaction)
	txn.Body = payload
	txn.Header.Principal = origin
	txn.Header.Memo = x.Tx.Memo
	txn.Header.Metadata = x.Tx.Metadata
	txn.Header.Authorities = x.Tx.Authorities

	if x.Tx.Expires.Value != nil {
		txn.Header.Expire = new(protocol.ExpireOptions)
		txn.Header.Expire.AtTime = x.Tx.Expires.Value
	}

	return txn
}

type ActionResponse struct {
	TransactionHash types.Bytes                        `json:"transactionHash"`
	SignatureHashes []types.Bytes                      `json:"signatureHashes"`
	SimpleHash      types.Bytes                        `json:"simpleHash"`
	Log             types.String                       `json:"log"`
	Code            types.String                       `json:"code"`
	Codespace       types.String                       `json:"codespace"`
	Error           types.String                       `json:"error"`
	Mempool         types.String                       `json:"mempool"`
	Result          *protocol.TransactionStatus        `json:"result"`
	Flow            []*client.TransactionQueryResponse `json:"flow"`
}

type ActionDataResponse struct {
	EntryHash types.Bytes32 `json:"entryHash"`
	ActionResponse
}

type ActionLiteDataResponse struct {
	AccountUrl types.String  `json:"accountUrl"`
	AccountId  types.Bytes32 `json:"accountId"`
	ActionDataResponse
}

func ActionResponseFromLiteData(r *client.TxResponse, accountUrl string, accountId []byte, entryHash []byte) *ActionLiteDataResponse {
	ar := &ActionLiteDataResponse{}
	ar.AccountUrl = types.String(accountUrl)
	_ = ar.AccountId.FromBytes(accountId)
	ar.ActionDataResponse = *ActionResponseFromData(r, entryHash)
	return ar
}

func ActionResponseFromData(r *client.TxResponse, entryHash []byte) *ActionDataResponse {
	ar := &ActionDataResponse{}
	_ = ar.EntryHash.FromBytes(entryHash)
	ar.ActionResponse = *ActionResponseFrom(r)
	return ar
}

func ActionResponseFrom(r *client.TxResponse) *ActionResponse {
	ar := &ActionResponse{
		TransactionHash: r.TransactionHash,
		SignatureHashes: make([]types.Bytes, len(r.SignatureHashes)),
		SimpleHash:      r.SimpleHash,
		Error:           types.String(r.Message),
		Code:            types.String(fmt.Sprint(r.Code)),
	}
	for i, hash := range r.SignatureHashes {
		ar.SignatureHashes[i] = hash
	}

	result := new(protocol.TransactionStatus)
	if Remarshal(r.Result, result) != nil {
		return ar
	}

	if result.Failed() {
		ar.Code = types.String(fmt.Sprint(result.CodeNum()))
	}
	if result.Error != nil {
		ar.Error = types.String(result.Error.Message)
	}
	ar.Result = result
	return ar
}

type RpcError struct {
	Msg string
	Err error
}

func (e *RpcError) Unwrap() error { return e.Err }
func (e *RpcError) Error() string { return e.Msg }

var (
	ApiToString = map[protocol.AccountType]string{
		protocol.AccountTypeLiteTokenAccount: "Lite Account",
		protocol.AccountTypeTokenAccount:     "ADI Token Account",
		protocol.AccountTypeIdentity:         "ADI",
		protocol.AccountTypeKeyBook:          "Key Book",
		protocol.AccountTypeKeyPage:          "Key Page",
		protocol.AccountTypeDataAccount:      "Data Chain",
		protocol.AccountTypeLiteDataAccount:  "Lite Data Chain",
	}
)

func amountToBigInt(tokenUrl string, amount string) (*big.Int, error) {
	//query the token
	qr, err := GetUrl(tokenUrl)
	if err != nil {
		return nil, fmt.Errorf("error retrieving token url, %v", err)
	}
	t := protocol.TokenIssuer{}
	err = Remarshal(qr.Data, &t)
	if err != nil {
		return nil, err
	}

	return parseAmount(amount, t.Precision)
}

func parseAmount(amount string, precision uint64) (*big.Int, error) {
	amt, _ := big.NewFloat(0).SetPrec(128).SetString(amount)
	if amt == nil {
		return nil, fmt.Errorf("invalid amount %s", amount)
	}

	oneToken := big.NewFloat(math.Pow(10.0, float64(precision))) //Convert to fixed point; multiply by the precision
	amt.Mul(amt, oneToken)                                       // Note that we are using floating point here.  Precision can be lost
	round := big.NewFloat(.9)                                    // To adjust for lost precision, round to the nearest int
	if amt.Sign() < 0 {                                          // Just to be safe, account for negative numbers
		round = big.NewFloat(-.9)
	}
	amt.Add(amt, round)               //                              Round up (positive) or down (negative) to the lowest int
	iAmt, _ := amt.Int(big.NewInt(0)) //                              Then convert to a big Int
	return iAmt, nil                  //                              Return the int
}

func GetTokenUrlFromAccount(u *url.URL) (*url.URL, error) {
	var err error
	var tokenUrl *url.URL
	isLiteTokenAccount, err := IsLiteTokenAccount(u.String())
	if err != nil {
		return nil, err
	}
	if isLiteTokenAccount {
		_, tokenUrl, err = protocol.ParseLiteTokenAddress(u)
		if err != nil {
			return nil, fmt.Errorf("cannot extract token url from lite token account, %v", err)
		}
	} else {
		res, err := GetUrl(u.String())
		if err != nil {
			return nil, err
		}
		if res.Type != protocol.AccountTypeTokenAccount.String() {
			return nil, fmt.Errorf("expecting token account but received %s", res.Type)
		}
		ta := protocol.TokenAccount{}
		err = Remarshal(res.Data, &ta)
		if err != nil {
			return nil, fmt.Errorf("error remarshaling token account, %v", err)
		}
		tokenUrl = ta.TokenUrl
	}
	if tokenUrl == nil {
		return nil, fmt.Errorf("invalid token url was obtained from %s", u.String())
	}
	return tokenUrl, nil
}
func amountToString(precision uint64, amount *big.Int) string {
	bf := big.Float{}
	bd := big.Float{}
	bd.SetFloat64(math.Pow(10.0, float64(precision)))
	bf.SetInt(amount)
	bal := big.Float{}
	bal.Quo(&bf, &bd)
	return bal.Text('f', int(precision))
}

func formatAmount(tokenUrl string, amount *big.Int) (string, error) {
	//query the token
	tokenData, err := GetUrl(tokenUrl)
	if err != nil {
		return "", fmt.Errorf("error retrieving token url, %v", err)
	}
	t := protocol.TokenIssuer{}
	err = Remarshal(tokenData.Data, &t)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("%s %s", amountToString(t.Precision, amount), t.Symbol), nil
}

func natural(name string) string {
	var splits []int

	var wasLower bool
	for i, r := range name {
		if wasLower && unicode.IsUpper(r) {
			splits = append(splits, i)
		}
		wasLower = unicode.IsLower(r)
	}

	w := new(strings.Builder)
	w.Grow(len(name) + len(splits))

	var word string
	var split int
	var offset int
	for len(splits) > 0 {
		split, splits = splits[0], splits[1:]
		split -= offset
		offset += split
		word, name = name[:split], name[split:]
		w.WriteString(word)
		w.WriteRune(' ')
	}

	w.WriteString(name)
	return w.String()
}

func QueryAcmeOracle() (*protocol.AcmeOracle, error) {
	resp, err := Client.Describe(context.Background())
	if err != nil {
		return nil, err
	}

	return resp.Values.Oracle, err
}

func ValidateSigType(input string) (protocol.SignatureType, error) {
	sigtype, ok := protocol.SignatureTypeByName(input)
	if !ok || sigtype == protocol.SignatureTypeLegacyED25519 {
		sigtype = protocol.SignatureTypeED25519
	}
	return sigtype, nil
}

func routeAccount(account *url.URL) (partition, server string, err error) {
	desc, err := Client.Describe(context.Background())
	if err != nil {
		return "", "", errors.BadRequest.WithFormat("query routing table: %w", err)
	}

	table := desc.Values.Routing
	rn := account.Routing()

	// Check for an override
	for _, o := range table.Overrides {
		if o.Account.Equal(account) {
			partition = o.Partition
			goto server
		}
	}

	// Find the matching route
	for _, route := range table.Routes {
		// Compare the top N bits of the routing number to the
		if rn>>(64-route.Length) == route.Value {
			partition = route.Partition
			goto server
		}
	}

	return "", "", errors.Conflict.WithFormat("no route matches %v", account)

server:
	for _, p := range desc.Network.Partitions {
		if strings.EqualFold(partition, p.Id) {
			if len(p.Nodes) == 0 {
				return partition, "", nil
			}
			server = p.Nodes[0].Address
			break
		}
	}
	if server == "" {
		return partition, "", nil
	}

	u, err := stdurl.Parse(server)
	if err != nil {
		return "", "", errors.Conflict.WithFormat("network returned invalid server URL: %w", err)
	}

	port, err := strconv.ParseUint(u.Port(), 10, 32)
	if err != nil {
		return "", "", errors.Conflict.WithFormat("network returned invalid server URL: %w", err)
	}

	server = fmt.Sprintf("%s://%s:%d", u.Scheme, u.Hostname(), port+4)
	return partition, server, nil
}

func fatalf(format string, args ...interface{}) {
	fmt.Fprintf(os.Stderr, "Error: "+format+"\n", args...)
	os.Exit(1)
}

func check(err error) {
	if err != nil {
		fatalf("%v", err)
	}
}

func checkf(err error, format string, otherArgs ...interface{}) {
	if err != nil {
		fatalf(format+": %v", append(otherArgs, err)...)
	}
}

func must[V any](v V, err error) V {
	check(err)
	return v
}
