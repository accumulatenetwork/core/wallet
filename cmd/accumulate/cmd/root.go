package cmd

import (
	"errors"
	"fmt"
	"io/fs"
	stdurl "net/url"
	"os"
	"os/user"
	"path/filepath"
	"time"

	"github.com/AccumulateNetwork/jsonrpc2/v15"
	"github.com/fatih/color"
	"github.com/spf13/cobra"
	v3 "gitlab.com/accumulatenetwork/accumulate/pkg/api/v3"
	v3jrpc "gitlab.com/accumulatenetwork/accumulate/pkg/api/v3/jsonrpc"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/interactive"
)

type clientV3 interface {
	// v3.NodeService
	v3.ConsensusService
	v3.NetworkService
	v3.MetricsService
	v3.Querier
	// v3.EventService
	v3.Submitter
	v3.Validator
	v3.Faucet
}

var (
	NonInteractive bool
	WantJsonOutput bool

	DaemonConnStr string
	VaultName     string
	WalletDir     string
	Server        string

	Client        *client.Client
	ClientV3      clientV3
	ClientTimeout time.Duration
	ClientDebug   bool
	Q             v3.Querier2

	Prove   bool
	SigType string
	Entropy uint

	TxWait          time.Duration
	TxWaitSynth     time.Duration
	TxIgnorePending bool
	TxLocal         bool
	TxGetFor        *url.URL
)

var currentUser = func() *user.User {
	usr, err := user.Current()
	check(err)
	return usr
}()

func Execute() {
	ctx := new(Context)
	cmd := rootCmd.Build(ctx)

	cmd.PersistentPostRun = func(*cobra.Command, []string) {
		// Exit 1 if there is an error
		if ctx.DidError != nil {
			os.Exit(1)
		}
	}

	cobra.CheckErr(cmd.Execute())
}

var rootCmd = factory.New[*Context](func(ctx *Context) *cobra.Command {
	cmd := &cobra.Command{
		Use:   "accumulate",
		Short: "CLI for Accumulate Network",

		PersistentPreRunE: ctx.rootPreRun,
	}

	cmd.SetOut(os.Stdout)
	ctx.initConfig(cmd)

	return cmd
})

func init() {
	cmd := rootCmd

	// Add the commands
	cmd.AddCommand(initCmd)
	cmd.AddCommand(encryptCmd)
	cmd.AddCommand(accountCmd)
	cmd.AddCommand(adiCmd)
	cmd.AddCommand(configCmd)
	cmd.AddCommand(authCmd)
	cmd.AddCommand(bookCmd)
	cmd.AddCommand(creditsCmd)
	cmd.AddCommand(dataCmd)
	cmd.AddCommand(getCmd)
	cmd.AddCommand(keyCmd)
	cmd.AddCommand(oracleCmd)
	cmd.AddCommand(pageCmd)
	cmd.AddCommand(tokenCmd)
	cmd.AddCommand(txCmd)
	cmd.AddCommand(blocksCmd)
	cmd.AddCommand(operatorCmd, validatorCmd)
	cmd.AddCommand(followerCmd)
	cmd.AddCommand(versionCmd, describeCmd)
	cmd.AddCommand(walletCmd)
	cmd.AddCommand(ledgerCmd)
	cmd.AddCommand(resubmitCmd)
	cmd.AddCommand(stakingCmd)
	cmd.AddCommand(faucetCmd)
}

func (x *Context) rootPreRun(cmd *cobra.Command, _ []string) error {
	err := x.manageOldDb(cmd)
	if err != nil {
		return err
	}

	if NonInteractive && !cmd.Flag("pinentry").Changed {
		interactive.PinEntryMode = "none"
	}

	switch Server {
	case "local":
		Server = "http://127.0.1.1:26660"
	case "localhost":
		Server = "http://127.0.0.1:26660"
	case "devnet":
		Server = "https://devnet.accumulatenetwork.io"
	case "testnet", "kermit":
		Server = "https://kermit.accumulatenetwork.io"
	case "fozzie":
		Server = "https://fozzie.accumulatenetwork.io"
	case "mainnet":
		Server = "https://mainnet.accumulatenetwork.io"
	}
	if u, err := stdurl.Parse(Server); err != nil {
		return fmt.Errorf("invalid endpoint: %w", err)
	} else {
		switch u.Path {
		case "":
		case "/", "/v2", "/v3":
			u.Path = ""
			Server = u.String()
		default:
			return fmt.Errorf("invalid endpoint %q", Server)
		}
	}

	Client, err = client.New(Server + "/v2")
	if err != nil {
		return fmt.Errorf("failed to create client: %v", err)
	}
	Client.Timeout = ClientTimeout
	Client.DebugRequest = ClientDebug

	v3jrpc := v3jrpc.NewClient(Server + "/v3")
	v3jrpc.Client.Timeout = ClientTimeout
	ClientV3 = v3jrpc
	Q.Querier = v3jrpc

	if ClientDebug {
		jsonrpc2.DebugMethodFunc = true
	}

	return nil
}

func (x *Context) manageOldDb(cmd *cobra.Command) error {
	// Block the --database flag
	if cmd.Flags().Changed("database") {
		os.Exit(1)
	}

	var oldPath = filepath.Join(currentUser.HomeDir, ".accumulate")
	if cmd.Flags().Changed("wallet") {
		oldPath = WalletDir
	}

	// Does a new wallet exist?
	res, err := x.getWallet().Status(cmd.Context(), &api.StatusRequest{})
	if err != nil {
		return err
	}
	if res.Wallet.Exists {
		return nil
	}

	// Migrate the default wallet
	old, err := findOldDb(oldPath)
	if err != nil {
		return err
	}
	if old == "" {
		return nil // Nothing to move
	}

	rel, err := filepath.Rel(currentUser.HomeDir, old)
	if err != nil {
		rel = old
	}
	fmt.Fprint(os.Stderr, color.CyanString("Migrating ~/%s\n", rel))

	// Make the containing directory
	err = os.MkdirAll(WalletDir, 0700)
	if err != nil {
		return err
	}

	// Rename
	return os.Rename(old, filepath.Join(WalletDir, "index.db"))
}

func findDb(base string) (string, error) {
	// Check the new location
	file := filepath.Join(base, "index.db")
	_, err := os.Stat(file)
	switch {
	case err == nil:
		return file, nil
	case !errors.Is(err, fs.ErrNotExist):
		return "", err
	}

	// Check the old locations
	return findOldDb(base)
}

func findOldDb(base string) (string, error) {
	// Check unencrypted
	file := filepath.Join(base, "wallet_encrypted.db")
	_, err := os.Stat(file)
	switch {
	case err == nil:
		return file, nil
	case !errors.Is(err, fs.ErrNotExist):
		return "", err
	}

	// Check unencrypted
	file = filepath.Join(base, "wallet.db")
	_, err = os.Stat(file)
	switch {
	case err == nil:
		return file, nil
	case !errors.Is(err, fs.ErrNotExist):
		return "", err
	}

	return "", nil
}
