package cmd

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
)

var DataSigningKeys []string
var WriteState bool
var Scratch bool

func init() {
	dataCmd.AddCommand(dataGetCmd, dataWriteCmd, dataWriteToCmd)

	dataCmd.PersistentFlags().StringSliceVar(&DataSigningKeys, "sign-data", nil, "specify this to send random data as a signed & valid entry to data account")
	dataWriteCmd.PersistentFlags().BoolVar(&WriteState, "write-state", false, "Write to the account's state")
	dataCmd.PersistentFlags().BoolVar(&Scratch, "scratch", false, "Write to the scratch chain")

	accountCreateDataCmd.Flags().BoolVar(&flagAccount.Lite, "lite", false, "Create a lite data account")
	accountCreateDataCmd.Flags().StringVar(&flagAccount.LiteData, "lite-data", "", "Add first entry data to lite data account")
}

var dataCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "data",
		Short: "Create, add, and query adi data accounts",
	}
})

var dataGetCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "get",
		Short: "Query data accounts",
		Example: "" +
			"  accumulate data get [DataAccountURL]			  Get most current data entry by URL\n" +
			"  accumulate data get [DataAccountURL] [EntryHash]  Get data entry by entryHash in hex\n" +
			"  accumulate data get [DataAccountURL] [start index] [count] expand(optional) Get a set of data entries starting from start and going to start+count, if \"expand\" is specified, data entries will also be provided\n" +
			"",
		Args: cobra.RangeArgs(1, 4),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			switch len(args) {
			case 1:
				return x.GetDataEntry(cmd, args[0], args[1:])
			case 2:
				return x.GetDataEntry(cmd, args[0], args[1:])
			default:
				return x.GetDataEntrySet(cmd, args[0], args[1:])
			}
		}),
	}
})

var dataWriteCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "write [data account url] [entry 0 (optional)] ... [entry n (optional)]",
		Short: "Write to a data account",
		Args:  cobra.MinimumNArgs(2),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.WriteData(cmd, args[0], args[1:])
		}),
	}
}).With((*Context).submitFlags)

var dataWriteToCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "write-to [writer account] [lite data account] [extid_0 (optional)] ... [extid_n (optional)] [data]",
		Short: "Write to a lite data account with another account",
		Args:  cobra.MinimumNArgs(2),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.WriteDataTo(cmd, args[0], args[1:])
		}),
	}
}).With((*Context).submitFlags)

var accountCreateDataCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "data",
		Short: "Create a data account",
		Example: "" +
			"  accumulate account create data [adi] [new data account]     Create new ADI data account\n" +
			"  accumulate account create data --lite [actor account url] [extid_0 (optional)] ... [extid_n (optional)]    Create a new lite data account\n" +
			"",
		Args: cobra.MinimumNArgs(1),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			if flagAccount.Lite {
				return x.CreateLiteDataAccount(cmd, args[0], args[1:])
			} else if len(args) != 2 {
				_ = cmd.Usage()
				return "", errors.New("usage")
			} else {
				return x.CreateDataAccount(cmd, args[0], args[1:])
			}
		}),
	}
}).
	With((*Context).submitFlags).
	With((*Context).createFlags)

func (x *Context) GetDataEntry(cmd *cobra.Command, accountUrl string, args []string) (string, error) {
	u, err := url.Parse(accountUrl)
	if err != nil {
		return "", err
	}

	params := client.DataEntryQuery{}
	params.Url = u
	if len(args) > 0 {
		n, err := hex.Decode(params.EntryHash[:], []byte(args[0]))
		if err != nil {
			return "", err
		}
		if n != 32 {
			return "", fmt.Errorf("entry hash must be 64 hex characters in length")
		}
	}

	var res QueryResponse

	data, err := json.Marshal(&params)
	if err != nil {
		return "", err
	}

	err = Client.RequestAPIv2(context.Background(), "query-data", json.RawMessage(data), &res)
	if err != nil {
		return "", err
	}

	return x.PrintChainQueryResponseV2(cmd, &res)
}

func (x *Context) GetDataEntrySet(cmd *cobra.Command, accountUrl string, args []string) (string, error) {
	u, err := url.Parse(accountUrl)
	if err != nil {
		return "", err
	}

	if len(args) > 3 || len(args) < 2 {
		return "", fmt.Errorf("expecting the start index and count parameters with optional expand")
	}

	params := client.DataEntrySetQuery{}
	params.Url = u

	v, err := strconv.ParseInt(args[0], 10, 64)
	if err != nil {
		return "", fmt.Errorf("invalid start argument %s, %v", args[1], err)
	}
	params.Start = uint64(v)

	v, err = strconv.ParseInt(args[1], 10, 64)
	if err != nil {
		return "", fmt.Errorf("invalid count argument %s, %v", args[1], err)
	}
	params.Count = uint64(v)

	if len(args) > 2 {
		if args[2] == "expand" {
			params.Expand = true
		}
	}

	var res client.MultiResponse
	data, err := json.Marshal(&params)
	if err != nil {
		return "", err
	}

	err = Client.RequestAPIv2(context.Background(), "query-data-set", json.RawMessage(data), &res)
	if err != nil {
		return "", err
	}

	return x.PrintMultiResponse(cmd, &res)
}

func (x *Context) CreateLiteDataAccount(cmd *cobra.Command, origin string, args []string) (string, error) {
	u, err := url.Parse(origin)
	if err != nil {
		return "", err
	}

	signers, err := x.getSigners(cmd, u, nil)
	if err != nil {
		return "", fmt.Errorf("unable to prepare signers, %v", err)
	}

	if len(args) < 1 {
		return "", fmt.Errorf("expecting account url or 'lite' keyword")
	}

	var res *client.TxResponse
	//compute the chain id...
	wdt := protocol.WriteDataTo{}

	wdt.Entry, err = x.prepareData(cmd, u, args, true)
	if err != nil {
		return "", err
	}

	accountId := protocol.ComputeLiteDataAccountId(wdt.Entry)
	addr, err := protocol.LiteDataAddress(accountId)
	if err != nil {
		return "", fmt.Errorf("invalid lite data address created from name(s)")
	}
	wdt.Recipient = addr

	lite, _ := GetUrl(wdt.Recipient.String())
	if lite != nil {
		return "", fmt.Errorf("lite data address already exists %s", addr)
	}

	lde := protocol.FactomDataEntry{}
	copy(lde.AccountId[:], accountId)
	data := wdt.Entry.GetData()
	if len(data) > 0 {
		lde.Data = data[0]
		lde.ExtIds = data[1:]
	}
	entryHash := lde.Hash()
	res, resps, err := x.dispatchTxAndWait(cmd, &wdt, u, signers)
	if err != nil {
		return PrintJsonRpcError(err)
	}
	ar := ActionResponseFromLiteData(res, addr.String(), accountId, entryHash)
	ar.Flow = resps
	return ar.Print(x)
}

func (x *Context) CreateDataAccount(cmd *cobra.Command, origin string, args []string) (string, error) {
	u, err := url.Parse(origin)
	if err != nil {
		return "", err
	}

	signers, err := x.getSigners(cmd, u, nil)
	if err != nil {
		return "", fmt.Errorf("unable to prepare signers, %v", err)
	}

	if len(args) < 1 {
		return "", fmt.Errorf("expecting account url or 'lite' keyword")
	}

	accountUrl, err := url.Parse(args[0])
	if err != nil {
		return "", fmt.Errorf("invalid account url %s", args[0])
	}
	if u.Authority != accountUrl.Authority {
		return "", fmt.Errorf("account url to create (%s) doesn't match the authority adi (%s)", accountUrl.Authority, u.Authority)
	}

	cda := protocol.CreateDataAccount{}
	cda.Url = accountUrl
	cda.Authorities = x.Create.Authorities

	return x.dispatchTxAndPrintResponse(cmd, &cda, u, signers)
}

func (x *Context) WriteData(cmd *cobra.Command, accountUrl string, args []string) (string, error) {
	u, err := url.Parse(accountUrl)
	if err != nil {
		return "", err
	}

	signers, err := x.getSigners(cmd, u, nil)
	if err != nil {
		return "", err
	}

	if len(args) < 1 {
		return "", fmt.Errorf("expecting account url")
	}
	wd := protocol.WriteData{}
	wd.WriteToState = WriteState
	wd.Scratch = Scratch

	wd.Entry, err = x.prepareData(cmd, u, args, false)
	if err != nil {
		return PrintJsonRpcError(err)
	}
	res, resps, err := x.dispatchTxAndWait(cmd, &wd, u, signers)
	if err != nil {
		return "", err
	}
	ar := ActionResponseFromData(res, wd.Entry.Hash())
	ar.Flow = resps
	return ar.Print(x)
}

func (x *Context) prepareData(cmd *cobra.Command, principal *url.URL, args []string, isFirstLiteEntry bool) (*protocol.DoubleHashDataEntry, error) {
	var signers []*api.SignRequest
	for _, s := range DataSigningKeys {
		// Determine if the signer is a label, account, or label @ account
		label, account := parseLabelAndOrAccount(s)

		// If the user specified an account, find a signer
		if account != nil {
			paths, err := x.findSignersForAccount(cmd, account, label, nil, nil)
			if err != nil {
				return nil, err
			}
			signer, err := buildSigningRequestForPaths(cmd, paths, false)
			if err != nil {
				return nil, err
			}
			signers = append(signers, signer)
			continue
		}

		// If the user did not specify an account, assume they want to sign with
		// an lite identity
		r, err := x.getWallet().ResolveKey(cmd.Context(), &api.ResolveKeyRequest{Value: label})
		if err != nil {
			return nil, err
		}
		lite := protocol.LiteAuthorityForKey(r.Key.PublicKey, r.Key.KeyInfo.Type)
		signer, err := buildSigningRequest(cmd, lite, r.Key, false)
		if err != nil {
			return nil, err
		}
		signers = append(signers, signer)
	}

	entry := new(protocol.DoubleHashDataEntry)
	if isFirstLiteEntry {
		data := []byte{}
		if flagAccount.LiteData != "" {
			n, err := hex.Decode(data, []byte(flagAccount.LiteData))
			if err != nil {
				//if it is not a hex string, then just store the data as-is
				copy(data, flagAccount.LiteData)
			} else {
				//clip the padding
				data = data[:n]
			}
		}
		entry.Data = append(entry.Data, data)
	}
	for i := 0; i < len(args); i++ {
		data := make([]byte, len(args[i]))

		//attempt to hex decode it
		n, err := hex.Decode(data, []byte(args[i]))
		if err != nil {
			//if it is not a hex string, then just store the data as-is
			copy(data, args[i])
		} else {
			//clip the padding
			data = data[:n]
		}

		entry.Data = append(entry.Data, data)
	}

	if len(signers) > 0 {
		dataHash := entry.Hash()
		var signatures [][]byte
		for _, signer := range signers {
			signer.Transaction = new(protocol.Transaction)
			signer.Transaction.Body = &protocol.RemoteTransaction{Hash: *(*[32]byte)(dataHash)}
			res, err := x.getWallet().Sign(cmd.Context(), signer)
			if err != nil {
				return nil, err
			}
			b, err := res.Signature.MarshalBinary()
			if err != nil {
				return nil, err
			}
			signatures = append(signatures, b)
		}

		entry.Data = append(signatures, entry.Data...)
	}
	return entry, nil
}

func (x *Context) WriteDataTo(cmd *cobra.Command, accountUrl string, args []string) (string, error) {
	u, err := url.Parse(accountUrl)
	if err != nil {
		return "", err
	}

	signers, err := x.getSigners(cmd, u, nil)
	if err != nil {
		return "", err
	}

	if len(args) < 1 {
		return "", fmt.Errorf("expecting lite data account url")
	}

	wd := protocol.WriteDataTo{}
	r, err := url.Parse(args[0])
	if err != nil {
		return "", fmt.Errorf("unable to parse lite token account url")
	}

	accountId, err := protocol.ParseLiteDataAddress(r)
	if err != nil {
		return "", fmt.Errorf("invalid lite data account url")
	}

	wd.Recipient = r

	// Remove the recipient from the arg list
	args = args[1:]
	if len(args) == 0 {
		return "", fmt.Errorf("expecting data")
	}

	// args[0] is the
	wd.Entry, err = x.prepareData(cmd, u, args, false)
	if err != nil {
		return PrintJsonRpcError(err)
	}
	res, resps, err := x.dispatchTxAndWait(cmd, &wd, u, signers)
	if err != nil {
		return "", err
	}

	lda := protocol.LiteDataAccount{}
	q, err := GetUrl(wd.Recipient.String())
	if err == nil {
		err = Remarshal(q.Data, &lda)
		if err != nil {
			return "", err
		}
	}

	lde := protocol.FactomDataEntry{}
	copy(lde.AccountId[:], accountId)
	data := wd.Entry.GetData()
	if len(data) > 0 {
		lde.Data = data[0]
		lde.ExtIds = data[1:]
	}
	entryHash := lde.Hash()

	ar := ActionResponseFromLiteData(res, wd.Recipient.String(), lde.AccountId[:], entryHash)
	ar.Flow = resps
	return ar.Print(x)
}
