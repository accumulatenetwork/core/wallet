package cmd

import (
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	url2 "gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
)

func init() {
	authCmd.AddCommand(
		authEnableCmd,
		authDisableCmd,
		authAddCmd,
		authRemoveCmd)
}

var authCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:     "auth",
		Short:   "Manage authorization of an account",
		Aliases: []string{"manager"},
	}
})

var authEnableCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "enable [account url] [authority url or index (1-based)]",
		Short: "Enable authorization checks for an authority of an account",
		Args:  cobra.ExactArgs(2),
		Run:   x.runTxnCmdFunc(x.EnableAuth),
	}
}).With((*Context).submitFlags)

var authDisableCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "disable [account url] [authority url or index (1-based)]",
		Short: "Disable authorization checks for an authority of an account",
		Args:  cobra.ExactArgs(2),
		Run:   x.runTxnCmdFunc(x.DisableAuth),
	}
}).With((*Context).submitFlags)

var authAddCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:     "add [account url] [authority url]",
		Short:   "Add an authority to an account",
		Aliases: []string{"set"},
		Args:    cobra.ExactArgs(2),
		Run:     x.runTxnCmdFunc(x.AddAuth),
	}
}).With((*Context).submitFlags)

var authRemoveCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "remove [account url] [authority url or index (1-based)]",
		Short: "Remove an authority from an account",
		Args:  cobra.ExactArgs(2),
		Run:   x.runTxnCmdFunc(x.RemoveAuth),
	}
}).With((*Context).submitFlags)

func getAccountAuthority(accountUrl *url2.URL, name string) (*url2.URL, error) {
	account, err := getAccount(accountUrl.String())
	if err != nil {
		return nil, err
	}

	fullAccount, ok := account.(protocol.FullAccount)
	if !ok {
		return nil, fmt.Errorf("account type %v does not support advanced auth", account.Type())
	}
	auth := fullAccount.GetAuth()

	i, err := strconv.ParseUint(name, 10, 8)
	if err != nil {
		return url2.Parse(name)
	}

	if i == 0 || int(i) > len(auth.Authorities) {
		return nil, fmt.Errorf("%d is not a valid account authority number", i)
	}

	return auth.Authorities[i-1].Url, nil
}

func (x *Context) EnableAuth(cmd *cobra.Command, account *url2.URL, signers []*api.SignRequest, args []string) (string, error) {
	authority, err := getAccountAuthority(account, args[0])
	if err != nil {
		return "", err
	}

	op := &protocol.EnableAccountAuthOperation{Authority: authority}
	txn := &protocol.UpdateAccountAuth{Operations: []protocol.AccountAuthOperation{op}}
	return x.dispatchTxAndPrintResponse(cmd, txn, account, signers)
}

func (x *Context) DisableAuth(cmd *cobra.Command, account *url2.URL, signers []*api.SignRequest, args []string) (string, error) {
	authority, err := getAccountAuthority(account, args[0])
	if err != nil {
		return "", err
	}

	op := &protocol.DisableAccountAuthOperation{Authority: authority}
	txn := &protocol.UpdateAccountAuth{Operations: []protocol.AccountAuthOperation{op}}
	return x.dispatchTxAndPrintResponse(cmd, txn, account, signers)
}

func (x *Context) AddAuth(cmd *cobra.Command, account *url2.URL, signers []*api.SignRequest, args []string) (string, error) {
	authority, err := getAccountAuthority(account, args[0])
	if err != nil {
		return "", err
	}

	op := &protocol.AddAccountAuthorityOperation{Authority: authority}
	txn := &protocol.UpdateAccountAuth{Operations: []protocol.AccountAuthOperation{op}}
	return x.dispatchTxAndPrintResponse(cmd, txn, account, signers)
}

func (x *Context) RemoveAuth(cmd *cobra.Command, account *url2.URL, signers []*api.SignRequest, args []string) (string, error) {
	authority, err := getAccountAuthority(account, args[0])
	if err != nil {
		return "", err
	}

	op := &protocol.RemoveAccountAuthorityOperation{Authority: authority}
	txn := &protocol.UpdateAccountAuth{Operations: []protocol.AccountAuthOperation{op}}
	return x.dispatchTxAndPrintResponse(cmd, txn, account, signers)
}
