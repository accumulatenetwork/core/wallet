package cmd

import (
	"fmt"
	"math/big"
	"strconv"

	"github.com/spf13/cobra"
	url2 "gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
)

// creditsCmd represents the faucet command
var creditsCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "credits [token account] [key page or lite identity] [number of credits wanted] [max acme to spend (optional)]",
		Short: "Purchase credits with acme and send to recipient.",
		Args:  cobra.RangeArgs(3, 4),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.AddCredits(cmd, args[0], args[1:])
		}),
	}
}).With((*Context).submitFlags)

func (x *Context) AddCredits(cmd *cobra.Command, origin string, args []string) (string, error) {
	u, err := url2.Parse(origin)
	if err != nil {
		return "", err
	}

	signer, err := x.getSigners(cmd, u, nil)
	if err != nil {
		return "", err
	}

	u2, err := url2.Parse(args[0])
	if err != nil {
		return "", err
	}

	acmeOracle, err := QueryAcmeOracle()
	if err != nil {
		return "", err
	}

	// credits desired
	cred, err := strconv.ParseFloat(args[1], 64)
	if err != nil {
		return "", err
	}

	// ACME = credits ÷ oracle ÷ credits-per-dollar
	estAcmeRat := big.NewRat(int64(cred*protocol.CreditPrecision), protocol.CreditPrecision)
	estAcmeRat.Quo(estAcmeRat, big.NewRat(int64(acmeOracle.Price), protocol.AcmeOraclePrecision))
	estAcmeRat.Quo(estAcmeRat, big.NewRat(protocol.CreditsPerDollar, 1))

	// Convert rational to an ACME balance
	estAcmeRat.Mul(estAcmeRat, big.NewRat(protocol.AcmePrecision, 1))
	acmeSpend := estAcmeRat.Num()
	if !estAcmeRat.IsInt() {
		acmeSpend.Div(acmeSpend, estAcmeRat.Denom())
	}

	//now test the cost of the credits against the max amount the user wants to spend
	if len(args) > 2 {
		maxSpend, err := amountToBigInt(protocol.ACME, args[2]) // amount in acme
		if err != nil {
			return "", fmt.Errorf("amount must be an integer %v", err)
		}

		// Fail if the estimated amount is greater than the desired max
		if acmeSpend.Cmp(maxSpend) > 0 {
			return "", fmt.Errorf("amount of credits requested will not be satisfied by amount of acme to be spent")
		}
	}

	credits := protocol.AddCredits{}
	credits.Recipient = u2
	credits.Amount = *acmeSpend
	credits.Oracle = acmeOracle.Price

	return x.dispatchTxAndPrintResponse(cmd, &credits, u, signer)
}
