package cmd

import (
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/encoding"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/network"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/internal/build"
)

func init() {
	operatorCmd.AddCommand(
		operatorAddCmd,
		operatorRemoveCmd,
		operatorUpdateKeyCmd,
		operatorSetCmd,
	)
	operatorSetCmd.AddCommand(
		operatorSetGlobalsCmd,
		operatorSetNetworkCmd,
		operatorSetOracleCmd,
		operatorSetRoutingCmd,
	)
}

var operatorCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "operator",
		Short: "Manage operators",
	}
})

var operatorAddCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "add [partition ID] [key name or path]",
		Short: "Add a operator",
		Run:   x.runValCmdFunc(x.addOperator),
		Args:  cobra.ExactArgs(2),
	}
})

var operatorRemoveCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "remove [partition ID]  [key name or path]",
		Short: "Remove a operator",
		Run:   x.runValCmdFunc(x.removeOperator),
		Args:  cobra.ExactArgs(2),
	}
})

var operatorUpdateKeyCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "update-key [partition ID] [old key name or path] [new key name or path]",
		Short: "Update a operator's key",
		Run:   x.runValCmdFunc(x.updateOperatorKey),
		Args:  cobra.ExactArgs(3),
	}
})

var operatorSetCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "set [command]",
		Short: "Set network variables",
	}
})

var operatorSetGlobalsCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "globals [value]",
		Short: "Set dn.acme/globals",
		Args:  cobra.ExactArgs(1),
		Run:   x.operatorSet(protocol.Globals),
	}
})

var operatorSetOracleCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "oracle [value]",
		Short: "Set dn.acme/oracle",
		Args:  cobra.ExactArgs(1),
		Run:   x.operatorSet(protocol.Oracle),
	}
})

var operatorSetRoutingCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "routing [value]",
		Short: "Set dn.acme/routing",
		Args:  cobra.ExactArgs(1),
		Run:   x.operatorSet(protocol.Routing),
	}
})

var operatorSetNetworkCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "network [value]",
		Short: "Set dn.acme/network",
		Args:  cobra.ExactArgs(1),
		Run:   x.operatorSet(protocol.Network),
	}
})

func (x *Context) addOperator(cmd *cobra.Command, values *network.GlobalValues, pageCount int, _ string, args []string) (*protocol.Transaction, error) {
	newKey, err := x.resolvePublicKey(cmd, args[0])
	if err != nil {
		return nil, err
	}

	return build.AddOperator(values, pageCount, newKey)
}

func (x *Context) removeOperator(cmd *cobra.Command, values *network.GlobalValues, pageCount int, _ string, args []string) (*protocol.Transaction, error) {
	oldKey, err := x.resolvePublicKey(cmd, args[0])
	if err != nil {
		return nil, err
	}

	return build.RemoveOperator(values, pageCount, oldKey)
}

func (x *Context) updateOperatorKey(cmd *cobra.Command, _ *network.GlobalValues, _ int, _ string, args []string) (*protocol.Transaction, error) {
	oldKey, err := x.resolvePublicKey(cmd, args[0])
	if err != nil {
		return nil, err
	}

	newKey, err := x.resolvePublicKey(cmd, args[1])
	if err != nil {
		return nil, err
	}

	return build.UpdateOperatorKey(oldKey, newKey)
}

func (x *Context) operatorSet(path string) func(cmd *cobra.Command, args []string) {
	return x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
		signers, err := x.getSigners(cmd, protocol.DnUrl(), nil)
		if err != nil {
			return "", err
		}

		var value encoding.BinaryValue
		switch path {
		case protocol.Oracle:
			value = new(protocol.AcmeOracle)
		case protocol.Globals:
			value = new(protocol.NetworkGlobals)
		case protocol.Network:
			value = new(protocol.NetworkDefinition)
		case protocol.Routing:
			value = new(protocol.RoutingTable)
		default:
			panic(fmt.Errorf("unknown network variable account %s", path))
		}

		err = json.Unmarshal([]byte(args[0]), value)
		if err != nil {
			return "", err
		}

		b, err := value.MarshalBinary()
		if err != nil {
			return "", err
		}

		wd := new(protocol.WriteData)
		wd.WriteToState = true
		wd.Entry = &protocol.DoubleHashDataEntry{Data: [][]byte{b}}
		return x.dispatchTxAndPrintResponse(cmd, wd, protocol.DnUrl().JoinPath(path), signers)
	})
}
