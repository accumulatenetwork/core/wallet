package cmd

import (
	"bytes"
	"context"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"text/tabwriter"
	"time"

	"github.com/AccumulateNetwork/jsonrpc2/v15"
	"github.com/multiformats/go-multihash"
	"github.com/spf13/cobra"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	errors3 "gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/address"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/encoding"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	url2 "gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	errors2 "gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

//lint:file-ignore S1039 Don't care

func PrintJsonRpcError(err error) (string, error) {
	var message, code, data any
	switch e := err.(type) {
	case jsonrpc2.Error:
		message, code, data = e.Message, e.Code, e.Data
	case *errors2.Error:
		message, code, data = e.Message, e.Code, e.Data
	case *errors3.Error:
		message, code, data = e.Message, e.Code, e.Data
	default:
		return "", fmt.Errorf("error with request, %v", err)
	}

	if WantJsonOutput {
		out, e := json.Marshal(err)
		if e != nil {
			return "", err
		}
		return "", &RpcError{Err: err, Msg: string(out)}
	}

	var out string
	out += fmt.Sprintf("\n\tMessage\t\t:\t%v\n", message)
	out += fmt.Sprintf("\tError Code\t:\t%v\n", code)
	out += fmt.Sprintf("\tDetail\t\t:\t%s\n", data)
	return "", &RpcError{Err: err, Msg: out}
}

func (x *Context) printOutput(cmd *cobra.Command, out string, err error) {
	out = strings.TrimPrefix(out, "\n")
	out = strings.TrimSuffix(out, "\n")
	x.DidError = err
	if err == nil {
		cmd.Println(out)
		return
	}

	if !WantJsonOutput {
		if out != "" {
			cmd.Println(out)
		}
		cmd.PrintErrf("Error: %v\n", err)
		return
	}

	// Check if the error is an action response error
	var v interface{}
	switch err.(type) {
	case *ActionResponseError, *errors2.Error:
		v = err
	default:
		v = err.Error()
	}

	// JSON marshal the error
	b, _ := json.Marshal(map[string]interface{}{"error": v})

	// If the caller wants JSON, they probably want it on stdout
	if out != "" {
		cmd.Printf("%s\n", out)
	}
	cmd.Printf("%s\n", b)
}

//nolint:gosimple
func printGeneralTransactionParameters(res *client.TransactionQueryResponse) string {

	out := fmt.Sprintf("---\n")
	out += fmt.Sprintf("  - Transaction           : %x\n", res.TransactionHash)
	out += fmt.Sprintf("  - Signer Url            : %s\n", res.Origin)
	for _, book := range res.SignatureBooks {
		for _, page := range book.Pages {
			out += fmt.Sprintf("  - Signatures            :\n")
			out += fmt.Sprintf("    - Signer              : %s (%v)\n", page.Signer.Url, page.Signer.Type)
			for _, sig := range page.Signatures {
				keysig, ok := sig.(protocol.KeySignature)
				if !ok || sig.Type().IsSystem() {
					out += fmt.Sprintf("      -                   : %v\n", sig.Type())
					continue
				}
				out += fmt.Sprintf("      -                   : %x (sig)\n", keysig.GetSignature())
				out += fmt.Sprintf("      -                   : %x (key hash)\n", keysig.GetPublicKeyHash())
			}
		}
	}
	for _, id := range res.Produced {
		out += fmt.Sprintf("  - Produced synthetic    : %x\n", id)
	}
	out += fmt.Sprintf("===\n")
	return out
}

func PrintJson(v interface{}) (string, error) {
	data, err := json.Marshal(v)
	if err != nil {
		return "", err
	}
	return string(data), nil
}

func (x *Context) PrintChainQueryResponseV2(cmd *cobra.Command, res *QueryResponse) (string, error) {
	if WantJsonOutput || res.Type == "dataEntry" {
		return PrintJson(res)
	}

	out, err := x.outputForHumans(cmd, res)
	if err != nil {
		return "", err
	}

	for i, txid := range res.SyntheticTxids {
		out += fmt.Sprintf("  - Synthetic Transaction %d : %x\n", i, txid)
	}
	return out, nil
}

func (x *Context) PrintTransactionQueryResponseV2(res *client.TransactionQueryResponse) (string, error) {
	if WantJsonOutput {
		return PrintJson(res)
	}

	out, err := x.outputForHumansTx(res)
	if err != nil {
		return "", err
	}

	for i, txid := range res.Produced {
		out += fmt.Sprintf("  - Synthetic Transaction %d : %v\n", i, txid)
	}

	for _, receipt := range res.Receipts {
		// // TODO Figure out how to include the directory receipt and block
		// out += fmt.Sprintf("Receipt from %v#chain/%s in block %d\n", receipt.Account, receipt.Chain, receipt.DirectoryBlock)
		out += fmt.Sprintf("Receipt from %v#chain/%s\n", receipt.Account, receipt.Chain)
		if receipt.Error != "" {
			out += fmt.Sprintf("  Error!! %s\n", receipt.Error)
		}
		if !receipt.Proof.Validate(nil) {
			//nolint:gosimple
			out += fmt.Sprintf("  Invalid!!\n")
		}
	}

	return out, nil
}

func (x *Context) PrintMinorBlockQueryResponseV2(cmd *cobra.Command, res *client.MinorQueryResponse) (string, error) {
	if WantJsonOutput {
		return PrintJson(res)
	}

	str := fmt.Sprintf("--- block #%d, blocktime %v:\n", res.BlockIndex, getBlockTime(res.BlockTime))
	if res.TxCount > 0 {
		str += fmt.Sprintf("    total tx count\t: %d\n", res.TxCount)
	}

	if len(res.Transactions) > 0 {
		for _, tx := range res.Transactions {
			if tx.Data != nil {
				s, err := x.PrintTransactionQueryResponseV2(tx)
				if err != nil {
					return "", err
				}
				str += s
			}
		}
	} else {
		for i, txid := range res.TxIds {
			str += fmt.Sprintf("    txid #%d\t\t: %s\n", i+1, hex.EncodeToString(txid))
		}
	}
	return str, nil
}

func PrintMajorBlockQueryResponseV2(res *client.MajorQueryResponse) (string, error) {
	if WantJsonOutput {
		return PrintJson(res)
	}

	str := fmt.Sprintf("--- major block #%d, major blocktime %v:\n", res.MajorBlockIndex, getBlockTime(res.MajorBlockTime))

	if len(res.MinorBlocks) > 0 {
		for _, mnrBlk := range res.MinorBlocks {
			str += fmt.Sprintf("    minor block index: %d\n", mnrBlk.BlockIndex)
			str += fmt.Sprintf("    minor block time : %s\n", getBlockTime(mnrBlk.BlockTime))
		}
	} else {
		str += fmt.Sprintf("    (empty)") //nolint
	}
	return str, nil
}

func getBlockTime(blockTime *time.Time) string {
	if blockTime != nil {
		return blockTime.String()
	}
	return "<not recorded>"
}

func (x *Context) PrintMultiResponse(cmd *cobra.Command, res *client.MultiResponse) (string, error) {
	if WantJsonOutput || res.Type == "dataSet" {
		return PrintJson(res)
	}

	var out string
	switch res.Type {
	case "directory":
		out += fmt.Sprintf("\n\tADI Entries: start = %d, count = %d, total = %d\n", res.Start, res.Count, res.Total)

		if len(res.OtherItems) == 0 {
			for _, s := range res.Items {
				out += fmt.Sprintf("\t%v\n", s)
			}
			return out, nil
		}

		for _, s := range res.OtherItems {
			qr := new(client.ChainQueryResponse)
			var data json.RawMessage
			qr.Data = &data
			err := Remarshal(s, qr)
			if err != nil {
				return "", err
			}

			account, err := protocol.UnmarshalAccountJSON(data)
			if err != nil {
				return "", err
			}

			chainDesc := account.Type().String()
			if err == nil {
				if v, ok := ApiToString[account.Type()]; ok {
					chainDesc = v
				}
			}
			out += fmt.Sprintf("\t%v (%s)\n", account.GetUrl(), chainDesc)
		}
	case "pending":
		out += fmt.Sprintf("\n\tPending Tranactions -> Start: %d\t Count: %d\t Total: %d\n", res.Start, res.Count, res.Total)
		for i, item := range res.Items {
			out += fmt.Sprintf("\t%d\t%s", i, item)
		}
	case "txHistory":
		out += fmt.Sprintf("\n\tTrasaction History Start: %d\t Count: %d\t Total: %d\n", res.Start, res.Count, res.Total)
		for i := range res.Items {
			// Convert the item to a transaction query response
			txr := new(client.TransactionQueryResponse)
			err := Remarshal(res.Items[i], txr)
			if err != nil {
				return "", err
			}

			s, err := x.PrintTransactionQueryResponseV2(txr)
			if err != nil {
				return "", err
			}
			out += s
		}
	case "minorBlock":
		str := fmt.Sprintf("\n\tMinor block result Start: %d\t Count: %d\t Total blocks: %d\n", res.Start, res.Count, res.Total)
		for i := range res.Items {
			str += fmt.Sprintln("==========================================================================")

			// Convert the item to a minor query response
			mtr := new(client.MinorQueryResponse)
			err := Remarshal(res.Items[i], mtr)
			if err != nil {
				return "", err
			}

			s, err := x.PrintMinorBlockQueryResponseV2(cmd, mtr)
			if err != nil {
				return "", err
			}
			str += s
			str += fmt.Sprintln()
		}
		out += str
	case "majorBlock":
		str := fmt.Sprintf("\n\tMajor block result Start: %d\t Count: %d\t Total blocks: %d\n", res.Start, res.Count, res.Total)
		for i := range res.Items {
			str += fmt.Sprintln("==========================================================================")

			// Convert the item to a major query response
			mtr := new(client.MajorQueryResponse)
			err := Remarshal(res.Items[i], mtr)
			if err != nil {
				return "", err
			}

			s, err := PrintMajorBlockQueryResponseV2(mtr)
			if err != nil {
				return "", err
			}
			str += s
			str += fmt.Sprintln()
		}
		out += str
	}

	return out, nil
}

//nolint:gosimple
func (x *Context) outputForHumans(cmd *cobra.Command, res *QueryResponse) (string, error) {
	out, err := outputForHumansSystem(res)
	if out != "" || err != nil {
		return out, err
	}

	switch res.Type {
	case protocol.AccountTypeLiteTokenAccount.String():
		ata := protocol.LiteTokenAccount{}
		err := Remarshal(res.Data, &ata)
		if err != nil {
			return "", err
		}

		amt, err := formatAmount(ata.TokenUrl.String(), &ata.Balance)
		if err != nil {
			amt = "unknown"
		}
		params := client.UrlQuery{}
		params.Url = ata.Url.RootIdentity()
		qres := new(QueryResponse)
		litIdentity := new(protocol.LiteIdentity)
		qres.Data = litIdentity
		err = queryAs("query", &params, &qres)
		if err != nil {
			return "", err
		}
		var out string
		out += fmt.Sprintf("\n\tAccount Url\t:\t%v\n", ata.Url)
		out += fmt.Sprintf("\tToken Url\t:\t%v\n", ata.TokenUrl)
		out += fmt.Sprintf("\tBalance\t\t:\t%s\n", amt)
		out += fmt.Sprintf("\tCreditBalance\t:\t%v\n", protocol.FormatAmount(litIdentity.CreditBalance, protocol.CreditPrecisionPower))
		out += fmt.Sprintf("\tLast Used On\t:\t%v\n", time.Unix(0, int64(litIdentity.LastUsedOn*uint64(time.Millisecond))))
		out += fmt.Sprintf("\tLock Height\t:\t%v\n", ata.LockHeight)

		return out, nil
	case protocol.AccountTypeLiteIdentity.String():
		ata := protocol.LiteIdentity{}
		err := Remarshal(res.Data, &ata)
		if err != nil {
			return "", err
		}

		var out string
		out += fmt.Sprintf("\n\tAccount Url\t:\t%v\n", ata.Url)
		out += fmt.Sprintf("\tCredits\t\t:\t%v\n", protocol.FormatAmount(ata.CreditBalance, protocol.CreditPrecisionPower))
		out += fmt.Sprintf("\tLast Used On\t:\t%v\n", time.Unix(0, int64(ata.LastUsedOn*uint64(time.Millisecond))))
		return out, nil
	case protocol.AccountTypeTokenAccount.String():
		ata := protocol.TokenAccount{}
		err := Remarshal(res.Data, &ata)
		if err != nil {
			return "", err
		}

		amt, err := formatAmount(ata.TokenUrl.String(), &ata.Balance)
		if err != nil {
			amt = "unknown"
		}

		var out string
		out += fmt.Sprintf("\n\tAccount Url\t:\t%v\n", ata.Url)
		out += fmt.Sprintf("\tToken Url\t:\t%s\n", ata.TokenUrl)
		out += fmt.Sprintf("\tBalance\t\t:\t%s\n", amt)
		for _, a := range ata.Authorities {
			out += fmt.Sprintf("\tKey Book Url\t:\t%s\n", a.Url)
		}

		return out, nil
	case protocol.AccountTypeIdentity.String():
		adi := protocol.ADI{}
		err := Remarshal(res.Data, &adi)
		if err != nil {
			return "", err
		}

		var out string
		out += fmt.Sprintf("\n\tADI url\t\t:\t%v\n", adi.Url)
		for _, a := range adi.Authorities {
			out += fmt.Sprintf("\tKey Book Url\t:\t%s\n", a.Url)
		}

		return out, nil
	case protocol.AccountTypeKeyBook.String():
		book := protocol.KeyBook{}
		err := Remarshal(res.Data, &book)
		if err != nil {
			return "", err
		}

		var out string
		out += fmt.Sprintf("\n\tPage Count\n")
		out += fmt.Sprintf("\t%d\n", book.PageCount)
		return out, nil
	case protocol.AccountTypeKeyPage.String():
		ss := protocol.KeyPage{}
		err := Remarshal(res.Data, &ss)
		if err != nil {
			return "", err
		}

		out := fmt.Sprintf("\n\tCredit Balance\t:\t%v\n", protocol.FormatAmount(ss.CreditBalance, protocol.CreditPrecisionPower))
		out = fmt.Sprintf("%s\n\tAcceptThreshold:\t%d\n", out, ss.AcceptThreshold)
		out = fmt.Sprintf("%s\n\tRejectThreshold:\t%d\n", out, ss.RejectThreshold)
		out = fmt.Sprintf("%s\n\tResponseThreshold:\t%d\n", out, ss.ResponseThreshold)
		buf := new(bytes.Buffer)
		tw := tabwriter.NewWriter(buf, 1, 1, 2, ' ', 0)
		fmt.Fprintf(tw, "Index\tLast Used On\tKey Address\tDelegate\t\n")
		for i, k := range ss.Keys {
			var keyName string
			if k.PublicKeyHash != nil {
				keyName = address.FormatMH(k.PublicKeyHash, multihash.IDENTITY)
				key, err := x.getWallet().ResolveKey(context.Background(), &api.ResolveKeyRequest{Value: keyName})
				if err == nil {
					if key.Key.Labels != nil && len(key.Key.Labels.Names) > 0 {
						keyName = key.Key.Labels.Names[0]
					} else {
						keyName = key.Address
					}
				}
			}
			var delegate string
			if k.Delegate != nil {
				delegate = k.Delegate.ShortString()
			}
			fmt.Fprintf(tw, "%d\t%v\t%s\t%s\n", i, time.Unix(0, int64(k.LastUsedOn*uint64(time.Millisecond))), keyName, delegate)
		}

		err = tw.Flush()
		if err != nil {
			return "", err
		}
		out += "\n\t" + strings.ReplaceAll(buf.String(), "\n", "\n\t")
		return out, nil
	case "token", protocol.AccountTypeTokenIssuer.String():
		ti := protocol.TokenIssuer{}
		err := Remarshal(res.Data, &ti)
		if err != nil {
			return "", err
		}

		out := fmt.Sprintf("\n\tToken URL\t:\t%s", ti.Url)
		out += fmt.Sprintf("\n\tSymbol\t\t:\t%s", ti.Symbol)
		out += fmt.Sprintf("\n\tPrecision\t:\t%d", ti.Precision)
		if ti.SupplyLimit != nil {
			out += fmt.Sprintf("\n\tSupply Limit\t\t:\t%s", amountToString(ti.Precision, ti.SupplyLimit))
		}
		out += fmt.Sprintf("\n\tTokens Issued\t:\t%s", amountToString(ti.Precision, &ti.Issued))
		out += fmt.Sprintf("\n\tProperties URL\t:\t%s", ti.Properties)
		out += "\n"
		return out, nil
	case protocol.AccountTypeLiteIdentity.String():
		li := protocol.LiteIdentity{}
		err := Remarshal(res.Data, &li)
		if err != nil {
			return "", err
		}
		params := client.DirectoryQuery{}
		params.Url = li.Url
		params.Start = uint64(0)
		params.Count = uint64(10)
		params.Expand = true

		var adiRes client.MultiResponse
		if err := Client.RequestAPIv2(context.Background(), "query-directory", &params, &adiRes); err != nil {
			return PrintJsonRpcError(err)
		}
		return x.PrintMultiResponse(cmd, &adiRes)
	default:
		return printReflection("", "", reflect.ValueOf(res.Data)), nil
	}
}

func outputForHumansSystem(res *QueryResponse) (string, error) {
	if res.Type != protocol.AccountTypeDataAccount.String() {
		return "", nil
	}

	account := protocol.DataAccount{}
	err := Remarshal(res.Data, &account)
	if err != nil {
		return "", err
	}

	_, ok := protocol.ParsePartitionUrl(account.GetUrl())
	if !ok {
		return "", nil
	}

	var v encoding.BinaryValue
	switch strings.Trim(account.GetUrl().Path, "/") {
	case protocol.Network:
		v = new(protocol.NetworkDefinition)
	case protocol.Oracle:
		v = new(protocol.AcmeOracle)
	case protocol.Globals:
		v = new(protocol.NetworkGlobals)
	case protocol.Routing:
		v = new(protocol.RoutingTable)
	default:
		return "", nil
	}

	err = v.UnmarshalBinary(account.Entry.GetData()[0])
	if err != nil {
		return "", err
	}

	return printReflection("", "", reflect.ValueOf(&struct {
		Url   *url.URL
		Value interface{}
	}{account.GetUrl(), v})), nil
}

func (x *Context) outputForHumansTx(res *client.TransactionQueryResponse) (string, error) {
	out, err := x.outputForHumansTx2(res.Transaction)
	if err != nil {
		return "", err
	}

	out += printGeneralTransactionParameters(res)
	return out, nil
}

func (x *Context) outputForHumansTx2(txn *protocol.Transaction) (string, error) {
	var out string
	// If the initiator is blank, don't call ID because that calls Hash and the
	// hash will be invalid
	if txn.Header.Initiator != ([32]byte{}) {
		out += fmt.Sprintf("\tID:\t%v\n", txn.ID())
	}
	out += fmt.Sprintf("\tPrincipal:\t%v\n", txn.Header.Principal)
	if txn.Header.Memo != "" {
		out += fmt.Sprintf("\tMemo:\t%v\n", txn.Header.Memo)
	}
	if len(txn.Header.Metadata) > 0 {
		out += fmt.Sprintf("\tMetadata:\t%x\n", txn.Header.Metadata)
	}
	if txn.Header.Expire != nil && txn.Header.Expire.AtTime != nil {
		out += fmt.Sprintf("\tExpiration:\t%v\n", *txn.Header.Expire.AtTime)
	}
	if txn.Header.HoldUntil != nil && txn.Header.HoldUntil.MinorBlock > 0 {
		out += fmt.Sprintf("\tHold until:\tBlock %v\n", txn.Header.HoldUntil.MinorBlock)
	}
	for _, auth := range txn.Header.Authorities {
		out += fmt.Sprintf("\tSigner:\t%v\n", auth)
	}
	out += fmt.Sprintln()

	switch body := txn.Body.(type) {
	case *protocol.SendTokens:
		for i := range body.To {
			amt, err := formatAmount("acc://ACME", &body.To[i].Amount)
			if err != nil {
				amt = "unknown"
			}
			out += fmt.Sprintf("Send %s from %s to %s\n", amt, txn.Header.Principal, body.To[i].Url)
		}
		return out, nil

	case *protocol.WriteData:
		if !protocol.DnUrl().ParentOf(txn.Header.Principal) {
			break
		}
		if len(body.Entry.GetData()) < 1 {
			break
		}
		path := txn.Header.Principal.Path
		path = strings.ToLower(path)
		path = strings.Trim(path, "/")
		var value encoding.BinaryValue
		switch path {
		case protocol.Oracle:
			value = new(protocol.AcmeOracle)
		case protocol.Globals:
			value = new(protocol.NetworkGlobals)
		case protocol.Network:
			value = new(protocol.NetworkDefinition)
		case protocol.Routing:
			value = new(protocol.RoutingTable)
		}
		if value == nil {
			break
		}
		if value.UnmarshalBinary(body.Entry.GetData()[0]) != nil {
			break
		}

		out += fmt.Sprintf("Update to network parameter account %v\n", txn.Header.Principal)
		out += "\n" + printReflection("", "", reflect.ValueOf(value))
		return out, nil

	case *protocol.SyntheticDepositTokens:
		amt, err := formatAmount(body.Token.String(), &body.Amount)
		if err != nil {
			amt = "unknown"
		}
		out += fmt.Sprintf("Receive %s to %s (cause: %X)\n", amt, txn.Header.Principal, body.Cause)
		return out, nil

	case *protocol.SyntheticCreateIdentity:
		for _, account := range body.Accounts {
			out += fmt.Sprintf("Created %v (%v)\n", account.GetUrl(), account.Type())
		}
		return out, nil

	case *protocol.CreateIdentity:
		out += fmt.Sprintf("ADI URL \t\t:\t%s\n", body.Url)
		out += fmt.Sprintf("Key Book URL\t\t:\t%s\n", body.KeyBookUrl)

		mhAddress := address.FormatMH(body.KeyHash, multihash.IDENTITY)
		out += fmt.Sprintf("Multi-Hash Key Address \t:\t%s\n", mhAddress)

		key, err := x.getWallet().ResolveKey(context.Background(), &api.ResolveKeyRequest{Value: mhAddress})
		if err == nil {
			out += fmt.Sprintf("Native Key Address\t:\t%s\n", key.Address)
			if key.Key.Labels != nil && len(key.Key.Labels.Names) > 0 {
				out += fmt.Sprintf("Key Name(s)) \t:\t%s\n", strings.Join(key.Key.Labels.Names, ", "))
			}
		}
		return out, nil
	}

	out += printReflection("", "", reflect.ValueOf(txn.Body))
	return out, nil
}

func printReflection(field, indent string, value reflect.Value) string {
	typ := value.Type()
	out := fmt.Sprintf("%s%s:", indent, field)
	if field == "" {
		out = ""
	}

	if typ.AssignableTo(reflect.TypeOf(new(url2.URL))) {
		v := value.Interface().(*url2.URL)
		if v == nil {
			return out + " (nil)\n"
		}
		return out + " " + v.String() + "\n"
	}

	switch value.Kind() {
	case reflect.Ptr, reflect.Interface:
		if value.IsNil() {
			return ""
		}
		return printReflection(field, indent, value.Elem())
	case reflect.Slice, reflect.Array:
		if value.Type().Elem().Kind() == reflect.Uint8 {
			out += fmt.Sprintf(" %s\n", getHashString(value))
		} else {
			out += "\n"
			for i, n := 0, value.Len(); i < n; i++ {
				out += printReflection(fmt.Sprintf("%d (elem)", i), indent+"   ", value.Index(i))
			}
		}
		return out
	case reflect.Map:
		out += "\n"
		for iter := value.MapRange(); iter.Next(); {
			out += printReflection(fmt.Sprintf("%s (key)", iter.Key()), indent+"   ", iter.Value())
		}
		return out
	case reflect.Struct:
		out += "\n"
		out += fmt.Sprintf("%s   (type): %s\n", indent, natural(typ.Name()))

		callee := value
		m, ok := typ.MethodByName("Type")
		if !ok {
			m, ok = reflect.PtrTo(typ).MethodByName("Type")
			callee = value.Addr()
		}
		if ok && m.Type.NumIn() == 1 && m.Type.NumOut() == 1 {
			v := m.Func.Call([]reflect.Value{callee})[0]
			if _, ok := v.Type().MethodByName("GetEnumValue"); ok {
				out += fmt.Sprintf("%s   %s: %s\n", indent, natural(v.Type().Name()), natural(fmt.Sprint(v)))
			}
		}

		for i, n := 0, value.NumField(); i < n; i++ {
			f := typ.Field(i)
			if !f.IsExported() {
				continue
			}
			out += printReflection(f.Name, indent+"   ", value.Field(i))
		}
		return out
	default:
		return out + " " + fmt.Sprint(value) + "\n"
	}
}

func getHashString(value reflect.Value) string {
	hash, ok := value.Interface().([32]uint8)
	if ok {
		return hex.EncodeToString(hash[:])
	} else {
		hash, ok := value.Interface().([]uint8)
		if ok {
			return hex.EncodeToString(hash)
		} else {
			return "(unknown hash format)"
		}
	}
}

func outputTransactionResultForHumans(t protocol.TransactionResult) string {
	var out string

	switch c := t.(type) {
	case *protocol.AddCreditsResult:
		amt, err := formatAmount(protocol.ACME, &c.Amount)
		if err != nil {
			amt = "unknown"
		}
		out += fmt.Sprintf("Oracle\t$%.2f / ACME\n", float64(c.Oracle)/protocol.AcmeOraclePrecision)
		out += fmt.Sprintf("\t\t\t\t  Credits\t%.2f\n", float64(c.Credits)/protocol.CreditPrecision)
		out += fmt.Sprintf("\t\t\t\t  Amount\t%s\n", amt)
	case *protocol.WriteDataResult:
		out += fmt.Sprintf("Account URL\t%s\n", c.AccountUrl)
		out += fmt.Sprintf("\t\t\t\t  Account ID\t%x\n", c.AccountID)
		out += fmt.Sprintf("\t\t\t\t  Entry Hash\t%x\n", c.EntryHash)
	}
	return out
}

func (a *ActionLiteDataResponse) Print(x *Context) (string, error) {
	var out string
	if WantJsonOutput {
		ok := a.Code == "0" || a.Code == ""
		if ok {
			a.Code = "ok"
		}
		b, err := json.Marshal(a)
		if err != nil {
			return "", err
		}
		out = string(b)
	} else {
		s, err := a.ActionDataResponse.Print(x)
		if err != nil {
			return "", err
		}
		out = fmt.Sprintf("\n\tAccount Url\t\t:%s\n", a.AccountUrl[:])
		out += fmt.Sprintf("\n\tAccount Id\t\t:%x\n", a.AccountId[:])
		out += s[1:]
	}
	return out, nil
}

func (a *ActionDataResponse) Print(x *Context) (string, error) {
	var out string
	if WantJsonOutput {
		ok := a.Code == "0" || a.Code == ""
		if ok {
			a.Code = "ok"
		}
		b, err := json.Marshal(a)
		if err != nil {
			return "", err
		}
		out = string(b)
	} else {
		s, err := a.ActionResponse.Print(x)
		if err != nil {
			return "", err
		}
		out = fmt.Sprintf("\n\tEntry Hash\t\t:%x\n%s", a.EntryHash[:], s[1:])
	}
	return out, nil
}

type ActionResponseError struct {
	ActionResponse
}

func (a *ActionResponseError) Error() string {
	b, err := json.Marshal(a)
	if err != nil {
		return err.Error()
	}
	return string(b)
}

func (a *ActionResponse) Print(x *Context) (string, error) {
	ok := a.Code == "0" || a.Code == ""

	var out string
	if WantJsonOutput {
		if !ok {
			return "", &ActionResponseError{*a}
		}

		a.Code = "ok"
		b, err := json.Marshal(a)
		if err != nil {
			return "", err
		}
		return string(b), nil
	} else {
		out += fmt.Sprintf("\n\tTransaction Hash\t: %x\n", a.TransactionHash)
		for i, hash := range a.SignatureHashes {
			out += fmt.Sprintf("\tSignature %d Hash\t: %x\n", i, hash)
		}
		out += fmt.Sprintf("\tSimple Hash\t\t: %x\n", a.SimpleHash)
		if !ok {
			out += fmt.Sprintf("\tError code\t\t: %s\n", a.Code)
		} else {
			//nolint:gosimple
			out += fmt.Sprintf("\tError code\t\t: ok\n")
		}
		if a.Error != "" {
			out += fmt.Sprintf("\tError\t\t\t: %s\n", a.Error)
		}
		if a.Log != "" {
			out += fmt.Sprintf("\tLog\t\t\t: %s\n", a.Log)
		}
		if a.Codespace != "" {
			out += fmt.Sprintf("\tCodespace\t\t: %s\n", a.Codespace)
		}
		if a.Result != nil {
			out += "\tResult\t\t\t: "
			d, err := json.Marshal(a.Result.Result)
			if err != nil {
				out += fmt.Sprintf("error remarshaling result %v\n", a.Result.Result)
			} else {
				v, err := protocol.UnmarshalTransactionResultJSON(d)
				if err != nil {
					out += fmt.Sprintf("error unmarshaling transaction result %v", err)
				} else {
					out += outputTransactionResultForHumans(v)
				}
			}
		}
		for _, response := range a.Flow {
			str, err := x.PrintTransactionQueryResponseV2(response)
			if err != nil {
				return PrintJsonRpcError(err)
			}
			out += str
		}
	}

	if ok {
		return out, nil
	}
	return "", errors.New(out)
}
