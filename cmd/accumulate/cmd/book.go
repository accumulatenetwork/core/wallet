package cmd

import (
	"context"
	"crypto/sha256"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	url2 "gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

func init() {
	bookCmd.AddCommand(
		bookGetCmd,
		bookCreateCmd,
		bookRegisterCmd,
		bookUnregisterCmd)

	// Add auth cmd for backwards compatibility
	bookCmd.AddCommand(authCmd)
}

// bookCmd are the commands associated with managing key books
var bookCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "book",
		Short: "Manage key books for a ADI chains",
	}
})

var bookGetCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "get",
		Short: "Deprecated - use `accumulate get ...`",
		Args:  cobra.ExactArgs(1),
		Run:   x.runCmdFunc2(x.GetAndPrintKeyBook),
	}
})

var bookCreateCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "create [origin adi url] [new key book url] [public key (optional)]",
		Short: "Create new key book and page. When public key 1 is specified it will be assigned to the page, otherwise the origin key is used.",
		Args:  cobra.RangeArgs(2, 3),
		Run:   x.runCmdFunc2(x.CreateKeyBook),
	}
}).
	With((*Context).submitFlags).
	With((*Context).createFlags)

var bookRegisterCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "register [url]",
		Short: "Registers a key book with the wallet",
		Args:  cobra.ExactArgs(1),
		Run:   x.runCmdFunc2(x.registerBookCmd),
	}
})

var bookUnregisterCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "unregister [url]",
		Short: "Unregisters a key book with the wallet",
		Args:  cobra.ExactArgs(1),
		Run:   x.runCmdFunc2(x.unregisterBookCmd),
	}
})

func (x *Context) GetAndPrintKeyBook(cmd *cobra.Command, args []string) (string, error) {
	res, _, err := GetKeyBook(args[0])
	if err != nil {
		return "", fmt.Errorf("error retrieving key book for %s", args[0])
	}

	return x.PrintChainQueryResponseV2(cmd, res)
}

func GetKeyBook(url string) (*QueryResponse, *protocol.KeyBook, error) {
	res, err := GetUrl(url)
	if err != nil {
		return nil, nil, err
	}

	if res.Type != protocol.AccountTypeKeyBook.String() {
		return nil, nil, fmt.Errorf("expecting key book but received %v", res.Type)
	}

	kb := protocol.KeyBook{}
	err = Remarshal(res.Data, &kb)
	if err != nil {
		return nil, nil, err
	}
	return res, &kb, nil
}

// CreateKeyBook create a new key book
func (x *Context) CreateKeyBook(cmd *cobra.Command, args []string) (string, error) {
	originUrl, err := url2.Parse(args[0])
	if err != nil {
		return "", err
	}

	signer, err := x.getSigners(cmd, originUrl, nil)
	if err != nil {
		return "", err
	}
	if len(args) < 2 {
		return "", fmt.Errorf("invalid number of arguments")
	}

	newUrl, err := url2.Parse(args[1])
	if err != nil {
		return "", err
	}
	if newUrl.Authority != originUrl.Authority {
		return "", fmt.Errorf("the authority of book url to create (%s) doesn't match the origin adi's authority (%s)", newUrl.Authority, originUrl.Authority)
	}

	keyBook := protocol.CreateKeyBook{}
	keyBook.Url = newUrl
	keyBook.Authorities = x.Create.Authorities

	if len(args) < 3 {
		// Take the public key hash from the first signer
		h := sha256.Sum256(signer[0].PublicKey)
		keyBook.PublicKeyHash = h[:]

	} else {
		k, err := x.resolvePublicKey(cmd, args[2])
		if err != nil {
			return "", fmt.Errorf("could not resolve public key hash %s: %w", args[2], err)
		}

		publicKeyHash := k.PublicKeyHash()
		keyBook.PublicKeyHash = publicKeyHash
	}

	return x.dispatchTxAndPrintResponse(cmd, &keyBook, originUrl, signer)
}

func (x *Context) unregisterBookCmd(cmd *cobra.Command, args []string) (string, error) {
	u, err := url.Parse(args[0])
	if err != nil {
		return "", err
	}

	_, err = x.getWallet().UnregisterBook(context.Background(), &api.UnregisterBookRequest{Url: u})
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("Unregistered %v", u), nil
}

func (x *Context) registerBookCmd(cmd *cobra.Command, args []string) (string, error) {
	u, err := url.Parse(args[0])
	if err != nil {
		return "", err
	}

	err = x.registerBook(cmd, u)
	if err != nil {
		return "", err
	}

	return fmt.Sprintf("Registered %v", u), nil
}

func (x *Context) registerBook(cmd *cobra.Command, u *url.URL) error {
	r, err := Q.QueryAccount(context.Background(), u, nil)
	if err != nil {
		return err
	}

	var book *protocol.KeyBook
	switch account := r.Account.(type) {
	case *protocol.KeyBook:
		book = account
	case *protocol.KeyPage:
		return x.registerBook(cmd, account.GetAuthority())
	default:
		return errors.BadRequest.WithFormat("%v is not an signing authority", u)
	}

	var pages []*protocol.KeyPage
	for i := 0; i < int(book.PageCount); i++ {
		var page *protocol.KeyPage
		_, err = Q.QueryAccountAs(context.Background(), u.JoinPath(strconv.FormatInt(int64(i+1), 10)), nil, &page)
		if err != nil {
			return err
		}
		pages = append(pages, page)
	}

	_, err = x.getWallet().RegisterBook(context.Background(), &api.RegisterBookRequest{Url: u, Pages: pages})
	return err
}
