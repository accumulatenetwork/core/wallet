package cmd

import (
	"bufio"
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/AccumulateNetwork/jsonrpc2/v15"
	"github.com/ghodss/yaml"
	"github.com/spf13/cobra"
	v3 "gitlab.com/accumulatenetwork/accumulate/pkg/api/v3"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"gitlab.com/accumulatenetwork/core/wallet/internal/flags"
	"golang.org/x/sync/errgroup"
)

var txCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "tx",
		Short: "Create and get token txs",
	}
})

var txSubmitCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "submit [transaction] [signature 0] [...]",
		Short: "Submit an arbitrary transaction + signature(s)",
		Args:  cobra.MinimumNArgs(2),
		Run:   x.runCmdFunc2(x.submit),
	}
}).With((*Context).submitFlags)

var txGetCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "get [id or hash]",
		Short: "Query a transaction",
		Args:  cobra.ExactArgs(1),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.GetTX(cmd, args[0])
		}),
	}
})

var txListCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "list",
		Short: "Lists transactions being composed",
		Args:  cobra.NoArgs,
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.listComposedTransactions(cmd)
		}),
	}
})

var txPendingCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "pending [account] [args]",
		Short: "Query an account's pending transactions",
		Example: "" +
			"  accumulate pending [account]                    List pending transactions\n" +
			"  accumulate pending [account] [hash or index]    Query a pending transaction by hash or index\n" +
			"  accumulate pending [account] [start] [end]      List pending transactions in the given range\n" +
			"",
		Args: cobra.RangeArgs(1, 3),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.GetPendingTx(cmd, args[0], args[1:])
		}),
	}
})

var txHistoryCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "history [account] [start] [end]",
		Short: "Query an account's transaction history",
		Args:  cobra.ExactArgs(3),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.GetTXHistory(cmd, args[0], args[1], args[2])
		}),
	}
})

var txCreateCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "create [sender] [recipient] [amount] [...]",
		Short: "Send tokens",
		Args: func(cmd *cobra.Command, args []string) error {
			err := cobra.MinimumNArgs(3)(cmd, args)
			if err != nil {
				return err
			}
			if len(args)%2 != 1 {
				return fmt.Errorf("odd number of receiver/amount pairs")
			}
			return nil
		},
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.CreateTX(cmd, args[0], args[1:])
		}),
	}
}).With((*Context).submitFlags)

var txExecuteCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "execute",
		Short: "Execute an arbitrary transaction",
		Example: "" +
			"  accumulate tx execute [account] [body]    Execute a transaction with the given principal and body (formatted as yaml or json)\n" +
			"  accumulate tx execute [name]              Execute a stored transaction\n" +
			"",
		Args: cobra.RangeArgs(1, 2),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.ExecuteTX(cmd, args[0], args[1:])
		}),
	}
}).
	With((*Context).submitFlags).
	With(func(ctx *Context, cmd *cobra.Command) {
		cmd.Flags().BoolVar(&ctx.Submit.SkipReview, "no-review", false, "Do not review the transaction to be issued")
	})

var txDeleteCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "delete [name]",
		Short: "Delete a named transaction",
		Args:  cobra.ExactArgs(1),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			_, err := x.getWallet().DeleteTransaction(cmd.Context(), &api.DeleteTransactionRequest{Name: args[0]})
			return "Deleted", err
		}),
	}
})

var txSignCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:     "sign [hash] [...]",
		Short:   "Sign one or more transactions",
		Aliases: []string{"sign-batch"},
		Args:    cobra.MinimumNArgs(1),
		Run: x.runSignFunc(func(cmd *cobra.Command, arg string) (string, error) {
			return x.SignTX(cmd, arg, protocol.VoteTypeAccept)
		}),
	}
}).With((*Context).signFlags)

var txRejectCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "reject [hash] [...]",
		Short: "Reject one or more transactions",
		Args:  cobra.MinimumNArgs(1),
		Run: x.runSignFunc(func(cmd *cobra.Command, arg string) (string, error) {
			return x.SignTX(cmd, arg, protocol.VoteTypeReject)
		}),
	}
}).With((*Context).signFlags)

var txAbstainCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "abstain [hash] [...]",
		Short: "Abstain from one or more transactions",
		Args:  cobra.MinimumNArgs(1),
		Run: x.runSignFunc(func(cmd *cobra.Command, arg string) (string, error) {
			return x.SignTX(cmd, arg, protocol.VoteTypeAbstain)
		}),
	}
}).With((*Context).signFlags)

func (x *Context) runSignFunc(fn func(cmd *cobra.Command, arg string) (string, error)) func(cmd *cobra.Command, args []string) {
	return x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
		if h, err := hex.DecodeString(args[0]); err != nil || len(h) != 32 {
			if u, err := url.Parse(args[0]); err == nil && u.Username() == "" {
				fmt.Fprintf(os.Stderr, "Warning: %v appears to be an account URL, this has been deprecated, instead use just the hash\n", args[0])
				args = args[1:]
			}
		}
		var all string
		for _, arg := range args {
			out, err := fn(cmd, arg)
			if err != nil {
				return "", err
			}
			all += out + "\n"
		}
		return all, nil
	})
}

func init() {
	txCmd.PersistentFlags().DurationVarP(&TxWait, "wait", "w", 0, "Wait for the transaction to complete")
	txCmd.PersistentFlags().DurationVar(&TxWaitSynth, "wait-synth", 0, "Wait for synthetic transactions to complete")
	txCmd.PersistentFlags().BoolVar(&Scratch, "scratch", false, "Read from the scratch chain")
	txCmd.PersistentFlags().BoolVar(&TxLocal, "local", false, "Use query-tx-local instead of query-tx (this is only useful when --server is pointed at a specific BVN)")
	txCmd.PersistentFlags().Var(flags.Url{V: &TxGetFor}, "for", "Determine the partition of this account and query that partition; implies --local")

	txCmd.AddCommand(
		txSubmitCmd,
		txGetCmd,
		txListCmd,
		txPendingCmd,
		txHistoryCmd,
		txCreateCmd,
		txExecuteCmd,
		txDeleteCmd,
		txSignCmd,
		txRejectCmd,
		txAbstainCmd)
}

func (x *Context) GetPendingTx(cmd *cobra.Command, origin string, args []string) (string, error) {
	u, err := url.Parse(origin)
	if err != nil {
		return "", err
	}
	//<record>#pending/<hash> - fetch an envelope by hash
	//<record>#pending/<index> - fetch an envelope by index/height
	//<record>#pending/<start>:<end> - fetch a range of envelope by index/height
	params := client.UrlQuery{}

	var out string
	var perr error
	switch len(args) {
	case 0:
		//query with no parameters
		u = u.WithFragment("pending")
		params.Url = u
		res := client.MultiResponse{}
		err = queryAs("query", &params, &res)
		if err != nil {
			return "", err
		}
		out, perr = x.PrintMultiResponse(cmd, &res)
	case 1:
		if len(args[0]) == 64 {
			//this looks like a transaction hash, now check it
			txid, err := hex.DecodeString(args[0])
			if err != nil {
				return "", fmt.Errorf("cannot decode transaction id")
			}
			u = u.WithFragment(fmt.Sprintf("pending/%x", txid))
		} else {
			height, err := strconv.ParseUint(args[0], 10, 64)
			if err != nil {
				return "", fmt.Errorf("expecting height, but could not convert argument, %v", err)
			}
			u = u.WithFragment(fmt.Sprintf("pending/%d", height))
		}
		params.Url = u
		res := client.TransactionQueryResponse{}
		err = queryAs("query", &params, &res)
		if err != nil {
			return "", err
		}
		out, perr = x.PrintTransactionQueryResponseV2(&res)
	case 2:
		//pagination
		start, err := strconv.Atoi(args[0])
		if err != nil {
			return "", fmt.Errorf("error converting start index %v", err)
		}
		count, err := strconv.Atoi(args[1])
		if err != nil {
			return "", fmt.Errorf("error converting count %v", err)
		}
		u = u.WithFragment(fmt.Sprintf("pending/%d:%d", start, count))
		params.Url = u
		res := client.MultiResponse{}
		err = queryAs("query", &params, &res)
		if err != nil {
			return "", err
		}
		out, perr = x.PrintMultiResponse(cmd, &res)
	default:
		return "", fmt.Errorf("invalid number of arguments")
	}

	return out, perr
}

func getTxUsingUrl(txID *url.TxID, wait time.Duration, ignorePending bool) (*client.TransactionQueryResponse, error) {
	params := new(client.TxnQuery)
	params.TxIdUrl = txID
	params.Prove = Prove
	params.IgnorePending = ignorePending

	return getTX(wait, params)
}

func getTxUsingHash(hash []byte, wait time.Duration, ignorePending bool) (*client.TransactionQueryResponse, error) {
	params := new(client.TxnQuery)
	params.Txid = hash
	params.Prove = Prove
	params.IgnorePending = ignorePending

	return getTX(wait, params)
}

func getTX(wait time.Duration, req *client.TxnQuery) (*client.TransactionQueryResponse, error) {
	var res client.TransactionQueryResponse

	if wait > 0 {
		req.Wait = wait

		t := Client.Timeout
		defer func() { Client.Timeout = t }()

		Client.Timeout = TxWait * 2
	}

	b, err := json.Marshal(req)
	if err != nil {
		return nil, err
	}

	method := "query-tx"
	if TxLocal || TxGetFor != nil {
		method = "query-tx-local"
		b = append(b[:len(b)-1], []byte(`,"includeRemote": true}`)...)
	}
	err = Client.RequestAPIv2(context.Background(), method, json.RawMessage(b), &res)
	if err != nil {
		return nil, err
	}
	return &res, nil
}

func parseHashOrTxid(s string) (*url.TxID, error) {
	hash, err := hex.DecodeString(s)
	if err == nil {
		return protocol.AccountUrl(protocol.Unknown).WithTxID(*(*[32]byte)(hash)), nil
	}

	txid, err := url.ParseTxID(s)
	if err == nil {
		return txid, nil
	}

	return nil, errors.BadRequest.WithFormat("invalid argument: %q is not a transaction hash or ID", s)
}

func getTX2(txid *url.TxID) (*client.TransactionQueryResponse, error) {
	req := new(client.TxnQuery)
	req.Prove = Prove
	if protocol.IsUnknown(txid.Account()) {
		h := txid.Hash()
		req.Txid = h[:]
	} else {
		req.TxIdUrl = txid
	}

	if TxGetFor == nil {
		return getTX(TxWait, req)
	}

	partition, server, err := routeAccount(TxGetFor)
	if err != nil {
		return nil, err
	}
	if server == "" {
		return nil, errors.UnknownError.WithFormat("no server found for %v", partition)
	}

	c := Client
	defer func() { Client = c }()
	Client, err = client.New(server)
	if err != nil {
		return nil, err
	}

	Client.DebugRequest = ClientDebug
	return getTX(TxWait, req)
}

func (x *Context) listComposedTransactions(cmd *cobra.Command) (string, error) {
	txns, err := x.getWallet().ListTransactions(cmd.Context(), &api.ListTransactionsRequest{})
	if err != nil {
		return "", err
	}

	if WantJsonOutput {
		b, err := json.Marshal(txns)
		if err != nil {
			return "", err
		}
		return string(b), nil
	}

	var s string
	for _, name := range txns.Names {
		s += fmt.Sprintln(name)
	}
	return s, nil
}

func (x *Context) GetTX(cmd *cobra.Command, hashOrUrl string) (string, error) {
	txid, err := parseHashOrTxid(hashOrUrl)
	if err != nil {
		txn, e := x.getWallet().GetTransaction(cmd.Context(), &api.GetTransactionRequest{Name: hashOrUrl})
		if e != nil {
			return "", err
		}

		return x.outputForHumansTx2(txn.Transaction)
	}

	res, err := getTX2(txid)
	if err != nil {
		return PrintJsonRpcError(err)
	}

	out, err := x.PrintTransactionQueryResponseV2(res)
	if err != nil {
		return "", err
	}

	if TxWaitSynth == 0 || len(res.Produced) == 0 {
		return out, nil
	}

	if TxWaitSynth > 0 {
		Client.Timeout = TxWaitSynth * 2
	}

	errg := new(errgroup.Group)
	for _, txid := range res.Produced {
		lclTxId := txid
		errg.Go(func() error {
			res, err := getTxUsingUrl(lclTxId, TxWaitSynth, true)
			if err != nil {
				return err
			}

			o, err := x.PrintTransactionQueryResponseV2(res)
			if err != nil {
				return err
			}

			if WantJsonOutput {
				out += "\n"
			}
			out += o
			return nil
		})
	}
	err = errg.Wait()
	if err != nil {
		var rpcErr jsonrpc2.Error
		if errors.As(err, &rpcErr) {
			return PrintJsonRpcError(err)
		}
		return PrintJsonRpcError(err)
	}

	return out, nil
}

func (x *Context) GetTXHistory(cmd *cobra.Command, accountUrl string, startArg string, endArg string) (string, error) {
	var res client.MultiResponse
	start, err := strconv.Atoi(startArg)
	if err != nil {
		return "", err
	}
	end, err := strconv.Atoi(endArg)
	if err != nil {
		return "", err
	}
	u, err := url.Parse(accountUrl)
	if err != nil {
		return "", err
	}

	params := new(client.TxHistoryQuery)
	params.UrlQuery.Url = u
	params.QueryPagination.Start = uint64(start)
	params.QueryPagination.Count = uint64(end)
	params.Scratch = Scratch

	data, err := json.Marshal(params)
	if err != nil {
		return "", err
	}

	if err := Client.RequestAPIv2(context.Background(), "query-tx-history", json.RawMessage(data), &res); err != nil {
		return PrintJsonRpcError(err)
	}

	return x.PrintMultiResponse(cmd, &res)
}

func (x *Context) CreateTX(cmd *cobra.Command, sender string, args []string) (string, error) {
	//sender string, receiver string, amount string
	u, err := url.Parse(sender)
	if err != nil {
		return "", err
	}

	signer, err := x.getSigners(cmd, u, nil)
	if err != nil {
		return "", err
	}

	tokenUrl, err := GetTokenUrlFromAccount(u)
	if err != nil {
		return "", fmt.Errorf("invalid token url was obtained from %s, %v", u.String(), err)
	}

	if len(args)%2 != 0 {
		return "", fmt.Errorf("odd number of receiver/amount pairs")
	}

	send := new(protocol.SendTokens)
	for i := 0; i < len(args); i += 2 {
		u2, err := url.Parse(args[i])
		if err != nil {
			return "", fmt.Errorf("invalid receiver url %s, %v", args[0], err)
		}

		amount := args[i+1]

		amt, err := amountToBigInt(tokenUrl.String(), amount)
		if err != nil {
			return "", err
		}

		send.AddRecipient(u2, amt)
	}

	seen := map[string]bool{}
	for _, r := range send.To {
		s := r.Url.String() + " " + r.Amount.String()
		if seen[s] {
			return "", fmt.Errorf("duplicate outputs for %v", r.Url)
		}
		seen[s] = true
	}

	return x.dispatchTxAndPrintResponse(cmd, send, u, signer)
}

func waitForTxnUsingHash(hash []byte, wait time.Duration, ignorePending bool) ([]*client.TransactionQueryResponse, error) {
	var queryResponses []*client.TransactionQueryResponse
	queryRes, err := getTxUsingHash(hash, wait, ignorePending)
	if err != nil {
		return nil, err
	}
	return waitForTxn(queryResponses, queryRes, wait)
}

func waitForTxn(queryResponses []*client.TransactionQueryResponse, queryRes *client.TransactionQueryResponse, wait time.Duration) ([]*client.TransactionQueryResponse, error) {
	queryResponses = append(queryResponses, queryRes)
	if queryRes.Produced != nil {
		for _, txid := range queryRes.Produced {
			txhash := txid.Hash()
			resp, err := waitForTxnUsingHash(txhash[:], wait, true)
			if err != nil {
				return nil, err
			}
			queryResponses = append(queryResponses, resp...)
		}
	}
	return queryResponses, nil
}

func (x *Context) ExecuteTX(cmd *cobra.Command, sender string, args []string) (string, error) {
	// If tx execute is called without a payload, assume [sender] is meant to be
	// the name of a built transaction
	var txn *protocol.Transaction
	if len(args) == 0 {
		resp, err := x.getWallet().GetTransaction(cmd.Context(), &api.GetTransactionRequest{Name: sender})
		if err != nil {
			return "", fmt.Errorf("load %v: %v", sender, err)
		}
		txn = resp.Transaction

	} else {
		u, err := url.Parse(sender)
		if err != nil {
			return "", err
		}
		txn = x.buildTransaction(nil, u)
	}

	signer, err := x.getSigners(cmd, txn.Header.Principal, nil)
	if err != nil {
		return "", fmt.Errorf("unable to prepare signer, %v", err)
	}

	if txn.Body == nil {
		txn.Body, err = parseTx(cmd, args[0])
		if err != nil {
			return "", err
		}
	}

	if !x.Submit.SkipReview {
		out, err := x.outputForHumansTx2(txn)
		if err != nil {
			return "", err
		}
		fmt.Println(out)
		fmt.Println()
		fmt.Print("Is this the transaction you wish to execute? Ctrl-C to abort.")
		_, err = bufio.NewReader(os.Stdin).ReadString('\n')
		if err != nil {
			return "", err
		}
	}

	return x.dispatchTxAndPrintResponse(cmd, txn, nil, signer)
}

func parseTx(cmd *cobra.Command, s string) (protocol.TransactionBody, error) {
	var typ struct {
		Type protocol.TransactionType
	}
	err := yaml.Unmarshal([]byte(s), &typ)
	if err != nil {
		return nil, fmt.Errorf("invalid payload 1: %v", err)
	}

	txn, err := protocol.NewTransactionBody(typ.Type)
	if err != nil {
		return nil, fmt.Errorf("invalid payload 2: %v", err)
	}

	err = yaml.Unmarshal([]byte(s), txn)
	if err != nil {
		return nil, fmt.Errorf("invalid payload 3: %v", err)
	}

	return txn, nil
}

func (x *Context) SignTX(cmd *cobra.Command, hashStr string, vote protocol.VoteType) (string, error) {
	var txid *url.TxID
	if h, err := hex.DecodeString(hashStr); err == nil {
		txid = protocol.UnknownUrl().WithTxID(*(*[32]byte)(h))
	} else if x, e2 := url.ParseTxID(hashStr); e2 == nil {
		txid = protocol.UnknownUrl().WithTxID(x.Hash())
	} else {
		return "", err
	}

	// This is a workaround for a simulator bug
	Q := v3.Querier2{Querier: &v3.Collator{Querier: Q, Network: ClientV3}}

	r, err := Q.QueryTransaction(cmd.Context(), txid, nil)
	if err != nil {
		return "", err
	}

	signers, err := x.getSigners(cmd, r.Message.Transaction.Header.Principal, r.Message.Transaction)
	if err != nil {
		return "", err
	}
	for _, signer := range signers {
		signer.Vote = vote
		signer.Memo = x.Sig.Memo
		signer.Metadata = x.Sig.Metadata
	}

	return x.dispatchTxAndPrintResponse(cmd, r.Message.Transaction, r.Message.Transaction.Header.Principal, signers)
}

func (x *Context) submit(cmd *cobra.Command, args []string) (string, error) {
	txn := new(protocol.Transaction)
	err := txn.UnmarshalJSON([]byte(args[0]))
	if err != nil {
		return "", err
	}

	var sigs []protocol.Signature
	for _, arg := range args[1:] {
		sig, err := protocol.UnmarshalSignatureJSON([]byte(arg))
		if err != nil {
			return "", err
		}
		sigs = append(sigs, sig)
	}

	req := new(client.ExecuteRequest)
	req.Envelope = &messaging.Envelope{Transaction: []*protocol.Transaction{txn}, Signatures: sigs}
	if x.Submit.Pretend {
		req.CheckOnly = true
	}

	res, err := Client.ExecuteDirect(context.Background(), req)
	if err != nil {
		return PrintJsonRpcError(err)
	}
	if res.Code != 0 {
		result := new(protocol.TransactionStatus)
		if Remarshal(res.Result, result) != nil {
			return "", errors.EncodingError.With(res.Message)
		}
		return "", result.Error
	}

	var resps []*client.TransactionQueryResponse
	if TxWait != 0 {
		resps, err = waitForTxnUsingHash(res.TransactionHash, TxWait, TxIgnorePending)
		if err != nil {
			return PrintJsonRpcError(err)
		}
	}

	result, err := ActionResponseFrom(res).Print(x)
	if err != nil {
		return "", err
	}
	if res.Code == 0 {
		for _, response := range resps {
			str, err := x.PrintTransactionQueryResponseV2(response)
			if err != nil {
				return PrintJsonRpcError(err)
			}
			result = fmt.Sprint(result, str, "\n")
		}
	}
	return result, nil
}
