package cmd

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"math/big"
	"sort"
	"strconv"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

func init() {
	accountCmd.AddCommand(
		accountHistoryCmd,
	)
	accountHistoryCmd.AddCommand(
		accountHistoryMajorBlockCmd,
	)
}

var accountHistoryCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "history",
		Short: "Show the history of an account",
	}
})

var accountHistoryMajorBlockCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "major-block <account> <start> [<end>]",
		Short: "Show the history of an account from a major block or a range (inclusive)",
		Args:  cobra.RangeArgs(2, 3),
		Run:   x.runCmdFunc(majorBlockAccountHistory),
	}
})

func majorBlockAccountHistory(args []string) (string, error) {
	// Parse parameters
	url, err := url.Parse(args[0])
	if err != nil {
		return "", errors.BadRequest.WithFormat("invalid account: %w", err)
	}

	start, err := strconv.ParseUint(args[1], 10, 64)
	if err != nil {
		return "", errors.BadRequest.WithFormat("invalid start: %w", err)
	}

	end := start
	if len(args) > 2 {
		end, err = strconv.ParseUint(args[2], 10, 64)
		if err != nil {
			return "", errors.BadRequest.WithFormat("invalid end: %w", err)
		}
	}

	// Compare range to the major block height
	account, err := getAccount("dn.acme/anchors")
	if err != nil {
		return "", err
	}
	anchors, ok := account.(*protocol.AnchorLedger)
	if !ok {
		return "", errors.InternalError.WithFormat("unexpected account type: want %v, got %v", protocol.AccountTypeAnchorLedger, account.Type())
	}
	if start >= anchors.MajorBlockIndex {
		if WantJsonOutput {
			return "[]", nil
		}
		return fmt.Sprintf("No history: current major block height is %d", anchors.MajorBlockIndex-1), nil
	}

	// Find what partition the account belongs to
	partition, _, err := routeAccount(url)
	if err != nil {
		return "", err
	}

	// Convert major block index to minor block index
	var firstMinor, lastMinor uint64
	if start > 1 {
		firstMinor, err = majorToMinor(partition, start-1)
		if err != nil {
			return "", err
		}
	}
	if end >= anchors.MajorBlockIndex {
		account, err := getAccount(protocol.PartitionUrl(partition).JoinPath("ledger").String())
		if err != nil {
			return "", err
		}
		ledger, ok := account.(*protocol.SystemLedger)
		if !ok {
			return "", errors.InternalError.WithFormat("unexpected account type: want %v, got %v", protocol.AccountTypeSystemLedger, account.Type())
		}
		lastMinor = ledger.Index
	} else {
		lastMinor, err = majorToMinor(partition, end)
		if err != nil {
			return "", err
		}
	}

	// Find the transaction range
	firstTxn, lastTxn, err := findTransactionRange(url, firstMinor, lastMinor)
	if err != nil {
		return "", err
	}
	if firstTxn == lastTxn {
		if WantJsonOutput {
			return "[]", nil
		}
		return fmt.Sprintf("%s has no history for the given range", url), nil
	}

	// Get the token issuer, when applicable
	account, err = getAccount(url.String())
	if err != nil {
		return "", err
	}
	var issuer *protocol.TokenIssuer
	switch account := account.(type) {
	case *protocol.TokenIssuer:
		issuer = account
	case protocol.AccountWithTokens:
		a, err := getAccount(account.GetTokenUrl().String())
		if err != nil {
			return "", err
		}
		var ok bool
		issuer, ok = a.(*protocol.TokenIssuer)
		if !ok {
			return "", errors.Conflict.With("token account's token URL resolves to an account that is not an issuer")
		}
	}

	// Get all the transactions for the minor block range
	req2 := new(client.TxHistoryQuery)
	req2.Url = url
	req2.Start = uint64(firstTxn)
	req2.Count = uint64(lastTxn - firstTxn)
	res2, err := Client.QueryTxHistory(context.Background(), req2)
	if err != nil {
		return "", errors.UnknownError.WithFormat("query transaction history: %w", err)
	}

	if WantJsonOutput {
		b, err := json.Marshal(res2.Items)
		return string(b), err
	}

	// Display
	tw := table.NewWriter()
	tw.Style().Options.DrawBorder = false

	total := new(big.Int)
	for _, raw := range res2.Items {
		res, err := RemarshalAs[client.TransactionQueryResponse](raw)
		if err != nil {
			return "", errors.UnknownError.WithFormat("remarshal response: %w", err)
		}

		switch body := res.Transaction.Body.(type) {
		case *protocol.SyntheticForwardTransaction:
			// Skip
			continue

		case *protocol.SendTokens:
			for _, to := range body.To {
				total.Sub(total, &to.Amount)
				tw.AppendRow(table.Row{"Sent", fmt.Sprintf("%s %s to %v", protocol.FormatBigAmount(&to.Amount, int(issuer.Precision)), issuer.Symbol, to.Url), hex.EncodeToString(res.TransactionHash)})
			}

		case *protocol.WriteData:
			for _, b := range body.Entry.GetData() {
				tw.AppendRow(table.Row{"Wrote", fmt.Sprintf("%d bytes", len(b)), hex.EncodeToString(res.TransactionHash)})
			}

		case *protocol.SyntheticWriteData:
			for _, b := range body.Entry.GetData() {
				tw.AppendRow(table.Row{"Wrote", fmt.Sprintf("%d bytes", len(b)), hex.EncodeToString(res.TransactionHash)})
			}

		case *protocol.SystemWriteData:
			for _, b := range body.Entry.GetData() {
				tw.AppendRow(table.Row{"Wrote", fmt.Sprintf("%d bytes", len(b)), hex.EncodeToString(res.TransactionHash)})
			}

		case *protocol.WriteDataTo:
			for _, b := range body.Entry.GetData() {
				tw.AppendRow(table.Row{"Wrote", fmt.Sprintf("%d bytes to %v", len(b), body.Recipient), hex.EncodeToString(res.TransactionHash)})
			}

		case *protocol.BurnTokens:
			total.Sub(total, &body.Amount)
			tw.AppendRow(table.Row{"Burned", fmt.Sprintf("%s %s", protocol.FormatBigAmount(&body.Amount, int(issuer.Precision)), issuer.Symbol), hex.EncodeToString(res.TransactionHash)})

		case *protocol.AddCredits:
			total.Sub(total, &body.Amount)
			tw.AppendRow(table.Row{"Spent", fmt.Sprintf("%s ACME on credits", protocol.FormatBigAmount(&body.Amount, protocol.AcmePrecisionPower)), hex.EncodeToString(res.TransactionHash)})

		case *protocol.SyntheticDepositTokens:
			if body.Token.Equal(protocol.AcmeUrl()) {
				total.Add(total, &body.Amount)
			}
			tw.AppendRow(table.Row{"Deposited", fmt.Sprintf("%s %s", protocol.FormatBigAmount(&body.Amount, int(issuer.Precision)), issuer.Symbol), hex.EncodeToString(res.TransactionHash)})

		case *protocol.SyntheticDepositCredits:
			tw.AppendRow(table.Row{"Deposited", fmt.Sprintf("%s credits", protocol.FormatAmount(body.Amount, protocol.CreditPrecisionPower)), hex.EncodeToString(res.TransactionHash)})

		case *protocol.IssueTokens:
			total.Add(total, &body.Amount)
			tw.AppendRow(table.Row{"Issued", fmt.Sprintf("%s %s to %v", protocol.FormatBigAmount(&body.Amount, int(issuer.Precision)), issuer.Symbol, body.Recipient), hex.EncodeToString(res.TransactionHash)})
			for _, to := range body.To {
				total.Sub(total, &to.Amount)
				tw.AppendRow(table.Row{"Issued", fmt.Sprintf("%s %s to %v", protocol.FormatBigAmount(&to.Amount, int(issuer.Precision)), issuer.Symbol, to.Url), hex.EncodeToString(res.TransactionHash)})
			}

		case *protocol.SyntheticBurnTokens:
			total.Sub(total, &body.Amount)
			tw.AppendRow(table.Row{"Burned", fmt.Sprintf("%s %s", protocol.FormatBigAmount(&body.Amount, int(issuer.Precision)), issuer.Symbol), hex.EncodeToString(res.TransactionHash)})

		case *protocol.BlockValidatorAnchor:
			if body.AcmeBurnt.Cmp(new(big.Int)) == 0 {
				continue
			}
			total.Sub(total, &body.AcmeBurnt)
			tw.AppendRow(table.Row{"Burned", fmt.Sprintf("%s ACME", protocol.FormatBigAmount(&body.AcmeBurnt, protocol.AcmePrecisionPower)), hex.EncodeToString(res.TransactionHash)})

		default:
			tw.AppendRow(table.Row{res.Transaction.Body.Type(), "", hex.EncodeToString(res.TransactionHash)})
		}
	}
	if issuer != nil {
		tw.AppendRow(table.Row{""})
		tw.AppendRow(table.Row{"Total", fmt.Sprintf("%s %s", protocol.FormatBigAmount(total, int(issuer.Precision)), issuer.Symbol)})
	}
	return tw.Render(), nil
}

func majorToMinor(partition string, major uint64) (uint64, error) {
	// Get major block index entry
	anchors := protocol.PartitionUrl(partition).JoinPath(protocol.AnchorPool)
	req := new(client.GeneralQuery)
	req.Url = anchors.WithFragment(fmt.Sprintf("chain/major-block/%d", major))
	ie := new(protocol.IndexEntry)
	ce := new(client.ChainEntry)
	ce.Value = ie
	res := new(client.ChainQueryResponse)
	res.Data = ce
	err := Client.RequestAPIv2(context.Background(), "query", req, res)
	if err != nil {
		return 0, errors.UnknownError.WithFormat("query major block: %w", err)
	}

	// Get root chain index entry
	ledger := protocol.PartitionUrl(partition).JoinPath(protocol.Ledger)
	req.Url = ledger.WithFragment(fmt.Sprintf("chain/root-index/%d", ie.RootIndexIndex))
	ie = new(protocol.IndexEntry)
	ce = new(client.ChainEntry)
	ce.Value = ie
	res = new(client.ChainQueryResponse)
	res.Data = ce
	err = Client.RequestAPIv2(context.Background(), "query", req, res)
	if err != nil {
		return 0, errors.UnknownError.WithFormat("query root index: %w", err)
	}

	return ie.BlockIndex, nil
}

func findTransactionRange(account *url.URL, firstBlock, lastBlock uint64) (firstTxn, lastTxn int, err error) {
	req := new(client.GeneralQuery)
	req.Url = account.WithQuery("count=0").WithFragment("chain/main")
	mres := new(client.MultiResponse)
	err = Client.RequestAPIv2(context.Background(), "query", req, mres)
	if err != nil {
		return 0, 0, errors.UnknownError.WithFormat("query main chain history: %w", err)
	}
	if mres.Total == 0 {
		return 0, 0, nil
	}

	// Using panics for control flow is not great, but it's more readable than
	// anything else I could think of
	defer func() {
		r := recover()
		if r == nil {
			return
		}
		c, ok := r.(cancelError)
		if !ok {
			panic(r)
		}
		err = c.Error
	}()

	firstTxn = sort.Search(int(mres.Total), func(i int) bool {
		return getMainChainBlock(account, i) > firstBlock
	})
	if firstTxn == int(mres.Total) {
		return 0, 0, errors.NotFound.WithFormat("no transactions exist on %v between %d and %d", account, firstBlock, lastBlock)
	}

	lastTxn = sort.Search(int(mres.Total)-firstTxn, func(i int) bool {
		return getMainChainBlock(account, firstTxn+i) > lastBlock
	}) + firstTxn
	return firstTxn, lastTxn, nil
}

type cancelError struct {
	Error error
}

func getMainChainBlock(account *url.URL, i int) uint64 {
	req := new(client.GeneralQuery)
	req.Url = account.WithFragment(fmt.Sprintf("txn/%d", i))
	req.Prove = true
	res := new(client.TransactionQueryResponse)
	err := Client.RequestAPIv2(context.Background(), "query", req, res)
	if err != nil {
		panic(cancelError{Error: errors.UnknownError.WithFormat("query main chain history: %w", err)})
	}
	for _, r := range res.Receipts {
		if r.Chain == "main" {
			return r.LocalBlock
		}
	}

	panic(cancelError{Error: errors.NotFound.WithFormat("missing main chain receipt for %v#txn/%d", account, i)})
}
