package cmd

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	tmed25519 "github.com/cometbft/cometbft/crypto/ed25519"
	tmjson "github.com/cometbft/cometbft/libs/json"
	"github.com/cometbft/cometbft/privval"
	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/accumulate/exp/apiutil"
	v3 "gitlab.com/accumulatenetwork/accumulate/pkg/api/v3"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/address"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/daemon/services"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/types"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/interactive"
)

func init() {
	keyImportCmd.AddCommand(keyImportPrivateCmd)
	keyImportCmd.AddCommand(keyImportFactoidCmd)
	keyImportCmd.AddCommand(keyImportLiteCmd)
	keyExportCmd.AddCommand(keyExportPrivateCmd)
	keyExportCmd.AddCommand(keyExportMnemonicCmd)
	keyExportCmd.AddCommand(keyExportAllCmd)
	keyExportCmd.AddCommand(keyExportSeedCmd)

	keyCmd.AddCommand(keyImportCmd)
	keyCmd.AddCommand(keyExportCmd)
	keyCmd.AddCommand(keyGenerateCmd)
	keyCmd.AddCommand(keyListCmd)
	keyCmd.AddCommand(keyShowCmd)
	keyCmd.AddCommand(keyLabelCmd)
	keyCmd.AddCommand(keyRestoreCounterCmd)
	keyLabelCmd.AddCommand(keyLabelAssignCmd, keyLabelRenameCmd, keyLabelRemoveCmd)
	keyImportPrivateCmd.Flags().StringVar(&SigType, "sigtype", "ed25519", "Specify the signature type use rcd1 for RCD1 type ; ed25519 for accumulate ed25519 ; btc for Bitcoin ; btclegacy for Legacy Bitcoin  ; eth for Ethereum ")
	keyImportLiteCmd.Flags().StringVar(&SigType, "sigtype", "ed25519", "Specify the signature type use rcd1 for RCD1 type ; ed25519 for accumulate ED25519 ; btc for Bitcoin ; btclegacy for Legacy Bitcoin  ; eth for Ethereum ")
	keyGenerateCmd.Flags().StringVar(&SigType, "sigtype", "ed25519", "Specify the signature type use rcd1 for RCD1 type ; ed25519 for accumulate ED25519 ; btc for Bitcoin ; btclegacy for Legacy Bitcoin  ; eth for Ethereum ")
	keyImportPrivateCmd.Flags().BoolVarP(&flagKey.Force, "force", "f", false, "If there is an existing external key, overwrite it")
	keyGenerateCmd.Flags().BoolVarP(&flagKey.Force, "force", "f", false, "If there is an existing external key, overwrite it")
	keyListCmd.Flags().BoolVarP(&flagKey.Verbose, "verbose", "", false, "verbose output")
	keyLabelAssignCmd.Flags().BoolVarP(&flagKey.Force, "force", "f", false, "If there is an key label by that name, overwrite it")
	keyLabelRenameCmd.Flags().BoolVarP(&flagKey.Force, "force", "f", false, "If there is an key label by that name, overwrite it")
}

var flagKey = struct {
	Force   bool
	Verbose bool
}{}

var keyImportCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "import",
		Short: "Import private key from hex or factoid secret address",
	}
})

var keyImportPrivateCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "private [key name/label]",
		Short: "Import private key in hex from terminal input",
		Args:  cobra.RangeArgs(1, 2),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			if len(args) == 2 {
				return x.importFilePV(cmd, args[0], args[1])
			}

			var sigType protocol.SignatureType
			var found bool
			if cmd.Flag("sigtype").Changed {
				sigType, found = protocol.SignatureTypeByName(SigType)
				if !found {
					return "", fmt.Errorf("unknown signature type %s", SigType)
				}
			}
			return x.ImportKeyPrompt(cmd, args[0], sigType)
		}),
	}
})

var keyImportFactoidCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "factoid",
		Short: "Import secret factoid key from terminal input",
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.ImportFactoidKey(cmd)
			x.printOutput(cmd, out, err)
		},
	}
})

var keyImportLiteCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "lite",
		Short: "Import private key in hex and derive label for the key",
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			var out string
			var err error
			var sigType protocol.SignatureType
			var found bool
			if SigType != "" {
				sigType, found = protocol.SignatureTypeByName(SigType)
				if !found {
					err = fmt.Errorf("unknown signature type %s", SigType)
				}
			}
			if err == nil {
				out, err = x.ImportKeyPrompt(cmd, "", sigType)
			}
			x.printOutput(cmd, out, err)
		},
	}
})

var keyListCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "list",
		Short: "list keys in the wallet",
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.listKeys(cmd)
			x.printOutput(cmd, out, err)
		},
	}
})

var keyShowCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "show [label]",
		Short: "show a key's public address and labels",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.showKey(cmd, args[0])
			x.printOutput(cmd, out, err)
		},
	}
})

var _ = keyCmd.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:    "route",
		Short:  "show which partition a key or keys routes to",
		Hidden: true,
		Run:    x.runCmdFunc2(x.routeKeyCmd),
	}
})

var keyExportAllCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "all",
		Short: "export wallet with private keys and accounts",
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.ExportKeys(cmd)
			x.printOutput(cmd, out, err)
		},
	}
})

var keyExportMnemonicCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "mnemonic",
		Short: "export mnemonic phrase",
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.ExportMnemonic(cmd)
			x.printOutput(cmd, out, err)
		},
	}
})

var keyExportSeedCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "seed",
		Short: "export key seed",
		Args:  cobra.ExactArgs(0),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.ExportSeed(cmd)
			x.printOutput(cmd, out, err)
		},
	}
})

var keyExportPrivateCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "private [key name/label]",
		Short: "export key private",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.ExportKey(cmd, args[0])
			x.printOutput(cmd, out, err)
		},
	}
})

var keyExportCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "export",
		Short: "export wallet private data and accounts",
	}
})

var keyGenerateCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "generate [key name/label]",
		Short: "generate key private and give it a name",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.GenerateKey(cmd, args[0])
			x.printOutput(cmd, out, err)
		},
	}
})

var keyLabelAssignCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use: "assign [existing label/name] [new key label]",
		Short: "assign public key of existing label to a new label, " +
			"note: the new key label will also map to the key pointed to by the existing label resulting in multiple " +
			"names pointing to the same key",
		Args: cobra.ExactArgs(2),
		Run:  x.runCmdFunc2(x.assignKeyLabel),
	}
})

var keyLabelRenameCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "rename [existing label/name] [new key name]",
		Short: "rename a key label to new label, note: the old key name will be replaced with the new key name",
		Args:  cobra.ExactArgs(2),
		Run:   x.runCmdFunc2(x.renameKeyLabel),
	}
})

var keyLabelRemoveCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "remove [existing label/name]",
		Short: "remove a key label",
		Args:  cobra.ExactArgs(1),
		Run:   x.runCmdFunc2(x.removeKeyLabel),
	}
})
var keyCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "key",
		Short: "Create and manage Keys for accounts, ADI Key Books / Pages",
	}
})

var keyLabelCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "label [assign | rename]",
		Short: "Manage names/aliases for keys in wallet",
	}
})

var keyRestoreCounterCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "restore-counters",
		Short: "Restores key counters",
		Long:  "Repairs corrupted key counters by scanning the vault and updating counters to reflect all generated keys. This is only necessary to repair a vault with corrupted key counters.",
		Run:   x.runCmdFunc2(x.restoreKeyCounters),
	}
})

type KeyResponse struct {
	Lite          types.String   `json:"name,omitempty"`
	Addresses     []types.String `json:"addresses,omitempty"`
	Names         []types.String `json:"names,omitempty"`
	PrivateKey    types.Bytes    `json:"privateKey,omitempty"`
	PublicKey     types.Bytes    `json:"publicKey,omitempty"`
	KeyInfo       api.KeyInfo    `json:"keyInfo,omitempty"`
	KeyHash       types.Bytes    `json:"keyHash,omitempty"`
	NativeAddress types.String   `json:"nativeAddress,omitempty"`
	LastUsedOn    uint64         `json:"lastUsedOn,omitempty"`
	LiteAccount   *url.URL       `json:"liteAccount,omitempty"`
	Seed          types.Bytes    `json:"seed,omitempty"`
	Mnemonic      types.String   `json:"mnemonic,omitempty"`
}

func responseForAddr(a address.Address, labels ...string) *KeyResponse {
	r := new(KeyResponse)
	r.Addresses = []types.String{types.String(a.String())}
	r.PrivateKey, _ = a.GetPrivateKey()
	r.PublicKey, _ = a.GetPublicKey()
	r.KeyHash, _ = a.GetPublicKeyHash()
	r.KeyInfo.Type = a.GetType()

	for _, l := range labels {
		r.Names = append(r.Names, types.String(l))
	}

	// Add an MH address
	if r.KeyHash != nil && a.GetType() != protocol.SignatureTypeUnknown {
		r.Addresses = append(r.Addresses, types.String((&address.UnknownHash{Hash: r.KeyHash}).String()))
	}

	lt, err := protocol.LiteTokenAddressFromHash(r.KeyHash, protocol.ACME)
	if err != nil {
		log.Fatal("Could not create LTA for hash: ", err)
	}
	r.LiteAccount = lt

	switch a := a.(type) {
	case *services.Key:
		r.LastUsedOn = a.LastUsedOn
		r.KeyInfo = a.KeyInfo
		if a.Labels != nil {
			r.Lite = types.String(a.Labels.Lite)
			for _, l := range a.Labels.Names {
				r.Names = append(r.Names, types.String(l))
			}
		}

	case *address.PrivateKey:
		r.Addresses = []types.String{
			types.String(a.PublicKey.String()),                             // Known-type
			types.String((&address.UnknownHash{Hash: r.KeyHash}).String()), // MH
		}
	}

	return r
}

func (r *KeyResponse) Print() string {
	if WantJsonOutput {
		s, err := json.Marshal(r)
		if err != nil {
			// Should never happen
			log.Fatal("Failed to marshal key response: ", err)
		}
		return string(s) + "\n"
	}

	tw := table.NewWriter()

	if len(r.Names) > 0 {
		var names []string
		for _, n := range r.Names {
			names = append(names, *n.AsString())
		}
		tw.AppendRow([]interface{}{"Names", strings.Join(names, ", ")})
	}
	if r.LiteAccount != nil {
		tw.AppendRow([]interface{}{"Lite Account", r.LiteAccount})
	}
	for i, a := range r.Addresses {
		if i == 0 {
			tw.AppendRow([]interface{}{"Public Address", a})
		} else {
			tw.AppendRow([]interface{}{"", a})
		}
	}
	if r.PrivateKey != nil {
		sk := new(address.PrivateKey)
		sk.Type = r.KeyInfo.Type
		sk.Key = r.PrivateKey
		sk.PublicKey.Key = r.PublicKey
		tw.AppendRow([]interface{}{"Private Key", sk.String()})
	}
	tw.AppendRow([]interface{}{"Key Type", r.KeyInfo.Type.String()})
	if r.KeyInfo.Derivation != "" {
		tw.AppendRow([]interface{}{"Derivation", r.KeyInfo.Derivation})
	}
	if r.KeyInfo.WalletId != nil {
		tw.AppendRow([]interface{}{"Wallet ID", r.KeyInfo.WalletId})
	}
	if r.LastUsedOn > 0 {
		// Print the timestamp as a date if it is within this century
		ts := time.UnixMilli(int64(r.LastUsedOn))
		a := time.Date(2000, 0, 0, 0, 0, 0, 0, time.UTC)
		b := time.Date(2100, 0, 0, 0, 0, 0, 0, time.UTC)
		if a.Before(ts) && ts.Before(b) {
			tw.AppendRow([]any{"Last used on", fmt.Sprintf("%v (%d)", ts, r.LastUsedOn)})
		} else {
			tw.AppendRow([]any{"Last used on", r.LastUsedOn})
		}
	}

	tw.Style().Options.DrawBorder = false
	return tw.Render()
}

func PrintKeyPublic() {
	fmt.Println("  accumulate key list			List generated keys associated with the wallet")
}

func PrintKeyExport() {
	fmt.Println("  accumulate key export all			            export all keys in wallet")
	fmt.Println("  accumulate key export private [key name]			export the private key by key name")
	fmt.Println("  accumulate key export mnemonic		            export the mnemonic phrase if one was entered")
	fmt.Println("  accumulate key export seed                       export the seed generated from the mnemonic phrase")
}

func PrintKeyGenerate() {
	fmt.Println("  accumulate key generate [key name]     Generate a new key and give it a name in the wallet")
}

func PrintKeyImport() {
	fmt.Println("  accumulate key import mnemonic [mnemonic phrase...]     Import the mneumonic phrase used to generate keys in the wallet")
	fmt.Println("  accumulate key import private [key name]      Import a key and give it a name in the wallet, prompt for key")
	fmt.Println("  accumulate key import factoid   Import a factoid private address, prompt for key")

	fmt.Println("  accumulate key import lite        Import a key as a lite address, prompt for key")
}

func PrintKey() {
	PrintKeyGenerate()
	PrintKeyPublic()
	PrintKeyImport()

	PrintKeyExport()
}

func (x *Context) listKeys(cmd *cobra.Command) (string, error) {
	keys, err := x.getWallet().KeyList(context.Background(), &api.KeyListRequest{})
	if err != nil {
		return "", err
	}

	if WantJsonOutput {
		data, err := json.Marshal(&keys)
		if err != nil {
			return "", err
		}
		return string(data), err
	}
	out := ""
	for _, k := range keys.KeyList {
		k := k
		out += "\n" + responseForAddr(&services.Key{Key: k}, "").Print() + "\n"
	}

	return out, nil
}

func (x *Context) showKey(cmd *cobra.Command, s string) (string, error) {
	addr, err := x.walletdResolveKey(s, false)
	if err != nil {
		return "", err
	}
	addr.PrivateKey = nil

	_, ok := addr.GetPrivateKey()
	if ok {
		return "", fmt.Errorf("an internal error occurred")
	}

	return responseForAddr(addr, "").Print(), nil
}

func (x *Context) routeKeyCmd(cmd *cobra.Command, args []string) (string, error) {
	var keys []api.Key
	if len(args) == 0 {
		r, err := x.getWallet().KeyList(cmd.Context(), &api.KeyListRequest{})
		if err != nil {
			return "", err
		}
		keys = r.KeyList

	} else {
		for _, arg := range args {
			key, err := x.getWallet().ResolveKey(context.Background(), &api.ResolveKeyRequest{Value: arg})
			if err != nil {
				return "", err
			}
			keys = append(keys, *key.Key)
		}
	}

	ns, err := ClientV3.NetworkStatus(cmd.Context(), v3.NetworkStatusOptions{Partition: protocol.Directory})
	if err != nil {
		return "", err
	}

	var all []string
	for _, key := range keys {
		u, err := url.Parse(key.Labels.Lite)
		if err != nil {
			return "", err
		}
		partition, err := apiutil.RouteAccount(ns.Routing, u)
		if err != nil {
			return "", err
		}

		addr := &services.Key{Key: key}
		tw := table.NewWriter()
		tw.Style().Options.DrawBorder = false
		tw.AppendRow([]any{"Labels", strings.Join(key.Labels.Names, ", ")})
		tw.AppendRow([]any{"Lite Account", u.String()})
		tw.AppendRow([]any{"Address", addr.String()})
		tw.AppendRow([]any{"Partition", partition})
		all = append(all, tw.Render())
	}

	return strings.Join(all, "\n\n"), nil
}

// Deprecated: use resolveAddress
func (x *Context) resolvePublicKey(cmd *cobra.Command, s string) (*services.Key, error) {
	k, err := x.walletdResolveKey(s, false)
	if err != nil {
		return nil, err
	}
	k.PrivateKey = nil
	return k, nil
}

func (x *Context) ImportKeyPrompt(cmd *cobra.Command, label string, signatureType protocol.SignatureType) (out string, err error) {
	token, err := getPasswdPrompt(cmd, "Import private key", "Private Key: ", true)
	if err != nil {
		return "", db.ErrInvalidPassword
	}
	addr, err := address.Parse(token)
	if err != nil {
		return "", err
	}

	// Private key format such as AS1
	if pk, ok := addr.GetPrivateKey(); ok {
		if signatureType != 0 && signatureType != addr.GetType() {
			return "", fmt.Errorf("address is %v not %v", addr.GetType(), signatureType)
		}
		return x.ImportKey(cmd, pk, label, addr.GetType())
	}

	if uk, ok := addr.(*address.Unknown); ok {
		if signatureType == 0 {
			signatureType = protocol.SignatureTypeED25519
		}
		return x.ImportKey(cmd, uk.Value, label, signatureType)
	}

	return "", fmt.Errorf("address is not a private key")
}

func (x *Context) importFilePV(cmd *cobra.Command, label, filepath string) (out string, err error) {
	b, err := os.ReadFile(filepath)
	if err != nil {
		return "", err
	}
	key := new(privval.FilePVKey)
	err = tmjson.Unmarshal(b, key)
	if err != nil {
		return "", fmt.Errorf("error reading PrivValidator key from %v: %w", filepath, err)
	}

	switch key.PrivKey.Type() {
	case tmed25519.KeyType:
		return x.ImportKey(cmd, key.PrivKey.Bytes(), label, protocol.SignatureTypeED25519)
	default:
		return "", fmt.Errorf("unsupported key type %v", key.PrivKey.Type())
	}
}

func getPasswdPrompt(cmd *cobra.Command, long, short string, mask bool) (string, error) {
	b, err := interactive.GetPassword(long, short, cmd.InOrStdin(), cmd.ErrOrStderr(), mask)
	return b, err
}

// ImportKey will import the private key and assign it to the label
func (x *Context) ImportKey(cmd *cobra.Command, token []byte, label string, signatureType protocol.SignatureType) (out string, err error) {
	key, err := x.getWallet().ImportKey(context.Background(), &api.ImportKeyRequest{
		Seed:   token,
		Labels: []string{label},
		Type:   signatureType,
		Force:  flagKey.Force,
	})
	if err != nil {
		return "", err
	}

	return responseForAddr(keyResp2walletKey(key), label).Print(), nil
}

func (x *Context) walletdResolveKey(s string, private bool) (*services.Key, error) {
	key, err := x.getWallet().ResolveKey(context.Background(), &api.ResolveKeyRequest{Value: s, IncludePrivateKey: private})
	if err != nil {
		return nil, err
	}

	return keyResp2walletKey(key), nil
}

func keyResp2walletKey(key *api.ResolveKeyResponse) *services.Key {
	return &services.Key{
		Hash: key.PublicKeyHash,
		Key:  *key.Key,
	}
}

func (x *Context) ExportKey(cmd *cobra.Command, label string) (string, error) {
	key, err := x.walletdResolveKey(label, true)
	if err != nil {
		return "", err
	}

	return responseForAddr(key, label).Print(), nil
}

func (x *Context) GenerateKey(cmd *cobra.Command, labels ...string) (string, error) {
	sigtype, err := validateSigType(SigType)
	if err != nil {
		return "", err
	}

	res, err := x.getWallet().GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Type:   sigtype,
		Labels: labels,
		Force:  flagKey.Force,
	})
	if err != nil {
		if strings.HasPrefix(err.Error(), "key already exists") {
			return "", fmt.Errorf("%w; this indicates a corrupted key counter. Run `accumulate key restore-counters` to resolve this.", err)
		}
		return "", err
	}
	return responseForAddr(&services.Key{Key: res.Key}, labels...).Print(), nil
}

func (x *Context) ExportKeys(cmd *cobra.Command) (out string, err error) {
	res, err := x.getWallet().ExportVault(context.Background(), &api.ExportVaultRequest{})
	if err != nil {
		return "", err
	}

	if WantJsonOutput {
		out += "{\"keys\":["
	}
	for i, v := range res.Vault.Keys {
		k := &services.Key{Key: v}
		label := k.String()
		if k.Labels != nil {
			if len(k.Labels.Names) > 0 {
				label = k.Labels.Names[0]
			} else {
				label = k.Labels.Lite
			}
		}
		str := responseForAddr(k, label).Print()
		if err != nil {
			out += fmt.Sprintf("invalid key for key name %s (error %v)\n", label, err)
		} else {
			if WantJsonOutput && i != 0 {
				out += ","
			}
			out += "\n"
			out += str
			out += "\n"
		}
	}
	if WantJsonOutput {
		out += "]}"
		var b bytes.Buffer
		err := json.Indent(&b, []byte(out), "", "    ")
		if err == nil {
			out = b.String()
		}
	}
	return out, nil
}

func (x *Context) ExportSeed(cmd *cobra.Command) (string, error) {
	res, err := x.getWallet().ExportVault(context.Background(), &api.ExportVaultRequest{})
	if err != nil {
		return "", err
	}
	if res.Vault.SeedInfo.Seed == nil {
		return "", fmt.Errorf("mnemonic seed not found")
	}
	if WantJsonOutput {
		a := KeyResponse{}
		a.Seed = res.Vault.SeedInfo.Seed
		dump, err := json.Marshal(&a)
		if err != nil {
			return "", err
		}
		return fmt.Sprintf("%s\n", string(dump)), nil
	} else {
		return fmt.Sprintf(" seed: %x\n", res.Vault.SeedInfo.Seed), nil
	}
}

func (x *Context) ExportMnemonic(cmd *cobra.Command) (string, error) {
	res, err := x.getWallet().ExportVault(context.Background(), &api.ExportVaultRequest{})
	if err != nil {
		return "", err
	}
	if res.Vault.SeedInfo.Mnemonic == "" {
		return "", fmt.Errorf("mnemonic seed not found")
	}
	if WantJsonOutput {
		a := KeyResponse{}
		a.Mnemonic = types.String(res.Vault.SeedInfo.Mnemonic)
		dump, err := json.Marshal(&a)
		if err != nil {
			return "", err
		}
		return fmt.Sprintf("%s\n", string(dump)), nil
	} else {
		return fmt.Sprintf("mnemonic phrase: %s\n", res.Vault.SeedInfo.Mnemonic), nil
	}
}

func (x *Context) ImportFactoidKey(cmd *cobra.Command) (out string, err error) {
	token, err := getPasswdPrompt(cmd, "Import Factoid key", "Private Key: ", true)
	if err != nil {
		return "", db.ErrInvalidPassword
	}
	if !strings.Contains(token, "Fs") {
		return "", fmt.Errorf("key to import is not a factoid address")
	}
	label, _, privatekey, err := protocol.GetFactoidAddressRcdHashPkeyFromPrivateFs(token)
	if err != nil {
		return "", err
	}
	return x.ImportKey(cmd, privatekey, label, protocol.SignatureTypeRCD1)
}

func validateSigType(input string) (protocol.SignatureType, error) {
	sigtype, ok := protocol.SignatureTypeByName(input)
	if !ok || sigtype == protocol.SignatureTypeLegacyED25519 {
		sigtype = protocol.SignatureTypeED25519
	}
	return sigtype, nil
}

func (x *Context) assignKeyLabel(cmd *cobra.Command, args []string) (out string, err error) {
	_, err = x.getWallet().KeyAssign(context.Background(), &api.KeyRotateRequest{OldKeyLabel: args[0], NewKeyLabel: args[1], Force: flagKey.Force})
	if err != nil {
		return "", err
	}
	k, err := x.walletdResolveKey(args[1], false)
	if err != nil {
		return "", err
	}
	return responseForAddr(k, args[1]).Print(), nil
}

func (x *Context) removeKeyLabel(cmd *cobra.Command, args []string) (out string, err error) {
	_, err = x.getWallet().KeyRemove(context.Background(), &api.KeyRotateRequest{OldKeyLabel: args[0], Force: flagKey.Force})
	if err != nil {
		return "", err
	}

	k, err := x.walletdResolveKey(args[1], false)
	if err != nil {
		return "", err
	}
	return responseForAddr(k, args[0]).Print(), nil
}

func (x *Context) renameKeyLabel(cmd *cobra.Command, args []string) (out string, err error) {
	_, err = x.getWallet().KeyRename(context.Background(), &api.KeyRotateRequest{OldKeyLabel: args[0], NewKeyLabel: args[1], Force: flagKey.Force})
	if err != nil {
		return out, err
	}

	k, err := x.walletdResolveKey(args[1], false)
	if err != nil {
		return "", err
	}
	return responseForAddr(k, args[1]).Print(), nil
}

func (x *Context) restoreKeyCounters(cmd *cobra.Command, args []string) (out string, err error) {
	_, err = x.getWallet().RestoreKeyCounters(context.Background(), &api.RestoreKeyCountersRequest{})
	if err != nil {
		return "", err
	}
	return "restored", nil
}
