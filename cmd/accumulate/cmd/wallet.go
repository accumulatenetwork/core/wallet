package cmd

import (
	"bufio"
	"context"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"io/fs"
	"net"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"github.com/AccumulateNetwork/jsonrpc2/v15"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"github.com/zalando/go-keyring"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/logging"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"gitlab.com/accumulatenetwork/core/wallet/internal/util"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/interactive"
	sshutil "gitlab.com/accumulatenetwork/core/wallet/pkg/util/ssh"
	daemon "gitlab.com/accumulatenetwork/core/wallet/pkg/wallet"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/jsonrpc"
	"golang.org/x/exp/slog"
)

var walletCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:     "vault",
		Aliases: []string{"wallet"},
		Short:   "initialize wallet or start wallet as a service",
	}
})

var walletInitCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "init",
		Short: "Initialize the wallet",
		Args:  cobra.NoArgs,
		Run:   x.runCmdFunc2(x.initWallet),
	}
})

var walletServeCmd = factory.New(func(x *Context) *cobra.Command {
	// inWallet is a helper that prepends ~/.accumulate/wallet to the given path
	inWallet := func(s string) string {
		return filepath.Join(currentUser.HomeDir, ".accumulate", "wallet", s)
	}

	cmd := &cobra.Command{
		Use:     "serve",
		Aliases: []string{"serve-socket"},
		Short:   "run wallet service daemon via a unix socket",
		Args:    cobra.ExactArgs(0),
		Run:     x.runCmdFunc2(x.runWalletdSocket),
	}
	cmd.Flags().BoolVar(&flagRunWalletd.AutoShutdown, "auto-shutdown", false, "Shutdown the daemon once the last token expires")

	cmd.Flags().DurationVar(&flagRunWalletd.CiStopAfter, "ci-stop-after", 0, "FOR CI ONLY - stop the node after some time")
	cmd.Flag("ci-stop-after").Hidden = true

	// Daemon configuration
	cmd.Flags().Duration("max-ttl", time.Hour, "The maximum time an authentication token can be saved for")
	cmd.Flags().String("pid-file", inWallet("daemon.pid"), "PID file to use for the wallet daemon")
	cmd.Flags().String("log-file", inWallet("daemon.log"), "Log file to use for the wallet daemon")
	check(x.BindPFlag("daemon-max-ttl", cmd.Flags().Lookup("max-ttl")))
	check(x.BindPFlag("daemon-pid-file", cmd.Flags().Lookup("pid-file")))
	check(x.BindPFlag("daemon-log-file", cmd.Flags().Lookup("log-file")))

	// Global flags
	rflags := cmd.Root().PersistentFlags()
	rflags.String("socket", inWallet("daemon.socket"), "Socket file to use for the wallet daemon")
	check(x.BindPFlag("daemon-socket", rflags.Lookup("socket")))

	// Prevent the default pre-run
	cmd.PersistentPreRun = func(cmd *cobra.Command, args []string) {}
	return cmd
})

var walletExportCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "export [location to export]",
		Short: "export wallet details",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			err := x.ExportAccounts(cmd, args[0])
			x.printOutput(cmd, "File downloaded successfully", err)
		},
	}
})

var walletListCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "list",
		Short: "List registered vaults",
		Args:  cobra.NoArgs,
		Run:   x.runCmdFunc2(x.listVaults),
	}
})

func init() {
	walletCmd.AddCommand(
		walletInitCmd,
		walletExportCmd,
		walletServeCmd,
		walletListCmd,
	)
}

var flagRunWalletd = struct {
	CiStopAfter  time.Duration
	AutoShutdown bool
}{}

func (x *Context) createJrpc(ctx context.Context, cancel context.CancelFunc, tlsFiles []string, logFile string) (*walletd.JrpcMethods, error) {
	// Open the log file
	lf, err := os.OpenFile(logFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0600)
	if err != nil {
		return nil, errors.UnknownError.WithFormat("open log file: %w", err)
	}
	logger := slog.New(slog.NewTextHandler(io.MultiWriter(lf, os.Stderr), &slog.HandlerOptions{
		ReplaceAttr: logging.Jsonify,
		Level:       slog.LevelError, // TODO: make this configurable
	}))
	go func() {
		<-ctx.Done()
		logger.Info("Stopping")
		_ = lf.Close()
	}()

	var onExpire func()
	if flagRunWalletd.AutoShutdown {
		onExpire = func() {
			logger.Info("Last token expired, shutting down")
			cancel()
		}
	}

	return walletd.NewJrpc(walletd.Options{
		OnExpire:    onExpire,
		Logger:      logger,
		TLSFiles:    tlsFiles,
		MaxTokenTTL: x.GetDuration("daemon-max-ttl"),
	})
}

func (x *Context) runWalletdSocket(cmd *cobra.Command, _ []string) (string, error) {
	wg := new(sync.WaitGroup)
	defer wg.Wait()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, os.Interrupt)

	go func() {
		<-sigs
		signal.Stop(sigs)
		cancel()
	}()

	pidFile := x.GetString("daemon-pid-file")
	logFile := x.GetString("daemon-log-file")
	err := x.startWalletdSocket(ctx, cancel, wg, x.DaemonSocket(), pidFile, logFile)
	if err != nil {
		return "", errors.UnknownError.Wrap(err)
	}

	// Clean up PID file and socket
	defer func() {
		_ = os.Remove(pidFile)
		_ = os.Remove(x.DaemonSocket())
	}()

	absPath, err := filepath.Abs(x.DaemonSocket())
	if err != nil {
		return "", errors.UnknownError.WithFormat("make absolute path: %w", err)
	}
	fmt.Printf("Listening on %v\n", absPath)

	// Run the root's pre-run
	x.Set("spawn-daemon", false)
	err = x.rootPreRun(cmd, nil)
	if err != nil {
		return "", err
	}

	// Wait for exit
	<-ctx.Done()
	return "", nil
}

func (x *Context) startWalletdSocket(ctx context.Context, cancel context.CancelFunc, wg *sync.WaitGroup, socketFile, pidFile, logFile string) error {
	// Create ~/.accumulate/wallet if appropriate
	home, err := os.UserHomeDir()
	checkf(err, "get user home directory")
	err = os.MkdirAll(filepath.Join(home, ".accumulate", "wallet"), 0700)
	checkf(err, "create wallet directory")

	// Record our PID
	err = os.WriteFile(pidFile, []byte(fmt.Sprintln(os.Getpid())), 0600)
	if err != nil {
		return errors.UnknownError.WithFormat("write PID file: %w", err)
	}

	// Initialize JRPC
	jrpc, err := x.createJrpc(ctx, cancel, nil, logFile)
	if err != nil {
		return errors.UnknownError.WithFormat("initialize walletd: %w", err)
	}

	// Start the listener
	l, err := net.Listen("unix", socketFile)
	if err != nil {
		return errors.UnknownError.WithFormat("start walletd: %w", err)
	}

	// Close the listener and remove the socket when the process stops
	go func() {
		<-ctx.Done()
		_ = l.Close()
		_ = os.Remove(socketFile)
	}()

	// Force requests to be non-concurrent
	mux := jrpc.NewMux()
	lock := new(sync.Mutex)
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		lock.Lock()
		defer lock.Unlock()
		mux.ServeHTTP(w, r)
	})

	// Start the HTTP server
	server := &http.Server{
		Handler:           handler,
		ReadHeaderTimeout: 15 * time.Second,
	}
	go func() { _ = server.Serve(l) }()

	if wg != nil {
		wg.Add(1)
	}

	// Shutdown when the process stops
	go func() {
		if wg != nil {
			defer wg.Done()
		}

		<-ctx.Done()
		ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		cancel()
		_ = server.Shutdown(ctx)
	}()

	// Create a direct wallet
	dt := new(jsonrpc.DefaultTransport)
	dt.Transport = util.DirectHttpTransport(handler)
	_wallet = newWalletWithTransport(dt, false)
	return nil
}

func (x *Context) InitDBImport(cmd *cobra.Command, memDb bool) error {
	mnemonicString, err := getPasswdPrompt(cmd, "Mnemonic seed phrase", "Enter mnemonic: ", true)
	if err != nil {
		return db.ErrInvalidPassword
	}
	mnemonic := strings.Split(mnemonicString, " ")
	_, err = x.getWallet().ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{Mnemonic: mnemonic})
	if err != nil {
		return err
	}
	return nil
}

func (x *Context) newMnemonic(cmd *cobra.Command) (string, error) {
	r, err := x.getWallet().GenerateMnemonic(cmd.Context(), &api.GenerateMnemonicRequest{Entropy: uint64(Entropy)})
	if err != nil {
		return "", err
	}
	mnemonicString := strings.Join(r.Mnemonic, " ")
	_, err = promptMnemonic(mnemonicString)
	if err != nil {
		return "", err
	}
	mnemonicConfirm, err := promptMnemonicConfirm()
	if err != nil {
		return "", err
	}
	if mnemonicString != mnemonicConfirm {
		return "", fmt.Errorf("mnemonic doesn't match")
	}
	return mnemonicString, nil
}

func (x *Context) InitDBCreate(cmd *cobra.Command) error {
	mnemonicString, err := x.newMnemonic(cmd)
	if err != nil {
		return err
	}
	mnemonic := strings.Split(mnemonicString, " ")
	_, err = x.getWallet().ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{Mnemonic: mnemonic})
	if err != nil {
		return err
	}
	return nil
}

func (x *Context) listVaults(cmd *cobra.Command, _ []string) (string, error) {
	res, err := x.getWallet().ListVaults(context.Background(), &api.ListVaultsRequest{})
	if err != nil {
		return "", err
	}

	var s string
	for _, v := range res.Vaults {
		s += v + "\n"
	}
	return s, nil
}

type promptContent struct {
	errorMsg string
	label    string
}

func promptMnemonic(mnemonic string) (string, error) {
	pc := promptContent{
		"",
		"Please write down your mnemonic phrase and press <enter> when done. '" +
			mnemonic + "'",
	}
	items := []string{"\n"}
	index := -1
	var result string
	var err error

	for index < 0 {
		prompt := promptui.SelectWithAdd{
			Label: pc.label,
			Items: items,
		}
		index, result, err = prompt.Run()
		if index == -1 {
			items = append(items, result)
		}
	}

	if err != nil {
		return "", err
	}

	return result, nil
}

func promptMnemonicConfirm() (string, error) {
	validate := func(input string) error {
		if len(input) <= 0 {
			return errors.BadRequest.With("Invalid mnemonic entered")
		}
		return nil
	}

	prompt := promptui.Prompt{
		Label:    "Please re-enter the mnemonic phrase.",
		Validate: validate,
	}
	result, err := prompt.Run()
	if err != nil {
		return "", err
	}
	return result, nil
}

// allServices includes all of the daemon services. Using allServices instead of
// wallet.Client allows tests to skip the transport layer and ensures no
// dependence on the client.
type allServices interface {
	daemon.GeneralService
	daemon.VaultService
	daemon.TransactionService
	daemon.SigningService
	daemon.KeyService
	daemon.LedgerService
	daemon.AccountService
}

var _wallet allServices

func (x *Context) getWallet() allServices {
	// Have we already created the client?
	if _wallet != nil {
		return _wallet
	}

	var tr jsonrpc.Transport
	var direct bool
	if strings.HasPrefix(DaemonConnStr, "ssh-") {
		// Connect over SSH to the given daemon
		u, err := url.Parse(DaemonConnStr)
		checkf(err, "--daemon")

		if u.Path == "" {
			fatalf("--daemon: invalid SSH connection string")
		}
		network := strings.TrimPrefix(u.Scheme, "ssh-")
		address := strings.TrimPrefix(u.Path, "/")

		client, err := sshutil.Connect(u)
		check(err)
		tr = jsonrpc.SSHDialTransport(client, network, address)

	} else if DaemonConnStr != "" {
		// Connect to the given daemon
		u, err := url.Parse(DaemonConnStr)
		checkf(err, "--daemon")
		tr = jsonrpc.NetDialTransport(u.Scheme, u.Host)

	} else if x.SpawnDaemon() {
		// Use a custom transport that starts the daemon if it's not running
		mu := new(sync.Mutex)
		tr = jsonrpc.DialTransport(func(string, string) (net.Conn, error) {
			mu.Lock()
			defer mu.Unlock()
			return daemonSpawnOrConnect(x.DaemonSocket())
		})

	} else if ok, conn, err := daemonTryConnect(x.DaemonSocket()); err != nil {
		checkf(err, "check for running daemon")

	} else if ok {
		// If someone has already spawned a daemon, use that one

		// We don't actually want to use _this_ connection
		conn.Close()

		// Set up a transport that will connect to the existing daemon (but
		// won't automatically spawn a new daemon)
		tr = jsonrpc.DialTransport(func(string, string) (net.Conn, error) {
			ok, conn, err := daemonTryConnect(x.DaemonSocket())
			switch {
			case err != nil:
				return nil, errors.UnknownError.Wrap(err)
			case !ok:
				return nil, errors.InternalError.With("daemon exited")
			default:
				return conn, nil
			}
		})

	} else {
		// Or use a custom transport that runs the daemon in-process
		jrpc, err := walletd.NewJrpc(walletd.Options{Logger: slog.Default()})
		checkf(err, "start daemon")
		dt := new(jsonrpc.DefaultTransport)
		tr = dt
		dt.Transport = util.DirectHttpTransport(jrpc.NewMux())
		direct = true
	}

	_wallet = newWalletWithTransport(tr, !direct)
	return _wallet
}

// binHash is the hash of the accumulate binary, or nil
var binHash = func() string {
	path, err := os.Executable()
	if err != nil {
		return ""
	}

	f, err := os.Open(path)
	if err != nil {
		return ""
	}

	h := sha256.New()
	_, err = io.Copy(h, f)
	if err != nil {
		return ""
	}

	return hex.EncodeToString(h.Sum(nil))
}()

func getOldAuthToken() []byte {
	if binHash == "" {
		return nil
	}

	v, err := keyring.Get(binHash, currentUser.Username)
	if err != nil {
		return nil
	}

	b, err := hex.DecodeString(v)
	if err != nil {
		return nil
	}
	return b
}

func newWalletWithTransport(tr jsonrpc.Transport, reuseAuthToken bool) *jsonrpc.Client {
	jsonrpc2.DebugMethodFunc = true

	// Create or refresh an auth token
	wallet, err := jsonrpc.NewWith("http://socket/wallet", tr)
	checkf(err, "create wallet daemon client")

	var authToken []byte
	if reuseAuthToken {
		authToken = getOldAuthToken()
	}
	r, err := wallet.RefreshToken(context.Background(), &api.RefreshTokenRequest{
		Token: authToken,
	})
	checkf(err, "refresh token")
	authToken = r.Token

	if reuseAuthToken && binHash != "" {
		_ = keyring.Set(binHash, currentUser.Username, hex.EncodeToString(authToken))
	}

	// This is conditional to allow tests to override this behavior
	if _, ok := tr.(*jsonrpc.AutoWalletTransport); !ok {
		tr = &jsonrpc.AutoWalletTransport{
			Transport: tr,
			SetWallet: func(_ any, v *string) { *v = WalletDir },
			SetVault:  func(_ any, v *string) { *v = VaultName },
			SetToken:  func(_ any, v *[]byte) { *v = authToken },
		}
	}

	it := new(jsonrpc.InteractiveAuthnTransport)
	it.Transport = tr
	it.Use1Password = true
	it.GetPassword = interactive.AuthenticateVault

	vst := new(vaultSelectTransport)
	vst.Transport = it

	// Create a client with a fake address
	wallet, err = jsonrpc.NewWith("http://socket/wallet", vst)
	checkf(err, "create wallet daemon client")
	return wallet
}

func daemonTryConnect(socketfile string) (bool, net.Conn, error) {
	conn, err := net.Dial("unix", socketfile)
	switch {
	case err == nil:
		// Successful connection
		return true, conn, nil

	case errors.Is(err, fs.ErrNotExist):
		// There's no socket
		return false, nil, nil

	case isConnRefused(err):
		// The socket exists but no one is listening
		err = os.Remove(socketfile)
		if err != nil && !errors.Is(err, fs.ErrNotExist) {
			return false, nil, errors.UnknownError.WithFormat("delete old socket: %w", err)
		}
		return false, nil, nil

	default:
		// Unknown error
		return false, nil, err
	}
}

func daemonSpawnOrConnect(socketfile string) (net.Conn, error) {
	// Connect or spawn a daemon
	ok, conn, err := daemonTryConnect(socketfile)
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}
	if ok {
		return conn, nil
	}

	// Start the daemon
	slog.Info("Spawning daemon")
	execName, err := os.Executable()
	check(err)
	proc := exec.Command(execName, "wallet", "serve", "--auto-shutdown", "--socket="+socketfile)
	proc.Stderr = os.Stderr
	stdout, err := proc.StdoutPipe()
	check(err) // This should never fail
	check(proc.Start())

	// Wait for the "Listening on ..." message
	rd := bufio.NewReader(stdout)
	for {
		line, err := rd.ReadString('\n')
		if err != nil {
			return nil, errors.UnknownError.WithFormat("waiting for daemon to start: %w", err)
		}
		if strings.HasPrefix(line, "Listening on ") {
			socketfile = strings.TrimSpace(line[len("Listening on "):])
			break
		}
	}

	// Connect
	return net.Dial("unix", socketfile)
}

type vaultSelectTransport struct {
	jsonrpc.Transport
}

func (t vaultSelectTransport) Request(ctx context.Context, url, method string, params, result interface{}) error {
	// Make the request
	err := t.Transport.Request(ctx, url, method, params, result)
	if err == nil {
		return nil
	}

	// Check for a multi-vault error
	err2, ok := jsonrpc.DecodeError(err).(*errors.Error)
	if !ok || err2.Code != errors.VaultUnspecified {
		return err
	}

	// List vaults
	c := &jsonrpc.Client{Transport: t.Transport, Server: url}
	r, e := c.ListVaults(ctx, &api.ListVaultsRequest{})
	if e != nil {
		return err
	}

	// Chose a vault
	i, _, e := (&promptui.Select{
		Label: "Select a vault",
		Items: r.Vaults,
	}).Run()
	if e != nil {
		return err
	}

	// Update the flag
	VaultName = r.Vaults[i]

	// Retry the query
	return t.Transport.Request(ctx, url, method, params, result)
}
