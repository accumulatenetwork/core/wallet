package cmd

import (
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/accumulate/pkg/build"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"gitlab.com/accumulatenetwork/core/wallet/internal/flags"
	"gitlab.com/accumulatenetwork/core/wallet/internal/staking"
)

var stakingCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "staking",
		Short: "Setup staking",
	}
})

var stakingConvertCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "convert [account]",
		Short: "Convert an existing token account into a staking account",
		Args:  cobra.ExactArgs(1),
		Run:   x.runCmdFunc2(x.convertStaking),

		PersistentPreRunE: stakingPreRun,
	}
}).
	With((*Context).submitFlags)

var stakingWithdrawCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "withdraw [from] [to] [amount]",
		Short: "Request a withdraw from a staking account",
		Args:  cobra.ExactArgs(3),
		Run:   x.runCmdFunc2(x.withdrawStaking),
	}
}).
	With((*Context).submitFlags)

var stakingFlags = struct {
	Type     staking.AccountType
	Rewards  *url.URL
	Delegate *url.URL
	HardLock bool
	Lockup   uint64
}{}

func init() {
	stakingCmd.AddCommand(
		stakingConvertCmd,
		stakingWithdrawCmd,
	)

	stakingConvertCmd.Flags().Var(flags.NewEnum(&stakingFlags.Type, staking.AccountTypeByName), "type", "Type of staking account (pure, delegated, core-validator, core-follower, or staking-validator)")
	stakingConvertCmd.Flags().Var(flags.Url{V: &stakingFlags.Rewards}, "rewards", "Staking rewards account, if rewards should not be sent back into the staking account")
	stakingConvertCmd.Flags().Var(flags.Url{V: &stakingFlags.Delegate}, "delegate", "The account the stake should be delegated to")
	// stakingConvertCmd.Flags().BoolVar(&stakingFlags.HardLock, "hard-lock", false, "Hard-lock the staking account to prevent tokens from being withdrawn until the lockup is over")
	// stakingConvertCmd.Flags().Uint64Var(&stakingFlags.Lockup, "lockup", 0, "Length of the lockup period in quarters, from 1 to 12 (3 mo to 3 y)")
	stakingConvertCmd.MarkFlagRequired("type")
}

func stakingPreRun(cmd *cobra.Command, args []string) error {
	err := cmd.Root().PersistentPreRunE(cmd, args)
	if err != nil {
		return err
	}

	if cmd.Flag("metadata").Changed {
		return errors.BadRequest.With("--metadata is incompatible with this command")
	}

	if stakingFlags.Type == staking.AccountTypeDelegated && stakingFlags.Delegate == nil {
		return errors.BadRequest.With("--delegate is required for --type=delegated")
	}
	if stakingFlags.Type != staking.AccountTypeDelegated && stakingFlags.Delegate != nil {
		return errors.BadRequest.WithFormat("--delegate is incompatible with --type=%s", stakingFlags.Type)
	}
	if stakingFlags.Lockup > 12 {
		return errors.BadRequest.With("--lockup cannot be greater than 12")
	}
	return nil
}

func (x *Context) convertStaking(cmd *cobra.Command, args []string) (string, error) {
	stake, err := url.Parse(args[0])
	if err != nil {
		return "", errors.BadRequest.WithFormat("invalid staking url: %w", err)
	}

	signers, err := x.getSigners(cmd, stake, nil)
	if err != nil {
		return "", err
	}

	acct, err := getAccount(stake.String())
	if err != nil {
		return "", errors.UnknownError.WithFormat("load stake account: %w", err)
	}
	tok, ok := acct.(*protocol.TokenAccount)
	if !ok {
		return "", errors.BadRequest.WithFormat("cannot stake a %v", acct.Type())
	}

	info := new(staking.Account)
	info.Type = stakingFlags.Type
	info.Payout = stakingFlags.Rewards
	info.Delegate = stakingFlags.Delegate
	info.HardLock = stakingFlags.HardLock
	info.Lockup = stakingFlags.Lockup - 2
	md, err := json.Marshal(info)
	if err != nil {
		return "", errors.EncodingError.With(err)
	}

	b := build.Transaction().For(stake).
		Metadata(md).
		UpdateAccountAuth().
		Add("staking.acme/book")
	for _, auth := range tok.Authorities {
		b = b.Disable(auth.Url)
	}
	txn, err := b.Done()
	if err != nil {
		return "", errors.UnknownError.WithFormat("build transaction: %w", err)
	}
	out1, err := x.dispatchTxAndPrintResponse(cmd, txn, stake, signers)
	if err != nil {
		return "", err
	}

	parts, err := staking.FormatActions(&staking.AddAccount{
		Type:     info.Type,
		Delegate: info.Delegate,
		Account:  stake,
		Payout:   info.Payout,
		// TODO HardLock, Lockup
	})
	if err != nil {
		return "", err
	}

	txn, err = build.Transaction().
		For("staking.acme/requests").
		Memo(fmt.Sprintf("transaction: %v", txn.ID())).
		WriteData().
		DoubleHash(parts).
		Done()
	if err != nil {
		return "", errors.UnknownError.WithFormat("build transaction: %w", err)
	}
	out2, err := x.dispatchTxAndPrintResponse(cmd, txn, stake, signers)
	if err != nil {
		return "", err
	}

	return out1 + "\n" + out2, nil
}

func (x *Context) withdrawStaking(cmd *cobra.Command, args []string) (string, error) {
	//[from] [to] [amount]
	from, err := url.Parse(args[0])
	if err != nil {
		return "", errors.BadRequest.WithFormat("invalid from url: %w", err)
	}
	fromAcct, err := getAccount(from.String())
	if err != nil {
		return "", errors.UnknownError.WithFormat("load stake account: %w", err)
	}
	fromTok, ok := fromAcct.(*protocol.TokenAccount)
	if !ok {
		return "", errors.BadRequest.WithFormat("cannot stake a %v", fromAcct.Type())
	}

	signers, err := x.getSigners(cmd, from, nil)
	if err != nil {
		return "", err
	}

	to, err := url.Parse(args[0])
	if err != nil {
		return "", errors.BadRequest.WithFormat("invalid to url: %w", err)
	}

	amount, err := amountToBigInt(fromTok.GetTokenUrl().String(), args[1])
	if err != nil {
		return "", err
	}

	parts, err := staking.FormatActions(&staking.WithdrawTokens{
		Account:   from,
		Recipient: to,
		Amount:    amount,
	})
	if err != nil {
		return "", err
	}

	return x.dispatchTxAndPrintResponse(cmd, &protocol.WriteData{
		Entry: &protocol.DoubleHashDataEntry{
			Data: parts,
		},
	}, protocol.AccountUrl("staking.acme", "requests"), signers)
}
