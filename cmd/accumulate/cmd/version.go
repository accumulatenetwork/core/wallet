package cmd

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"text/tabwriter"

	"github.com/spf13/cobra"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/pkg"
)

// version represents the faucet command
var versionCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "version",
		Short: "get version of the accumulate node",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, _ []string) {
			out, err := GetVersion()
			x.printOutput(cmd, out, err)
		},
	}
})

func GetVersion() (string, error) {
	var v struct {
		Network client.VersionResponse
		Wallet  struct {
			Version, Commit string
		}
	}
	v.Wallet.Version = pkg.Version
	v.Wallet.Commit = pkg.Commit

	if err := Client.RequestAPIv2(context.Background(), "version", nil, &client.ChainQueryResponse{Data: &v.Network}); err != nil {
		return PrintJsonRpcError(err)
	}

	if WantJsonOutput {
		str, err := json.Marshal(v)
		if err != nil {
			return "", err
		}
		return string(str), nil
	}

	buf := new(bytes.Buffer)
	tw := tabwriter.NewWriter(buf, 1, 1, 1, ' ', 0)
	if v.Network.VersionIsKnown {
		fmt.Fprintf(tw, "Network version\t%v\t(%v)\n", v.Network.Version, v.Network.Commit)
	} else {
		fmt.Fprintf(tw, "Network version\tis unknown\n")
	}
	if v.Wallet.Version != "" {
		fmt.Fprintf(tw, "Wallet version\t%v\t(%v)\n", v.Wallet.Version, v.Wallet.Commit)
	} else {
		fmt.Fprintf(tw, "Wallet version\tis unknown\n")
	}
	if err := tw.Flush(); err != nil {
		log.Fatal(err)
	}
	return strings.TrimSuffix(buf.String(), "\n"), nil
}

var describeCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "describe",
		Short: "describe the accumulate node",
		Run: func(cmd *cobra.Command, _ []string) {
			out, err := describe()
			x.printOutput(cmd, out, err)
		},
	}
})

func describe() (string, error) {
	var res interface{}

	if err := Client.RequestAPIv2(context.Background(), "describe", nil, &res); err != nil {
		return PrintJsonRpcError(err)
	}

	str, err := json.Marshal(res)
	if err != nil {
		return "", err
	}

	return string(str), nil
}
