package cmd

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/require"
	url2 "gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/testutils"
)

func init() {
	testMatrix.addTest(testCase6_1)
}

// testCase6_1 Create a token
func testCase6_1(t *testing.T, tc *testCmd) {

	_, err := tc.executeTx(t, "credits %s acc://RedWagon.acme/book/1 50000", liteAccounts[2])
	require.NoError(t, err)

	_, err = tc.executeTx(t, "token create acc://RedWagon.acme --sign-with=red1 acc://RedWagon.acme/redcoin red 8 500000000")
	require.NoError(t, err)
}

func TestIssueTokens_ManyOutputs(t *testing.T) {
	// Create a wallet
	tc := newTestCmd(t, testCmdOpts{})
	_, err := _wallet.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:  tc.dir,
		Token: testutils.TestToken[:],
	})
	require.NoError(t, err)

	// Import the mnemonic
	_, err = _wallet.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	// Generate a key
	key, err := _wallet.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Labels: []string{"yellowKey"},
	})
	require.NoError(t, err)

	// Get tokens
	lite := protocol.LiteAuthorityForKey(key.Key.PublicKey, key.Key.KeyInfo.Type)
	_, err = tc.executeTx(t, "faucet %s", lite.JoinPath("ACME"))
	require.NoError(t, err)

	// Get credits
	_, err = tc.executeTx(t, "credits %s %s 1000000", lite.JoinPath("ACME"), lite)
	require.NoError(t, err)

	// Create an ADI
	adi := protocol.AccountUrl("test6-2")
	_, err = tc.executeTx(t, "adi create %s %s yellowKey", lite, adi)
	require.NoError(t, err)

	// Get credits for the page
	_, err = tc.executeTx(t, "credits %s %s/book/1 1000000", lite.JoinPath("ACME"), adi)
	require.NoError(t, err)

	// Create a token issuer
	_, err = tc.executeTx(t, "token create %s %[1]s/redcoin RED 8 500000000", adi)
	require.NoError(t, err)

	// Issue tokens to many recipients
	cmd := "token issue "
	cmd += adi.JoinPath("redcoin").String()
	accounts := []*url2.URL{}
	for i := range liteAccounts[:10] {
		liteHash, err := protocol.ParseLiteAddress(url2.MustParse(liteAccounts[i]))
		require.NoError(t, err)
		recipient, err := protocol.LiteTokenAddressFromHash(liteHash, adi.JoinPath("redcoin").String())
		require.NoError(t, err)
		accounts = append(accounts, recipient)
		cmd += fmt.Sprintf(" %s %d", recipient.String(), i+1)
	}

	_, err = tc.executeTx(t, cmd)
	require.NoError(t, err)

	// Test the balances
	for i, account := range accounts {
		amt, err := testGetBalance(t, tc, account.String())
		require.NoError(t, err)
		require.Equal(t, amt, fmt.Sprintf("%d", uint64((i+1)*protocol.AcmePrecision)))
	}
}

func TestSendTokens_IdenticalOutputs(t *testing.T) {
	// Create a wallet
	tc := newTestCmd(t, testCmdOpts{})
	_, err := _wallet.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path: tc.dir,
	})
	require.NoError(t, err)

	// Import the mnemonic
	_, err = _wallet.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	// Generate a key
	key, err := _wallet.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Labels: []string{"yellowKey"},
	})
	require.NoError(t, err)

	// Get tokens
	lite := protocol.LiteAuthorityForKey(key.Key.PublicKey, key.Key.KeyInfo.Type)
	_, err = tc.executeTx(t, "faucet %s", lite.JoinPath("ACME"))
	require.NoError(t, err)

	// Get credits
	_, err = tc.executeTx(t, "credits %s %s 1000000", lite.JoinPath("ACME"), lite)
	require.NoError(t, err)

	// Send tokens to many recipients
	cmd := "tx create "
	cmd += lite.JoinPath("ACME").String()
	cmd += " " + liteAccounts[1] + " 1"
	cmd += " " + liteAccounts[1] + " 1"
	// cmd += " " + liteAccounts[1] + "#foo 1"

	_, err = tc.executeTx(t, cmd)
	require.Error(t, err)
	require.Contains(t, err.Error(), "duplicate outputs")

	// amt, err := testGetBalance(t, tc, liteAccounts[1])
	// require.NoError(t, err)
	// require.Equal(t, amt, fmt.Sprintf("%d", int(2)*protocol.AcmePrecision))
}
