package cmd

import (
	"os"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/testutils"
)

func TestCopyVaultSanitized(t *testing.T) {
	x := new(Context)
	cmd := rootCmd.Build(x)

	// Setup an encrypted wallet
	tc := newTestCmd(t, testCmdOpts{})
	_, err := _wallet.CreateWallet(cmd.Context(), &api.CreateWalletRequest{
		Path:       tc.dir,
		Token:      testutils.TestToken[:],
		MultiVault: false,
		Passphrase: "foo",
	})
	require.NoError(t, err)

	_, err = _wallet.ImportMnemonic(cmd.Context(), &api.ImportMnemonicRequest{
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	_, err = x.GenerateKey(cmd, "key")
	require.NoError(t, err)

	// Copy it
	NonInteractive = true
	dir := filepath.Join(t.TempDir(), "sanitized")
	require.NoError(t, os.MkdirAll(dir, 0700))
	_, err = x.copyVaultSanitized(cmd, []string{filepath.Join(dir, "index.db")})
	require.NoError(t, err)

	// Verify the copy can be read as a wallet
	newTestCmd(t, testCmdOpts{dir: dir})
	keys, err := _wallet.KeyList(cmd.Context(), &api.KeyListRequest{})
	require.NoError(t, err)
	require.Len(t, keys.KeyList, 1)
	require.Equal(t, "key", keys.KeyList[0].Labels.Names[0])

	_, err = _wallet.LockVault(cmd.Context(), &api.LockVaultRequest{
		Close: true,
		Token: testutils.TestToken[:],
	})
	require.NoError(t, err)

	require.NoError(t, os.RemoveAll(dir))
}
