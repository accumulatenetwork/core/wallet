package cmd

import (
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
)

func init() {
	getCmd.AddCommand(getKeyCmd)
}

// getCmd represents the get command
var getCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "get [url]",
		Short: "Get data by URL",
		Args:  cobra.ExactArgs(1),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.Get(cmd, args[0])
		}),
	}
})

var getKeyCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "key [url] [key]",
		Short: "Lookup a key",
		Args:  cobra.ExactArgs(2),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return GetKey(args[0], args[1])
		}),
	}
})

var GetDirect bool

func init() {
	getCmd.Flags().BoolVar(&GetDirect, "direct", false, "Use debug-query-direct instead of query")
}

func (x *Context) Get(cmd *cobra.Command, urlStr string) (string, error) {
	u, err := url.Parse(urlStr)
	if err != nil {
		return "", err
	}

	req := new(client.GeneralQuery)
	req.Url = u
	req.Prove = Prove

	method := "query"
	if GetDirect {
		method = "debug-query-direct"
	}

	var res json.RawMessage
	err = queryAs(method, &req, &res)
	if err != nil {
		return "", err
	}

	if WantJsonOutput {
		return string(res), nil
	}

	// Is it an account?
	if json.Unmarshal(res, new(struct{ Type protocol.AccountType })) == nil {
		qr := new(QueryResponse)
		if json.Unmarshal(res, qr) != nil {
			return string(res), nil
		}

		return x.PrintChainQueryResponseV2(cmd, qr)
	}

	// Is it a transaction?
	if json.Unmarshal(res, new(struct{ Type protocol.TransactionType })) == nil {
		qr := new(client.TransactionQueryResponse)
		if json.Unmarshal(res, qr) != nil {
			return string(res), nil
		}
		return x.PrintTransactionQueryResponseV2(qr)
	}

	return string(res), nil
}

func getKey(urlStr string, key []byte) (*client.ResponseKeyPageIndex, error) {
	u, err := url.Parse(urlStr)
	if err != nil {
		return nil, err
	}

	params := new(client.KeyPageIndexQuery)
	params.Url = u
	params.Key = key

	res := new(client.ResponseKeyPageIndex)
	qres := new(client.ChainQueryResponse)
	qres.Data = res

	err = queryAs("query-key-index", &params, &qres)
	if err != nil {
		return nil, err
	}

	return res, nil
}

func GetKey(url, key string) (string, error) {
	k, err := resolveAddress(key)
	if err != nil {
		return "", err
	}

	hash, _ := k.GetPublicKeyHash()
	res, err := getKey(url, hash)
	if err != nil {
		return "", err
	}

	if WantJsonOutput {
		str, err := json.Marshal(res)
		if err != nil {
			return "", err
		}

		return string(str), nil
	}

	var out string
	out += fmt.Sprintf("Key book\t:\t%v\n", res.Authority)
	out += fmt.Sprintf("Key page\t:\t%v (index=%v)\n", res.Signer, res.Index)
	return out, nil
}
