package cmd

import (
	"fmt"
	"math"
	"math/big"
	"strconv"

	"github.com/spf13/cobra"
	url2 "gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
)

var tokenCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "token",
		Short: "Issue and get tokens",
	}
})

var tokenCmdGet = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "get [url]",
		Short: "get token by URL",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.GetToken(cmd, args[0])
			x.printOutput(cmd, out, err)
		},
	}
})

var tokenCmdCreate = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "create [sponsor] [new token url] [symbol] [precision (0 - 18)] [supply limit (optional)] [properties URL (optional)]",
		Short: "Create new token",
		Args:  cobra.RangeArgs(4, 6),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return x.CreateToken(cmd, args[0], args[1:])
		}),
	}
}).
	With((*Context).submitFlags).
	With((*Context).createFlags)

var tokenCmdIssue = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "issue [adi token url] [recipient url] [amount]",
		Short: "send tokens from a token url to a recipient",
		Args: func(cmd *cobra.Command, args []string) error {
			err := cobra.MinimumNArgs(3)(cmd, args)
			if err != nil {
				return err
			}
			if len(args)%2 != 1 {
				return fmt.Errorf("odd number of receiver/amount pairs")
			}
			return nil
		},
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.IssueTokenToRecipient(cmd, args[0], args[1:])
			x.printOutput(cmd, out, err)
		},
	}
}).With((*Context).submitFlags)

var tokenCmdBurn = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "burn [adi or lite token account] [amount]",
		Short: "burn tokens",
		Args:  cobra.ExactArgs(2),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.BurnTokens(cmd, args[0], args[1:])
			x.printOutput(cmd, out, err)
		},
	}
}).With((*Context).submitFlags)

func init() {
	tokenCmd.AddCommand(
		tokenCmdGet,
		tokenCmdCreate,
		tokenCmdIssue,
		tokenCmdBurn)
}

func PrintTokenGet() {
	fmt.Println("  accumulate token get [url] Get token by URL")
}

func PrintTokenCreate() {
	fmt.Println("  accumulate token create [origin adi url] [token url] [symbol] [precision (0 - 18)] [properties URL (optional)] 	Create new token")
	fmt.Println("  accumulate token create [origin lite url] [token url] [symbol] [precision (0 - 18)] [properties URL (optional)] 	Create new token")
}

func (x *Context) GetToken(cmd *cobra.Command, url string) (string, error) {
	res, err := GetUrl(url)
	if err != nil {
		return "", err
	}

	return x.PrintChainQueryResponseV2(cmd, res)
}

func (x *Context) CreateToken(cmd *cobra.Command, origin string, args []string) (string, error) {
	originUrl, err := url2.Parse(origin)
	if err != nil {
		return "", err
	}

	signer, err := x.getSigners(cmd, originUrl, nil)
	if err != nil {
		return "", err
	}

	url := args[0]
	symbol := args[1]
	precision := args[2]
	prcsn, err := strconv.Atoi(precision)
	if err != nil {
		return "", err
	}
	var supplyLimit *big.Int
	var properties *url2.URL
	if len(args) > 3 {
		limit, err := strconv.Atoi(args[3])
		if err != nil {
			properties, err = url2.Parse(args[4])
			if err != nil {
				return "", fmt.Errorf("invalid properties url, %v", err)
			}
			res, err := GetUrl(properties.String())
			if err != nil {
				return "", fmt.Errorf("cannot query properties url, %v", err)
			}
			//TODO: make a better test for properties to make sure contents are valid, for now we just see if it is at least a data account
			if res.Type != protocol.AccountTypeDataAccount.String() {
				return "", fmt.Errorf("properties url is not a valid properties data account")
			}
		}
		supplyLimit = big.NewInt(int64(math.Pow10(prcsn)) * int64(limit))
	}
	if len(args) > 4 {
		properties, err = url2.Parse(args[4])
		if err != nil {
			return "", fmt.Errorf("invalid properties url, %v", err)
		}
		res, err := GetUrl(properties.String())
		if err != nil {
			return "", fmt.Errorf("cannot query properties url, %v", err)
		}
		//TODO: make a better test for properties to make sure contents are valid, for now we just see if it is at least a data account
		if res.Type != protocol.AccountTypeDataAccount.String() {
			return "", fmt.Errorf("properties url is not a valid properties data account")
		}
	}

	u, err := url2.Parse(url)
	if err != nil {
		return "", err
	}

	params := protocol.CreateToken{}
	params.Url = u
	params.Symbol = symbol
	params.Precision = uint64(prcsn)
	params.Properties = properties
	params.SupplyLimit = supplyLimit
	params.Authorities = x.Create.Authorities

	return x.dispatchTxAndPrintResponse(cmd, &params, originUrl, signer)
}

func (x *Context) IssueTokenToRecipient(cmd *cobra.Command, origin string, args []string) (string, error) {
	originUrl, err := url2.Parse(origin)
	if err != nil {
		return "", err
	}

	signer, err := x.getSigners(cmd, originUrl, nil)
	if err != nil {
		return "", err
	}

	params := protocol.IssueTokens{}
	for i := 0; i < len(args); i += 2 {
		u2, err := url2.Parse(args[i])
		if err != nil {
			return "", fmt.Errorf("invalid receiver url %s, %v", args[i], err)
		}

		amt, err := amountToBigInt(originUrl.String(), args[i+1])
		if err != nil {
			return "", fmt.Errorf("invalid amount for receiver url %s, amount %s, %v",
				args[i], args[i+1], err)
		}

		params.To = append(params.To, &protocol.TokenRecipient{Url: u2, Amount: *amt})
	}

	seen := map[string]bool{}
	for _, r := range params.To {
		s := r.Url.String() + " " + r.Amount.String()
		if seen[s] {
			return "", fmt.Errorf("duplicate outputs for %v", r.Url)
		}
		seen[s] = true
	}

	return x.dispatchTxAndPrintResponse(cmd, &params, originUrl, signer)
}

func (x *Context) BurnTokens(cmd *cobra.Command, origin string, args []string) (string, error) {
	originUrl, err := url2.Parse(origin)
	if err != nil {
		return "", err
	}

	signer, err := x.getSigners(cmd, originUrl, nil)
	if err != nil {
		return "", err
	}

	tokenUrl, err := GetTokenUrlFromAccount(originUrl)
	if err != nil {
		return "", fmt.Errorf("invalid token url was obtained from %s, %v", originUrl.String(), err)
	}

	//query the token precision and reformat amount argument into a bigInt.
	amt, err := amountToBigInt(tokenUrl.String(), args[0])
	if err != nil {
		return "", err
	}

	params := protocol.BurnTokens{}
	params.Amount.Set(amt)

	return x.dispatchTxAndPrintResponse(cmd, &params, originUrl, signer)
}
