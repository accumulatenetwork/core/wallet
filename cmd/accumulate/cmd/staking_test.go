package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	. "gitlab.com/accumulatenetwork/accumulate/test/helpers"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/testutils"
)

// Convert ADI Token Account to Staking account, should pass
func TestStakingConvert(t *testing.T) {
	// Setup
	tc := newTestCmd(t, testCmdOpts{})
	_, err := _wallet.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:       tc.dir,
		Token:      testutils.TestToken[:],
		MultiVault: false,
		Passphrase: "",
	})
	require.NoError(t, err)

	// Import the mnemonic
	_, err = _wallet.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	key := new(KeyResponse)
	out, err := tc.execute(t, "account generate")
	require.NoError(t, err)
	require.NoError(t, json.Unmarshal([]byte(out), key))

	keyName := t.Name()
	_, err = tc.execute(t, fmt.Sprintf("key generate %s", keyName))
	require.NoError(t, err)

	lta := key.LiteAccount
	liteId := lta.RootIdentity().String()

	_, err = tc.executeTx(t, "faucet %s", lta)
	require.NoError(t, err)

	_, err = tc.executeTx(t, "credits %s %[1]s 10000", lta)
	require.NoError(t, err)

	staking := protocol.AccountUrl("staking")
	_, err = tc.executeTx(t, "adi create %s %s %s", liteId, staking, keyName)
	require.NoError(t, err)
	_, err = tc.executeTx(t, "credits %s %s/book/1 1000", lta, staking)
	require.NoError(t, err)
	_, err = tc.executeTx(t, "account create data %s --sign-with=%s %[1]s/requests --authority staking.acme/book", staking, keyName)
	require.NoError(t, err)
	_, err = tc.executeTx(t, "auth disable %s/requests --sign-with=%s %[1]s/book", staking, keyName)
	require.NoError(t, err)

	adi := protocol.AccountUrl("Foo" + t.Name())
	_, err = tc.executeTx(t, "adi create %s %s %s", liteId, adi, keyName)
	require.NoError(t, err)
	_, err = tc.executeTx(t, "credits %s %s/book/1 1000", lta, adi)
	require.NoError(t, err)

	_, err = tc.executeTx(t, "account create token %s --sign-with=%s %[1]s/tokens ACME --authority %[1]s/book", adi, keyName)
	require.NoError(t, err)

	b, err := tc.executeTx(t, "staking convert %s/tokens --sign-with=%s --type coreValidator", adi, keyName)
	require.NoError(t, err)
	b = strings.SplitN(b, "\n", 2)[0]
	ar := new(ActionResponse)
	require.NoError(t, json.Unmarshal([]byte(b), ar))

	_, err = tc.executeTx(t, "tx sign %x --sign-with=%s@staking.acme", ar.TransactionHash, keyName)
	require.NoError(t, err)

	for {
		tokens := GetAccount[*protocol.TokenAccount](t, tc.sim.DatabaseFor(adi), adi.JoinPath("tokens"))
		if len(tokens.Authorities) > 1 {
			break
		}
		time.Sleep(time.Second / 10)
	}

	tokens := GetAccount[*protocol.TokenAccount](t, tc.sim.DatabaseFor(adi), adi.JoinPath("tokens"))
	entry, ok := tokens.GetAuthority(staking.JoinPath("book"))
	require.True(t, ok, "Account must have staking as an authority")
	require.False(t, entry.Disabled, "Staking authority must be enabled")
	entry, ok = tokens.GetAuthority(adi.JoinPath("book"))
	require.True(t, ok, "Account must have ADI as an authority")
	require.True(t, entry.Disabled, "ADI authority must be disabled")
}
