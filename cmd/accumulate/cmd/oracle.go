package cmd

import (
	"encoding/json"
	"strconv"

	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
)

var oracleCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "oracle",
		Short: "Retrieve the oracle value",
		Args:  cobra.NoArgs,
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			return GetCreditValue()
		}),
	}
})

func GetCreditValue() (string, error) {
	acmeOracle, err := QueryAcmeOracle()
	if err != nil {
		return "", err
	}

	if WantJsonOutput {
		data, err := json.Marshal(&acmeOracle)
		if err != nil {
			return "", err
		}
		return string(data), nil
	}

	usd := float64(acmeOracle.Price) / protocol.AcmeOraclePrecision
	credits := (usd * protocol.CreditUnitsPerFiatUnit) / protocol.CreditPrecision
	out := "USD per ACME : $" + strconv.FormatFloat(usd, 'f', 4, 64)
	out += "\nCredits per ACME : " + strconv.FormatFloat(credits, 'f', 2, 64)

	return out, nil
}
