//go:build !windows

package cmd

import (
	"bytes"
	"fmt"
	"io"
	"io/fs"
	"os"
	"strconv"
	"syscall"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"golang.org/x/sys/unix"
)

var walletStopCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "stop [pid-file]",
		Short: "Stop the daemon",
		Args:  cobra.MaximumNArgs(1),
		Run:   x.runCmdFunc(x.stopDaemon),
	}
})

func init() {
	walletCmd.AddCommand(walletStopCmd)
}

func (x *Context) stopDaemon(args []string) (string, error) {
	pidFile := x.GetString("daemon-pid-file")
	if len(args) > 0 {
		pidFile = args[0]
	}

	// Look for a PID file
	f, err := os.Open(pidFile)
	switch {
	case err == nil:
		// Ok
	case errors.Is(err, fs.ErrNotExist):
		return "Not running (no pid file)", nil
	default:
		return "", err
	}
	defer f.Close()

	// Read and parse the PID file
	b, err := io.ReadAll(f)
	if err != nil {
		return "", errors.UnknownError.WithFormat("reading PID file: %w", err)
	}
	b = bytes.TrimSpace(b)

	pid, err := strconv.ParseUint(string(b), 10, 64)
	if err != nil {
		return "", errors.UnknownError.WithFormat("parsing PID: %w", err)
	}

	// Find the process
	proc, err := os.FindProcess(int(pid))
	if err != nil {
		return "", errors.UnknownError.WithFormat("find process %d: %w", pid, err)
	}

	// Is it alive?
	if !procLives(proc) {
		return "Not running", nil
	}

	// In succession, interrupt, terminate, and kill the process
	if !kill(proc, unix.SIGINT, "Interrupting daemon") &&
		!kill(proc, unix.SIGTERM, "Daemon did not exit - terminating") &&
		!kill(proc, unix.SIGKILL, "Daemon did not exit - killing") {
		return "", errors.UnknownError.With("unable to stop daemon")
	}

	return "Stopped", nil
}

func kill(proc *os.Process, signal os.Signal, msg string) bool {
	// Send the signal
	fmt.Println(msg)
	check(proc.Signal(signal))

	// Wait for the process to exit
	for i := 0; i < 1000 && procLives(proc); i++ {
		time.Sleep(time.Millisecond)
	}

	// Did it exit?
	return !procLives(proc)
}

// procLives checks if a process is alive
func procLives(proc *os.Process) bool {
	// Send signal zero
	err := proc.Signal(syscall.Signal(0))
	switch {
	case err == nil:
		return true // Process is alive
	case errors.Is(err, os.ErrProcessDone):
		return false // Process is dead
	default:
		check(err)
		panic("not reached")
	}
}
