package cmd

import (
	"context"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

func init() {
	resubmitCmd.AddCommand(resubmitAnchorCmd)
}

var resubmitCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use: "resubmit",
	}
})

var resubmitAnchorCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "anchor [from-server] [from-url] [to-url] [sequence-number]",
		Short: "Resubmit an anchor",
		Args:  cobra.ExactArgs(4),
		Run:   x.runCmdFunc2(x.resubmitAnchor),
	}
})

func (x *Context) resubmitAnchor(cmd *cobra.Command, args []string) (string, error) {
	from, err := client.New(args[0])
	if err != nil {
		return "", err
	}

	fromUrl, err := url.Parse(args[1])
	if err != nil {
		return "", err
	}

	toUrl, err := url.Parse(args[2])
	if err != nil {
		return "", err
	}

	num, err := strconv.ParseUint(args[3], 10, 64)
	if err != nil {
		return "", err
	}

	req1 := new(client.SyntheticTransactionRequest)
	req1.Source = fromUrl
	req1.Destination = toUrl
	req1.SequenceNumber = num
	req1.Anchor = true
	res1, err := from.QuerySynth(context.Background(), req1)
	if err != nil {
		return "", err
	}

	for i, signature := range res1.Signatures {
		switch signature.(type) {
		case *protocol.PartitionSignature:
			res1.Signatures[0], res1.Signatures[i] = res1.Signatures[i], res1.Signatures[0]
		}
	}

	req2 := new(client.ExecuteRequest)
	req2.Envelope = new(messaging.Envelope)
	req2.Envelope.Transaction = []*protocol.Transaction{res1.Transaction}
	req2.Envelope.Signatures = res1.Signatures

	res2, err := Client.ExecuteLocal(context.Background(), req2)
	if err != nil {
		return PrintJsonRpcError(err)
	}
	if res2.Code != 0 {
		result := new(protocol.TransactionStatus)
		if Remarshal(res2.Result, result) != nil {
			return "", errors.EncodingError.With(res2.Message)
		}
		return "", result.Error
	}

	var resps []*client.TransactionQueryResponse
	if TxWait != 0 {
		resps, err = waitForTxnUsingHash(res2.TransactionHash, TxWait, TxIgnorePending)
		if err != nil {
			return PrintJsonRpcError(err)
		}
	}

	result, err := ActionResponseFrom(res2).Print(x)
	if err != nil {
		return "", err
	}
	if res2.Code == 0 {
		for _, response := range resps {
			str, err := x.PrintTransactionQueryResponseV2(response)
			if err != nil {
				return PrintJsonRpcError(err)
			}
			result = fmt.Sprint(result, str, "\n")
		}
	}
	return result, nil
}
