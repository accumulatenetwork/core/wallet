package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/network"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/internal/build"
)

func init() {
	followerCmd.AddCommand(
		followerAddCmd,
		followerRemoveCmd,
		followerUpdateKeyCmd)

}

var followerCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "follower",
		Short: "Manage followers",
	}
})
var followerAddCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "add [partition ID] [key name or path]",
		Short: "Add a follower",
		Run:   x.runValCmdFunc(x.addFollower),
		Args:  cobra.ExactArgs(2),
	}
})

var followerRemoveCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "remove [partition ID] [key name or path]",
		Short: "Remove a follower",
		Run:   x.runValCmdFunc(x.removeFollower),
		Args:  cobra.ExactArgs(2),
	}
})

var followerUpdateKeyCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "update-key [partition ID] [old key name or path] [new key name or path]",
		Short: "Update a follower's key",
		Run:   x.runValCmdFunc(x.updateFollowerKey),
		Args:  cobra.ExactArgs(3),
	}
})

func (x *Context) addFollower(cmd *cobra.Command, values *network.GlobalValues, pageCount int, _ string, args []string) (*protocol.Transaction, error) {
	newKey, err := x.resolvePublicKey(cmd, args[0])
	if err != nil {
		return nil, err
	}

	return build.AddValidator(values, pageCount, newKey, args[0], false)
}

func (x *Context) removeFollower(cmd *cobra.Command, values *network.GlobalValues, pageCount int, partition string, args []string) (*protocol.Transaction, error) {
	oldKey, err := x.resolvePublicKey(cmd, args[0])
	if err != nil {
		return nil, err
	}

	return build.RemoveValidatorFrom(values, pageCount, oldKey, partition)
}

func (x *Context) updateFollowerKey(cmd *cobra.Command, values *network.GlobalValues, _ int, _ string, args []string) (*protocol.Transaction, error) {
	oldKey, err := x.resolvePublicKey(cmd, args[0])
	if err != nil {
		return nil, err
	}

	newKey, err := x.resolvePublicKey(cmd, args[1])
	if err != nil {
		return nil, err
	}

	return build.UpdateValidatorKey(values, oldKey, newKey)
}
