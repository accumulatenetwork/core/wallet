package cmd

import (
	"context"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/address"
	url2 "gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/daemon/services"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

func init() {
	adiCmd.AddCommand(
		adiGetCmd,
		adiListCmd,
		adiDirectoryCmd,
		adiCreateCmd,
		adiRegisterCmd)
}

var adiCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "adi",
		Short: "Create and manage ADI",
	}
})

var adiGetCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "get [url]",
		Short: "Get existing ADI by URL",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.GetADI(cmd, args[0])
			x.printOutput(cmd, out, err)
		},
	}
})

var adiListCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "list",
		Short: "Get existing ADI by URL",
		Args:  cobra.NoArgs,
		Run:   x.runCmdFunc2(x.listAdis),
	}
})

var adiDirectoryCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "directory [url] [from] [to]",
		Short: "Get directory of URL's associated with an ADI with starting index and number of directories to receive",
		Args:  cobra.ExactArgs(3),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.GetAdiDirectory(cmd, args[0], args[1], args[2])
			x.printOutput(cmd, out, err)
		},
	}
})

var adiCreateCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "create [sponsor or parent] [new ADI url] [public key (optional)] [key book url (optional)]",
		Short: "Create new ADI",
		Args:  cobra.RangeArgs(2, 4),
		Run: x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
			u, err := url2.Parse(args[0])
			if err != nil {
				return "", err
			}

			return x.createADI(cmd, u, args[1:])
		}),
	}
}).
	With((*Context).submitFlags).
	With((*Context).createFlags)

var adiRegisterCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "register",
		Short: "Register an existing ADI with this vault",
		Args:  cobra.ExactArgs(1),
		Run:   x.runCmdFunc2(x.registerAdi),
	}
})

func (x *Context) GetAdiDirectory(cmd *cobra.Command, origin string, start string, count string) (string, error) {
	u, err := url2.Parse(origin)
	if err != nil {
		return "", err
	}

	st, err := strconv.ParseInt(start, 10, 64)
	if err != nil {
		return "", fmt.Errorf("invalid start value")
	}

	ct, err := strconv.ParseInt(count, 10, 64)
	if err != nil {
		return "", fmt.Errorf("invalid count value")
	}
	if ct < 1 {
		return "", fmt.Errorf("count must be greater than zero")
	}

	params := client.DirectoryQuery{}
	params.Url = u
	params.Start = uint64(st)
	params.Count = uint64(ct)
	params.Expand = true

	data, err := json.Marshal(&params)
	if err != nil {
		return "", err
	}

	var res client.MultiResponse
	if err := Client.RequestAPIv2(context.Background(), "query-directory", json.RawMessage(data), &res); err != nil {
		return PrintJsonRpcError(err)
	}

	return x.PrintMultiResponse(cmd, &res)
}

func (x *Context) GetADI(cmd *cobra.Command, url string) (string, error) {
	res, err := GetUrl(url)
	if err != nil {
		return "", err
	}

	if res.Type != protocol.AccountTypeIdentity.String() {
		return "", fmt.Errorf("expecting ADI chain but received %v", res.Type)
	}

	return x.PrintChainQueryResponseV2(cmd, res)
}

func (x *Context) listAdis(cmd *cobra.Command, _ []string) (string, error) {
	res, err := x.getWallet().AdiList(context.Background(), &api.AdiListRequest{})
	if err != nil {
		return "", err
	}
	var out string
	for _, v := range res.ADIs {
		var lab string
		switch {
		case v.Key == nil:
			lab = "(no key)"
		case v.Key.Labels == nil:
			h := sha256.Sum256(v.Key.PublicKey)
			lab = (&address.UnknownHash{Hash: h[:]}).String()
		case len(v.Key.Labels.Names) > 0:
			lab = v.Key.Labels.Names[0]
		case v.Key.KeyInfo.Type != protocol.SignatureTypeUnknown:
			lab = (&services.Key{Key: *v.Key}).String()
		default:
			lab = v.Key.Labels.Lite
		}
		out += fmt.Sprintf("%v\t:\t%s\n", v.Url, lab)
	}
	return out, nil
}

func (x *Context) registerAdi(cmd *cobra.Command, args []string) (string, error) {
	u, err := url2.Parse(args[0])
	if err != nil {
		return "", errors.UnknownError.WithFormat("parse ADI: %w", err)
	}
	_, err = x.getWallet().RegisterADI(context.Background(), &api.RegisterADIRequest{ADI: u})
	if err != nil {
		return "", err
	}
	return "Registered", nil
}

func (x *Context) createADI(cmd *cobra.Command, origin *url2.URL, args []string) (string, error) {
	signer, err := x.getSigners(cmd, origin, nil)
	if err != nil {
		return "", err
	}

	var adiUrlStr string
	var bookUrlStr string

	//at this point :
	//args[0] must be the new adi you are creating
	//args[1] may be the public key you are assigning to the adi
	//args[2] may be the key book name
	//Note: if args[2] is not the keybook, the keypage also cannot be specified.
	if len(args) < 1 {
		return "", fmt.Errorf("insufficient number of command line arguments")
	}

	adiUrlStr = args[0]

	var k *services.Key
	if len(args) > 1 {
		k, err = x.resolvePublicKey(cmd, args[1])
		if err != nil {
			return "", err
		}
	}

	adiUrl, err := url2.Parse(adiUrlStr)
	if err != nil {
		return "", fmt.Errorf("invalid adi url %s, %v", adiUrlStr, err)
	}

	if a, _ := getAccount(adiUrlStr); a != nil {
		return "", fmt.Errorf("the account %s already exists", adiUrlStr)
	}

	var bookUrl *url2.URL
	if len(args) > 2 {
		bookUrlStr = args[2]

		bookUrl, err = url2.Parse(bookUrlStr)
		if err != nil {
			return "", fmt.Errorf("invalid book url %s, %v", bookUrlStr, err)
		}
	} else if k != nil {
		bookUrl = adiUrl.JoinPath("/book")
	}

	idc := protocol.CreateIdentity{}
	idc.Url = adiUrl
	idc.KeyBookUrl = bookUrl
	idc.Authorities = x.Create.Authorities
	if k != nil {
		idc.KeyHash = k.PublicKeyHash()
	}

	out, err := x.dispatchTxAndPrintResponse(cmd, &idc, origin, signer)
	if err != nil {
		return PrintJsonRpcError(err)
	}
	if k == nil {
		return out, nil
	}

	_, err = x.getWallet().RegisterADI(context.Background(), &api.RegisterADIRequest{ADI: adiUrl, Key: k.PublicKey})
	if err != nil {
		return "", fmt.Errorf("DB: %v", err)
	}

	return out, nil
}
