package cmd

import (
	"context"
	"encoding/json"
	"strings"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/testutils"
)

func TestSuggestion(t *testing.T) {
	// Setup
	tc := newTestCmd(t, testCmdOpts{})
	_, err := _wallet.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:       tc.dir,
		Token:      testutils.TestToken[:],
		MultiVault: false,
		Passphrase: "",
	})
	require.NoError(t, err)

	_, err = _wallet.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	key, err := _wallet.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Type: protocol.SignatureTypeED25519,
	})
	require.NoError(t, err)

	_, err = tc.executeTx(t, "faucet %s/ACME", key.Key.Labels.Lite)
	require.NoError(t, err)

	_, err = tc.executeTx(t, "credits %s/ACME %[1]s 10000", key.Key.Labels.Lite)
	require.NoError(t, err)

	// Submit a suggested transaction
	out, err := tc.executeTx(t, "tx create --suggest %s/ACME foo 1.00", key.Key.Labels.Lite)
	require.NoError(t, err)
	r := fromJSON[ActionResponse](t, out)

	// Verify
	for _, hash := range r.SignatureHashes {
		r, err := Q.QuerySignature(context.Background(), url.MustParse(key.Key.Labels.Lite).WithTxID(hash.AsBytes32()), nil)
		require.NoError(t, err)
		require.Equal(t, "suggest", r.Message.Signature.GetVote().String())
	}
}

func fromJSON[V any](t testing.TB, s string) (v V) {
	// Use a decoder to only grab the first value
	require.NoError(t, json.NewDecoder(strings.NewReader(s)).Decode(&v))
	return
}
