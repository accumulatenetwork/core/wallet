package cmd

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/accumulate/test/harness"
	. "gitlab.com/accumulatenetwork/accumulate/test/helpers"
	"gitlab.com/accumulatenetwork/accumulate/test/simulator"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/testutils"
)

func TestRegisterPage(t *testing.T) {
	// Setup
	tc := newTestCmd(t, testCmdOpts{})
	x := new(Context)
	cmd := rootCmd.Build(x)
	_, err := _wallet.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:       tc.dir,
		Token:      testutils.TestToken[:],
		MultiVault: false,
		Passphrase: "",
	})
	require.NoError(t, err)

	sim := harness.NewSim(t,
		simulator.SimpleNetwork("Simulator", 1, 1),
		simulator.Genesis(harness.GenesisTime),
	)
	Client = sim.S.NewDirectClientWithHook(func(h http.Handler) http.Handler { return h })
	ClientV3 = sim.S.Services()
	Q.Querier = ClientV3

	MakeIdentity(t, sim.Database("BVN0"), url.MustParse("foo"))
	MakeAccount(t, sim.Database("BVN0"),
		&protocol.KeyBook{Url: url.MustParse("foo/bar"), PageCount: 1},
		&protocol.KeyPage{Url: url.MustParse("foo/bar/1")},
	)

	// Register a page
	err = x.registerBook(cmd, url.MustParse("foo/bar/1"))
	require.NoError(t, err)

	// Verify the book is registered
	r, err := _wallet.ListAccounts(context.Background(), &api.ListAccountsRequest{
		Wallet: tc.dir,
		Token:  testutils.TestToken[:],
	})
	require.NoError(t, err)
	require.Len(t, r.Books, 1)
	require.Equal(t, "acc://foo/bar", r.Books[0].Url.String())
}
