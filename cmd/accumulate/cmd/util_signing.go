package cmd

import (
	"bytes"
	"fmt"
	"strings"
	"time"

	"github.com/fatih/color"
	"github.com/manifoldco/promptui"
	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/daemon/services"
)

var ErrNonInteractive = fmt.Errorf("non-interactive")

type signWithFlag []*url.URL

func (f *signWithFlag) Type() string { return "url" }

func (f *signWithFlag) String() string {
	s := make([]string, 0, len(*f))
	for _, u := range *f {
		s = append(s, u.ShortString())
	}
	return strings.Join(s, ",")
}

func (f *signWithFlag) Set(s string) error {
	for _, s := range strings.Split(s, ",") {
		s = strings.TrimSpace(s)
		label, account := parseLabelAndOrAccount(s)
		if account == nil {
			account = url.MustParse("*")
		}
		if label != "" {
			account = account.WithUserInfo(label)
		}
		*f = append(*f, account)
	}
	return nil
}

// getSigners will scan the database for a key that can be used to sign for the
// principal. If one or more signers are explicitly specified with --sign-with,
// getSigners will instead resolve those.
//
// If delegators are specified with --delegator, getSigners will load and
// register them. --delegator currently has no other effect on signing.
//
// The txnAuthorities parameter is a list of additional authorities that should
// be considered, in the case of signing a transaction that requires additional
// authorities, such as adding a delegate.
func (x *Context) getSigners(cmd *cobra.Command, principal *url.URL, txn *protocol.Transaction) ([]*api.SignRequest, error) {
	paths, err := x.getSigners2(cmd, principal, txn)
	if err != nil {
		return nil, err
	}

	signer, err := buildSigningRequestForPaths(cmd, paths, true)
	if err != nil {
		return nil, err
	}

	if x.Submit.Suggest {
		signer.Vote = protocol.VoteTypeSuggest
	}

	return []*api.SignRequest{signer}, nil
}

func (x *Context) getSigners2(cmd *cobra.Command, principal *url.URL, txn *protocol.Transaction) ([]*api.SigningPath, error) {
	// If the user manually specifies signers, use those
	var label string
	var signers []*url.URL
	for _, s := range x.Sign.With {
		if s.UserInfo == "" {
			signers = append(signers, s)
			continue
		}

		if label != "" {
			return nil, errors.BadRequest.WithFormat("cannot specify multiple keys with --sign-with")
		}

		label = s.UserInfo
		if s.Authority != "*" {
			s = s.WithUserInfo("")
			signers = append(signers, s)
		}
	}

	// Otherwise check the principal for signers
	return x.findSignersForAccount(cmd, principal, label, signers, txn)
}

func buildSigningRequestForPaths(cmd *cobra.Command, paths []*api.SigningPath, verifySigner bool) (*api.SignRequest, error) {
	// Group paths by final authority and build a list of authorities
	pathByAuth := map[[32]byte][]*api.SigningPath{}
	var authorities []*url.URL
	for _, path := range paths {
		book := path.Signers[len(path.Signers)-1].Identity()
		id := book.AccountID32()
		if len(pathByAuth[id]) == 0 {
			authorities = append(authorities, book)
		}
		pathByAuth[id] = append(pathByAuth[id], path)
	}

	// Choose an authority
	var authority *url.URL
	if len(authorities) == 1 {
		authority = authorities[0]
		fmt.Println("Authority:", authority)
	} else {
		if NonInteractive {
			return nil, fmt.Errorf("%w: unable to choose between multiple signing options", ErrNonInteractive)
		}
		i, _, err := (&promptui.Select{
			Label: "Which key book do you want to use?",
			Items: authorities,
		}).Run()
		if err != nil {
			return nil, err
		}
		authority = authorities[i]
	}

	// Expand each key into a separate path
	paths = nil
	for _, path := range pathByAuth[authority.AccountID32()] {
		for _, key := range path.Keys {
			path := path.Copy()
			path.Keys = []*api.Key{key}
			paths = append(paths, path)
		}
	}

	// Choose a path
	var items []string
	for _, path := range paths {
		var pages []string
		for _, s := range path.Signers {
			pages = append(pages, color.GreenString(s.ShortString()))
		}
		key := (&services.Key{Key: *path.Keys[0]}).String()
		if len(path.Keys[0].Labels.Names) > 0 {
			key = path.Keys[0].Labels.Names[0]
		}
		items = append(items, fmt.Sprintf("%s @ %s", key, strings.Join(pages, color.HiBlackString(" → "))))
	}
	var path *api.SigningPath
	if len(paths) == 1 {
		path = paths[0]
		fmt.Println("Signer:", items[0])
	} else {
		if NonInteractive {
			return nil, fmt.Errorf("%w: unable to choose between multiple signing options", ErrNonInteractive)
		}
		i, _, err := (&promptui.Select{
			Label: "Which signing path do you want to use?",
			Items: items,
		}).Run()
		if err != nil {
			return nil, err
		}
		path = paths[i]
	}

	// Build the request
	req, err := buildSigningRequest(cmd, path.Signers[0], path.Keys[0], verifySigner)
	if err != nil {
		return nil, err
	}

	req.Delegators = path.Signers[1:]
	return req, nil
}

// parseLabelAndOrAccount determines if the input string is a label, account
// URL, or label @ account URL.
func parseLabelAndOrAccount(str string) (label string, account *url.URL) {
	// Can the input be interpreted as a URL?
	u, err := url.Parse(str)
	if err != nil {
		// The input is not a URL so assume it's a label (though a value that is
		// not a valid URL is probably garbage)
		return str, nil
	}

	// Treat a URL with a username as key label @ account url
	if u.Username() != "" {
		return u.Username(), u.WithUserInfo("")
	}

	// If the URL is a lite account, use that
	if isLite(u) {
		return "", u
	}

	// If the URL has a path or ends in .acme, assume the input is an account
	if !u.IsRootIdentity() || strings.HasSuffix(u.Authority, protocol.TLD) {
		return "", u
	}

	// Otherwise assume the input is a label
	return str, nil
}

func isLite(u *url.URL) bool {
	if protocol.AcmeUrl().Equal(u) {
		return false
	}
	if strings.Contains(u.Hostname(), ".") {
		return false
	}

	if key, _ := protocol.ParseLiteAddress(u); key == nil {
		return false
	}
	return true
}

// findSignersForAccount determines the relevant authorities for the given
// account and looks up keys and determines signing paths for the authorities of
// that account. If filterKey is not empty, findSignersForAccount will only
// return signing paths using the given key. filterKey may be an address, lite
// identity, key label, public key, or public key hash. If filterKey is not
// empty, findSignersForAccount attempts to resolve it to a known key before
// doing anything else and will fail if it cannot be resolved.
//
//   - If account is a key page, findSignersForAccount will only return paths
//     starting from that page.
//   - If account is a lite data account, findSignersForAccount will return an
//     error since an LDA has no authorities.
//   - If account is a lite identity or lite token account, findSignersForAccount
//     will only return keys that match the lite identity.
//   - If account is a full account, findSignersForAccount will return paths for
//     its authorities.
//
// If account is a key page or full account, findSignersForAccount will pull and
// register the key book(s) and their pages prior to searching for keys and
// paths. findSignersForAccount will *not* automatically pull and register
// delegators.
func (x *Context) findSignersForAccount(cmd *cobra.Command, accountUrl *url.URL, filterKey string, filterSigners []*url.URL, txn *protocol.Transaction) ([]*api.SigningPath, error) {
	// Resolve the filter key now to avoid doing extra work if it's garbage
	var resolvedKey []byte
	if filterKey != "" {
		r, err := x.getWallet().ResolveKey(cmd.Context(), &api.ResolveKeyRequest{Value: filterKey})
		if err != nil {
			return nil, err
		}
		resolvedKey = r.Key.PublicKey
	}

	// Load the account
	account, err := querySignerAccount(cmd, accountUrl)
	if err != nil {
		return nil, err
	}

	var authorities []*url.URL
	if x.Sign.For != nil {
		// Use the specified authority
		authorities = []*url.URL{x.Sign.For}
		err = x.registerBook(cmd, x.Sign.For)
		if err != nil {
			return nil, err
		}

	} else {
		// Register the account's authorities
		authorities, err = x.registerAuthorities(cmd, account, false)
		if err != nil {
			return nil, err
		}

		// Register the requested signer's authorities
		for _, s := range filterSigners {
			signer, err := querySignerAccount(cmd, s)
			if err != nil {
				return nil, err
			}
			_, err = x.registerAuthorities(cmd, signer, true)
			if err != nil {
				return nil, err
			}
		}

		// If the transaction is UpdateKeyPage, register the page's delegates as
		// authorities
		if page, ok := account.(*protocol.KeyPage); ok && txn != nil && txn.Body.Type() == protocol.TransactionTypeUpdateKey {
			for _, entry := range page.Keys {
				if entry.Delegate != nil {
					err = x.registerBook(cmd, entry.Delegate)
					if err != nil {
						return nil, err
					}
					authorities = append(authorities, entry.Delegate)
				}
			}
		}

		// Register the transaction's authorities
		if txn != nil {
			for _, auth := range txn.GetAdditionalAuthorities() {
				err = x.registerBook(cmd, auth)
				if err != nil {
					return nil, err
				}
				authorities = append(authorities, auth)
			}
		}
	}

	// Find signing paths
	resp, err := x.getWallet().FindSigner(cmd.Context(), &api.FindSignerRequest{Authorities: authorities})
	if err != nil {
		return nil, err
	}

	if txn != nil && txn.Body.Type() == protocol.TransactionTypeUpdateKey {
		// Filter out delegated entries (and don't filter by the page)
		var paths []*api.SigningPath
		for _, p := range resp.Paths {
			if len(p.Signers) == 1 {
				paths = append(paths, p)
			}
		}
		resp.Paths = paths

	} else if page, ok := account.(*protocol.KeyPage); ok {
		// Filter out paths that end with a lower priority page
		_, myPageNo, _ := protocol.ParseKeyPageUrl(page.Url)
		keep := func(path *api.SigningPath) bool {
			signer := path.Signers[len(path.Signers)-1]
			book, theirPageNo, ok := protocol.ParseKeyPageUrl(signer)
			return !ok || !book.ParentOf(signer) || theirPageNo <= myPageNo
		}

		var paths []*api.SigningPath
		for _, p := range resp.Paths {
			if keep(p) {
				paths = append(paths, p)
			}
		}
		resp.Paths = paths
	}

	// Filter out paths that don't include the specified signers
	for _, s := range filterSigners {
		var paths []*api.SigningPath
		for _, p := range resp.Paths {
			if pathContainsSigner(p, s) {
				paths = append(paths, p)
			}
		}
		resp.Paths = paths
	}

	// Filter out paths except for those for the specified key
	if resolvedKey != nil {
		// Prune paths/keys that don't include/aren't the specified key
		var paths []*api.SigningPath
		for _, p := range resp.Paths {
			var keys []*api.Key
			for _, k := range p.Keys {
				if bytes.Equal(k.PublicKey, resolvedKey) {
					keys = append(keys, k)
				}
			}
			if len(keys) > 0 {
				p.Keys = keys
				paths = append(paths, p)
			}
		}
		resp.Paths = paths
	}

	if len(resp.Paths) == 0 {
		return nil, fmt.Errorf("cannot find any keys for %v", accountUrl)
	}
	return resp.Paths, nil
}

func pathContainsSigner(path *api.SigningPath, signer *url.URL) bool {
	for _, s := range path.Signers {
		if signer.Equal(s) || signer.PrefixOf(s) {
			return true
		}
	}
	return false
}

func querySignerAccount(cmd *cobra.Command, account *url.URL) (protocol.Account, error) {
	r, err := Q.QueryAccount(cmd.Context(), account, nil)
	switch {
	case err == nil:
		return r.Account, nil

	case !errors.Is(err, errors.NotFound):
		// Unknown error
		return nil, err

	case isLite(account):
		// Allow non-existent lite accounts
		return &protocol.LiteIdentity{Url: account.RootIdentity()}, nil

	default:
		// Non-lite, does not exist
		return nil, fmt.Errorf("account %v does not exist (%w)", account, err)
	}
}

func (x *Context) registerAuthorities(cmd *cobra.Command, account protocol.Account, isSigner bool) ([]*url.URL, error) {
	// Get the authority set
	var authorities []*url.URL
	var register bool
	switch account := account.(type) {
	case *protocol.KeyBook:
		// Scan the book, or its authorities
		register = true
		if isSigner {
			authorities = []*url.URL{account.Url}
			break
		}

		for _, auth := range account.GetAuth().Authorities {
			authorities = append(authorities, auth.Url)
		}

	case *protocol.LiteIdentity,
		*protocol.LiteTokenAccount:
		// Check the lite identity
		authorities = []*url.URL{account.GetUrl().RootIdentity()}

	case *protocol.LiteDataAccount:
		// A lite data account does not have an authority set
		return nil, fmt.Errorf("the signer for a lite data account must be specified explicitly")

	case *protocol.KeyPage:
		// Scan the book
		register = true
		authorities = []*url.URL{account.GetAuthority()}

	case protocol.FullAccount:
		// Scan all the account's authorities (including disabled ones)
		register = true
		for _, auth := range account.GetAuth().Authorities {
			authorities = append(authorities, auth.Url)
		}

	default:
		return nil, fmt.Errorf("unknown account type %v", account.Type())
	}

	// Add additional authorities
	if !isSigner {
		authorities = append(authorities, x.Create.Authorities...)
	}

	// Pull and register the authorities
	if register {
		for _, auth := range authorities {
			err := x.registerBook(cmd, auth)
			if err != nil {
				return nil, err
			}
		}
	}

	// If the authority set is empty, go up one level
	if len(authorities) == 0 {
		u, ok := account.GetUrl().Parent()
		if !ok {
			return nil, errors.InternalError.WithFormat("%q has no authority set but is a root account", account.GetUrl())
		}

		r, err := Q.QueryAccount(cmd.Context(), u, nil)
		if err != nil {
			return nil, err
		}
		return x.registerAuthorities(cmd, r.Account, false)
	}

	return authorities, nil
}

func buildSigningRequest(cmd *cobra.Command, signerUrl *url.URL, key *api.Key, verifySigner bool) (*api.SignRequest, error) {
	// Load the signer
	var signer protocol.Signer
	_, err := Q.QueryAccountAs(cmd.Context(), signerUrl, nil, &signer)
	switch {
	case err == nil:
		// Ok

	case !errors.Is(err, errors.NotFound):
		// Unknown error
		return nil, err

	case verifySigner || !isLite(signerUrl):
		// Signer does not exist
		return nil, fmt.Errorf("signer %v does not exist (%w)", signerUrl, err)

	default:
		// Signer is lite, does not exist, and verification is not requested -
		// fabricate a signer
		signer = &protocol.LiteIdentity{
			Url: signerUrl.RootIdentity(),
		}
	}

	// Set the timestamp
	timestamp := uint64(time.Now().UTC().UnixMilli())
	if _, entry, ok := signer.EntryByKey(key.PublicKey); ok && timestamp <= entry.GetLastUsedOn() {
		timestamp = entry.GetLastUsedOn() + 1
	}

	// Create the signature request
	req := new(api.SignRequest)
	req.Signer = signer.GetUrl()
	req.SignerVersion = signer.GetVersion()
	req.Timestamp = timestamp
	req.PublicKey = key.PublicKey
	return req, nil
}
