package cmd

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/ledger"
)

var walletID string
var ledgerSigType string

var ledgerCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "ledger",
		Short: "ledger commands",
	}
})

var ledgerInfoCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "info --walletID [wallet id] (optional) --debug",
		Short: "get list of wallets with their info",
		Args:  cobra.NoArgs,
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.queryWallets(cmd)
			x.printOutput(cmd, out, err)
		},
	}
})

var ledgerKeyGenerateCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "key generate [key name/label]",
		Short: "generate keypair on a ledger device and give it a name",
		Args:  cobra.ExactArgs(1),
		Run: func(cmd *cobra.Command, args []string) {
			out, err := x.legerGenerateKey(cmd, args)
			x.printOutput(cmd, out, err)
		},
	}
})

func init() {
	ledgerCmd.AddCommand(ledgerInfoCmd)
	ledgerCmd.AddCommand(ledgerKeyGenerateCmd)
	ledgerKeyGenerateCmd.Flags().StringVar(&walletID, "wallet-id", "", "specify the wallet id by \"ledger info\" index or ledger ID URL")
	ledgerKeyGenerateCmd.Flags().BoolVar(&ledger.Debug, "debug", false, "set debug logging on")
	ledgerKeyGenerateCmd.Flags().StringVar(&ledgerSigType, "sigtype", "accumulate", "set the signature type for the key to be generated")
}

func (x *Context) queryWallets(cmd *cobra.Command) (string, error) {
	resp, err := x.getWallet().LedgerQueryWallets(context.Background())
	if err != nil {
		return "", err
	}

	ledgerInfos := resp.LedgerWalletsInfo
	if WantJsonOutput {
		str, err := json.Marshal(ledgerInfos)
		if err != nil {
			return "", err
		}
		return string(str), nil
	} else {
		result := fmt.Sprintln("Wallets:")
		for i, ledgerInfo := range ledgerInfos {
			var wid string
			if ledgerInfo.WalletID != nil {
				wid = ledgerInfo.WalletID.String()
			} else {
				wid = "(unlock & start Accumulate app to display)"
			}

			result += fmt.Sprintf("%d\tManufacturer:\t%s\n", i+1, ledgerInfo.Manufacturer)
			result += fmt.Sprintf("\tProduct:\t%s\n", ledgerInfo.Product)
			result += fmt.Sprintf("\tVendor ID:\t%d\n", ledgerInfo.VendorID)
			result += fmt.Sprintf("\tProduct ID:\t%d\n", ledgerInfo.ProductID)
			if ledgerInfo.Status == "ok" {
				result += fmt.Sprintf("\tApp Version:\t%s\n", ledgerInfo.Version.Label)
			}
			result += fmt.Sprintf("\tStatus:\t\t%s\n", ledgerInfo.Status)
			result += fmt.Sprintf("\tWallet ID:\t%s\n", wid)
		}
		return result, nil
	}
}

func (x *Context) legerGenerateKey(cmd *cobra.Command, args []string) (string, error) {
	sigtype, err := validateSigType(ledgerSigType)
	if err != nil {
		return "", err
	}

	if !WantJsonOutput {
		cmd.Println("Please confirm the address on your Ledger device")
	}

	keyData, err := x.getWallet().LedgerGenerateKey(context.Background(), &api.GenerateLedgerKeyRequest{
		Labels:   []string{args[0]},
		WalletID: walletID,
		SigType:  sigtype,
	})
	if err != nil {
		return "", err
	}

	if WantJsonOutput {
		str, err := json.Marshal(keyData)
		if err != nil {
			return "", err
		}

		return string(str), nil
	}

	return fmt.Sprintf("\tname\t\t:\t%s\n\twallet ID\t:\t%s\n\tpublic key\t:\t%x\n\tkey type\t:\t%s\n", args[0],
		keyData.KeyInfo.WalletId, keyData.PublicKey, keyData.KeyInfo.Type), nil
}
