package cmd

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/spf13/cobra"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/network"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/build"
)

func init() {
	validatorCmd.AddCommand(
		validatorAddCmd,
		validatorRemoveCmd,
		validatorUpdateKeyCmd,
		validatorUpdateNameCmd)

	validatorCmd.PersistentFlags().StringVar(&validatorFlag.Temp, "temp", "", "Build the transaction in the wallet instead of submitting it")
}

var validatorCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "validator",
		Short: "Manage validators",
	}
})

var validatorAddCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "add [partition ID] [key name or path]",
		Short: "Add a validator",
		Run:   x.runValCmdFunc(x.addValidator),
		Args:  cobra.ExactArgs(2),
	}
})

var validatorRemoveCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "remove [partition ID] [key name or path]",
		Short: "Remove a validator",
		Run:   x.runValCmdFunc(x.removeValidator),
		Args:  cobra.ExactArgs(2),
	}
})

var validatorUpdateKeyCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "update-key [partition ID] [old key name or path] [new key name or path]",
		Short: "Update a validator's key",
		Run:   x.runValCmdFunc(x.updateValidatorKey),
		Args:  cobra.ExactArgs(3),
	}
})

var validatorUpdateNameCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "update-name [partition ID] [validator key] [new name]",
		Short: "Update a validator's name",
		Run:   x.runValCmdFunc(x.updateValidatorName),
		Args:  cobra.ExactArgs(3),
	}
})

var validatorFlag = struct {
	Temp string
}{}

func (x *Context) runValCmdFunc(fn func(cmd *cobra.Command, values *network.GlobalValues, pageCount int, partition string, args []string) (*protocol.Transaction, error)) func(cmd *cobra.Command, args []string) {
	return x.runCmdFunc2(func(cmd *cobra.Command, args []string) (string, error) {
		if validatorFlag.Temp != "" {
			// Make sure we don't submit the transaction
			x.Submit.Pretend = true
		}

		describe, err := Client.Describe(context.Background())
		if err != nil {
			return PrintJsonRpcError(err)
		}
		if describe.Values.Globals == nil {
			return "", errors.New("cannot determine the network's global values")
		}

		req := new(client.GeneralQuery)
		req.Url = protocol.DnUrl().JoinPath(protocol.Operators, "1")
		resp := new(client.ChainQueryResponse)
		page := new(protocol.KeyPage)
		resp.Data = page
		err = Client.RequestAPIv2(context.Background(), "query", req, resp)
		if err != nil {
			return PrintJsonRpcError(err)
		}

		partition := args[0]
		if partition == "dn" {
			partition = protocol.Directory
		}
		args[0] = protocol.DnUrl().JoinPath(protocol.Network).String()
		args, principal, signers, err := x.parseArgsAndPrepareSigner(cmd, args)
		if err != nil {
			return "", err
		}

		_, err = x.loadValuesFromTemp(cmd, protocol.Network, &describe.Values)
		if err != nil {
			return "", err
		}

		txn, err := fn(cmd, &describe.Values, len(page.Keys), partition, args)
		if err != nil {
			return "", err
		}

		if validatorFlag.Temp == "" {
			return x.dispatchTxAndPrintResponse(cmd, txn, principal, signers)
		}

		// TODO Can we do something with signatures

		body, ok := txn.Body.(*protocol.WriteData)
		if !ok {
			return "", fmt.Errorf("can't use a temp transaction - builder returned %v, expected %v", txn.Body.Type(), protocol.TransactionTypeWriteData)
		}

		_, err = x.getWallet().WriteDataTransaction(cmd.Context(), &api.WriteDataRequest{Name: validatorFlag.Temp, WriteData: body, Overwrite: true})
		if err != nil {
			return "", err
		}

		b, err := json.MarshalIndent(describe.Values.Network, "", "  ")
		if err != nil {
			return "", err
		}

		return fmt.Sprintf("New network definition:\n%s\n", b), nil
	})
}

func (x *Context) loadValuesFromTemp(_ *cobra.Command, account string, values *network.GlobalValues) (*protocol.Transaction, error) {
	if validatorFlag.Temp == "" {
		return nil, nil
	}

	w := x.getWallet()
	resp, err := w.CreateTransaction(context.Background(), &api.CreateTransactionRequest{
		Name:        validatorFlag.Temp,
		Principal:   protocol.DnUrl().JoinPath(account),
		Memo:        x.Tx.Memo,
		Metadata:    x.Tx.Metadata,
		GetOrCreate: true,
	})
	if err != nil {
		return nil, err
	}
	if resp.Transaction.Body == nil {
		return resp.Transaction, nil
	}
	body, ok := resp.Transaction.Body.(*protocol.WriteData)
	if !ok {
		return nil, fmt.Errorf("transaction %s is a %v, expected %v", validatorFlag.Temp, resp.Transaction.Body.Type(), protocol.TransactionTypeWriteData)
	}

	// The value must be reset otherwise unmarshalling may add to the existing
	// value instead of overwriting it
	switch account {
	case protocol.Oracle:
		values.Oracle = new(protocol.AcmeOracle)
		err = values.ParseOracle(body.Entry)
	case protocol.Globals:
		values.Globals = new(protocol.NetworkGlobals)
		err = values.ParseGlobals(body.Entry)
	case protocol.Network:
		values.Network = new(protocol.NetworkDefinition)
		err = values.ParseNetwork(body.Entry)
	case protocol.Routing:
		values.Routing = new(protocol.RoutingTable)
		err = values.ParseRouting(body.Entry)
	}
	if err != nil {
		return nil, err
	}

	return resp.Transaction, nil
}

func (x *Context) addValidator(cmd *cobra.Command, values *network.GlobalValues, pageCount int, partition string, args []string) (*protocol.Transaction, error) {
	newKey, err := x.resolvePublicKey(cmd, args[1])
	if err != nil {
		return nil, err
	}

	return build.AddValidator(values, pageCount, newKey, partition, true)
}

func (x *Context) removeValidator(cmd *cobra.Command, values *network.GlobalValues, pageCount int, partition string, args []string) (*protocol.Transaction, error) {
	oldKey, err := x.resolvePublicKey(cmd, args[1])
	if err != nil {
		return nil, err
	}

	return build.RemoveValidatorFrom(values, pageCount, oldKey, partition)
}

func (x *Context) updateValidatorKey(cmd *cobra.Command, values *network.GlobalValues, _ int, _ string, args []string) (*protocol.Transaction, error) {
	oldKey, err := x.resolvePublicKey(cmd, args[1])
	if err != nil {
		return nil, err
	}

	newKey, err := x.resolvePublicKey(cmd, args[2])
	if err != nil {
		return nil, err
	}

	return build.UpdateValidatorKey(values, oldKey, newKey)
}

func (x *Context) updateValidatorName(cmd *cobra.Command, values *network.GlobalValues, _ int, _ string, args []string) (*protocol.Transaction, error) {
	key, err := x.resolvePublicKey(cmd, args[1])
	if err != nil {
		return nil, err
	}

	name, err := url.Parse(args[2])
	if err != nil {
		return nil, err
	}

	return build.UpdateValidatorName(values, key, name)
}
