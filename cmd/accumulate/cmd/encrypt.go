package cmd

import (
	"context"
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/interactive"
)

var encryptCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "encrypt",
		Short: "encrypt the database",
		Args:  cobra.NoArgs,
		Run:   x.runCmdFunc2(x.encryptDatabase),
	}
})

func PrintEncrypt() {
	fmt.Println("  accumulate encrypt 		Encrypt the database, will be prompted for password")
}

//nolint:govet
func (x *Context) encryptDatabase(cmd *cobra.Command, _ []string) (string, error) {
	if true {
		return "", fmt.Errorf("TODO FIX")
	}

	password, err := interactive.GetNewPassword(WalletDir)
	if err != nil {
		return "", err
	}

	_, err = x.getWallet().EncryptVault(context.Background(), &api.EncryptVaultRequest{Passphrase: password})
	if err != nil {
		return "", err
	}

	return "", nil
}
