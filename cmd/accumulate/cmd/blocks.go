package cmd

import (
	"context"
	"fmt"
	"strconv"
	"time"

	"github.com/jedib0t/go-pretty/v6/table"
	"github.com/spf13/cobra"
	client "gitlab.com/accumulatenetwork/accumulate/pkg/client/api/v2"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/cmd/factory"
)

const minorBlockApiTimeout = 2 * time.Minute
const majorBlockApiTimeout = 30 * time.Second

var blocksCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "blocks",
		Short: "Create and get blocks",
	}
})

var blocksMinorCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "minor [partition-url] [start index] [count] [tx fetch mode expand|ids|countOnly|omit (optional)] [block filter mode excludenone|excludeempty (optional)]",
		Short: "Query minor blocks",
		Args:  cobra.RangeArgs(3, 5),
		Run:   x.runCmdFunc2(x.GetMinorBlocks),
	}
})

var blocksMajorCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "major [partition-url] [start index] [count]",
		Short: "Query major blocks",
		Args:  cobra.ExactArgs(3),
		Run:   x.runCmdFunc2(x.GetMajorBlocks),
	}
})

var blocksMajorTimeCmd = factory.New(func(x *Context) *cobra.Command {
	return &cobra.Command{
		Use:   "time [partition-url] [start index] [count]",
		Short: "List the timestamps of major blocks",
		Args:  cobra.ExactArgs(3),
		Run:   x.runCmdFunc(x.majorBlockTimes),
	}
})

var blocksFlag = struct {
	LocalTime bool
}{}

func init() {
	blocksCmd.AddCommand(
		blocksMinorCmd,
		blocksMajorCmd)

	blocksMajorCmd.AddCommand(
		blocksMajorTimeCmd)

	blocksMajorTimeCmd.Flags().BoolVar(&blocksFlag.LocalTime, "local", false, "Use local time")
}

var (
	BlocksWait      time.Duration
	BlocksNoWait    bool
	BlocksWaitSynth time.Duration
)

func init() {
	blocksCmd.Flags().DurationVarP(&BlocksWait, "wait", "w", 0, "Wait for the transaction to complete")
	blocksCmd.Flags().DurationVar(&BlocksWaitSynth, "wait-synth", 0, "Wait for synthetic transactions to complete")
}

func PrintGetMinorBlocks() {
	fmt.Println("  accumulate blocks minor [partition-url] [start index] [count] [tx fetch mode expand|ids|countOnly|omit (optional)] [block filter mode excludenone|excludeempty (optional)] Get minor blocks")
}

func PrintGetMajorBlocks() {
	fmt.Println("  accumulate blocks major [partition-url] [start index] [count] Get major blocks")
}

func PrintBlocks() {
	PrintGetMinorBlocks()
	PrintGetMajorBlocks()
}

func (x *Context) GetMinorBlocks(cmd *cobra.Command, args []string) (string, error) {
	u, err := url.Parse(args[0])
	if err != nil {
		return "", err
	}
	start, err := strconv.Atoi(args[1])
	if err != nil {
		return "", err
	}
	end, err := strconv.Atoi(args[2])
	if err != nil {
		return "", err
	}

	var ok bool
	txFetchMode := client.TxFetchModeExpand
	if len(args) > 3 {
		txFetchMode, ok = client.TxFetchModeByName(args[3])
		if !ok {
			return "", fmt.Errorf("%s is not a valid fetch mode. Use expand|ids|countOnly|omit", args[4])
		}
	}

	blockFilterMode := client.BlockFilterModeExcludeNone
	if len(args) > 4 {
		blockFilterMode, ok = client.BlockFilterModeByName(args[4])
		if !ok {
			return "", fmt.Errorf("%s is not a block filter mode. Use excludenone|excludeempty", args[4])
		}
	}

	params := new(client.MinorBlocksQuery)
	params.UrlQuery.Url = u
	params.QueryPagination.Start = uint64(start)
	params.QueryPagination.Count = uint64(end)
	params.TxFetchMode = txFetchMode
	params.BlockFilterMode = blockFilterMode

	// Temporary increase timeout, we may get a large result set which takes a while to construct
	globalTimeout := Client.Timeout
	Client.Timeout = minorBlockApiTimeout
	defer func() {
		Client.Timeout = globalTimeout
	}()

	res, err := Client.QueryMinorBlocks(context.Background(), params)
	if err != nil {
		return PrintJsonRpcError(err)
	}

	return x.PrintMultiResponse(cmd, res)
}

func (x *Context) GetMajorBlocks(cmd *cobra.Command, args []string) (string, error) {
	u, err := url.Parse(args[0])
	if err != nil {
		return "", err
	}
	start, err := strconv.Atoi(args[1])
	if err != nil {
		return "", err
	}
	end, err := strconv.Atoi(args[2])
	if err != nil {
		return "", err
	}

	params := new(client.MajorBlocksQuery)
	params.UrlQuery.Url = u
	params.QueryPagination.Start = uint64(start)
	params.QueryPagination.Count = uint64(end)

	// Temporary increase timeout, we may get a large result set which takes a while to construct
	globalTimeout := Client.Timeout
	Client.Timeout = majorBlockApiTimeout
	defer func() {
		Client.Timeout = globalTimeout
	}()

	res, err := Client.QueryMajorBlocks(context.Background(), params)
	if err != nil {
		return PrintJsonRpcError(err)
	}

	return x.PrintMultiResponse(cmd, res)
}

func (x *Context) majorBlockTimes(args []string) (string, error) {
	u, err := url.Parse(args[0])
	if err != nil {
		return "", err
	}
	start, err := strconv.Atoi(args[1])
	if err != nil {
		return "", err
	}
	end, err := strconv.Atoi(args[2])
	if err != nil {
		return "", err
	}

	params := new(client.MajorBlocksQuery)
	params.UrlQuery.Url = u
	params.QueryPagination.Start = uint64(start)
	params.QueryPagination.Count = uint64(end)

	// Temporary increase timeout, we may get a large result set which takes a while to construct
	globalTimeout := Client.Timeout
	Client.Timeout = majorBlockApiTimeout
	defer func() {
		Client.Timeout = globalTimeout
	}()

	res, err := Client.QueryMajorBlocks(context.Background(), params)
	if err != nil {
		return PrintJsonRpcError(err)
	}

	if WantJsonOutput {
		return PrintJson(res)
	}

	tw := table.NewWriter()
	tw.AppendRow([]any{"Block", "Time", "Minor blocks"})
	tw.Style().Options.DrawBorder = false
	for _, block := range res.Items {
		res := new(client.MajorQueryResponse)
		err := Remarshal(block, res)
		if err != nil {
			return "", err
		}
		var time string
		switch {
		case res.MajorBlockTime == nil:
			time = "Unknown"
		case blocksFlag.LocalTime:
			time = res.MajorBlockTime.Local().Format("2006-01-02 15:04:05 MST")
		default:
			time = res.MajorBlockTime.Format("2006-01-02 15:04:05 MST")
		}
		tw.AppendRow([]any{res.MajorBlockIndex, time, len(res.MinorBlocks)})
	}
	return tw.Render(), nil
}
