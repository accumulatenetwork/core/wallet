package cmd

import (
	"context"
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	errors3 "gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	. "gitlab.com/accumulatenetwork/accumulate/test/helpers"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/testutils"
)

func init() {
	testMatrix.addTest(testCase4_1)
	testMatrix.addTest(testCase4_2)
	testMatrix.addTest(testCase4_3)
	testMatrix.addTest(testCase4_4)
	testMatrix.addTest(testCase4_5)
	testMatrix.addTest(testCase4_6)
	testMatrix.addTest(testCase4_7)
	testMatrix.addTest(testCase4_8)
	testMatrix.addTest(testCase4_8_1)
	testMatrix.addTest(testCase4_8_2)
	testMatrix.addTest(testCase4_8_3)

}

// testCase4_1 Create a key book with a page using the same key as the book
func testCase4_1(t *testing.T, tc *testCmd) {

	_, err := tc.executeTx(t, "book create acc://RedWagon.acme --sign-with=red1 acc://RedWagon.acme/book4_1")
	require.NoError(t, err)
}

// testCase4_2 Create a key book with a page with key red2
func testCase4_2(t *testing.T, tc *testCmd) {

	_, err := tc.executeTx(t, "book create acc://RedWagon.acme --sign-with=red1 acc://RedWagon.acme/book0 red2")
	require.NoError(t, err)
}

// testCase4_3 Add a key to a key page
func testCase4_3(t *testing.T, tc *testCmd) {

	_, err := tc.executeTx(t, "credits %s acc://RedWagon.acme/book0/1 1000 10", liteAccounts[2])
	require.NoError(t, err)

	_, err = tc.executeTx(t, "page key add acc://RedWagon.acme/book0/1 --sign-with=red2 red4")
	require.NoError(t, err)

	_, err = tc.executeTx(t, "page key add acc://RedWagon.acme/book0/1 --sign-with=red2 red5")
	require.NoError(t, err)
}

// testCase4_4 Create additional key pages sponsored by a book
func testCase4_4(t *testing.T, tc *testCmd) {

	_, err := tc.executeTx(t, "page create acc://RedWagon.acme/book0 --sign-with=red2 red3")
	require.NoError(t, err)

	_, err = tc.executeTx(t, "page create acc://RedWagon.acme/book0 --sign-with=red2 red5")
	require.NoError(t, err)

}

// testCase4_5 Create an adi token account bound to a key book
func testCase4_5(t *testing.T, tc *testCmd) {

	_, err := tc.executeTx(t, "account create token acc://RedWagon.acme --sign-with=red1 acc://RedWagon.acme/acct2 acc://ACME")
	require.NoError(t, err)
}

// testCase4_6 Delete a key in a key page
func testCase4_6(t *testing.T, tc *testCmd) {
	t.Skip("executor v2: error does not occur until transaction is executed")

	//remove red5
	_, err := tc.executeTx(t, "page key remove acc://RedWagon.acme/book0/1 --sign-with=red2 red5")
	require.NoError(t, err)

	//remove red4
	_, err = tc.executeTx(t, "page key remove acc://RedWagon.acme/book0/1 --sign-with=red2 red4")
	require.NoError(t, err)

	//remove red2
	_, err = tc.executeTx(t, "page key remove acc://RedWagon.acme/book0/1 --sign-with=red2 red2")
	var err2 *errors3.Error
	require.ErrorAs(t, err, &err2)
	require.Equal(t, err2.Message, "cannot delete last key of the highest priority page of a key book")
}

// testCase4_7 update a key in a key page
func testCase4_7(t *testing.T, tc *testCmd) {

	//replace key3 with key 4
	_, err := tc.executeTx(t, "page key update acc://RedWagon.acme/book0/1 --sign-with=red2 red2 red5")
	require.NoError(t, err)
}

// testCase4_8 Sign a transaction with a secondary key page
func testCase4_8(t *testing.T, tc *testCmd) {

	t.Log("Skipping test to await for full support for v2")

	//commandLine := fmt.Sprintf("tx create %s acc://RedWagon.acme/acct2 5", liteAccounts[0])
	//_, err := tc.execute(t, commandLine)
	//require.NoError(t, err)
	//
	//time.Sleep(2 * time.Second)
	//commandLine = fmt.Sprintf("tx create acc://RedWagon.acme/acct2 --sign-with=red3 1 1 acc://Redwagon.acme/acct 1.1234")
	//_, err = tc.execute(t, commandLine)
	//require.NoError(t, err)
	//
	//t.Log(r)
	//
	//time.Sleep(2 * time.Second)
}

// testCase4_8_1 check if keypage query displays threshold value
func testCase4_8_1(t *testing.T, tc *testCmd) {
	_, err := tc.execute(t, "page get acc://RedWagon.acme/book0/1")
	require.NoError(t, err)
}

// testCase4_8_2 check if we can add different sig types to key pages
func testCase4_8_2(t *testing.T, tc *testCmd) {
	// Create a new key book
	_, err := tc.execute(t, "key generate keytestmain")
	require.NoError(t, err)
	_, err = tc.executeTx(t, "book create acc://RedWagon.acme --sign-with=red1 acc://RedWagon.acme/keytest keytestmain")
	require.NoError(t, err)
	_, err = tc.executeTx(t, "credits %s acc://RedWagon.acme/keytest/1 1000 10", liteAccounts[2])
	require.NoError(t, err)

	// generate protocol signature with eth
	r, err := tc.execute(t, "key generate --sigtype eth myethkey")
	require.NoError(t, err)
	kr := KeyResponse{}
	require.NoError(t, json.Unmarshal([]byte(r), &kr))
	require.Equal(t, protocol.SignatureTypeETH, kr.KeyInfo.Type)
	_, err = tc.executeTx(t, "page key add acc://RedWagon.acme/keytest/1 --sign-with=keytestmain myethkey")
	require.NoError(t, err)

	// generate protocol signature with btc
	r, err = tc.execute(t, "key generate --sigtype btc mybtckey")
	require.NoError(t, err)
	kr = KeyResponse{}
	require.NoError(t, json.Unmarshal([]byte(r), &kr))
	require.Equal(t, protocol.SignatureTypeBTC, kr.KeyInfo.Type)
	_, err = tc.executeTx(t, "page key add acc://RedWagon.acme/keytest/1 --sign-with=keytestmain mybtckey")
	require.NoError(t, err)

	// generate protocol signature with RCD1
	r, err = tc.execute(t, "key generate --sigtype rcd1 myrcd1key")
	require.NoError(t, err)
	kr = KeyResponse{}
	require.NoError(t, json.Unmarshal([]byte(r), &kr))
	require.Equal(t, protocol.SignatureTypeRCD1, kr.KeyInfo.Type)
	_, err = tc.executeTx(t, "page key add acc://RedWagon.acme/keytest/1 --sign-with=keytestmain myrcd1key")
	require.NoError(t, err)

	// generate protocol signature with btclegacy
	r, err = tc.execute(t, "key generate --sigtype btclegacy mybtclegacykey")
	require.NoError(t, err)
	kr = KeyResponse{}
	require.NoError(t, json.Unmarshal([]byte(r), &kr))
	require.Equal(t, protocol.SignatureTypeBTCLegacy, kr.KeyInfo.Type)
	_, err = tc.executeTx(t, "page key add acc://RedWagon.acme/keytest/1 --sign-with=keytestmain mybtclegacykey")
	require.NoError(t, err)
}

// testCase4_8_3 check if additional signers supoorts different signature types
func testCase4_8_3(t *testing.T, tc *testCmd) {
	_, err := tc.executeTx(t, "tx execute --no-review acc://RedWagon.acme/keytest/1 --sign-with=myethkey '{ type: updateKeyPage }'")
	assert.NoError(t, err)

	_, err = tc.executeTx(t, "tx execute --no-review acc://RedWagon.acme/keytest/1 --sign-with=myrcd1key '{ type: updateKeyPage }'")
	assert.NoError(t, err)

	_, err = tc.executeTx(t, "tx execute --no-review acc://RedWagon.acme/keytest/1 --sign-with=mybtckey '{ type: updateKeyPage }'")
	assert.NoError(t, err)

	_, err = tc.executeTx(t, "tx execute --no-review acc://RedWagon.acme/keytest/1 --sign-with=mybtclegacykey '{ type: updateKeyPage }'")
	assert.NoError(t, err)

}

func TestSignWithSpecificPage(t *testing.T) {
	// Setup
	tc := newTestCmd(t, testCmdOpts{})
	_, err := _wallet.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:       tc.dir,
		Token:      testutils.TestToken[:],
		MultiVault: false,
		Passphrase: "",
	})
	require.NoError(t, err)

	// Import the mnemonic
	_, err = _wallet.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	key := new(KeyResponse)
	out, err := tc.execute(t, "account generate")
	require.NoError(t, err)
	require.NoError(t, json.Unmarshal([]byte(out), key))

	lta := key.LiteAccount
	liteId := lta.RootIdentity().String()

	_, err = tc.executeTx(t, "faucet %s", lta)
	require.NoError(t, err)

	_, err = tc.executeTx(t, "credits %s %s 1000 100", lta, liteId)
	require.NoError(t, err)

	adi := protocol.AccountUrl("test4-15")
	_, err = tc.execute(t, "key generate myKey")
	require.NoError(t, err)
	_, err = tc.executeTx(t, "adi create %s %s myKey", liteId, adi)
	require.NoError(t, err)

	_, err = tc.executeTx(t, "credits %s %s/book/1 1000 100", lta, adi)
	require.NoError(t, err)

	_, err = tc.executeTx(t, "page create %s/book --sign-with=myKey myKey", adi)
	require.NoError(t, err)

	_, err = tc.executeTx(t, "credits %s %s/book/2 1000 100", lta, adi)
	require.NoError(t, err)

	// Execute
	p1b := GetAccount[*protocol.KeyPage](t, tc.sim.DatabaseFor(adi), adi.JoinPath("book", "1"))
	p2b := GetAccount[*protocol.KeyPage](t, tc.sim.DatabaseFor(adi), adi.JoinPath("book", "2"))
	_, err = tc.executeTx(t, "account create token %s --sign-with=%s %[1]s/tokens ACME", adi, adi.JoinPath("book", "2").WithUserInfo("myKey"))
	require.NoError(t, err)

	// Verify
	p1a := GetAccount[*protocol.KeyPage](t, tc.sim.DatabaseFor(adi), adi.JoinPath("book", "1"))
	p2a := GetAccount[*protocol.KeyPage](t, tc.sim.DatabaseFor(adi), adi.JoinPath("book", "2"))
	assert.Equal(t, int(p1a.CreditBalance), int(p1b.CreditBalance), "Expected page 1's balance to remain the same")
	assert.Less(t, int(p2a.CreditBalance), int(p2b.CreditBalance), "Expected page 2's balance to change")
}
