package api

import (
	"strings"
)

func (w *VaultList) Add(name string) bool {
	for _, n := range w.Vaults {
		if strings.EqualFold(n, name) {
			return false
		}
	}

	w.Vaults = append(w.Vaults, name)
	return true
}

func (w *VaultList) Remove(name string) bool {
	for i, n := range w.Vaults {
		if strings.EqualFold(n, name) {
			w.Vaults = append(w.Vaults[:i], w.Vaults[i+1:]...)
			return true
		}
	}
	return false
}
