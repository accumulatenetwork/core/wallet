package walletd

// Package classification awesome.
//
// Documentation of our awesome API.
//
//     Schemes: http
//     BasePath: /
//     Version: 1.0.0
//     Host: some-url.com
//
//     Consumes:
//     - application/json
//
//     Produces:
//     - application/json
//
//     Security:
//     - basic
//
//    SecurityDefinitions:
//    basic:
//      type: basic
//
// swagger:meta

import (
	"context"
	"crypto/tls"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"time"

	"github.com/go-playground/validator/v10"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/daemon/services"
	"gitlab.com/accumulatenetwork/core/wallet/internal/ledger"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/jsonrpc"
	"golang.org/x/exp/slog"
)

//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-types --package api --out api/types_gen.go api/transaction_types.yml api/api_general.yml api/api_ledger.yml api/api_wallet.yml --reference api/wallet.yml --go-include gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging
//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-types --package api --out api/wallet_gen.go api/wallet.yml
//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-enum --out api/enums_gen.go --package api api/enums.yml

var (
	NoWalletVersionCheck bool
)

type Options struct {
	Logger      *slog.Logger
	TLSFiles    []string
	UseMemDB    bool
	MaxTokenTTL time.Duration
	OnExpire    func()
}

type JrpcMethods struct {
	Options
	keys     services.KeyStore
	wallets  services.WalletManager
	validate *validator.Validate
	api      *http.Server
}

func NewJrpc(opts Options) (*JrpcMethods, error) {
	if opts.Logger == nil {
		opts.Logger = slog.Default()
	}
	if opts.MaxTokenTTL == 0 {
		opts.MaxTokenTTL = 65 * time.Minute
	}

	var err error
	m := new(JrpcMethods)
	m.Options = opts
	m.keys = &services.TokenKeyStore{}
	m.wallets = &services.WalletManagerWithKeys{
		InMemory: opts.UseMemDB,
		Keys:     m.keys,
	}

	m.validate, err = protocol.NewValidator()
	if err != nil {
		return nil, err
	}

	return m, nil
}

func (m *JrpcMethods) NewMux() http.Handler {
	ledger, err := ledger.NewAPI()
	if err != nil {
		slog.Error("Initialize Ledger API", "error", err)
	}

	var general wallet.GeneralService = &services.GeneralService{Keys: m.keys, MaxTTL: m.MaxTokenTTL}
	if m.OnExpire != nil {
		general = &expirationChecker{
			GeneralService: general,
			onExpire:       m.OnExpire,
		}
	}

	h, err := jsonrpc.NewHandler(
		jsonrpc.GeneralServiceServer{Service: general},
		jsonrpc.VaultServiceServer{Service: &services.VaultService{Vaults: m.wallets, Keys: m.keys, MaxTTL: m.MaxTokenTTL}},
		jsonrpc.TransactionServiceServer{Service: &services.TransactionService{Vaults: m.wallets}},
		jsonrpc.SigningServiceServer{Service: &services.SigningService{Vaults: m.wallets, Ledger: ledger}},
		jsonrpc.KeyServiceServer{Service: &services.KeyService{Vaults: m.wallets}},
		jsonrpc.LedgerServiceServer{Service: &services.LedgerService{Vaults: m.wallets, Ledger: ledger}},
		jsonrpc.AccountServiceServer{Service: &services.AccountService{Vaults: m.wallets}},
	)
	if err != nil {
		panic(err)
	}
	return h
}

// listenHttpUrl takes a string such as `http://localhost:123` and creates a TCP
// listener.
func listenHttpUrl(s string) (net.Listener, bool, error) {
	u, err := url.Parse(s)
	if err != nil {
		return nil, false, fmt.Errorf("invalid address: %v", err)
	}

	if u.Path != "" && u.Path != "/" {
		return nil, false, fmt.Errorf("invalid address: path is not empty")
	}

	var secure bool
	switch u.Scheme {
	case "tcp", "http":
		secure = false
	case "https":
		secure = true
	default:
		return nil, false, fmt.Errorf("invalid address: unsupported scheme %q", u.Scheme)
	}

	l, err := net.Listen("tcp", u.Host)
	if err != nil {
		return nil, false, err
	}

	return l, secure, nil
}

func (m *JrpcMethods) Start(addr string) error {
	// Run JSON-RPC server
	m.api = &http.Server{
		Handler:           m.NewMux(),
		ReadHeaderTimeout: 15 * time.Second,
	}
	l, secure, err := listenHttpUrl(addr)
	if err != nil {
		return err
	}
	var cert *tls.Certificate

	if secure {
		if len(m.Options.TLSFiles) != 2 {
			cert, err = GenerateSelfSignedCertificate()
		} else {
			cert, err = GetSignedCertificate(m.Options.TLSFiles)
		}
		if err != nil {
			return err
		}
		m.api.TLSConfig = new(tls.Config)
		m.api.TLSConfig.Certificates = append(m.api.TLSConfig.Certificates, *cert)
		fmt.Printf("Walletd listening at %s\n", addr)
		return m.api.ServeTLS(l, "", "")
	}
	fmt.Printf("Walletd listening at %s\n", addr)
	return m.api.Serve(l)
}

func (m *JrpcMethods) Stop(ctx context.Context) error {
	if m.api == nil {
		return nil
	}
	return m.api.Shutdown(ctx)
}

type expirationChecker struct {
	wallet.GeneralService
	onExpire func()
	last     time.Time
	timer    *time.Timer
}

func (e *expirationChecker) RefreshToken(ctx context.Context, req *api.RefreshTokenRequest) (*api.RefreshTokenResponse, error) {
	// Send the request
	res, err := e.GeneralService.RefreshToken(ctx, req)
	if err != nil || res.UnlockedUntil == nil {
		return nil, err
	}

	// Is there an existing timer that expires later?
	if e.timer != nil && e.last.After(*res.UnlockedUntil) {
		return res, nil
	}

	// Stop the old timer
	if e.timer != nil {
		e.timer.Stop()
	}

	// Call the callback a minute after the token expires
	e.last = res.UnlockedUntil.Add(time.Minute)
	duration := time.Until(e.last)
	e.timer = time.AfterFunc(duration, e.onExpire)
	slog.InfoCtx(ctx, "Will shutdown", "at", e.last, "in", duration)
	return res, nil
}
