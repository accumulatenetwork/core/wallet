package walletd_test

import (
	"context"
	"math/big"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/testutils"
)

func TestSignProvidedTransaction(t *testing.T) {
	w, v, close := createTestWalletClient(t, false)
	defer close()

	// Create a wallet
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:  v.FilePath,
		Token: testutils.TestToken[:],
	})
	require.NoError(t, err)

	// Generate a key
	_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Wallet:   v.FilePath,
		Token:    testutils.TestToken[:],
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	_, err = w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Wallet: v.FilePath,
		Token:  testutils.TestToken[:],
		Type:   protocol.SignatureTypeED25519,
		Labels: []string{"yellowkey"},
	})
	require.NoError(t, err)

	// Sign a transaction
	resp, err := w.SignTransaction(context.Background(), &api.SignTransactionRequest{
		Wallet: v.FilePath,
		Token:  testutils.TestToken[:],
		Transaction: &protocol.Transaction{
			Header: protocol.TransactionHeader{
				Principal: url.MustParse("tokens"),
			},
			Body: &protocol.BurnTokens{
				Amount: *big.NewInt(1),
			},
		},
		KeyName:       "yellowkey",
		Signer:        url.MustParse("page"),
		SignerVersion: 1,
		Timestamp:     1234567890,
	})
	require.NoError(t, err)

	require.NotNil(t, resp.Signature)
	require.NotNil(t, resp.Transaction)
}
