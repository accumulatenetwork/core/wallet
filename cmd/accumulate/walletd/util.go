package walletd

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"crypto/x509/pkix"
	"encoding/pem"
	"fmt"
	"math/big"
	"strings"
	"time"
)

func GenerateSelfSignedCertificate() (*tls.Certificate, error) {
	max := big.NewInt(1e9)
	max.Mul(max, max)
	serial, err := rand.Int(rand.Reader, max)
	if err != nil {
		return nil, err
	}

	ca := &x509.Certificate{
		SerialNumber: serial,
		Subject: pkix.Name{
			Organization:  []string{"DeFi Devs"},
			Country:       []string{"US"},
			Province:      []string{""},
			Locality:      []string{""},
			StreetAddress: []string{""},
			PostalCode:    []string{""},
		},
		NotBefore:             time.Now(),
		NotAfter:              time.Now().AddDate(10, 0, 0),
		IsCA:                  true,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageClientAuth, x509.ExtKeyUsageServerAuth},
		KeyUsage:              x509.KeyUsageDigitalSignature | x509.KeyUsageCertSign,
		BasicConstraintsValid: true,
	}

	caPrivKey, err := rsa.GenerateKey(rand.Reader, 4096)
	if err != nil {
		return nil, err
	}

	caBytes, err := x509.CreateCertificate(rand.Reader, ca, ca, &caPrivKey.PublicKey, caPrivKey)
	if err != nil {
		return nil, err
	}

	caPEM := new(bytes.Buffer)
	err = pem.Encode(caPEM, &pem.Block{
		Type:  "CERTIFICATE",
		Bytes: caBytes,
	})
	if err != nil {
		return nil, err
	}

	caPrivKeyPEM := new(bytes.Buffer)
	err = pem.Encode(caPrivKeyPEM, &pem.Block{
		Type:  "RSA PRIVATE KEY",
		Bytes: x509.MarshalPKCS1PrivateKey(caPrivKey),
	})
	if err != nil {
		return nil, err
	}

	cert, err := tls.X509KeyPair(caPEM.Bytes(), caPrivKeyPEM.Bytes())
	if err != nil {
		return nil, err
	}
	return &cert, nil
}

func GetSignedCertificate(TlsFiles []string) (*tls.Certificate, error) {
	if !strings.Contains(TlsFiles[0], ".crt") || !strings.Contains(TlsFiles[1], ".key") {
		return nil, fmt.Errorf("the flag --tls input must be of format smaplecert.crt samplekey.key")
	}
	cer, err := tls.LoadX509KeyPair(TlsFiles[0], TlsFiles[1])
	if err != nil {
		return nil, err
	}
	return &cer, nil
}
