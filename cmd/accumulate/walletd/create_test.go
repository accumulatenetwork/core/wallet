package walletd_test

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/jsonrpc"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/testutils"
)

func TestCreateWallet(t *testing.T) {
	cases := []struct {
		Name      string
		Multi     bool
		OnDisk    bool
		Encrypted bool
	}{
		{"Single-vault, in-memory, unencrypted", false, false, false},
		{"Single-vault, in-memory, encrypted", false, false, true},
		{"Single-vault, on-disk, unencrypted", false, true, false},
		{"Single-vault, on-disk, encrypted", false, true, true},
		{"Multi-vault, in-memory, unencrypted", true, false, false},
		{"Multi-vault, in-memory, encrypted", true, false, true},
		{"Multi-vault, on-disk, unencrypted", true, true, false},
		{"Multi-vault, on-disk, encrypted", true, true, true},
	}

	for _, c := range cases {
		t.Run(c.Name, func(t *testing.T) {
			w, v, close := createTestWalletClient(t, !c.OnDisk)
			defer close()

			var passphrase string
			if c.Encrypted {
				passphrase = "foo"

				// Enable automatic unlocking
				w.Transport = &jsonrpc.InteractiveAuthnTransport{
					Transport:   w.Transport,
					GetPassword: func(*api.VaultInfo, int, error) ([]byte, error) { return []byte(passphrase), nil },
				}
			}

			var vault string
			if c.Multi {
				vault = "main"
			}

			lock := func() {
				// Close if on disk
				_, err := w.LockVault(context.Background(), &api.LockVaultRequest{Token: testutils.TestToken[:], Close: c.OnDisk})
				require.NoError(t, err)
			}

			// Create a wallet
			_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
				Path:       v.FilePath,
				Token:      testutils.TestToken[:],
				MultiVault: c.Multi,
				Passphrase: passphrase,
			})
			require.NoError(t, err)

			lock()

			if c.Multi {
				// Create a vault
				_, err = w.CreateVault(context.Background(), &api.CreateVaultRequest{
					Wallet: v.FilePath,
					Token:  testutils.TestToken[:],
					Vault:  vault,
				})
				require.NoError(t, err)

				lock()
			}

			// Generate a key
			_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
				Wallet:   v.FilePath,
				Token:    testutils.TestToken[:],
				Vault:    vault,
				Mnemonic: testutils.Yellow,
			})
			require.NoError(t, err)

			lock()

			_, err = w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
				Wallet: v.FilePath,
				Token:  testutils.TestToken[:],
				Type:   protocol.SignatureTypeED25519,
				Vault:  vault,
				Labels: []string{"yellowkey"},
			})
			require.NoError(t, err)
		})
	}
}

func TestConvertWallet(t *testing.T) {
	w, v, close := createTestWalletClient(t, false)
	defer close()

	// Create a single-vault wallet
	passphrase := "foo"
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:       v.FilePath,
		Token:      testutils.TestToken[:],
		MultiVault: false,
		Passphrase: passphrase,
	})
	require.NoError(t, err)

	// Generate a key
	_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Wallet:   v.FilePath,
		Token:    testutils.TestToken[:],
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	_, err = w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Wallet: v.FilePath,
		Token:  testutils.TestToken[:],
		Type:   protocol.SignatureTypeED25519,
		Labels: []string{"yellowkey"},
	})
	require.NoError(t, err)

	// Convert to a multi-vault wallet
	_, err = w.ConvertWallet(context.Background(), &api.ConvertWalletRequest{
		Wallet:     v.FilePath,
		Token:      testutils.TestToken[:],
		Vault:      "main",
		Passphrase: passphrase,
	})
	require.NoError(t, err)

	// Lock and close everything
	_, err = w.LockVault(context.Background(), &api.LockVaultRequest{Token: testutils.TestToken[:], Close: true})
	require.NoError(t, err)

	// Enable automatic unlocking
	w.Transport = &jsonrpc.InteractiveAuthnTransport{
		Transport:   w.Transport,
		GetPassword: func(*api.VaultInfo, int, error) ([]byte, error) { return []byte(passphrase), nil },
	}

	// Read the key
	r, err := w.ResolveKey(context.Background(), &api.ResolveKeyRequest{
		Wallet: v.FilePath,
		Token:  testutils.TestToken[:],
		Vault:  "main",
		Value:  "yellowkey",
	})
	require.NoError(t, err)
	fmt.Println(r.Address)
}

func TestAdoptWallet(t *testing.T) {
	// Set up a vault
	w, v, close := createTestWalletClient(t, false)
	defer close()
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:  v.FilePath,
		Token: testutils.TestToken[:],
	})
	require.NoError(t, err)

	_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Wallet:   v.FilePath,
		Token:    testutils.TestToken[:],
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)

	_, err = w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Wallet: v.FilePath,
		Token:  testutils.TestToken[:],
		Type:   protocol.SignatureTypeED25519,
		Labels: []string{"yellowkey"},
	})
	require.NoError(t, err)

	_, err = w.LockVault(context.Background(), &api.LockVaultRequest{Token: testutils.TestToken[:], Close: true})
	require.NoError(t, err)

	// Make it look like an old vault
	require.NoError(t, os.MkdirAll(filepath.Join(v.FilePath, "foo"), 0700))
	require.NoError(t, os.Rename(filepath.Join(v.FilePath, "index.db"), filepath.Join(v.FilePath, "foo", "wallet.db")))

	// Set up a multi-vault wallet
	_, err = w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:       v.FilePath,
		Token:      testutils.TestToken[:],
		MultiVault: true,
	})
	require.NoError(t, err)

	// Adopt the 'old' vault
	_, err = w.AdoptVault(context.Background(), &api.AdoptVaultRequest{
		Wallet:       v.FilePath,
		Token:        testutils.TestToken[:],
		NewVaultName: "foo",
		OldVaultPath: filepath.Join(v.FilePath, "foo", "wallet.db"),
	})
	require.NoError(t, err)

	// Close everything
	_, err = w.LockVault(context.Background(), &api.LockVaultRequest{Token: testutils.TestToken[:], Close: true})
	require.NoError(t, err)

	// Verify
	res, err := w.ResolveKey(context.Background(), &api.ResolveKeyRequest{
		Wallet: v.FilePath,
		Token:  testutils.TestToken[:],
		Vault:  "foo",
		Value:  "yellowkey",
	})
	require.NoError(t, err)
	fmt.Println(res.Address)
}

func TestOpen(t *testing.T) {
	w, v, close := createTestWalletClient(t, false)
	defer close()

	// Create an encrypted wallet
	passphrase := "foo"
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:       v.FilePath,
		Token:      testutils.TestToken[:],
		Passphrase: passphrase,
	})
	require.NoError(t, err)

	// Close it
	_, err = w.LockVault(context.Background(), &api.LockVaultRequest{
		Close: true,
		Token: testutils.TestToken[:],
	})
	require.NoError(t, err)

	// Open it
	_, err = w.OpenVault(context.Background(), &api.OpenVaultRequest{
		Wallet: v.FilePath,
		Token:  testutils.TestToken[:],
	})
	require.NoError(t, err)

	// Verify it is locked
	status, err := w.Status(context.Background(), &api.StatusRequest{
		Wallet: v.FilePath,
		Token:  testutils.TestToken[:],
	})
	require.NoError(t, err)
	require.True(t, status.Wallet.Open)
	require.False(t, status.Wallet.Unlocked)
}

func TestUnlockDoesNotCreate(t *testing.T) {
	const passphrase = "foo"

	t.Run("Single", func(t *testing.T) {
		w, v, close := createTestWalletClient(t, false)
		defer close()

		_, err := w.UnlockVault(context.Background(), &api.UnlockVaultRequest{
			Wallet:     v.FilePath,
			Token:      testutils.TestToken[:],
			TTL:        time.Second,
			Passphrase: passphrase,
		})
		require.ErrorIs(t, err, errors.NotFound)
	})

	t.Run("Multi", func(t *testing.T) {
		w, v, close := createTestWalletClient(t, false)
		defer close()

		_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
			Path:       v.FilePath,
			Token:      testutils.TestToken[:],
			Passphrase: passphrase,
		})
		require.NoError(t, err)

		_, err = w.UnlockVault(context.Background(), &api.UnlockVaultRequest{
			Wallet:     v.FilePath,
			Token:      testutils.TestToken[:],
			TTL:        time.Second,
			Passphrase: passphrase,
			Vault:      "main",
		})
		require.ErrorContains(t, err, "not a registered vault")
	})
}
