package walletd_test

import (
	"context"
	"encoding/json"
	"testing"
	"time"

	"github.com/AccumulateNetwork/jsonrpc2/v15"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"gitlab.com/accumulatenetwork/core/wallet/internal/util"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/jsonrpc"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/wallet/testutils"
	"golang.org/x/exp/slog"
)

func init() {
	jsonrpc2.DebugMethodFunc = true
}

func createTestWalletClient(t *testing.T, useMemDB bool) (*jsonrpc.Client, *api.VaultInfo, func()) {
	jrpc, err := walletd.NewJrpc(walletd.Options{Logger: slog.Default(), UseMemDB: useMemDB})
	require.NoError(t, err)

	// Use a custom transport that doesn't require an HTTP server
	tr := new(jsonrpc.DefaultTransport)
	tr.Transport = util.DirectHttpTransport(jrpc.NewMux())
	tr.Timeout = time.Hour

	// Create a client with a fake address
	w, err := jsonrpc.NewWith("http://socket/wallet", tr)
	require.NoError(t, err)

	// Close the database so it can be deleted
	close := func() {
		_, err := w.LockVault(context.Background(), &api.LockVaultRequest{Close: true, Token: testutils.TestToken[:]})
		assert.NoError(t, err)
	}

	// AUtomatically set the token
	w.Transport = &jsonrpc.AutoWalletTransport{
		Transport: w.Transport,
		SetToken:  func(_ any, v *[]byte) { *v = testutils.TestToken[:] },
	}

	return w, &api.VaultInfo{
		FilePath: t.TempDir(),
	}, close
}

func TestInteractiveAuthentication(t *testing.T) {
	// Set up an encrypted vault
	w, v, close := createTestWalletClient(t, true)
	defer close()

	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:       v.FilePath,
		Token:      testutils.TestToken[:],
		MultiVault: false,
		Passphrase: "foo",
	})
	require.NoError(t, err)

	// Generate something
	_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Wallet:   v.FilePath,
		Token:    testutils.TestToken[:],
		Mnemonic: testutils.Yellow,
	})
	require.NoError(t, err)
	_, err = w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Wallet: v.FilePath,
		Token:  testutils.TestToken[:],
		Type:   protocol.SignatureTypeED25519,
		Labels: []string{"yellowkey"},
	})
	require.NoError(t, err)

	// Lock it
	_, err = w.LockVault(context.Background(), &api.LockVaultRequest{Token: testutils.TestToken[:]})
	require.NoError(t, err)

	// Do something that requires the vault to be unlocked
	_, err = w.KeyList(context.Background(), &api.KeyListRequest{
		Wallet: v.FilePath,
		Token:  testutils.TestToken[:],
	})
	require.Error(t, err)
	require.ErrorIs(t, err, errors.NoPassword)

	// Enable automatic unlocking
	w.Transport = &jsonrpc.InteractiveAuthnTransport{
		Transport:   w.Transport,
		GetPassword: func(*api.VaultInfo, int, error) ([]byte, error) { return []byte("foo"), nil },
	}

	// Verify that the vault is automatically unlocked
	_, err = w.KeyList(context.Background(), &api.KeyListRequest{
		Wallet: v.FilePath,
		Token:  testutils.TestToken[:],
	})
	require.NoError(t, err)
}

// Remarshal uses mapstructure to convert a generic JSON-decoded map into a struct.
func Remarshal(src interface{}, dst interface{}) error {
	data, err := json.Marshal(src)
	if err != nil {
		return err
	}
	return json.Unmarshal(data, dst)
}

func RemarshalAs[T any](src any) (*T, error) {
	v := new(T)
	return v, Remarshal(src, v)
}
