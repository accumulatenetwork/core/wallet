//go:build tools
// +build tools

package wallet

// Force `go mod tidy` to include tool dependencies
import (
	_ "github.com/rinchsan/gosimports/cmd/gosimports"
	_ "github.com/wailsapp/wails/v2/cmd/wails"
	_ "gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-types"
	_ "gotest.tools/gotestsum"
)
