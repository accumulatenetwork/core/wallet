package db

import (
	"crypto/rand"
	"time"

	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

// Symmetric implements [Encrypted] via symmetric encryption using a key derived
// from a passphrase and a salt.
type Symmetric struct {
	DB

	// Retention sets the default key retention policy. A value of zero
	// indicates indefinite key retention.
	Retention time.Duration

	// New indicates that the database is new and the passphrase should be
	// confirmed the first time.
	New bool

	salt     []byte
	key      []byte
	deadline *time.Time
}

func NewSymmetric(raw DB) *Symmetric {
	s := new(Symmetric)
	s.DB = raw
	s.New = true                   // Ask for confirmation of the passphrase
	s.Retention = 10 * time.Minute // Default retention period
	return s
}

// OpenSymmetric creates or opens a symmetrically encrypted database.
func OpenSymmetric(raw DB, passphrase []byte) (*Symmetric, error) {
	s := NewSymmetric(raw)
	_, err := s.UnlockFor(passphrase, s.Retention)
	if err != nil {
		return nil, err
	}
	return s, nil
}

func (s *Symmetric) Raw() DB { return s.DB }

// Get will get an entry in the database given a bucket and key
func (e *Symmetric) Get(bucket []byte, key []byte) (value []byte, err error) {
	value, err = e.Raw().Get(bucket, key)
	if err != nil {
		return nil, err
	}
	return e.decrypt(value)
}

// Put will write data to a given bucket using the key
func (e *Symmetric) Put(bucket []byte, key []byte, value []byte) error {
	value, err := e.encrypt(value)
	if err != nil {
		return err
	}
	return e.Raw().Put(bucket, key, value)
}

func (e *Symmetric) GetBucket(bucket []byte) (*Bucket, error) {
	v, err := e.DB.GetBucket(bucket)
	if err != nil {
		return nil, err
	}

	buck := new(Bucket)
	for _, kv := range v.KeyValueList {
		v, err := e.decrypt(kv.Value)
		if err != nil {
			return nil, err
		}
		buck.Put(kv.Key, v)
	}
	return buck, nil
}

func (s *Symmetric) Lock() {
	// Reset the key
	s.key = nil
}

func (s *Symmetric) IsLocked() bool {
	// If the deadline has expired, wipe the key
	if s.deadline != nil && time.Now().After(*s.deadline) {
		s.key = nil
		return true
	}
	return s.key == nil
}

// UnlockFor unlocks the wallet, prompting the user to enter the wallet
// password. UnlockFor locks the database after the specified duration has
// elapsed.
func (s *Symmetric) UnlockFor(passphrase []byte, duration time.Duration) (dl time.Time, err error) {
	// Ensure the key is set
	_, err = s.loadKey(passphrase)
	if err != nil {
		return time.Time{}, err
	}

	// If the caller specified a deadline earlier than the encrypter's own
	// deadline, use it
	if duration == 0 {
		return time.Time{}, nil
	}
	deadline := time.Now().Add(duration)
	if s.deadline == nil || s.deadline.After(deadline) {
		s.deadline = &deadline
	} else {
		deadline = *s.deadline
	}

	// Lock if something fails
	defer func() {
		if err != nil {
			s.Lock()
		}
	}()

	// Fail immediately if the password is invalid, instead of waiting until
	// someone tries to decrypt some data
	err = Magic(s)
	switch {
	case err == nil:
		// Ok

	case errors.Is(err, errors.NotFound):
		// Uninitialized
		err = PutMagic(s)
		if err != nil {
			return time.Time{}, err
		}

	default:
		// Unknown error
		return time.Time{}, err
	}

	// Explicitly lock to remove the key from memory
	go func() { time.Sleep(duration); s.Lock() }()
	return dl, nil
}

func (s *Symmetric) decrypt(value []byte) ([]byte, error) {
	key, err := s.loadKey(nil)
	if err != nil {
		return nil, err
	}
	return SymDecrypt(value, key)
}

func (s *Symmetric) encrypt(value []byte) ([]byte, error) {
	key, err := s.loadKey(nil)
	if err != nil {
		return nil, err
	}
	return SymEncrypt(value, key)
}

func (s *Symmetric) loadKey(passphrase []byte) ([]byte, error) {
	// If the deadline has not expired and the key is set, return it
	key := s.key
	if s.deadline != nil && time.Now().After(*s.deadline) {
		s.key = nil
	} else if key != nil {
		return key, nil
	}

	// Load the salt
	salt, err := s.loadSalt()
	if err != nil {
		return nil, err
	}

	// Check for a passphrase
	if passphrase == nil {
		return nil, errors.NoPassword.With("no password specified for encrypted vault")
	}

	// Only ask for confirmation the first time
	s.New = false

	// Calculate the key
	key, err = DeriveSymKey(passphrase, salt)
	if err != nil {
		return nil, err
	}

	// Save the key and deadline
	s.key = key
	if s.Retention == 0 {
		s.deadline = nil
	} else {
		t := time.Now().Add(s.Retention)
		s.deadline = &t
	}
	return key, nil
}

func (s *Symmetric) loadSalt() ([]byte, error) {
	// Is the salt already loaded?
	if s.salt != nil {
		return s.salt, nil
	}

	// Load the salt
	salt, err := s.DB.Get(BucketConfig, []byte("salt"))
	switch {
	case err == nil:
		s.salt = salt
		return salt, nil
	case !errors.Is(err, errors.NotFound):
		return nil, err
	}

	// Generate salt
	salt = make([]byte, 30)
	_, err = rand.Read(salt)
	if err != nil {
		return nil, err
	}

	// Store the salt in the database
	err = s.DB.Put(BucketConfig, []byte("salt"), salt)
	if err != nil {
		return nil, err
	}

	s.salt = salt
	return salt, nil
}
