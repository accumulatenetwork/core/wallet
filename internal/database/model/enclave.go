package model

import (
	"encoding/hex"
	"encoding/json"
	"errors"
	"io"

	"github.com/awnumar/memguard"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/encoding"
)

type Enclave struct {
	memguard.Enclave
}

var _ encoding.BinaryValue = (*Enclave)(nil)

func NewEnclave(b []byte, preserve bool) *Enclave {
	if preserve {
		c := make([]byte, len(b))
		copy(c, b)
		b = c
	}
	return &Enclave{*memguard.NewEnclave(b)}
}

// Equal returns true if the two enclaves are reference-equal.
func (e *Enclave) Equal(f *Enclave) bool {
	return e.Enclave.Enclave == f.Enclave.Enclave
}

// CopyAsInterface does nothing as Enclave is immutable so there's no need to copy.
func (e *Enclave) CopyAsInterface() any { return e }

func (e *Enclave) UnmarshalBinaryFrom(rd io.Reader) error {
	b, err := io.ReadAll(rd)
	if err != nil {
		return err
	}
	return e.UnmarshalBinary(b)
}

func (e *Enclave) MarshalBinary() ([]byte, error) {
	locked, err := e.Open()
	if err != nil {
		return nil, err
	}
	defer locked.Destroy()
	b := make([]byte, locked.Size())
	copy(b, locked.Bytes())
	return b, nil
}

func (e *Enclave) UnmarshalBinary(b []byte) error {
	if len(b) == 0 {
		return errors.New("enclave cannot be empty")
	}

	c := make([]byte, len(b))
	copy(c, b)

	// TODO We should be wiping the original bytes here, but for reasons I don't
	// understand that causes a panic
	//   memguard.WipeBytes(b)

	e.Enclave = *memguard.NewEnclave(c)
	return nil
}

func (e *Enclave) MarshalJSON() ([]byte, error) {
	b, err := e.MarshalBinary()
	if err != nil {
		return nil, err
	}
	return json.Marshal(hex.EncodeToString(b))
}

func (e *Enclave) UnmarshalJSON(b []byte) error {
	var s string
	err := json.Unmarshal(b, &s)
	if err != nil {
		return err
	}
	b, err = hex.DecodeString(s)
	if err != nil {
		return err
	}
	return e.UnmarshalBinary(b)
}
