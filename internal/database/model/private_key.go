package model

import (
	"errors"

	"github.com/awnumar/memguard"
	"gitlab.com/accumulatenetwork/accumulate/pkg/database/values"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
)

// These types are wrapped in this way to prevent callers from accessing the
// underlying Value in an unsafe manner.

type PrivateKey struct {
	privateKey
}

type privateKey struct {
	values.Value[*Enclave]
}

func (m *Model) PrivateKey(lite *url.URL) *PrivateKey {
	return &PrivateKey{privateKey{m.getPrivateKey(lite)}}
}

// Get loads the contents of the enclave as a locked buffer. The buffer must be
// destroyed once it is no longer needed.
func (p *privateKey) Get() (*memguard.LockedBuffer, error) {
	v, err := p.Value.Get()
	if err != nil {
		return nil, err
	}
	b, err := v.Open()
	if err != nil {
		return nil, err
	}
	return b, nil
}

// GetCopy loads, unlocks, and copies the contents of the enclave. The value
// must be wiped once it is no longer needed. GetCopy *does not* guarantee
// constant-time.
func (p *privateKey) GetCopy() ([]byte, error) {
	b, err := p.Get()
	if err != nil {
		return nil, err
	}
	defer b.Destroy()
	c := make([]byte, b.Size())
	copy(c, b.Bytes())
	return c, nil
}

func (p *privateKey) GetAs(target any) error {
	var err error
	switch target := target.(type) {
	case *[]byte:
		*target, err = p.GetCopy()
	case **memguard.LockedBuffer:
		*target, err = p.Get()
	case **Enclave:
		*target, err = p.Value.Get()
	case **memguard.Enclave:
		v, err := p.Value.Get()
		if err == nil {
			*target = &v.Enclave
		}
	default:
		return p.Value.GetAs(target)
	}
	return err
}

// Put creates an enclave with the buffer and stores it. The original buffer is
// wiped.
func (p *privateKey) Put(b []byte) error {
	if len(b) == 0 {
		return errors.New("cannot create an enclave from an empty buffer")
	}
	return p.Value.Put(NewEnclave(b, false))
}

// PutCopy creates an enclave with a copy of the buffer and stores it. The
// original buffer is not modified.
func (p *privateKey) PutCopy(b []byte) error {
	if len(b) == 0 {
		return errors.New("cannot create an enclave from an empty buffer")
	}
	return p.Value.Put(NewEnclave(b, true))
}

// PutLocked creates an enclave from the locked buffer and stores it. The buffer
// is wiped and destroyed.
func (p *privateKey) PutLocked(b *memguard.LockedBuffer) error {
	if b.Size() == 0 {
		return errors.New("cannot create an enclave from an empty buffer")
	}
	return p.Value.Put(&Enclave{*b.Seal()})
}
