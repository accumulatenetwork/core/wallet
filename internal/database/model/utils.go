package model

func store[T any](lastErr *error, put func(T) error, v T) {
	if *lastErr == nil {
		*lastErr = put(v)
	}
}
