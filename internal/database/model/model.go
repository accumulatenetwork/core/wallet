package model

import (
	"errors"

	"github.com/awnumar/memguard"
	"github.com/cometbft/cometbft/libs/log"
	"gitlab.com/accumulatenetwork/accumulate/pkg/database/keyvalue"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/record"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
)

//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-types --package model --language go-alt types.yml
//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-model --package model --logger optionalLogger model.yml

// Version is incremented whenever a bucket format is changed.
var Version = db.NewVersion(0, 0, 5, 0)

type optionalLogger struct {
	L log.Logger
}

func New(db db.DB) *Model {
	return NewEncrypted(db, nil)
}

func NewEncrypted(db db.DB, key *memguard.Enclave) *Model {
	c := new(Model)
	c.store = keyvalue.RecordStore{Store: &kvStore{db, key}}
	return c
}

func With(db db.DB, fn func(db *Model) error) error {
	index := New(db)
	err := fn(index)
	if err != nil {
		return err
	}
	return index.Commit()
}

// AsEncrypted returns a new model using the given key for encryption.
func (m *Model) AsEncrypted(key *memguard.Enclave) *Model {
	return NewEncrypted(m.unwrap().db, key)
}

// Decrypt calls AsEncrypted and verifies the magic.
func (m *Model) Decrypt(key *memguard.Enclave) (*Model, error) {
	n := m.AsEncrypted(key)
	err := n.Config().Magic().Get()
	if err != nil {
		return nil, err
	}
	return n, nil
}

func (m *Model) unwrap() *kvStore {
	return m.store.(keyvalue.RecordStore).Store.(*kvStore)
}

func (m *Model) Unwrap() db.DB {
	// Use a db.DB implementation that goes through kvStore
	return kvDb{m.unwrap()}
}

type kvDb struct {
	store *kvStore
}

func (db kvDb) Close() error { return errors.New("not supported") }

func (db kvDb) Name() string { return db.store.db.Name() }

func (db kvDb) Get(bucket []byte, key []byte) (value []byte, err error) {
	return db.store.Get(record.NewKey(string(bucket), string(key)))
}

func (db kvDb) Put(bucket []byte, key []byte, value []byte) error {
	return db.store.Put(record.NewKey(string(bucket), string(key)), value)
}

func (db kvDb) GetBucket(bucket []byte) (*db.Bucket, error) {
	return db.store.getBucket(record.NewKey(string(bucket)))
}

func (db kvDb) Delete(bucket []byte, key []byte) error {
	return db.store.Delete(record.NewKey(string(bucket), string(key)))
}

func (db kvDb) DeleteBucket(bucket []byte) error {
	return db.store.db.DeleteBucket(bucket)
}
