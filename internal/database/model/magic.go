package model

import (
	"bytes"
	"crypto/sha256"

	"gitlab.com/accumulatenetwork/accumulate/pkg/database/values"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

// magic is used for encryption verification when a database is opened
var magic = sha256.Sum256([]byte("accumulate"))

type magicValue struct {
	values.Value[[]byte]
}

func (c *Config) Magic() magicValue {
	return magicValue{c.getMagic()}
}

func (m magicValue) Get() error {
	v, err := m.Value.Get()
	if err != nil {
		if err.Error() == "cipher: message authentication failed" {
			return errors.BadPassword.With("wrong password")
		}
		return err
	}
	if !bytes.Equal(v, magic[:]) {
		return db.ErrInvalidPassword
	}
	return nil
}

func (m magicValue) Put() error {
	return m.Value.Put(magic[:])
}
