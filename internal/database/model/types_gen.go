// Copyright 2022 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package model

// GENERATED BY go run ./tools/cmd/gen-types. DO NOT EDIT.

//lint:file-ignore S1001,S1002,S1008,SA4013 generated code

import (
	"io"

	"gitlab.com/accumulatenetwork/accumulate/pkg/types/encoding"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
)

type Address struct {
	fieldsSet  []bool
	Labels     []string               `json:"labels,omitempty" form:"labels" query:"labels" validate:"required"`
	Type       protocol.SignatureType `json:"type,omitempty" form:"type" query:"type" validate:"required"`
	Derivation string                 `json:"derivation,omitempty" form:"derivation" query:"derivation" validate:"required"`
	WalletId   *url.URL               `json:"walletId,omitempty" form:"walletId" query:"walletId" validate:"required"`
	PublicKey  []byte                 `json:"publicKey,omitempty" form:"publicKey" query:"publicKey" validate:"required"`
	LastUsedOn uint64                 `json:"lastUsedOn,omitempty" form:"lastUsedOn" query:"lastUsedOn" validate:"required"`
	extraData  []byte
}

var machine_Address = &encoding.Machine[*Address]{
	ExtraData: func(v *Address) *[]byte { return &v.extraData },
	Seen:      func(v *Address) *[]bool { return &v.fieldsSet },
	Fields: []*encoding.Field[*Address]{
		{Name: "Labels", Number: 1, Binary: true, OmitEmpty: true, Required: true, Accessor: encoding.SliceField[*Address, string, encoding.StringField[encoding.SliceIndex[string]]](func(v *Address) *[]string { return &v.Labels })},
		{Name: "Type", Number: 2, Binary: true, OmitEmpty: true, Required: true, Accessor: encoding.EnumField[*Address, *protocol.SignatureType, protocol.SignatureType](func(v *Address) *protocol.SignatureType { return &v.Type })},
		{Name: "Derivation", Number: 3, Binary: true, OmitEmpty: true, Required: true, Accessor: encoding.StringField[*Address](func(v *Address) *string { return &v.Derivation })},
		{Name: "WalletId", Number: 4, Binary: true, OmitEmpty: true, Required: true, Accessor: encoding.UrlPtrField[*Address](func(v *Address) **url.URL { return &v.WalletId })},
		{Name: "PublicKey", Number: 5, Binary: true, OmitEmpty: true, Required: true, Accessor: encoding.BytesField[*Address](func(v *Address) *[]byte { return &v.PublicKey })},
		{Name: "LastUsedOn", Number: 6, Binary: true, OmitEmpty: true, Required: true, Accessor: encoding.UintField[*Address](func(v *Address) *uint64 { return &v.LastUsedOn })},
	},
}

func (v *Address) IsValid() error                    { return machine_Address.IsValid(v) }
func (v *Address) Copy() *Address                    { return encoding.Copy(machine_Address, v) }
func (v *Address) CopyAsInterface() interface{}      { return v.Copy() }
func (v *Address) Equal(u *Address) bool             { return machine_Address.Equal(v, u) }
func (v *Address) MarshalBinary() ([]byte, error)    { return machine_Address.MarshalBinary(v) }
func (v *Address) UnmarshalBinary(data []byte) error { return machine_Address.Unmarshal(data, v) }
func (v *Address) UnmarshalBinaryFrom(rd io.Reader) error {
	return machine_Address.UnmarshalFrom(rd, v)
}
func (v *Address) MarshalJSON() ([]byte, error) { return machine_Address.JSONMarshal(v) }
func (v *Address) UnmarshalJSON(b []byte) error { return machine_Address.JSONUnmarshal(b, v) }
