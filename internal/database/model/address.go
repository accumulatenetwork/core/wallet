package model

import (
	"encoding/hex"
	"strings"

	"gitlab.com/accumulatenetwork/accumulate/pkg/database"
	"gitlab.com/accumulatenetwork/accumulate/pkg/database/values"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"gitlab.com/accumulatenetwork/core/wallet/internal/sortutil"
)

func (m *Model) addressBy(v values.Value[*url.URL]) values.Value[*Address] {
	lite, err := v.Get()
	if err != nil {
		return errorValue[*Address]{err}
	}
	return m.Address(lite)
}

func (m *Model) AddressByLabel(label string) values.Value[*Address] {
	return m.addressBy(m.getLookup().ByLabel(strings.ToLower(label)))
}

func (m *Model) AddressByPublicKey(pubKey []byte) values.Value[*Address] {
	return m.addressBy(m.getLookup().ByPublicKey(hex.EncodeToString(pubKey)))
}

func (m *Model) AddressByHash(hash []byte) values.Value[*Address] {
	return m.Address(protocol.LiteAuthorityForHash(hash))
}

func (m *Model) Addresses() []values.Value[*Address] {
	keys, err := m.getAddressKeys()
	if err != nil {
		return []values.Value[*Address]{errorValue[*Address]{err}}
	}

	var addrs []values.Value[*Address]
	for _, key := range keys {
		addrs = append(addrs, m.Address(key.Lite))
	}
	return addrs
}

func (m *Model) getAddressKeys() ([]addressKey, error) {
	b, err := m.unwrap().getBucket(m.Key().Append("Address"))
	switch {
	case err == nil:
		// Ok
	case errors.Is(err, errors.NotFound):
		return nil, nil
	default:
		return nil, err
	}

	var keys []addressKey
	for _, x := range b.KeyValueList {
		keys = append(keys, addressKey{
			Lite: url.MustParse(string(x.Key)),
		})
	}
	return keys, nil
}

func (a *Address) AddLabel(s string) {
	ptr, _ := sortutil.BinaryInsert(&a.Labels, func(r string) int {
		return strings.Compare(
			strings.ToLower(r),
			strings.ToLower(s),
		)
	})
	*ptr = s
}

func (a *Address) RemoveLabel(s string) {
	sortutil.Remove(&a.Labels, func(r string) int {
		return strings.Compare(
			strings.ToLower(r),
			strings.ToLower(s),
		)
	})
}

func (a *Address) Hash() []byte {
	hash, err := protocol.PublicKeyHash(a.PublicKey, a.Type)
	if err != nil {
		panic(err)
	}
	return hash
}

func (a *Address) LiteIdentity() *url.URL {
	return protocol.LiteAuthorityForHash(a.Hash())
}

// addressValue is a wrapper around Value that maintains the lookups.
type addressValue struct {
	values.Value[*Address]
	model  *Model
	loaded bool
	old    *Address
}

func (c *Model) newAddress(k addressKey) values.Value[*Address] {
	return &addressValue{Value: c.baseNewAddress(k), model: c}
}

func (a *addressValue) Get() (*Address, error) {
	v, err := a.Value.Get()
	if err != nil {
		return nil, err
	}
	if !a.loaded {
		a.old = v
		a.loaded = true
	}
	return v, nil
}

func (a *addressValue) Put(v *Address) error {
	if v.Type == 0 {
		return errors.BadRequest.With("cannot store an address without a type")
	}
	if v.PublicKey == nil {
		return errors.BadRequest.With("cannot store an address without a public key")
	}
	lite := a.Key().Get(1).(*url.URL)
	if !v.LiteIdentity().Equal(lite) {
		return errors.BadRequest.With("public key and lite address do not match")
	}
	if !a.loaded {
		a.old, _ = a.Value.Get()
		a.loaded = true
	}
	return a.Value.Put(v)
}

func (a *addressValue) Commit() error {
	// Base commit
	err := a.Value.Commit()
	if !a.loaded || err != nil {
		return err
	}

	new, err := a.Value.Get()
	if err != nil {
		panic(err) // Should not be possible
	}
	if a.old != nil && new.Equal(a.old) {
		return nil
	}

	// Sanity check
	lite := a.Key().Get(1).(*url.URL)
	if !new.LiteIdentity().Equal(lite) {
		return errors.BadRequest.With("public key and lite address do not match")
	}

	// For a new key
	lup := a.model.getLookup()
	if a.old == nil {
		store(&err, lup.ByPublicKey(hex.EncodeToString(new.PublicKey)).Put, lite)
	}

	// Get a map of old labels
	labels := map[string]bool{}
	if a.old != nil {
		for _, label := range a.old.Labels {
			label = strings.ToLower(label)
			labels[label] = true
		}
	}

	// Add new labels
	for _, label := range new.Labels {
		label = strings.ToLower(label)
		if !labels[label] {
			store(&err, lup.ByLabel(label).Put, lite)
		}
		delete(labels, label)
	}
	if err != nil {
		return err
	}

	// Remove labels that have been removed
	kvs := a.model.unwrap()
	for label := range labels {
		// This is a hack to get around Value's lack of a Delete method
		delete(lup.byLabel, lookupByLabelKey{label}.ForMap())
		err = kvs.Delete(lup.Key().Append("ByLabel", label))
		if err != nil {
			return err
		}
	}

	// Commit the changes to Lookup
	return lup.Commit()
}

type errorValue[T any] struct {
	err error
}

func (e errorValue[T]) Key() *database.Key  { panic("stub") }
func (e errorValue[T]) IsDirty() bool       { return false }
func (e errorValue[T]) Commit() error       { return e.err }
func (e errorValue[T]) Get() (v T, _ error) { return v, e.err }
func (e errorValue[T]) GetAs(any) error     { return e.err }
func (e errorValue[T]) Put(T) error         { return e.err }

func (e errorValue[T]) Resolve(key *database.Key) (database.Record, *database.Key, error) {
	return nil, nil, e.err
}

func (e errorValue[T]) Walk(opts database.WalkOptions, fn database.WalkFunc) error {
	return e.err
}
