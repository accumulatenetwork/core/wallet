// Copyright 2022 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package model

// GENERATED BY go run ./tools/cmd/gen-model. DO NOT EDIT.

//lint:file-ignore S1008,U1000 generated code

import (
	record "gitlab.com/accumulatenetwork/accumulate/pkg/database"
	"gitlab.com/accumulatenetwork/accumulate/pkg/database/values"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
)

type Model struct {
	logger optionalLogger
	store  record.Store

	multi      *Multi
	config     *Config
	accounts   *Accounts
	address    map[addressMapKey]values.Value[*Address]
	privateKey map[privateKeyMapKey]values.Value[*Enclave]
	lookup     *Lookup
}

func (c *Model) Key() *record.Key { return nil }

type addressKey struct {
	Lite *url.URL
}

type addressMapKey struct {
	Lite [32]byte
}

func (k addressKey) ForMap() addressMapKey {
	return addressMapKey{values.MapKeyUrl(k.Lite)}
}

type privateKeyKey struct {
	Lite *url.URL
}

type privateKeyMapKey struct {
	Lite [32]byte
}

func (k privateKeyKey) ForMap() privateKeyMapKey {
	return privateKeyMapKey{values.MapKeyUrl(k.Lite)}
}

func (c *Model) Multi() *Multi {
	return values.GetOrCreate(c, &c.multi, (*Model).newMulti)
}

func (c *Model) newMulti() *Multi {
	v := new(Multi)
	v.logger = c.logger
	v.store = c.store
	v.key = (*record.Key)(nil).Append("Multi")
	v.parent = c
	return v
}

func (c *Model) Config() *Config {
	return values.GetOrCreate(c, &c.config, (*Model).newConfig)
}

func (c *Model) newConfig() *Config {
	v := new(Config)
	v.logger = c.logger
	v.store = c.store
	v.key = (*record.Key)(nil).Append("Config")
	v.parent = c
	return v
}

func (c *Model) Accounts() *Accounts {
	return values.GetOrCreate(c, &c.accounts, (*Model).newAccounts)
}

func (c *Model) newAccounts() *Accounts {
	v := new(Accounts)
	v.logger = c.logger
	v.store = c.store
	v.key = (*record.Key)(nil).Append("Accounts")
	v.parent = c
	return v
}

func (c *Model) Address(lite *url.URL) values.Value[*Address] {
	return values.GetOrCreateMap(c, &c.address, addressKey{lite}, (*Model).newAddress)
}

func (c *Model) baseNewAddress(k addressKey) values.Value[*Address] {
	return values.NewValue(c.logger.L, c.store, (*record.Key)(nil).Append("Address", k.Lite), false, values.Struct[Address]())
}

func (c *Model) getPrivateKey(lite *url.URL) values.Value[*Enclave] {
	return values.GetOrCreateMap(c, &c.privateKey, privateKeyKey{lite}, (*Model).newPrivateKey)
}

func (c *Model) newPrivateKey(k privateKeyKey) values.Value[*Enclave] {
	return values.NewValue(c.logger.L, c.store, (*record.Key)(nil).Append("PrivateKey", k.Lite), false, values.Struct[Enclave]())
}

func (c *Model) getLookup() *Lookup {
	return values.GetOrCreate(c, &c.lookup, (*Model).newLookup)
}

func (c *Model) newLookup() *Lookup {
	v := new(Lookup)
	v.logger = c.logger
	v.store = c.store
	v.key = (*record.Key)(nil).Append("Lookup")
	v.parent = c
	return v
}

func (c *Model) Resolve(key *record.Key) (record.Record, *record.Key, error) {
	if key.Len() == 0 {
		return nil, nil, errors.InternalError.With("bad key for model (1)")
	}

	switch key.Get(0) {
	case "Multi":
		return c.Multi(), key.SliceI(1), nil
	case "Config":
		return c.Config(), key.SliceI(1), nil
	case "Accounts":
		return c.Accounts(), key.SliceI(1), nil
	case "Address":
		if key.Len() < 2 {
			return nil, nil, errors.InternalError.With("bad key for model (2)")
		}
		lite, okLite := key.Get(1).(*url.URL)
		if !okLite {
			return nil, nil, errors.InternalError.With("bad key for model (3)")
		}
		v := c.Address(lite)
		return v, key.SliceI(2), nil
	case "PrivateKey":
		if key.Len() < 2 {
			return nil, nil, errors.InternalError.With("bad key for model (4)")
		}
		lite, okLite := key.Get(1).(*url.URL)
		if !okLite {
			return nil, nil, errors.InternalError.With("bad key for model (5)")
		}
		v := c.getPrivateKey(lite)
		return v, key.SliceI(2), nil
	case "Lookup":
		return c.getLookup(), key.SliceI(1), nil
	default:
		return nil, nil, errors.InternalError.With("bad key for model (6)")
	}
}

func (c *Model) IsDirty() bool {
	if c == nil {
		return false
	}

	if values.IsDirty(c.multi) {
		return true
	}
	if values.IsDirty(c.config) {
		return true
	}
	if values.IsDirty(c.accounts) {
		return true
	}
	for _, v := range c.address {
		if v.IsDirty() {
			return true
		}
	}
	for _, v := range c.privateKey {
		if v.IsDirty() {
			return true
		}
	}
	if values.IsDirty(c.lookup) {
		return true
	}

	return false
}

func (c *Model) Walk(opts record.WalkOptions, fn record.WalkFunc) error {
	if c == nil {
		return nil
	}

	skip, err := values.WalkComposite(c, opts, fn)
	if skip || err != nil {
		return errors.UnknownError.Wrap(err)
	}
	values.WalkField(&err, c.multi, c.newMulti, opts, fn)
	values.WalkField(&err, c.config, c.newConfig, opts, fn)
	values.WalkField(&err, c.accounts, c.newAccounts, opts, fn)
	values.WalkMap(&err, c.address, c.newAddress, nil, opts, fn)
	values.WalkMap(&err, c.privateKey, c.newPrivateKey, nil, opts, fn)
	values.WalkField(&err, c.lookup, c.newLookup, opts, fn)
	return err
}

func (c *Model) Commit() error {
	if c == nil {
		return nil
	}

	var err error
	values.Commit(&err, c.multi)
	values.Commit(&err, c.config)
	values.Commit(&err, c.accounts)
	for _, v := range c.address {
		values.Commit(&err, v)
	}
	for _, v := range c.privateKey {
		values.Commit(&err, v)
	}
	values.Commit(&err, c.lookup)

	return err
}

type Multi struct {
	logger optionalLogger
	store  record.Store
	key    *record.Key
	parent *Model

	enabled values.Value[bool]
	salt    values.Value[[32]byte]
	names   values.Set[string]
	vault   map[multiVaultMapKey]values.Value[*api.VaultInfo]
}

func (c *Multi) Key() *record.Key { return c.key }

type multiVaultKey struct {
	Name string
}

type multiVaultMapKey struct {
	Name string
}

func (k multiVaultKey) ForMap() multiVaultMapKey {
	return multiVaultMapKey{k.Name}
}

func (c *Multi) Enabled() values.Value[bool] {
	return values.GetOrCreate(c, &c.enabled, (*Multi).newEnabled)
}

func (c *Multi) newEnabled() values.Value[bool] {
	return values.NewValue(c.logger.L, c.store, c.key.Append("Enabled"), true, values.Wrapped(values.BoolWrapper))
}

func (c *Multi) Salt() values.Value[[32]byte] {
	return values.GetOrCreate(c, &c.salt, (*Multi).newSalt)
}

func (c *Multi) newSalt() values.Value[[32]byte] {
	return values.NewValue(c.logger.L, c.store, c.key.Append("Salt"), false, values.Wrapped(values.HashWrapper))
}

func (c *Multi) Names() values.Set[string] {
	return values.GetOrCreate(c, &c.names, (*Multi).newNames)
}

func (c *Multi) newNames() values.Set[string] {
	return values.NewSet(c.logger.L, c.store, c.key.Append("Names"), values.Wrapped(values.StringWrapper), values.CompareString)
}

func (c *Multi) Vault(name string) values.Value[*api.VaultInfo] {
	return values.GetOrCreateMap(c, &c.vault, multiVaultKey{name}, (*Multi).newVault)
}

func (c *Multi) newVault(k multiVaultKey) values.Value[*api.VaultInfo] {
	return values.NewValue(c.logger.L, c.store, c.key.Append("Vault", k.Name), false, values.Struct[api.VaultInfo]())
}

func (c *Multi) Resolve(key *record.Key) (record.Record, *record.Key, error) {
	if key.Len() == 0 {
		return nil, nil, errors.InternalError.With("bad key for multi (1)")
	}

	switch key.Get(0) {
	case "Enabled":
		return c.Enabled(), key.SliceI(1), nil
	case "Salt":
		return c.Salt(), key.SliceI(1), nil
	case "Names":
		return c.Names(), key.SliceI(1), nil
	case "Vault":
		if key.Len() < 2 {
			return nil, nil, errors.InternalError.With("bad key for multi (2)")
		}
		name, okName := key.Get(1).(string)
		if !okName {
			return nil, nil, errors.InternalError.With("bad key for multi (3)")
		}
		v := c.Vault(name)
		return v, key.SliceI(2), nil
	default:
		return nil, nil, errors.InternalError.With("bad key for multi (4)")
	}
}

func (c *Multi) IsDirty() bool {
	if c == nil {
		return false
	}

	if values.IsDirty(c.enabled) {
		return true
	}
	if values.IsDirty(c.salt) {
		return true
	}
	if values.IsDirty(c.names) {
		return true
	}
	for _, v := range c.vault {
		if v.IsDirty() {
			return true
		}
	}

	return false
}

func (c *Multi) Walk(opts record.WalkOptions, fn record.WalkFunc) error {
	if c == nil {
		return nil
	}

	skip, err := values.WalkComposite(c, opts, fn)
	if skip || err != nil {
		return errors.UnknownError.Wrap(err)
	}
	values.WalkField(&err, c.enabled, c.newEnabled, opts, fn)
	values.WalkField(&err, c.salt, c.newSalt, opts, fn)
	values.WalkField(&err, c.names, c.newNames, opts, fn)
	values.WalkMap(&err, c.vault, c.newVault, nil, opts, fn)
	return err
}

func (c *Multi) Commit() error {
	if c == nil {
		return nil
	}

	var err error
	values.Commit(&err, c.enabled)
	values.Commit(&err, c.salt)
	values.Commit(&err, c.names)
	for _, v := range c.vault {
		values.Commit(&err, v)
	}

	return err
}

type Config struct {
	logger optionalLogger
	store  record.Store
	key    *record.Key
	parent *Model

	version values.Value[[]byte]
	salt    values.Value[[]byte]
	magic   values.Value[[]byte]
}

func (c *Config) Key() *record.Key { return c.key }

func (c *Config) Version() values.Value[[]byte] {
	return values.GetOrCreate(c, &c.version, (*Config).newVersion)
}

func (c *Config) newVersion() values.Value[[]byte] {
	return values.NewValue(c.logger.L, c.store, c.key.Append("version"), false, values.Wrapped(values.RawWrapper))
}

func (c *Config) Salt() values.Value[[]byte] {
	return values.GetOrCreate(c, &c.salt, (*Config).newSalt)
}

func (c *Config) newSalt() values.Value[[]byte] {
	return values.NewValue(c.logger.L, c.store, c.key.Append("salt"), false, values.Wrapped(values.RawWrapper))
}

func (c *Config) getMagic() values.Value[[]byte] {
	return values.GetOrCreate(c, &c.magic, (*Config).newMagic)
}

func (c *Config) newMagic() values.Value[[]byte] {
	return values.NewValue(c.logger.L, c.store, c.key.Append("magic"), false, values.Wrapped(values.RawWrapper))
}

func (c *Config) Resolve(key *record.Key) (record.Record, *record.Key, error) {
	if key.Len() == 0 {
		return nil, nil, errors.InternalError.With("bad key for config (1)")
	}

	switch key.Get(0) {
	case "version":
		return c.Version(), key.SliceI(1), nil
	case "salt":
		return c.Salt(), key.SliceI(1), nil
	case "magic":
		return c.getMagic(), key.SliceI(1), nil
	default:
		return nil, nil, errors.InternalError.With("bad key for config (2)")
	}
}

func (c *Config) IsDirty() bool {
	if c == nil {
		return false
	}

	if values.IsDirty(c.version) {
		return true
	}
	if values.IsDirty(c.salt) {
		return true
	}
	if values.IsDirty(c.magic) {
		return true
	}

	return false
}

func (c *Config) Walk(opts record.WalkOptions, fn record.WalkFunc) error {
	if c == nil {
		return nil
	}

	skip, err := values.WalkComposite(c, opts, fn)
	if skip || err != nil {
		return errors.UnknownError.Wrap(err)
	}
	values.WalkField(&err, c.version, c.newVersion, opts, fn)
	values.WalkField(&err, c.salt, c.newSalt, opts, fn)
	values.WalkField(&err, c.magic, c.newMagic, opts, fn)
	return err
}

func (c *Config) Commit() error {
	if c == nil {
		return nil
	}

	var err error
	values.Commit(&err, c.version)
	values.Commit(&err, c.salt)
	values.Commit(&err, c.magic)

	return err
}

type Accounts struct {
	logger optionalLogger
	store  record.Store
	key    *record.Key
	parent *Model

	tokens values.Set[*url.URL]
	books  values.Set[*url.URL]
	book   map[accountsBookMapKey]*AccountsBook
}

func (c *Accounts) Key() *record.Key { return c.key }

type accountsBookKey struct {
	Url *url.URL
}

type accountsBookMapKey struct {
	Url [32]byte
}

func (k accountsBookKey) ForMap() accountsBookMapKey {
	return accountsBookMapKey{values.MapKeyUrl(k.Url)}
}

func (c *Accounts) Tokens() values.Set[*url.URL] {
	return values.GetOrCreate(c, &c.tokens, (*Accounts).newTokens)
}

func (c *Accounts) newTokens() values.Set[*url.URL] {
	return values.NewSet(c.logger.L, c.store, c.key.Append("Tokens"), values.Wrapped(values.UrlWrapper), values.CompareUrl)
}

func (c *Accounts) Books() values.Set[*url.URL] {
	return values.GetOrCreate(c, &c.books, (*Accounts).newBooks)
}

func (c *Accounts) newBooks() values.Set[*url.URL] {
	return values.NewSet(c.logger.L, c.store, c.key.Append("Books"), values.Wrapped(values.UrlWrapper), values.CompareUrl)
}

func (c *Accounts) Book(url *url.URL) *AccountsBook {
	return values.GetOrCreateMap(c, &c.book, accountsBookKey{url}, (*Accounts).newBook)
}

func (c *Accounts) newBook(k accountsBookKey) *AccountsBook {
	v := new(AccountsBook)
	v.logger = c.logger
	v.store = c.store
	v.key = c.key.Append("Book", k.Url)
	v.parent = c
	return v
}

func (c *Accounts) Resolve(key *record.Key) (record.Record, *record.Key, error) {
	if key.Len() == 0 {
		return nil, nil, errors.InternalError.With("bad key for accounts (1)")
	}

	switch key.Get(0) {
	case "Tokens":
		return c.Tokens(), key.SliceI(1), nil
	case "Books":
		return c.Books(), key.SliceI(1), nil
	case "Book":
		if key.Len() < 2 {
			return nil, nil, errors.InternalError.With("bad key for accounts (2)")
		}
		url, okUrl := key.Get(1).(*url.URL)
		if !okUrl {
			return nil, nil, errors.InternalError.With("bad key for accounts (3)")
		}
		v := c.Book(url)
		return v, key.SliceI(2), nil
	default:
		return nil, nil, errors.InternalError.With("bad key for accounts (4)")
	}
}

func (c *Accounts) IsDirty() bool {
	if c == nil {
		return false
	}

	if values.IsDirty(c.tokens) {
		return true
	}
	if values.IsDirty(c.books) {
		return true
	}
	for _, v := range c.book {
		if v.IsDirty() {
			return true
		}
	}

	return false
}

func (c *Accounts) Walk(opts record.WalkOptions, fn record.WalkFunc) error {
	if c == nil {
		return nil
	}

	skip, err := values.WalkComposite(c, opts, fn)
	if skip || err != nil {
		return errors.UnknownError.Wrap(err)
	}
	values.WalkField(&err, c.tokens, c.newTokens, opts, fn)
	values.WalkField(&err, c.books, c.newBooks, opts, fn)
	values.WalkMap(&err, c.book, c.newBook, nil, opts, fn)
	return err
}

func (c *Accounts) Commit() error {
	if c == nil {
		return nil
	}

	var err error
	values.Commit(&err, c.tokens)
	values.Commit(&err, c.books)
	for _, v := range c.book {
		values.Commit(&err, v)
	}

	return err
}

type AccountsBook struct {
	logger optionalLogger
	store  record.Store
	key    *record.Key
	parent *Accounts

	pages values.List[*protocol.KeyPage]
}

func (c *AccountsBook) Key() *record.Key { return c.key }

func (c *AccountsBook) Pages() values.List[*protocol.KeyPage] {
	return values.GetOrCreate(c, &c.pages, (*AccountsBook).newPages)
}

func (c *AccountsBook) newPages() values.List[*protocol.KeyPage] {
	return values.NewList(c.logger.L, c.store, c.key.Append("Pages"), values.Struct[protocol.KeyPage]())
}

func (c *AccountsBook) Resolve(key *record.Key) (record.Record, *record.Key, error) {
	if key.Len() == 0 {
		return nil, nil, errors.InternalError.With("bad key for book (1)")
	}

	switch key.Get(0) {
	case "Pages":
		return c.Pages(), key.SliceI(1), nil
	default:
		return nil, nil, errors.InternalError.With("bad key for book (2)")
	}
}

func (c *AccountsBook) IsDirty() bool {
	if c == nil {
		return false
	}

	if values.IsDirty(c.pages) {
		return true
	}

	return false
}

func (c *AccountsBook) Walk(opts record.WalkOptions, fn record.WalkFunc) error {
	if c == nil {
		return nil
	}

	skip, err := values.WalkComposite(c, opts, fn)
	if skip || err != nil {
		return errors.UnknownError.Wrap(err)
	}
	values.WalkField(&err, c.pages, c.newPages, opts, fn)
	return err
}

func (c *AccountsBook) Commit() error {
	if c == nil {
		return nil
	}

	var err error
	values.Commit(&err, c.pages)

	return err
}

type Lookup struct {
	logger optionalLogger
	store  record.Store
	key    *record.Key
	parent *Model

	byLabel     map[lookupByLabelMapKey]values.Value[*url.URL]
	byPublicKey map[lookupByPublicKeyMapKey]values.Value[*url.URL]
}

func (c *Lookup) Key() *record.Key { return c.key }

type lookupByLabelKey struct {
	Value string
}

type lookupByLabelMapKey struct {
	Value string
}

func (k lookupByLabelKey) ForMap() lookupByLabelMapKey {
	return lookupByLabelMapKey{k.Value}
}

type lookupByPublicKeyKey struct {
	Value string
}

type lookupByPublicKeyMapKey struct {
	Value string
}

func (k lookupByPublicKeyKey) ForMap() lookupByPublicKeyMapKey {
	return lookupByPublicKeyMapKey{k.Value}
}

func (c *Lookup) ByLabel(value string) values.Value[*url.URL] {
	return values.GetOrCreateMap(c, &c.byLabel, lookupByLabelKey{value}, (*Lookup).newByLabel)
}

func (c *Lookup) newByLabel(k lookupByLabelKey) values.Value[*url.URL] {
	return values.NewValue(c.logger.L, c.store, c.key.Append("ByLabel", k.Value), false, values.Wrapped(values.UrlWrapper))
}

func (c *Lookup) ByPublicKey(value string) values.Value[*url.URL] {
	return values.GetOrCreateMap(c, &c.byPublicKey, lookupByPublicKeyKey{value}, (*Lookup).newByPublicKey)
}

func (c *Lookup) newByPublicKey(k lookupByPublicKeyKey) values.Value[*url.URL] {
	return values.NewValue(c.logger.L, c.store, c.key.Append("ByPublicKey", k.Value), false, values.Wrapped(values.UrlWrapper))
}

func (c *Lookup) Resolve(key *record.Key) (record.Record, *record.Key, error) {
	if key.Len() == 0 {
		return nil, nil, errors.InternalError.With("bad key for lookup (1)")
	}

	switch key.Get(0) {
	case "ByLabel":
		if key.Len() < 2 {
			return nil, nil, errors.InternalError.With("bad key for lookup (2)")
		}
		value, okValue := key.Get(1).(string)
		if !okValue {
			return nil, nil, errors.InternalError.With("bad key for lookup (3)")
		}
		v := c.ByLabel(value)
		return v, key.SliceI(2), nil
	case "ByPublicKey":
		if key.Len() < 2 {
			return nil, nil, errors.InternalError.With("bad key for lookup (4)")
		}
		value, okValue := key.Get(1).(string)
		if !okValue {
			return nil, nil, errors.InternalError.With("bad key for lookup (5)")
		}
		v := c.ByPublicKey(value)
		return v, key.SliceI(2), nil
	default:
		return nil, nil, errors.InternalError.With("bad key for lookup (6)")
	}
}

func (c *Lookup) IsDirty() bool {
	if c == nil {
		return false
	}

	for _, v := range c.byLabel {
		if v.IsDirty() {
			return true
		}
	}
	for _, v := range c.byPublicKey {
		if v.IsDirty() {
			return true
		}
	}

	return false
}

func (c *Lookup) Walk(opts record.WalkOptions, fn record.WalkFunc) error {
	if c == nil {
		return nil
	}

	skip, err := values.WalkComposite(c, opts, fn)
	if skip || err != nil {
		return errors.UnknownError.Wrap(err)
	}
	values.WalkMap(&err, c.byLabel, c.newByLabel, nil, opts, fn)
	values.WalkMap(&err, c.byPublicKey, c.newByPublicKey, nil, opts, fn)
	return err
}

func (c *Lookup) Commit() error {
	if c == nil {
		return nil
	}

	var err error
	for _, v := range c.byLabel {
		values.Commit(&err, v)
	}
	for _, v := range c.byPublicKey {
		values.Commit(&err, v)
	}

	return err
}
