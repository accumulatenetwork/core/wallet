package model

import (
	"github.com/awnumar/memguard"
	"gitlab.com/accumulatenetwork/accumulate/pkg/database"
	record "gitlab.com/accumulatenetwork/accumulate/pkg/database"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

type kvStore struct {
	db  db.DB
	key *memguard.Enclave
}

func (s *kvStore) isEncrypted(k *record.Key) bool {
	if s.key == nil {
		return false
	}

	bucket, key := convertKey(k)
	return s.isEncrypted2(string(bucket), string(key))
}

func (s *kvStore) isEncrypted2(bucket, key string) bool {
	if s.key == nil {
		return false
	}

	// It would be nice to encode this directly in model.yml
	switch bucket {
	case "Config":
		switch key {
		case "version", "salt":
			return false
		}
	}
	return true
}

func (s *kvStore) getBucket(k *database.Key) (*db.Bucket, error) {
	bucket, ok := k.Get(0).(string)
	if !ok {
		panic("first key part is not a string")
	}

	b, err := s.db.GetBucket([]byte(bucket))
	if err != nil {
		return nil, err
	}
	if s.key == nil {
		return b, nil
	}

	key, err := s.key.Open()
	if err != nil {
		return nil, err
	}
	defer key.Destroy()
	for i, item := range b.KeyValueList {
		if !s.isEncrypted2(bucket, string(item.Key)) {
			continue
		}
		b.KeyValueList[i].Value, err = db.SymDecrypt(item.Value, key.Bytes())
		if err != nil {
			return nil, err
		}
	}
	return b, nil
}

func (s *kvStore) Get(k *database.Key) ([]byte, error) {
	v, err := s.db.Get(convertKey(k))
	if err != nil {
		return nil, err
	}
	if !s.isEncrypted(k) {
		return v, nil
	}

	key, err := s.key.Open()
	if err != nil {
		return nil, err
	}
	defer key.Destroy()

	return db.SymDecrypt(v, key.Bytes())
}

func (s *kvStore) Put(k *database.Key, value []byte) error {
	if s.isEncrypted(k) {
		key, err := s.key.Open()
		if err != nil {
			return err
		}
		defer key.Destroy()

		value, err = db.SymEncrypt(value, key.Bytes())
		if err != nil {
			return err
		}
	}

	bucket, kk := convertKey(k)
	return s.db.Put(bucket, kk, value)
}

func (s *kvStore) Delete(k *database.Key) error {
	bucket, key := convertKey(k)
	return s.db.Delete(bucket, key)
}

func (s *kvStore) ForEach(func(*record.Key, []byte) error) error {
	return errors.NotSupported.With("ForEach is not supported")
}

func convertKey(k *database.Key) ([]byte, []byte) {
	if k.Len() < 2 {
		panic("key is too short")
	}
	bucket, ok := k.Get(0).(string)
	if !ok {
		panic("first key part is not a string")
	}
	key := k.SliceI(1).String() // Hash like Accumulate does?
	return []byte(bucket), []byte(key)
}
