package model

import (
	"crypto/ed25519"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	acctesting "gitlab.com/accumulatenetwork/accumulate/test/testing"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

func generate(seed ...any) (*Address, ed25519.PrivateKey) {
	sk := acctesting.GenerateKey()
	return &Address{
		Type:      protocol.SignatureTypeED25519,
		PublicKey: sk[32:],
	}, sk
}

func TestLookupByPublicKey(t *testing.T) {
	// Create a key
	m := New(new(db.MemoryDB))
	addr, _ := generate()
	require.NoError(t, m.Address(addr.LiteIdentity()).Put(addr))
	require.NoError(t, m.Commit())

	// Verify lookup via public key
	v, err := m.AddressByPublicKey(addr.PublicKey).Get()
	require.NoError(t, err)
	require.Equal(t, addr, v)
}

func TestLookupByHash(t *testing.T) {
	// Create a key
	m := New(new(db.MemoryDB))
	addr, _ := generate()
	require.NoError(t, m.Address(addr.LiteIdentity()).Put(addr))
	require.NoError(t, m.Commit())

	// Verify lookup via hash
	v, err := m.AddressByHash(addr.Hash()).Get()
	require.NoError(t, err)
	require.Equal(t, addr, v)
}

func TestLookupByLabel(t *testing.T) {
	// Create a key
	m := New(new(db.MemoryDB))
	addr, _ := generate()
	addr.Labels = []string{
		"foo",
		"Bar",
	}
	require.NoError(t, m.Address(addr.LiteIdentity()).Put(addr))
	require.NoError(t, m.Commit())

	// Verify lookup via label
	v, err := m.AddressByLabel("Foo").Get()
	require.NoError(t, err)
	require.Equal(t, addr, v)

	v, err = m.AddressByLabel("Bar").Get()
	require.NoError(t, err)
	require.Equal(t, addr, v)

	_, err = m.AddressByLabel("Baz").Get()
	require.ErrorIs(t, err, errors.NotFound)
}
