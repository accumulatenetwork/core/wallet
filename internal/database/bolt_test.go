package db

import (
	"os"
	"path/filepath"
	"testing"
)

func TestBoltDatabase(t *testing.T) {
	dirName, e := os.MkdirTemp("", "boltTest")
	if e != nil {
		t.Fatal(e)
	}

	err := os.MkdirAll(dirName, 0600)
	if err != nil {
		t.Fatal(err)
	}
	defer os.RemoveAll(dirName)

	db, err := OpenBolt(filepath.Join(dirName, "test.db"))
	if err != nil {
		t.Fatal(err)
	}
	defer db.Close()

	databaseTests(t, db)
}
