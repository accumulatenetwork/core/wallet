package db

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"fmt"
	"io"

	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"golang.org/x/crypto/scrypt"
)

func DeriveSymKey(password, salt []byte) ([]byte, error) {
	key, err := scrypt.Key(password, salt, 16384, 8, 1, 32)
	if err != nil {
		return nil, err
	}

	if len(key) != 32 {
		return nil, fmt.Errorf("key length must be 32 bytes, found %d", len(key))
	}

	return key, err
}

func CheckSymKey(key []byte) error {
	if len(key) != 32 {
		return fmt.Errorf("AES key must be 32 bytes, length given is %d", len(key))
	}
	return nil
}

func SymEncrypt(plaintext []byte, key []byte) ([]byte, error) {
	err := CheckSymKey(key)
	if err != nil {
		return nil, err
	}

	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	nonce := make([]byte, gcm.NonceSize())
	if _, err = io.ReadFull(rand.Reader, nonce); err != nil {
		return nil, err
	}

	return gcm.Seal(nonce, nonce, plaintext, nil), nil
}

func SymDecrypt(ciphertext []byte, key []byte) ([]byte, error) {
	err := CheckSymKey(key)
	if err != nil {
		return nil, err
	}

	c, err := aes.NewCipher(key)
	if err != nil {
		return nil, err
	}

	gcm, err := cipher.NewGCM(c)
	if err != nil {
		return nil, err
	}

	nonceSize := gcm.NonceSize()
	if len(ciphertext) < nonceSize {
		return nil, errors.BadPassword.With("ciphertext too short")
	}

	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]
	return gcm.Open(nil, nonce, ciphertext, nil)
}
