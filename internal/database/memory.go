package db

import (
	"crypto/sha256"
	"fmt"
	"time"
)

// MemoryDB holds the main map of buckets for the in-memory database
type MemoryDB struct {
	buckets map[[32]byte]*Bucket
	closed  bool
}

func (b *MemoryDB) Name() string {
	return "memory"
}

// Close clears out the data and uninitializes the MemoryDB
func (b *MemoryDB) Close() error {
	if b.closed {
		return fmt.Errorf("closed")
	}
	b.closed = true
	b.buckets = nil
	return nil
}

func (b *MemoryDB) IsLocked() bool                             { return false }
func (b *MemoryDB) Lock()                                      {}
func (b *MemoryDB) UnlockFor(time.Duration) (time.Time, error) { return time.Time{}, nil }

// Get will get an entry in the database given a bucket and key
func (b *MemoryDB) GetRaw(bucket []byte, key []byte) (value []byte, err error) {
	if b.closed {
		return nil, fmt.Errorf("closed")
	}
	if b.buckets == nil {
		b.buckets = make(map[[32]byte]*Bucket)
	}

	v, ok := b.buckets[sha256.Sum256(bucket)]
	if !ok {
		return nil, ErrNoBucket
	}

	value = v.Get(key)
	if value == nil {
		return nil, ErrNotFound
	}

	u := make([]byte, len(value))
	copy(u, value)
	return u, nil
}
func (b *MemoryDB) Get(bucket []byte, key []byte) (value []byte, err error) {
	return b.GetRaw(bucket, key)
}

// PutRaw will write data to a given bucket using the key
func (b *MemoryDB) PutRaw(bucket []byte, key []byte, value []byte) error {
	if b.closed {
		return fmt.Errorf("closed")
	}
	if b.buckets == nil {
		b.buckets = make(map[[32]byte]*Bucket)
	}

	var v *Bucket
	var ok bool
	bk := sha256.Sum256(bucket)
	if v, ok = b.buckets[bk]; !ok {
		v = NewBucket()
		b.buckets[bk] = v
	}

	u := make([]byte, len(value))
	copy(u, value)

	v.Put(key, u)
	return nil
}

// Put will write data to a given bucket using the key
func (b *MemoryDB) Put(bucket []byte, key []byte, value []byte) error {
	return b.PutRaw(bucket, key, value)
}

// GetBucket will return the contents of a bucket
func (b *MemoryDB) GetBucket(bucket []byte) (buck *Bucket, err error) {
	if b.closed {
		return nil, fmt.Errorf("closed")
	}
	if b.buckets == nil {
		b.buckets = make(map[[32]byte]*Bucket)
	}

	buck, ok := b.buckets[sha256.Sum256(bucket)]
	if !ok {
		return nil, ErrNoBucket
	}

	return buck.Copy(), nil
}

// Delete will remove a key/value pair from the bucket
func (b *MemoryDB) Delete(bucket []byte, key []byte) (err error) {
	if b.closed {
		return fmt.Errorf("closed")
	}
	if b.buckets == nil {
		b.buckets = make(map[[32]byte]*Bucket)
	}

	if buck, ok := b.buckets[sha256.Sum256(bucket)]; ok {
		err = buck.Delete(key)
	} else {
		err = ErrNoBucket
	}
	return err
}

// DeleteBucket will delete all key/value pairs from a bucket
func (b *MemoryDB) DeleteBucket(bucket []byte) error {
	if b.closed {
		return fmt.Errorf("closed")
	}
	delete(b.buckets, sha256.Sum256(bucket))
	return nil
}
