package db

import (
	"bytes"
	"crypto/sha256"
	"time"
)

// magic is used for encryption verification when a database is opened
var magic = sha256.Sum256([]byte("accumulate"))

type Encrypted interface {
	DB
	Raw() DB        // Raw returns the underlying database
	IsLocked() bool //
	Lock()          // Lock the database

	// UnlockFor unlocks the encrypter and returns the time at which it will be
	// locked again. If the caller specifies a non-zero deadline, the encrypter
	// must set a deadline no later than that, though the encrypter may choose
	// to use an earlier deadline than the one requested. If the caller does not
	// specify a deadline (passes the zero value), the encrypter may still set a
	// deadline. The encrypter must return the deadline if one is set, or the
	// zero value otherwise.
	UnlockFor([]byte, time.Duration) (time.Time, error)
}

func Magic(raw DB) error {
	v, err := raw.Get(BucketConfig, []byte("magic"))
	if err != nil {
		return err
	}
	if !bytes.Equal(v, magic[:]) {
		return ErrInvalidPassword
	}
	return nil
}

func PutMagic(raw DB) error {
	return raw.Put(BucketConfig, []byte("magic"), magic[:])
}

// FakeEncrypted implements [Encrypted] without actually encrypting anything.
type FakeEncrypted struct {
	DB
}

func (e *FakeEncrypted) Raw() DB        { return e }
func (e *FakeEncrypted) Lock()          {}
func (e *FakeEncrypted) IsLocked() bool { return false }

func (e *FakeEncrypted) UnlockFor(passphrase []byte, duration time.Duration) (dl time.Time, err error) {
	return time.Time{}, nil
}
