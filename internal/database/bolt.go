package db

import (
	"fmt"
	"time"

	"github.com/boltdb/bolt"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

var ErrNotOpen = errors.BadRequest.With("database is not open")
var ErrAlreadyOpen = errors.Conflict.With("database is already open")
var ErrNotFound = errors.NotFound.With("key not found")
var ErrNoBucket = errors.NotFound.With("bucket not defined")
var ErrInvalidPassword = errors.BadPassword.With("invalid password")
var ErrMalformedEncryptedDatabase = errors.InternalError.With("malformed encrypted database")
var ErrDatabaseAlreadyEncrypted = errors.BadRequest.With("database already encrypted")

var BucketConfig = []byte("Config")

type BoltDB struct {
	db       *bolt.DB
	filename string
}

func (b *BoltDB) Name() string {
	return b.filename
}

// Close the database
func (b *BoltDB) Close() error {
	if b.db == nil {
		return ErrNotOpen
	}
	return b.db.Close()
}

func OpenBolt(filename string) (*BoltDB, error) {
	var err error
	b := new(BoltDB)
	b.filename = filename
	b.db, err = bolt.Open(filename, 0600, &bolt.Options{Timeout: time.Millisecond})
	switch {
	case err == nil:
		return b, nil
	case errors.Is(err, bolt.ErrTimeout):
		return nil, ErrAlreadyOpen
	default:
		return nil, err
	}
}

func (b *BoltDB) ForEachBucket(fn func(*Bucket) error) error {
	return b.db.View(func(tx *bolt.Tx) error {
		c := tx.Cursor()
		for key, _ := c.First(); key != nil; key, _ = c.Next() {
			c := tx.Bucket(key).Cursor()
			bucket := new(Bucket)
			bucket.Name = string(key)
			for k, v := c.First(); k != nil; k, v = c.Next() {
				bucket.Put(k, v)
			}
			err := fn(bucket)
			if err != nil {
				return err
			}
		}
		return nil
	})
}

// Get will get an entry in the database given a bucket and key
func (b *BoltDB) Get(bucket []byte, key []byte) (value []byte, err error) {
	if b.db == nil {
		return nil, ErrNotOpen
	}

	err = b.db.View(func(tx *bolt.Tx) error {
		bk := tx.Bucket(bucket)
		if bk == nil {
			return ErrNoBucket
		}
		value = bk.Get(key)
		if value == nil {
			return ErrNotFound
		}
		return nil
	})

	return value, err
}

func (b *BoltDB) Put(bucket []byte, key []byte, value []byte) error {
	if b.db == nil {
		return ErrNotOpen
	}

	return b.db.Update(func(tx *bolt.Tx) error {
		buck, err := tx.CreateBucketIfNotExists(bucket)
		if err != nil {
			return fmt.Errorf("DB: %s", err)
		}
		return buck.Put(key, value)
	})
}

// GetBucket will return the contents of a bucket
func (b *BoltDB) GetBucket(bucket []byte) (buck *Bucket, err error) {
	if b.db == nil {
		return nil, ErrNotOpen
	}

	err = b.db.View(func(tx *bolt.Tx) error {
		bk := tx.Bucket(bucket)
		if bk == nil {
			return ErrNoBucket
		}
		c := bk.Cursor()
		buck = new(Bucket)
		for k, v := c.First(); k != nil; k, v = c.Next() {
			buck.Put(k, v)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}

	return buck.Copy(), nil
}

// Delete will remove a key/value pair from the bucket
func (b *BoltDB) Delete(bucket []byte, key []byte) error {
	if b.db == nil {
		return ErrNotOpen
	}

	return b.db.Update(func(tx *bolt.Tx) error {
		buck := tx.Bucket(bucket)
		if buck == nil {
			return ErrNoBucket
		}
		return buck.Delete(key)
	})
}

// DeleteBucket will delete all key/value pairs from a bucket
func (b *BoltDB) DeleteBucket(bucket []byte) error {
	if b.db == nil {
		return ErrNotOpen
	}

	return b.db.Update(func(tx *bolt.Tx) error {
		if tx.Bucket(bucket) == nil {
			return nil // Bucket does not exist
		}
		return tx.DeleteBucket(bucket)
	})
}
