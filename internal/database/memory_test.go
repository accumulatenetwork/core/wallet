package db

import (
	"testing"
)

func TestMemoryDatabase(t *testing.T) {
	db := MemoryDB{}
	defer db.Close()
	databaseTests(t, &db)
}
