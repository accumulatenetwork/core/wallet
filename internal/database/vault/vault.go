package vault

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"

	"github.com/awnumar/memguard"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/model"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

var ErrNotRegistered = errors.NotFound.With("not a registered vault")
var ErrAlreadyExists = errors.Conflict.With("already exists")

type KeyStore interface {
	// GetKey retrieves the encryption key for the given vault.
	GetKey(*Vault) (*memguard.Enclave, error)
}

type Vault struct {
	wallet *Wallet
	info   *api.VaultInfo
	db     db.DB
}

func (v *Vault) Info() *api.VaultInfo { return v.info }

func (v *Vault) compare(name string) int {
	a := strings.ToLower(v.info.Name)
	b := strings.ToLower(name)
	return strings.Compare(a, b)
}

func (v *Vault) Close() error {
	if v.db == nil {
		return nil
	}
	db := v.db
	v.db = nil
	return db.Close()
}

func (v *Vault) With(keys KeyStore, fn func(*model.Model) error) error {
	m, err := v.Open(keys)
	if err != nil {
		return err
	}

	err = fn(m)
	if err != nil {
		return err
	}

	return m.Commit()
}

func (v *Vault) Status(keys KeyStore) (*api.VaultStatus, error) {
	// The token is ignored for now

	s := new(api.VaultStatus)
	s.VaultInfo = *v.Info()
	s.Open = v.db != nil

	if pw, _ := keys.GetKey(v); pw != nil {
		s.Unlocked = true
	}

	if s.VaultInfo.FilePath == "" {
		return s, nil
	}

	_, err := os.Stat(s.VaultInfo.FilePath)
	switch {
	case err == nil:
		s.Exists = true
	case !errors.Is(err, fs.ErrNotExist):
		return nil, errors.UnknownError.WithFormat("stat %s: %w", s.VaultInfo.FilePath, err)
	}
	return s, nil
}

func (v *Vault) Open(keys KeyStore) (*model.Model, error) {
	// Open the database
	err := v.openDb(keys)
	if err != nil {
		return nil, err
	}

	// Is it encrypted?
	m := model.New(v.db)
	err = m.Config().Magic().Get()
	switch {
	case err == nil:
		return model.New(v.db), nil

	case !errors.Is(err, db.ErrInvalidPassword):
		return nil, err
	}

	// Get the key
	key, err := keys.GetKey(v)
	if err != nil {
		return nil, err
	}

	// Verify
	m, err = m.Decrypt(key)
	if err != nil {
		return nil, err
	}

	return m, nil
}

func (v *Vault) OpenRaw(keys KeyStore) (*model.Model, error) {
	err := v.openDb(keys)
	if err != nil {
		return nil, err
	}
	return model.New(v.db), nil
}

// Create creates the underlying database and returns an unencrypted model.
func (v *Vault) Create(keys KeyStore) (*model.Model, error) {
	// Already opened?
	if v.db != nil {
		return nil, ErrAlreadyExists
	}

	// Load the info
	err := v.loadInfo(keys)
	if err != nil {
		return nil, err
	}

	// Use a memory database?
	if v.wallet.UseMemDB {
		v.db = new(db.MemoryDB)
		return model.New(v.db), nil
	}

	// Must not exist
	_, err = os.Stat(v.info.FilePath)
	switch {
	case err == nil:
		return nil, ErrAlreadyExists
	case !errors.Is(err, fs.ErrNotExist):
		return nil, err
	}

	// Open it
	v.db, err = db.OpenBolt(v.info.FilePath)
	if err != nil {
		return nil, err
	}
	return model.New(v.db), nil
}

// openDb opens the database.
func (v *Vault) openDb(keys KeyStore) error {
	// Already opened?
	if v.db != nil {
		return nil
	}

	// Load the info
	err := v.loadInfo(keys)
	if err != nil {
		return err
	}

	// Use a memory database?
	if v.wallet.UseMemDB {
		v.db = new(db.MemoryDB)
		return nil
	}

	// Database must exist
	_, err = os.Stat(v.info.FilePath)
	if err != nil {
		return err
	}

	// Open it
	v.db, err = db.OpenBolt(v.info.FilePath)
	if err != nil {
		return err
	}
	return nil
}

// loadInfo loads the vault's info from the index.
func (v *Vault) loadInfo(keys KeyStore) error {
	// Check if the vault info has already been loaded
	if v.info.FilePath != "" {
		return nil
	}

	// Open the index vault
	index, err := v.wallet.Index().Open(keys)
	if err != nil {
		return err
	}

	// Look up the vault info
	info, err := index.Multi().Vault(v.info.Name).Get()
	switch {
	case errors.Is(err, errors.NotFound):
		return fmt.Errorf("%s is %w", v.info.Name, ErrNotRegistered)
	case err != nil:
		return err
	}

	// The stored file path is relative, so make it absolute (add the directory)
	v.info = info
	v.info.FilePath = filepath.Join(v.wallet.Directory, v.info.FilePath)
	return nil
}
