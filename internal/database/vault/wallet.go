package vault

import (
	"path/filepath"
	"strings"

	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/sortutil"
)

const Index = "index"

type Wallet struct {
	UseMemDB  bool
	Directory string

	vaults []*Vault
}

func (w *Wallet) Index() *Vault { return w.Vault(Index) }

func (w *Wallet) Vault(name string) *Vault {
	if w == nil {
		return nil
	}
	if name == "" {
		name = Index
	}

	ptr, new := sortutil.BinaryInsert(&w.vaults, func(u *Vault) int {
		return u.compare(name)
	})
	if !new {
		return *ptr
	}

	v := &Vault{
		wallet: w,
		info: &api.VaultInfo{
			Name: strings.ToLower(name),
		},
	}
	if v.info.Name == Index {
		v.info.FilePath = filepath.Join(w.Directory, "index.db")
	}
	*ptr = v
	return v
}

func (w *Wallet) Each(fn func(*Vault) error) error {
	if w == nil {
		return nil
	}

	for _, v := range w.vaults {
		err := fn(v)
		if err != nil {
			return err
		}
	}
	return nil
}
