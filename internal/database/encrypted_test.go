package db

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func TestEncryptedDatabase(t *testing.T) {
	db := &Symmetric{
		DB: new(MemoryDB),
	}
	_, err := db.UnlockFor([]byte("foo"), time.Hour)
	require.NoError(t, err)
	defer db.Close()
	databaseTests(t, db)
}

func TestMagic(t *testing.T) {
	db, err := OpenSymmetric(new(MemoryDB), retriever("foo"))
	require.NoError(t, err)
	_, err = db.UnlockFor([]byte("foo"), time.Second)
	require.NoError(t, err)
}

type retriever []byte

func (s retriever) Retrieve(_ DB, confirm bool) ([]byte, error) {
	return s, nil
}
