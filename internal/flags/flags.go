package flags

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"
	"reflect"
	"strings"
	"time"

	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
)

type Enum[T any] struct {
	V *T
	F func(string) (T, bool)
}

func NewEnum[T any](v *T, f func(string) (T, bool)) *Enum[T] {
	return &Enum[T]{v, f}
}

func (f *Enum[T]) String() string {
	return fmt.Sprint(*f.V)
}

func (f *Enum[T]) Set(s string) error {
	var ok bool
	*f.V, ok = f.F(s)
	if !ok {
		return fmt.Errorf("%q is not a valid %s", s, f.Type())
	}
	return nil
}

func (f *Enum[T]) Type() string {
	return reflect.TypeOf(new(T)).Elem().Name()
}

type Url struct {
	V **url.URL
}

func (f Url) String() string {
	if *f.V == nil {
		return "<nil>"
	}
	return (*f.V).String()
}

func (f Url) Set(s string) error {
	var err error
	*f.V, err = url.Parse(s)
	return err
}

func (f Url) Type() string {
	return "url"
}

type UrlSlice []*url.URL

func (f *UrlSlice) String() string {
	if len(*f) == 0 {
		return "<nil>"
	}
	var s []string
	for _, v := range *f {
		s = append(s, v.String())
	}
	return strings.Join(s, ",")
}

func (f *UrlSlice) Set(s string) error {
	for _, s := range strings.Split(s, ",") {
		u, err := url.Parse(s)
		if err != nil {
			return err
		}
		*f = append(*f, u)
	}
	return nil
}

func (f *UrlSlice) Type() string {
	return "url"
}

type Bytes []byte

func (f Bytes) Type() string   { return "url" }
func (f Bytes) String() string { return fmt.Sprintf("hex:%x", []byte(f)) }

func (f *Bytes) Set(s string) error {
	ss := strings.SplitN(s, ":", 2)
	if len(ss) < 2 {
		*f = []byte(s)
		return nil
	}

	var err error
	switch ss[0] {
	case "hex":
		*f, err = hex.DecodeString(ss[1])
	case "base64":
		*f, err = base64.RawStdEncoding.DecodeString(ss[1])
	default:
		*f = []byte(s)
	}
	return err
}

type Time struct {
	Value *time.Time
}

func (t *Time) Type() string { return "time" }
func (t *Time) String() string {
	if t.Value == nil {
		return "<nil>"
	}
	return t.Value.String()
}

func (t *Time) Set(s string) error {
	v, err := time.Parse(time.RFC3339, s)
	if err == nil {
		t.Value = &v
		return nil
	}

	d, e := time.ParseDuration(s)
	if e == nil {
		v := time.Now().Add(d)
		t.Value = &v
		return nil
	}

	return err // return the time.Parse error
}
