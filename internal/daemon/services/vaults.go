package services

import (
	"context"
	"crypto/rand"
	"crypto/sha256"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/awnumar/memguard"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/model"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

func (m *VaultService) Status(ctx context.Context, req *api.StatusRequest) (*api.StatusResponse, error) {
	w := m.Vaults.Wallet(req.Wallet)

	// Get the index vault's status
	s, err := w.Index().Status(req.Token)
	if err != nil {
		return nil, err
	}

	res := new(api.StatusResponse)
	res.Wallet = new(api.WalletStatus)
	res.Wallet.VaultStatus = *s

	// If the index vault is unlocked, check if the wallet is multi-vault
	if s.Unlocked {
		index, err := w.Index().Open(req.Token)
		if err != nil {
			return nil, err
		}
		res.Wallet.Multi, err = index.Multi().Enabled().Get()
		if err != nil {
			return nil, err
		}
	}

	// Get the status of each other vault
	err = w.Each(func(v Vault) error {
		// Skip the index vault and any vaults that aren't loaded
		if v == w.Index() || v.Info().FilePath == "" {
			return nil
		}

		s, err := w.Index().Status(req.Token)
		if err != nil {
			return err
		}
		res.Vaults = append(res.Vaults, s)
		return nil
	})
	if err != nil {
		return nil, err
	}

	return res, nil
}

func (m *VaultService) CreateWallet(ctx context.Context, req *api.CreateWalletRequest) (*api.CreateWalletResponse, error) {
	defaultTTL(m.MaxTTL, &req.TTL)

	if !m.Vaults.UseMemDB() {
		// Check the path
		st, err := os.Stat(req.Path)
		switch {
		case errors.Is(err, fs.ErrNotExist):
			// Ok
		case err != nil:
			return nil, err
		case !st.IsDir():
			return nil, fmt.Errorf("%q is a file", req.Path)
		}

		// Make sure it exists
		err = os.MkdirAll(req.Path, 0700)
		if err != nil {
			return nil, fmt.Errorf("create %q: %w", req.Path, err)
		}
	}

	// Create the database
	passphrase := memguard.NewEnclave([]byte(req.Passphrase))
	deadline, err := m.createDb(m.Vaults.Wallet(req.Path).Index(), req.Token, req.TTL, passphrase)
	if err != nil {
		return nil, err
	}

	// We're done if this is a single-vault wallet
	if !req.MultiVault {
		return &api.CreateWalletResponse{
			UnlockedUntil: deadline,
		}, nil
	}

	// Create a salt for filenames
	var salt [32]byte
	_, err = rand.Read(salt[:])
	if err != nil {
		return nil, err
	}

	err = m.Vaults.Wallet(req.Path).Index().With(req.Token, func(db *model.Model) error {
		// Record that the wallet is multi-vault
		err = db.Multi().Enabled().Put(true)
		if err != nil {
			return err
		}

		// Record the filename salt
		return db.Multi().Salt().Put(salt)
	})
	if err != nil {
		return nil, err
	}

	return &api.CreateWalletResponse{
		UnlockedUntil: deadline,
	}, nil
}

func (m *VaultService) CreateVault(ctx context.Context, req *api.CreateVaultRequest) (_ *api.CreateVaultResponse, err error) {
	defaultTTL(m.MaxTTL, &req.TTL)

	// Validate request
	if strings.EqualFold(req.Vault, "index") {
		return nil, errors.BadRequest.WithFormat("%s is a reserved name", req.Vault)
	}

	// Record the new vault
	vault, err := createVault(m.Vaults.Wallet(req.Wallet), req.Token, req.Vault, req.Passphrase != "")
	if err != nil {
		return nil, err
	}

	// Setup the database
	passphrase := memguard.NewEnclave([]byte(req.Passphrase))
	deadline, err := m.createDb(vault, req.Token, req.TTL, passphrase)
	if err != nil {
		return nil, err
	}

	return &api.CreateVaultResponse{
		UnlockedUntil: deadline,
	}, nil
}

func (m *VaultService) ConvertWallet(ctx context.Context, req *api.ConvertWalletRequest) (*api.ConvertWalletResponse, error) {
	if m.Vaults.UseMemDB() {
		return nil, errors.NotSupported
	}

	// Make sure the wallet isn't already multi-vault
	v := m.Vaults.Wallet(req.Wallet).Index()
	db, err := v.Open(req.Token)
	if err != nil {
		return nil, err
	}

	ok, err := db.Multi().Enabled().Get()
	if err != nil {
		return nil, err
	}
	if ok {
		return nil, errors.BadRequest.WithFormat("wallet is already multi-vault")
	}

	// Close the index
	err = v.Close()
	if err != nil {
		return nil, err
	}

	// Move it
	temp := filepath.Join(req.Wallet, req.Vault+".db")
	err = os.Rename(v.Info().FilePath, temp)
	if err != nil {
		return nil, err
	}

	// Create a new multi-vault wallet
	_, err = m.CreateWallet(ctx, &api.CreateWalletRequest{
		Path:       req.Wallet,
		Token:      req.Token,
		MultiVault: true,
		Passphrase: req.Passphrase,
	})
	if err != nil {
		return nil, err
	}

	// Commit on success
	index, err := m.Vaults.Wallet(req.Wallet).Index().Open(req.Token)
	if err != nil {
		return nil, err
	}
	defer commitOnSuccess(&err, index)

	// Rename the old vault
	name := strings.ToLower(req.Vault)
	file, err := generateVaultFileName(index, name, req.Passphrase != "")
	if err != nil {
		return nil, err
	}
	dbPath := filepath.Join(req.Wallet, file)
	err = os.Rename(temp, dbPath)
	if err != nil {
		return nil, err
	}

	// Record it
	err = index.Multi().Names().Add(name)
	if err != nil {
		return nil, err
	}
	err = index.Multi().Vault(name).Put(&api.VaultInfo{
		Name:     req.Vault,
		FilePath: file,
	})
	if err != nil {
		return nil, err
	}

	return &api.ConvertWalletResponse{}, nil
}

func (m *VaultService) AdoptVault(ctx context.Context, req *api.AdoptVaultRequest) (_ *api.AdoptVaultResponse, err error) {
	if m.Vaults.UseMemDB() {
		return nil, errors.NotSupported
	}

	// Verify the old vault exists
	_, err = os.Stat(req.OldVaultPath)
	if err != nil {
		return nil, err
	}
	oldRaw, err := db.OpenBolt(req.OldVaultPath)
	if err != nil {
		return nil, err
	}

	// Check for a 1password reference
	var onePass string
	ref, err := oldRaw.Get(db.BucketConfig, []byte("1password.ref"))
	switch {
	case err == nil:
		onePass = string(ref)
		err = oldRaw.Delete(db.BucketConfig, []byte("1password.ref"))
	case errors.Is(err, errors.NotFound):
		// Ok
		err = nil
	}
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	// Check if it is encrypted
	var encrypted bool
	err = db.Magic(oldRaw)
	switch {
	case err == nil:
		// Unencrypted
	case errors.Is(err, db.ErrInvalidPassword):
		encrypted = true
	default:
		return nil, err
	}
	err = oldRaw.Close()
	if err != nil {
		return nil, err
	}

	// Index must be open
	index, err := m.Vaults.Wallet(req.Wallet).Index().Open(req.Token)
	if err != nil {
		return nil, err
	}
	defer commitOnSuccess(&err, index)

	// Verify the wallet supports multiple vaults
	ok, err := index.Multi().Enabled().Get()
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, errors.NotSupported.WithFormat("wallet is not configured for multiple vaults")
	}

	// Verify the vault doesn't exist
	name := strings.ToLower(req.NewVaultName)
	info, err := index.Multi().Vault(name).Get()
	switch {
	case err == nil:
		// If the database is the same, overwrite it
		if info.FilePath != filepath.Dir(req.OldVaultPath) {
			return nil, errors.Conflict.WithFormat("%s has already been registered", req.NewVaultName)
		}
	case !errors.Is(err, errors.NotFound):
		return nil, err
	}

	// Calculate the new file name
	file, err := generateVaultFileName(index, name, encrypted)
	if err != nil {
		return nil, err
	}

	// Move it
	err = os.Rename(req.OldVaultPath, filepath.Join(req.Wallet, file))
	if err != nil {
		return nil, err
	}

	// Record it
	err = index.Multi().Names().Add(name)
	if err != nil {
		return nil, err
	}
	err = index.Multi().Vault(name).Put(&api.VaultInfo{
		Name:           req.NewVaultName,
		FilePath:       file,
		OnePasswordRef: onePass,
	})
	if err != nil {
		return nil, err
	}

	return &api.AdoptVaultResponse{}, nil
}

func createVault(wallet Wallet, token []byte, name string, encrypted bool) (_ Vault, err error) {
	// Index must be open
	index, err := wallet.Index().Open(token)
	if err != nil {
		return nil, err
	}
	defer commitOnSuccess(&err, index)

	// Verify the wallet supports multiple vaults
	ok, err := index.Multi().Enabled().Get()
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, errors.NotSupported.WithFormat("wallet is not configured for multiple vaults")
	}

	// Verify the vault hasn't been created
	ogname, name := name, strings.ToLower(name)
	_, err = index.Multi().Names().Index(name)
	switch {
	case err == nil:
		return nil, errors.Conflict.WithFormat("%s has already been registered", ogname)
	case !errors.Is(err, errors.NotFound):
		return nil, err
	}

	// Generate the filename
	file, err := generateVaultFileName(index, name, encrypted)
	if err != nil {
		return nil, err
	}

	// Record it
	err = index.Multi().Names().Add(name)
	if err != nil {
		return nil, err
	}
	err = index.Multi().Vault(name).Put(&api.VaultInfo{
		Name:     ogname,
		FilePath: file,
	})
	if err != nil {
		return nil, err
	}

	return wallet.Vault(name), nil
}

func generateVaultFileName(index *model.Model, name string, encrypted bool) (string, error) {
	salt, err := index.Multi().Salt().Get()
	if err != nil {
		return "", err
	}

	// H(salt | hash) for encrypted, H(hash | salt) for unencrypted
	hash := sha256.Sum256([]byte(name))
	if encrypted {
		hash = sha256.Sum256(append(salt[:], hash[:]...))
	} else {
		hash = sha256.Sum256(append(hash[:], salt[:]...))
	}

	return fmt.Sprintf("%x.db", hash[:]), nil
}

func (m *VaultService) OpenVault(ctx context.Context, req *api.OpenVaultRequest) (*api.OpenVaultResponse, error) {
	_, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).OpenRaw(req.Token)
	if err != nil {
		return nil, err
	}
	return &api.OpenVaultResponse{}, nil
}

func (m *VaultService) CopyVaultSanitized(ctx context.Context, req *api.CopyVaultSanitizedRequest) (*api.CopyVaultSanitizedResponse, error) {
	_, err := os.Stat(req.Destination)
	switch {
	case errors.Is(err, fs.ErrNotExist):
		// Ok
	case err != nil:
		return nil, err
	}

	// Open the destination
	dst, err := db.OpenBolt(req.Destination)
	if err != nil {
		return nil, err
	}
	defer dst.Close()

	// Open the source (without migrating)
	src, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).(*VaultWithKeys).open(req.Token)
	if err != nil {
		return nil, err
	}

	// Verify the vault is new/empty, and copy the veri
	err = model.With(dst, func(m *model.Model) error {
		_, err = m.Config().Version().Get()
		switch {
		case err == nil:
			return errors.Conflict.WithFormat("%s already exists", req.Destination)
		case errors.Is(err, errors.NotFound):
			return nil
		default:
			return err
		}
	})
	if err != nil {
		return nil, err
	}

	// Copy
	err = SanitizedCopy(dst, src.Unwrap())
	if err != nil {
		return nil, err
	}

	return &api.CopyVaultSanitizedResponse{}, nil
}

func (m *VaultService) ListVaults(ctx context.Context, req *api.ListVaultsRequest) (*api.ListVaultsResponse, error) {
	// Verify the database is open
	if _, err := m.Vaults.Wallet(req.Wallet).Index().Open(req.Token); err != nil {
		return nil, err
	}

	db, err := m.Vaults.Wallet(req.Wallet).Index().Open(req.Token)
	if err != nil {
		return nil, err
	}
	ok, err := db.Multi().Enabled().Get()
	if err != nil {
		return nil, err
	}
	if !ok {
		return nil, errors.BadRequest.With("not a multi-vault wallet")
	}

	vaults, err := db.Multi().Names().Get()
	if err != nil {
		return nil, err
	}

	return &api.ListVaultsResponse{
		Vaults: vaults,
	}, nil
}

func (m *VaultService) ImportVault(ctx context.Context, req *api.ImportVaultRequest) (*api.ImportVaultResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.NewVaultName).Open(req.Token)
	if err != nil {
		return nil, err
	}
	s, err := importAccounts(wallet.Unwrap(), req.Vault)
	fmt.Println(s)
	return new(api.ImportVaultResponse), err
}

func (m *VaultService) EncryptVault(ctx context.Context, req *api.EncryptVaultRequest) (*api.EncryptVaultResponse, error) {
	return nil, errors.NotSupported
	// oldDb, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	// if err != nil {
	// 	return nil, err
	// }

	// newDb, newPath, err := encryptDatabase(oldDb, req.Vault, req.Password, m.Vaults.UseMemDB())
	// if err != nil {
	// 	return nil, err
	// }

	// req.Vault.FilePath = newPath
	// name := req.Vault.NameOrPath()

	// // Update the vault info
	// w := m.Vaults.GetWallet(req.Wallet)
	// if req.Vault.Name != "" {
	// 	err = putAs(w.Index(), BucketVaults, []byte(name), req.Vault)
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// }

	// // Remember the new DB
	// w.Put(newDb)

	// // Close the old database
	// oldDb.Lock()
	// err = oldDb.Close()
	// if err != nil {
	// 	m.Logger.Error("Failed to close a database", "error", err, "name", name)
	// }

	// return &api.EncryptVaultResponse{}, nil
}

func (m *VaultService) Set1PasswordRef(ctx context.Context, req *api.Set1PasswordRefRequest) (*api.Set1PasswordRefResponse, error) {
	index, err := m.Vaults.Wallet(req.Wallet).Index().Open(req.Token)
	if err != nil {
		return nil, err
	}
	defer commitOnSuccess(&err, index)

	name := strings.ToLower(req.Vault)
	info, err := index.Multi().Vault(name).Get()
	if err != nil {
		return nil, err
	}

	info.OnePasswordRef = req.Value
	err = index.Multi().Vault(name).Put(info)
	if err != nil {
		return nil, err
	}

	return &api.Set1PasswordRefResponse{}, nil
}

func defaultTTL(max time.Duration, value *time.Duration) {
	if *value == 0 {
		*value = 10 * time.Minute
	}
	if max != 0 && *value > max {
		*value = max
	}
}
