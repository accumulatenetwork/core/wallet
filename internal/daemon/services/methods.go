package services

import (
	"context"
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/AccumulateNetwork/jsonrpc2/v15"
	"github.com/awnumar/memguard"
	"github.com/cosmos/go-bip39"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/model"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"gitlab.com/accumulatenetwork/core/wallet/pkg"
	"gitlab.com/accumulatenetwork/core/wallet/pkg/interactive"
	"golang.org/x/exp/slog"
)

func (*GeneralService) Version(_ context.Context) (*api.VersionResponse, error) {
	resp := api.VersionResponse{
		WalletVersion: pkg.Version,
		WalletCommit:  pkg.Commit,
	}
	resp.DatabaseVersion = model.Version.String()
	return &resp, nil
}

func (g *GeneralService) RefreshToken(_ context.Context, req *api.RefreshTokenRequest) (*api.RefreshTokenResponse, error) {
	defaultTTL(g.MaxTTL, &req.TTL)

	// Generate a random token
	newToken := make([]byte, 16)
	_, err := rand.Read(newToken)
	if err != nil {
		return nil, err
	}
	newToken = []byte(hex.EncodeToString(newToken))

	// Refresh
	deadline, err := g.Keys.RefreshToken(req.Token, newToken, req.TTL)
	if err != nil {
		return nil, err
	}

	return &api.RefreshTokenResponse{
		Token:         newToken,
		UnlockedUntil: deadline,
	}, nil
}

func (*GeneralService) GenerateMnemonic(_ context.Context, req *api.GenerateMnemonicRequest) (*api.GenerateMnemonicResponse, error) {
	entropy, err := bip39.NewEntropy(int(req.Entropy))
	if err != nil {
		return nil, err
	}
	mnemonic, err := bip39.NewMnemonic(entropy)
	if err != nil {
		return nil, err
	}
	return &api.GenerateMnemonicResponse{
		Mnemonic: strings.Split(mnemonic, " "),
	}, nil
}

func (*GeneralService) Decode(_ context.Context, req *api.DecodeRequest) (*api.DecodeResponse, error) {
	//decode as envelope
	env := messaging.Envelope{}
	err := env.UnmarshalBinary(req.DataBinary)
	if err == nil {
		resp := api.DecodeResponse{}
		data, err := env.MarshalJSON()
		if err != nil {
			return nil, err
		}
		resp.DataJson = data
		return &resp, nil
	}

	//unmarshal as account
	ac, err := protocol.UnmarshalAccount(req.DataBinary)
	if err == nil {
		resp := api.DecodeResponse{}
		data, err := json.Marshal(&ac)
		if err != nil {
			return nil, err
		}
		resp.DataJson = data
		return &resp, nil
	}

	//unmarshal as a transaction
	tx := protocol.Transaction{}
	err = tx.UnmarshalBinary(req.DataBinary)
	if err == nil {
		resp := api.DecodeResponse{}
		data, err := tx.MarshalJSON()
		if err != nil {
			return nil, err
		}
		resp.DataJson = data
		return &resp, nil
	}

	//unmarshal as a transaction body
	tb, err := protocol.UnmarshalTransactionBody(req.DataBinary)
	if err == nil {
		resp := api.DecodeResponse{}
		data, err := json.Marshal(&tb)
		if err != nil {
			return nil, err
		}
		resp.DataJson = data
		return &resp, nil
	}

	//unmarshal as a transaction body
	th := protocol.TransactionHeader{}
	err = th.UnmarshalBinary(req.DataBinary)
	if err == nil {
		resp := api.DecodeResponse{}
		data, err := json.Marshal(&th)
		if err != nil {
			return nil, err
		}
		resp.DataJson = data
		return &resp, nil
	}

	return nil, fmt.Errorf("cannot decode binary")
}

func (*GeneralService) Encode(_ context.Context, req *api.EncodeRequest) (*api.EncodeResponse, error) {
	//process as envelope
	env := messaging.Envelope{}
	err := env.UnmarshalJSON(req.DataJson)
	if err == nil {
		resp := api.EncodeResponse{}
		resp.DataBinary, err = env.MarshalBinary()
		if err != nil {
			return nil, err
		}
		return &resp, nil
	}

	//process as account
	ac, err := protocol.UnmarshalAccountJSON(req.DataJson)
	if err == nil {
		resp := api.EncodeResponse{}
		resp.DataBinary, err = ac.MarshalBinary()
		if err != nil {
			return nil, err
		}
		return &resp, nil
	}

	//process as full transaction
	tx := protocol.Transaction{}
	err = tx.UnmarshalJSON(req.DataJson)
	if err == nil {
		resp := api.EncodeResponse{}
		resp.DataBinary, err = tx.MarshalBinary()
		if err != nil {
			return nil, err
		}
		resp.TransactionHash = tx.GetHash()
		return &resp, nil
	}

	//process as transaction header only
	th := protocol.TransactionHeader{}
	err = th.UnmarshalJSON(req.DataJson)
	if err == nil {
		resp := api.EncodeResponse{}
		resp.DataBinary, err = th.MarshalBinary()
		if err != nil {
			return nil, err
		}
		return &resp, nil
	}

	//process as transaction body
	tb, err := protocol.UnmarshalTransactionBodyJSON(req.DataJson)
	if err == nil {
		resp := api.EncodeResponse{}
		resp.DataBinary, err = tb.MarshalBinary()
		if err != nil {
			return nil, err
		}
		return &resp, nil
	}

	return nil, fmt.Errorf("malformed encoding request")
}

func (m *TransactionService) CreateEnvelope(_ context.Context, req *api.CreateEnvelopeRequest) (*api.CreateEnvelopeResponse, error) {
	return nil, jsonrpc2.NewError(jsonrpc2.ErrorCodeInternal, jsonrpc2.ErrorMessageInternal,
		fmt.Errorf("create envelope not yet implelemnted"))
}

func (m *TransactionService) CreateTransaction(_ context.Context, req *api.CreateTransactionRequest) (*api.TransactionResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	txn, err := loadTransactionFromCache(wallet.Unwrap(), req.Name)
	if err == nil {
		if !req.GetOrCreate {
			return nil, fmt.Errorf("transaction already available with the name %s", req.Name)
		}
		resp := api.TransactionResponse{}
		resp.Name = req.Name
		resp.Transaction = txn
		return &resp, nil
	}

	if !errors.Is(err, errors.NotFound) {
		return nil, err
	}
	txn = new(protocol.Transaction)
	txn.Header.Principal = req.Principal.JoinPath(req.Path...)
	txn.Header.Memo = req.Memo
	txn.Header.Metadata = req.Metadata

	//cache the transaction, fail if it exists
	err = storeTransactionInCache(wallet.Unwrap(), req.Name, txn)
	if err != nil {
		return nil, err
	}

	resp := api.TransactionResponse{}
	resp.Name = req.Name
	resp.Transaction = txn
	return &resp, nil
}

func loadTransactionFromCache(wallet db.DB, name string) (*protocol.Transaction, error) {
	var ret protocol.Transaction
	value, _ := wallet.Get(BucketTransactionCache, []byte(name))
	if value == nil {
		return nil, errors.NotFound.WithFormat("transaction %s does not exist, first call create-transaction", name)
	}

	err := ret.UnmarshalBinary(value)
	if err != nil {
		return nil, err
	}
	return &ret, err
}

func loadEnvelopeFromCache(wallet db.DB, name string) (*messaging.Envelope, error) {
	var ret messaging.Envelope
	value, _ := wallet.Get(BucketTransactionCache, []byte(name))
	if value == nil {
		return nil, errors.NotFound.WithFormat("transaction %s does not exist, first call create-transaction", name)
	}

	err := ret.UnmarshalBinary(value)
	if err != nil {
		return nil, err
	}
	return &ret, err
}

// reserve for future use
var _ = loadEnvelopeFromCache

// bodyPtr provides some generic shenanigans that enforces the type of any passed into the function and allows for proper casting
type bodyPtr[T any] interface {
	*T
	protocol.TransactionBody
}

// resolveTransactionAs will attempt to cast the transaction body to a specific type, if body is nil it will return an empty typed object
func resolveTransactionToBodyAs[T any, PT bodyPtr[T]](body *protocol.TransactionBody) (*T, error) {
	var ret *T
	if *body == nil {
		ret = new(T)
		*body = PT(ret)
		return ret, nil
	}
	switch bod := (*body).(type) {
	case PT:
		ret = (*T)(bod)
	default:
		return nil, fmt.Errorf("type for trasaction is %s", (*body).Type().String())
	}
	return ret, nil
}

func loadTransactionFromCacheAs[T any, PT bodyPtr[T]](wallet db.DB, name string) (*protocol.Transaction, *T, error) {
	t, err := loadTransactionFromCache(wallet, name)
	if err != nil {
		return nil, nil, err
	}

	ret, err := resolveTransactionToBodyAs[T, PT](&t.Body)
	if err != nil {
		return nil, nil, err
	}

	return t, ret, err
}

func storeTransactionInCache(wallet db.DB, name string, txn *protocol.Transaction) error {
	data, err := txn.MarshalBinary()
	if err != nil {
		return err
	}
	err = wallet.Put(BucketTransactionCache, []byte(name), data)
	if err != nil {
		return err
	}
	return nil
}

func (m *TransactionService) WriteDataTransaction(_ context.Context, req *api.WriteDataRequest) (*api.TransactionResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	tx, wd, err := loadTransactionFromCacheAs[protocol.WriteData](wallet.Unwrap(), req.Name)
	if err != nil {
		return nil, err
	}

	if !req.Overwrite && wd != nil {
		return nil, fmt.Errorf("data is already set for transaction %s", req.Name)
	}

	tx.Body = req.WriteData

	err = storeTransactionInCache(wallet.Unwrap(), req.Name, tx)
	if err != nil {
		return nil, err
	}

	//send the response which is a mirror of the transaction thus far
	resp := api.TransactionResponse{}
	resp.Name = req.Name
	resp.Transaction = tx
	return &resp, nil
}

func (m *KeyService) KeyList(_ context.Context, req *api.KeyListRequest) (*api.KeyListResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	resp := api.KeyListResponse{}
	k, err := listKeys(wallet.Unwrap(), false)
	if err != nil {
		return nil, err
	}
	for _, v := range k {
		resp.KeyList = append(resp.KeyList, v.Key)
	}
	return &resp, nil
}

func (m *KeyService) KeyAssign(_ context.Context, req *api.KeyRotateRequest) (*api.KeyRotateResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}
	resp := api.KeyRotateResponse{}
	err = assignKeyLabel(wallet.Unwrap(), req.OldKeyLabel, req.NewKeyLabel, req.Force)
	if err != nil {
		return nil, err
	}
	return &resp, nil
}

func (m *KeyService) KeyRename(_ context.Context, req *api.KeyRotateRequest) (*api.KeyRotateResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}
	resp := api.KeyRotateResponse{}
	err = renameKeyLabel(wallet.Unwrap(), req.OldKeyLabel, req.NewKeyLabel, req.Force)
	if err != nil {
		return nil, err
	}
	return &resp, nil
}

func (m *KeyService) KeyRemove(_ context.Context, req *api.KeyRotateRequest) (*api.KeyRotateResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}
	resp := api.KeyRotateResponse{}
	err = removeKeyLabel(wallet.Unwrap(), req.OldKeyLabel)
	if err != nil {
		return nil, err
	}
	return &resp, nil
}

func (s *KeyService) ResolveKey(_ context.Context, req *api.ResolveKeyRequest) (*api.ResolveKeyResponse, error) {
	// Ensure the database is open
	wallet, err := s.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	addr, err := resolveAddress(wallet.Unwrap(), req.Value, true, true)
	if err != nil {
		return nil, err
	}

	if key, ok := addr.(*Key); ok {
		if !req.IncludePrivateKey {
			key.PrivateKey = nil
		}
		return &api.ResolveKeyResponse{
			Address:       addr.String(),
			PublicKeyHash: key.PublicKeyHash(),
			Key:           &key.Key,
		}, nil
	}

	resp := new(api.ResolveKeyResponse)
	resp.Address = addr.String()
	resp.PublicKeyHash, _ = addr.GetPublicKeyHash()
	resp.Key = new(api.Key)
	resp.Key.PublicKey, _ = addr.GetPublicKey()
	resp.Key.KeyInfo.Type = addr.GetType()
	if req.IncludePrivateKey {
		resp.Key.PrivateKey, _ = addr.GetPrivateKey()
	}
	resp.Key.Labels = new(api.KeyLabels)
	return resp, nil
}

func (m *TransactionService) AddSendTokensOutput(_ context.Context, req *api.AddSendTokensOutputRequest) (*api.TransactionResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	//make sure request looks ok
	if req.Recipient == nil {
		return nil, fmt.Errorf("\"recipient\" field was not specified")
	}

	txn, st, err := loadTransactionFromCacheAs[protocol.SendTokens](wallet.Unwrap(), req.Name)

	if err != nil {
		return nil, err
	}

	//TODO: determine if the transaction will be too large, if so, then we want make a new transaction
	// and store it in the cache as an envelope, or we can just return an error making the user create a new transaction
	// However, given that send token transactions usually want to be grouped together, it may make sense to go the
	// envelope route.

	st.To = append(st.To, req.Recipient)

	_, err = txn.MarshalBinary()
	if err != nil {
		return nil, err
	}
	err = storeTransactionInCache(wallet.Unwrap(), req.Name, txn)
	if err != nil {
		return nil, err
	}

	resp := api.TransactionResponse{}
	resp.Name = req.Name
	resp.Transaction = txn
	return &resp, nil
}

func (m *TransactionService) DeleteTransaction(_ context.Context, req *api.DeleteTransactionRequest) (*api.TransactionResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	value, err := wallet.Unwrap().Get(BucketTransactionCache, []byte(req.Name))
	if err != nil {
		return nil, err
	}
	resp := protocol.Transaction{}
	err = resp.UnmarshalBinary(value)
	if err != nil {
		return nil, err
	}
	err = wallet.Unwrap().Delete(BucketTransactionCache, []byte(req.Name))
	if err != nil {
		return nil, err
	}
	return &api.TransactionResponse{
		Name:        req.Name,
		Transaction: &resp,
	}, nil
}

func (s *KeyService) GenerateAddress(_ context.Context, req *api.GenerateAddressRequest) (*api.GenerateAddressResponse, error) {
	wallet, err := s.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	// Default to ED25519
	if req.Type == protocol.SignatureTypeUnknown {
		req.Type = protocol.SignatureTypeED25519
	}

	key, err := generateKey(wallet.Unwrap(), req.Labels, req.Type, false)
	if err != nil {
		return nil, err
	}
	key.PrivateKey = nil
	resp := api.GenerateAddressResponse{}
	resp.Key = key.Key

	return &resp, nil
}

func (m *TransactionService) AddMemoToTransaction(_ context.Context, req *api.AddMemoToTransactionRequest) (*api.TransactionResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	txn, err := loadTransactionFromCache(wallet.Unwrap(), req.Name)
	if err != nil {
		return nil, err
	}

	txn.Header.Memo = req.Memo

	err = storeTransactionInCache(wallet.Unwrap(), req.Name, txn)
	if err != nil {
		return nil, err
	}
	resp := api.TransactionResponse{}
	resp.Transaction = txn
	resp.Name = req.Name
	return &resp, nil
}

func (m *TransactionService) GetTransaction(_ context.Context, req *api.GetTransactionRequest) (*api.TransactionResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	txn, err := loadTransactionFromCache(wallet.Unwrap(), req.Name)
	if err != nil {
		return nil, err
	}

	resp := api.TransactionResponse{}
	resp.Name = req.Name
	resp.Transaction = txn

	return &resp, nil

	//TODO: once a transaction is composed, we should lock it and not make it usable for anything other than signing.
}

func (m *VaultService) ExportVault(_ context.Context, req *api.ExportVaultRequest) (*api.ExportVaultResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	res, err := exportAccounts(wallet.Unwrap())
	if err != nil {
		return nil, err
	}
	resp := api.ExportVaultResponse{
		Vault: res,
	}
	return &resp, nil
}

func (m *TransactionService) ListTransactions(_ context.Context, req *api.ListTransactionsRequest) (*api.ListTransactionsResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	bucket, err := wallet.Unwrap().GetBucket(BucketTransactionCache)
	if err != nil {
		return nil, err
	}
	resp := api.ListTransactionsResponse{}
	for _, value := range bucket.KeyValueList {
		resp.Names = append(resp.Names, string(value.Key))
	}
	return &resp, nil
}

func (m *VaultService) UnlockVault(_ context.Context, req *api.UnlockVaultRequest) (*api.UnlockVaultResponse, error) {
	defaultTTL(m.MaxTTL, &req.TTL)

	if req.Use1Password {
		if req.Vault == "" {
			return nil, errors.NotSupported.WithFormat("1Password integration is not supported for single-vault wallets")
		} else if req.Vault == "index" {
			return nil, errors.NotSupported.WithFormat("1Password integration is not supported for the index vault")
		}

		index, err := m.Vaults.Wallet(req.Wallet).Index().Open(req.Token)
		if err != nil {
			return nil, err
		}

		info, err := index.Multi().Vault(req.Vault).Get()
		if err != nil {
			return nil, err
		}

		if info.OnePasswordRef == "" {
			return nil, errors.BadRequest.WithFormat("1Password integration not set up for vault %s", req.Vault)
		}

		pass, err := interactive.GetOnePassword(info.OnePasswordRef)
		if err != nil {
			return nil, err
		}
		if pass == nil {
			return &api.UnlockVaultResponse{Success: false}, nil
		}
		req.Passphrase = string(pass)

	} else if req.Passphrase == "" {
		return nil, errors.BadRequest.WithFormat("missing password")
	}

	// Store the passphrase
	v := m.Vaults.Wallet(req.Wallet).Vault(req.Vault)
	deadline, err := m.Keys.PutPassphrase(v, req.Token, req.TTL, memguard.NewEnclave([]byte(req.Passphrase)))
	if err != nil {
		return nil, err
	}

	return &api.UnlockVaultResponse{
		Success:       true,
		UnlockedUntil: deadline,
	}, nil
}

func (m *VaultService) LockVault(_ context.Context, req *api.LockVaultRequest) (*api.LockVaultResponse, error) {
	lock := func(v Vault) {
		if v == nil {
			return
		}

		// Delete the passphrase
		err := m.Keys.DeleteKey(v, req.Token)
		if err != nil {
			slog.Error("Failed to lock a vault", "error", err, "name", v.Info().Name)
		}

		if !req.Close {
			return
		}

		// Unload the vaults if requested
		err = v.Close()
		if err != nil {
			slog.Error("Failed to close a vault", "error", err, "name", v.Info().Name)
		}
	}

	// Lock all wallets?
	if req.Wallet == "" {
		_ = m.Vaults.Each(func(w Wallet) error {
			return w.Each(func(v Vault) error { lock(v); return nil })
		})
		return new(api.LockVaultResponse), nil
	}

	// Lock all vaults?
	if req.Vault == "" {
		_ = m.Vaults.Wallet(req.Wallet).Each(func(v Vault) error { lock(v); return nil })
		return new(api.LockVaultResponse), nil
	}

	// Lock one vault
	lock(m.Vaults.Wallet(req.Wallet).Vault(req.Vault))
	return new(api.LockVaultResponse), nil
}

func (m *VaultService) ImportMnemonic(_ context.Context, req *api.ImportMnemonicRequest) (*api.ImportMnemonicResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	err = importMnemonic(wallet.Unwrap(), req.Mnemonic)
	if err != nil {
		return nil, err
	}

	return &api.ImportMnemonicResponse{}, nil
}
