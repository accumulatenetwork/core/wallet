package services

import (
	"encoding/binary"

	"gitlab.com/accumulatenetwork/accumulate/pkg/types/encoding"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/model"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

func commitOnSuccess(err *error, db *model.Model) {
	if r := recover(); r != nil {
		panic(r) // Do not commit on panic
	}
	if *err == nil {
		*err = db.Commit()
	}
}

func zero[T any]() (z T) { return z }

func load[T any](lastErr *error, get func() (T, error)) T {
	if *lastErr != nil {
		return zero[T]()
	}

	v, err := get()
	// if allowMissing && errors.Is(err, errors.NotFound) {
	// 	return zero[T]()
	// }
	if err != nil {
		*lastErr = err
		return zero[T]()
	}

	return v
}

func store[T any](lastErr *error, put func(T) error, v T) {
	if *lastErr == nil {
		*lastErr = put(v)
	}
}

type binPtr[T any] interface {
	*T
	encoding.BinaryValue
}

func getAs[V any, PV binPtr[V]](db db.DB, bucket, key []byte) (PV, error) {
	b, err := db.Get(bucket, key)
	if err != nil {
		return nil, err
	}

	v := PV(new(V))
	err = v.UnmarshalBinary(b)
	if err != nil {
		return nil, errors.EncodingError.Wrap(err)
	}

	return v, nil
}

var ErrNotEnoughData = errors.BadRequest.With("not enough data")
var ErrOverflow = errors.BadRequest.With("overflow")

func uvarintUnmarshalBinary(b []byte) (uint64, error) {
	v, n := binary.Uvarint(b)
	if n == 0 {
		return 0, ErrNotEnoughData
	}
	if n < 0 {
		return 0, ErrOverflow
	}
	return v, nil
}
