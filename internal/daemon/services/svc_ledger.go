package services

import (
	"context"
	"errors"
	"fmt"
	"strconv"

	"gitlab.com/accumulatenetwork/accumulate/pkg/types/address"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/bip44"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
)

type LedgerManager interface {
	Wallets() ([]*api.LedgerWalletInfo, error)
	Derive(id *url.URL, path bip44.Derivation, pin, display bool, labels ...string) (address.Address, error)
	Sign(id *url.URL, path bip44.Derivation, txn *protocol.Transaction, sig protocol.Signature) (protocol.Signature, error)
}

type LedgerService struct {
	Vaults WalletManager
	Ledger LedgerManager
}

func (s *LedgerService) LedgerQueryWallets(_ context.Context) (*api.LedgerWalletResponse, error) {
	if s.Ledger == nil {
		return nil, fmt.Errorf("Ledger support not available")
	}

	var err error
	resp := &api.LedgerWalletResponse{}
	resp.LedgerWalletsInfo, err = s.Ledger.Wallets()
	if err != nil {
		return nil, err
	}
	return resp, nil
}

func (s *LedgerService) LedgerGenerateKey(_ context.Context, req *api.GenerateLedgerKeyRequest) (*api.Key, error) {
	if s.Ledger == nil {
		return nil, fmt.Errorf("Ledger support not available")
	}

	selWallet, err := selectLedgerWallet(s.Ledger, req.WalletID)
	if err != nil {
		return nil, err
	}
	db2, err := s.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}
	keyData, err := generateLedgerKey(s.Ledger, db2.Unwrap(), selWallet.WalletID, req.Labels, req.SigType)
	if err != nil {
		return nil, err
	}
	return keyData, nil
}

func selectLedgerWallet(manager LedgerManager, walletID string) (*api.LedgerWalletInfo, error) {
	wallets, err := manager.Wallets()
	if err != nil {
		return nil, err
	}
	walletCnt := len(wallets)
	switch {
	case walletCnt == 1:
		return wallets[0], nil
	case walletCnt == 0:
		return nil, errors.New("no wallets found, please check if your wallet and the Accumulate app on it are online")
	case walletCnt > 1 && len(walletID) == 0:
		return nil, fmt.Errorf("there is more than wallets available (%d), please use the --wallet-id flag to select the correct wallet", walletCnt)
	}

	var selWallet *api.LedgerWalletInfo
	widUrl, err := url.Parse(walletID)
	if err != nil {
		for _, wallet := range wallets {
			if wallet.WalletID.Equal(widUrl) {
				selWallet = wallet
				break
			}
		}
	} else if walletIdx, err := strconv.Atoi(walletID); err == nil {
		if len(wallets) < walletIdx {
			return nil, fmt.Errorf("wallet with index number %d is not available", walletIdx)
		}
		selWallet = wallets[walletIdx-1]
	}

	if selWallet == nil {
		return nil, fmt.Errorf("No wallet with ID %s could be found on an active Ledger device.\nPlease use the \"accumulate ledger info\" command to identify "+
			"the connected wallets, make sure the Accumulate app is ready and the device is not locked.", walletID)
	}
	return selWallet, nil
}

func generateLedgerKey(manager LedgerManager, db db.DB, walletID *url.URL, labels []string, sigType protocol.SignatureType) (*api.Key, error) {
	// make sure the key name doesn't already exist
	for _, label := range labels {
		k := new(Key)
		err := k.loadByLabel(db, label, false, false)
		if err == nil {
			return nil, fmt.Errorf("key already exists for key name %s", label)
		}
	}

	// Derive new key & save it
	derivation, err := bip44.NewDerivationPath(sigType)
	if err != nil {
		return nil, err
	}

	address, err := getKeyIndex(db, walletID, sigType)
	if err != nil {
		return nil, err
	}

	derivation = bip44.Derivation{derivation.Purpose(), derivation.CoinType(), derivation.Account(), derivation.Chain(), address}
	derivationPath, err := derivation.ToPath()
	if err != nil {
		return nil, err
	}

	account, err := manager.Derive(walletID, derivation, false, true, labels...)
	if err != nil {
		return nil, fmt.Errorf("derive function failed on ledger wallet: %v", err)
	}

	pub, _ := account.GetPublicKey()
	key := &Key{
		Key: api.Key{
			PublicKey: pub,
			Labels: &api.KeyLabels{
				Names: labels,
			},
			KeyInfo: api.KeyInfo{
				Type:       account.GetType(),
				Derivation: derivationPath,
				WalletId:   walletID,
			}},
	}

	err = key.save(db, true, labels...)
	if err != nil {
		return nil, err
	}

	return &key.Key, nil
}
