package services

import (
	"fmt"
	"strings"

	"github.com/tyler-smith/go-bip39"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
)

func importMnemonic(wallet db.DB, mnemonic []string) error {
	mns := strings.Join(mnemonic, " ")

	if !bip39.IsMnemonicValid(mns) {
		return fmt.Errorf("invalid mnemonic provided")
	}

	// Generate a Bip32 HD wallet for the mnemonic and a user supplied password
	seed := bip39.NewSeed(mns, "")

	root, _ := wallet.Get(BucketMnemonic, []byte("seed"))
	if len(root) != 0 {
		return fmt.Errorf("mnemonic seed phrase already exists within wallet")
	}

	err := wallet.Put(BucketMnemonic, []byte("seed"), seed)
	if err != nil {
		return fmt.Errorf("DB: seed write error, %v", err)
	}

	err = wallet.Put(BucketMnemonic, []byte("phrase"), []byte(mns))
	if err != nil {
		return fmt.Errorf("DB: phrase write error %s", err)
	}

	return nil
}
