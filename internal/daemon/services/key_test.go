package services

import (
	"context"
	"math/big"
	"testing"
	"time"

	"github.com/awnumar/memguard"
	"github.com/karalabe/usb"
	"github.com/stretchr/testify/mock"
	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/address"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	. "gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/vault"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"gitlab.com/accumulatenetwork/core/wallet/internal/ledger"
)

func TestResolveAddress(t *testing.T) {
	wallet := new(db.MemoryDB)

	err := importMnemonic(wallet, []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"})
	require.NoError(t, err)

	cases := []protocol.SignatureType{
		protocol.SignatureTypeED25519,
		protocol.SignatureTypeRCD1,
		protocol.SignatureTypeETH,
		protocol.SignatureTypeBTC,
	}

	for _, c := range cases {
		t.Run(c.String(), func(t *testing.T) {
			k, err := generateKey(wallet, []string{c.String()}, c, false)
			require.NoError(t, err, "Must generate key")

			addr, err := address.Parse(k.String())
			require.NoError(t, err, "Must parse key's address")
			require.Equal(t, addr.GetType(), k.KeyInfo.Type, "Key and address type must match")

			k.Labels = nil // Don't compare labels
			a2, err := resolveAddress(wallet, addr.String(), false, false)
			a2.(*Key).Labels = nil
			require.NoError(t, err, "Must resolve key's address")
			require.IsType(t, (*Key)(nil), a2, "Resolved address must be a key")
			require.True(t, k.Equal(&a2.(*Key).Key), "Resolved address must equal generated key")
		})
	}
}

func TestGenerateLedgerKey(t *testing.T) {
	// Use a mock Ledger
	ledger := NewMockLedgerManager(t)
	ledger.EXPECT().Wallets().Return([]*api.LedgerWalletInfo{{
		WalletID: url.MustParse("myLedger"),
	}}, nil)

	ledger.EXPECT().Derive(mock.Anything, mock.Anything, mock.Anything, mock.Anything, mock.Anything).Return(&address.PublicKey{
		Type: protocol.SignatureTypeED25519,
		Key:  []byte{1, 2, 3, 31: 0},
	}, nil)

	// Set up a vault
	w, _ := DaemonForTest(t, UseMemDB, WithLedger(ledger))
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Token: TestToken[:],
	})
	require.NoError(t, err)

	// Generate a Ledger key
	_, err = w.LedgerGenerateKey(context.Background(), &api.GenerateLedgerKeyRequest{
		Token:   TestToken[:],
		Labels:  []string{"foo"},
		SigType: protocol.SignatureTypeED25519,
	})
	require.NoError(t, err)
}

func TestLedger(t *testing.T) {
	if !usb.Supported() {
		t.Skip("USB integration is not supported on this platform")
	}

	// Check if a Ledger is connected
	ledger, err := ledger.NewAPI()
	require.NoError(t, err)

	wallets, err := ledger.Wallets()
	require.NoError(t, err)
	if len(wallets) == 0 {
		t.Skip("No wallets connected")
	}

	// Set up a vault
	w, _ := DaemonForTest(t, UseMemDB, WithLedger(ledger))
	_, err = w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Token: TestToken[:],
	})
	require.NoError(t, err)

	// Generate a Ledger key
	key, err := w.LedgerGenerateKey(context.Background(), &api.GenerateLedgerKeyRequest{
		Token:   TestToken[:],
		Labels:  []string{"foo"},
		SigType: protocol.SignatureTypeED25519,
	})
	require.NoError(t, err)

	// Sign with the Ledger key
	_, err = w.Sign(context.Background(), &api.SignRequest{
		Token:         TestToken[:],
		PublicKey:     key.PublicKey,
		Signer:        url.MustParse("signer"),
		SignerVersion: 1,
		Timestamp:     1,
		Transaction: &protocol.Transaction{
			Header: protocol.TransactionHeader{
				Principal: url.MustParse("principal"),
			},
			Body: &protocol.SendTokens{
				To: []*protocol.TokenRecipient{{
					Url:    url.MustParse("recipient"),
					Amount: *big.NewInt(1),
				}},
			},
		},
	})
	require.NoError(t, err)
}

func TestRestoreKeyCounter(t *testing.T) {
	// Set up a vault
	w, m := DaemonForTest(t, UseMemDB)
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Token: TestToken[:],
	})
	require.NoError(t, err)

	// Generate keys
	_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Token:    TestToken[:],
		Mnemonic: []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"},
	})
	require.NoError(t, err)

	_, err = w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Token:  TestToken[:],
		Type:   SignatureTypeED25519,
		Labels: []string{"key1"},
	})
	require.NoError(t, err)

	_, err = w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Token:  TestToken[:],
		Type:   SignatureTypeED25519,
		Labels: []string{"key2"},
	})
	require.NoError(t, err)

	r, err := w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Token:  TestToken[:],
		Type:   SignatureTypeED25519,
		Labels: []string{"key3"},
	})
	require.NoError(t, err)
	require.Equal(t, "m/44'/281'/0'/0'/2'", r.Key.KeyInfo.Derivation)

	// Reset the key counter
	db, err := m.Wallet("").Vault("").Open(make([]byte, 32))
	require.NoError(t, err)
	require.NoError(t, db.Unwrap().DeleteBucket(BucketKeyCounter))

	// Generating a key fails
	_, err = w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Token:  TestToken[:],
		Type:   SignatureTypeED25519,
		Labels: []string{"key4"},
	})
	require.Error(t, err)
	require.Contains(t, err.Error(), "key already exists:")

	// Restore the counter
	_, err = w.RestoreKeyCounters(context.Background(), &api.RestoreKeyCountersRequest{
		Token: TestToken[:],
	})
	require.NoError(t, err)

	// Generating a key succeeds
	r, err = w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Token:  TestToken[:],
		Type:   SignatureTypeED25519,
		Labels: []string{"key4"},
	})
	require.NoError(t, err)
	require.Equal(t, "m/44'/281'/0'/0'/3'", r.Key.KeyInfo.Derivation)
}

func TestKeyStoreExpiration(t *testing.T) {
	key, err := newEncryptedKey(memguard.NewEnclave(make([]byte, 32)), memguard.NewEnclave(TestToken[:]))
	require.NoError(t, err)

	// Set up a key store
	v := new(vault.Vault)
	dl := time.Now().Add(time.Minute)
	s := &TokenKeyStore{
		keys: map[string]*tokenKeyStore{
			string(TestToken[:]): {
				deadline: &dl,
				keys: map[*vault.Vault]*encryptedKey{
					v: key,
				},
			},
		},
	}

	// Ensure there isn't a false positive
	u := &VaultWithKeys{v, nil, false}
	_, err = s.GetKey(u, TestToken[:])
	require.NoError(t, err)

	// Expire the key store
	dl = time.Now().Add(-time.Minute)

	require.True(t, s.keys[string(TestToken[:])].expired())

	// Ensure the deadline is respected
	_, err = s.GetKey(u, TestToken[:])
	require.Error(t, err)
	require.ErrorIs(t, err, errors.NoPassword)
}

func TestRefreshToken(t *testing.T) {
	// Get a token
	w, _ := DaemonForTest(t, UseMemDB)
	rt, err := w.RefreshToken(context.Background(), &api.RefreshTokenRequest{})
	require.NoError(t, err)
	token := rt.Token

	// Set up a vault
	_, err = w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Token:      token,
		Passphrase: "foo",
	})
	require.NoError(t, err)

	// Generate a key
	_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Token:    token,
		Mnemonic: []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"},
	})
	require.NoError(t, err)

	rk1, err := w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Token:  token,
		Type:   SignatureTypeED25519,
		Labels: []string{"key"},
	})
	require.NoError(t, err)

	// Refresh the token
	rt, err = w.RefreshToken(context.Background(), &api.RefreshTokenRequest{
		Token: token,
	})
	require.NoError(t, err)
	require.NotEqual(t, token, rt.Token)
	token = rt.Token

	// Verify the new token works
	rk2, err := w.ResolveKey(context.Background(), &api.ResolveKeyRequest{
		Token: token,
		Value: "key",
	})
	require.NoError(t, err)
	require.Equal(t, rk1.Key.PublicKey, rk2.Key.PublicKey)
}
