package services

import (
	"time"

	"github.com/awnumar/memguard"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/model"
)

type KeyStore interface {
	GetKey(v Vault, token []byte) (*memguard.Enclave, error)
	PutKey(v Vault, token []byte, ttl time.Duration, key *memguard.Enclave) (*time.Time, error)
	PutPassphrase(v Vault, token []byte, ttl time.Duration, passphrase *memguard.Enclave) (*time.Time, error)
	DeleteKey(v Vault, token []byte) error
	RefreshToken(oldToken, newToken []byte, ttl time.Duration) (*time.Time, error)
}

type WalletManager interface {
	UseMemDB() bool
	Each(fn func(Wallet) error) error
	Wallet(walletDir string) Wallet
}

type Wallet interface {
	Directory() string
	Index() Vault
	Vault(name string) Vault
	Each(fn func(Vault) error) error
}

type Vault interface {
	Info() *api.VaultInfo
	Status(token []byte) (*api.VaultStatus, error)
	Open(token []byte) (*model.Model, error)
	OpenRaw(token []byte) (*model.Model, error)
	Create(token []byte) (*model.Model, error)
	With(token []byte, fn func(*model.Model) error) error
	Close() error
}

type GeneralService struct {
	MaxTTL time.Duration
	Keys   KeyStore
}

type KeyService struct {
	Vaults WalletManager
}

type AccountService struct {
	Vaults WalletManager
}

type VaultService struct {
	MaxTTL time.Duration
	Keys   KeyStore
	Vaults WalletManager
}

type TransactionService struct {
	Vaults WalletManager
}
