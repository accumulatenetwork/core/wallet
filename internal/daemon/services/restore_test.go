package services

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/model"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

func TestRestoreKeys(t *testing.T) {
	// Set up a wallet
	wallet := &db.FakeEncrypted{DB: new(db.MemoryDB)}
	err := importMnemonic(wallet, []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"})
	require.NoError(t, err)

	// Generate a key and save it the old way
	k, err := generateKeyNoSave(wallet, []string{"yellowKey"}, protocol.SignatureTypeED25519, false)
	require.NoError(t, err)
	require.NoError(t, k.oldSave(wallet, true, "yellowKey"))

	// Restore accounts
	_, err = RestoreAccounts(wallet)
	require.NoError(t, err)

	// Load the converted Address and verify it matches
	lite := protocol.LiteAuthorityForKey(k.PublicKey, k.KeyInfo.Type)
	addr, err := model.New(wallet).Address(lite).Get()
	require.NoError(t, err)
	assert.Equal(t, k.PublicKey, addr.PublicKey)
	assert.Equal(t, k.KeyInfo.Type, addr.Type)
	assert.Equal(t, k.KeyInfo.Derivation, addr.Derivation)
	assert.ElementsMatch(t, k.Labels.Names, addr.Labels)

	sk, err := model.New(wallet).PrivateKey(lite).GetCopy()
	require.NoError(t, err)
	assert.Equal(t, k.PrivateKey, sk)
}

func TestRestoreExternalKey(t *testing.T) {
	// Set up a wallet
	wallet := &db.FakeEncrypted{DB: new(db.MemoryDB)}
	err := importMnemonic(wallet, []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"})
	require.NoError(t, err)

	// Generate a key and save it the old way
	k, err := generateKeyNoSave(wallet, []string{"yellowKey"}, protocol.SignatureTypeED25519, false)
	require.NoError(t, err)
	require.NoError(t, k.oldSave(wallet, true, "yellowKey"))

	// Erase its private key
	require.NoError(t, wallet.DeleteBucket(BucketKeys))

	// Restore accounts
	_, err = RestoreAccounts(wallet)
	require.NoError(t, err)

	// Load the converted Address and verify it matches
	lite := protocol.LiteAuthorityForKey(k.PublicKey, k.KeyInfo.Type)
	addr, err := model.New(wallet).Address(lite).Get()
	require.NoError(t, err)
	assert.Equal(t, k.PublicKey, addr.PublicKey)
	assert.Equal(t, k.KeyInfo.Type, addr.Type)
	assert.Equal(t, k.KeyInfo.Derivation, addr.Derivation)
	assert.ElementsMatch(t, k.Labels.Names, addr.Labels)

	_, err = model.New(wallet).PrivateKey(lite).GetCopy()
	require.ErrorIs(t, err, errors.NotFound)
}

var (
	BucketKeys    = []byte("keys")
	BucketLite    = []byte("lite")
	BucketKeyInfo = []byte("keyinfo")
	BucketLabel   = []byte("label")
)

func (k *Key) oldSave(wallet db.DB, incrementAccountIndex bool, labels ...string) error {
	if k.KeyInfo.Type == protocol.SignatureTypeUnknown {
		return fmt.Errorf("signature type is was not specified")
	}

	err := wallet.Put(BucketKeys, k.PublicKey, k.PrivateKey)
	if err != nil {
		return err
	}

	for _, label := range labels {
		err = wallet.Put(BucketLabel, []byte(label), k.PublicKey)
		if err != nil {
			return err
		}
	}

	//lite label is should never be defined by user defined so define it here.
	keyHash := k.PublicKeyHash()
	lt, err := protocol.LiteTokenAddressFromHash(keyHash, protocol.ACME)
	if err != nil {
		return fmt.Errorf("lite label cannot be specified for lite token account")
	}

	liteLabel, _ := LabelForLiteTokenAccount(lt.String())
	err = wallet.Put(BucketLite, []byte(liteLabel), k.PublicKey)
	if err != nil {
		return err
	}

	data, err := k.KeyInfo.MarshalBinary()
	if err != nil {
		return err
	}

	err = wallet.Put(BucketKeyInfo, k.PublicKey, data)
	if err != nil {
		return err
	}

	if incrementAccountIndex {
		//increment the index for the account
		walletId := k.KeyInfo.WalletId
		if walletId == nil {
			walletId, err = getWalletIdFromSeed(wallet)
			if err != nil {
				return err
			}
		}
		err = incrementKeyIndex(wallet, walletId, k.KeyInfo.Type)
		if err != nil {
			return err
		}
	}

	return nil
}

func TestSanitizedCopy(t *testing.T) {
	// Set up a wallet
	src := &db.FakeEncrypted{DB: new(db.MemoryDB)}
	err := importMnemonic(src, []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"})
	require.NoError(t, err)

	// Generate a key and save it the old way
	k, err := generateKeyNoSave(src, []string{"yellowKey"}, protocol.SignatureTypeED25519, false)
	require.NoError(t, err)
	require.NoError(t, k.oldSave(src, true, "yellowKey"))

	dst := new(db.MemoryDB)
	err = SanitizedCopy(dst, src)
	require.NoError(t, err)

	// Private key is present in source
	v, err := src.Get(BucketKeys, k.Key.PublicKey)
	require.NoError(t, err)
	require.NotZero(t, v)
	require.NotEqual(t, make([]byte, len(v)), v)

	// Private key is not present in destination
	v, err = dst.Get(BucketKeys, k.Key.PublicKey)
	require.NoError(t, err)
	require.NotZero(t, v)
	require.Equal(t, make([]byte, len(v)), v)

	// Seed is present in source
	seed := []byte("seed")
	v, err = src.Get(BucketMnemonic, seed)
	require.NoError(t, err)
	require.NotZero(t, v)
	require.NotEqual(t, make([]byte, len(v)), v)

	// Seed is not present in destination
	v, err = dst.Get(BucketMnemonic, seed)
	require.NoError(t, err)
	require.NotZero(t, v)
	require.Equal(t, make([]byte, len(v)), v)
}
