package services

import (
	"context"
	"crypto"
	"crypto/ecdsa"
	"crypto/ed25519"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"fmt"

	btc "github.com/btcsuite/btcd/btcec"
	"gitlab.com/accumulatenetwork/accumulate/pkg/client/signing"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/bip44"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/model"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

type SigningService struct {
	Vaults WalletManager
	Ledger LedgerManager
}

func (s *SigningService) ResetLastUsedOn(_ context.Context, req *api.ResetLastUsedOnRequest) (*api.ResetLastUsedOnResponse, error) {
	// Ensure the database is open
	vault, err := s.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}
	defer commitOnSuccess(&err, vault)

	addr, err := resolveAddress(vault.Unwrap(), req.Key, false, false)
	if err != nil {
		return nil, err
	}
	key, ok := addr.(*Key)
	if !ok {
		return nil, fmt.Errorf("%v is not a known key", addr.String())
	}

	key.model.LastUsedOn = 0
	err = vault.Address(key.model.LiteIdentity()).Put(key.model)
	if err != nil {
		return nil, err
	}

	return &api.ResetLastUsedOnResponse{}, nil
}

func (s *SigningService) Sign(_ context.Context, req *api.SignRequest) (_ *api.SignResponse, err error) {
	vault, err := s.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}
	defer commitOnSuccess(&err, vault)

	// Resolve the key
	if req.PublicKey == nil {
		return nil, errors.BadRequest.WithFormat("Signature.PublicKey is required")
	}
	key, err := lookupByPubKey(vault.Unwrap(), req.PublicKey)
	if err != nil {
		return nil, err
	}

	// Resolve named transaction?
	if req.TransactionName != "" {
		if req.Transaction != nil {
			return nil, errors.BadRequest.With("TransactionName, Hash, and Transaction parameters are mutually exclusive")
		}
		req.Transaction, err = loadTransactionFromCache(vault.Unwrap(), req.TransactionName)
		if err != nil {
			return nil, err
		}
	}
	if req.Transaction != nil && len(req.Hash) != 0 {
		return nil, errors.BadRequest.With("TransactionName, Hash, and Transaction parameters are mutually exclusive")
	}

	return sign(s.Ledger, vault, signOpts{req, key, false})
}

func (s *SigningService) SignMessage(_ context.Context, req *api.SignMessageRequest) (_ *api.SignResponse, err error) {
	vault, err := s.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}
	defer commitOnSuccess(&err, vault)

	key, err := lookupByLabel(vault.Unwrap(), req.KeyName)
	if err != nil {
		return nil, err
	}

	hash := req.Message
	if !req.IsHash {
		h := sha256.Sum256(req.Message)
		hash = h[:]
	}

	return sign(s.Ledger, vault, signOpts{
		SignRequest: &api.SignRequest{
			Signer:        req.Signer,
			SignerVersion: req.SignerVersion,
			Timestamp:     req.Timestamp,
			Vote:          req.Vote,
			Delegators:    req.Delegators,

			// Sign as a transaction, not a raw hash - i.e. don't append the
			// signature metadata
			Transaction: &protocol.Transaction{
				Body: &protocol.RemoteTransaction{
					Hash: *(*[32]byte)(hash),
				},
			},
		},
		Key: key,
	})
}

func (s *SigningService) SignTransaction(_ context.Context, req *api.SignTransactionRequest) (_ *api.SignResponse, err error) {
	vault, err := s.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}
	defer commitOnSuccess(&err, vault)

	var txn *protocol.Transaction
	switch {
	case req.Name != "" && req.Transaction != nil:
		return nil, errors.BadRequest.With("Name and Transaction parameters are mutually exclusive")
	case req.Name != "":
		txn, err = loadTransactionFromCache(vault.Unwrap(), req.Name)
		if err != nil {
			return nil, err
		}
	case req.Transaction != nil:
		txn = req.Transaction
	default:
		return nil, errors.BadRequest.With("either Name or Transaction must be specified")
	}

	key, err := lookupByLabel(vault.Unwrap(), req.KeyName)
	if err != nil {
		return nil, err
	}

	return sign(s.Ledger, vault, signOpts{
		SignRequest: &api.SignRequest{
			Signer:        req.Signer,
			SignerVersion: req.SignerVersion,
			Timestamp:     req.Timestamp,
			Delegators:    req.Delegators,
			Transaction:   txn,
		},
		Key:          key,
		InitiateOnly: req.InitiateOnly,
	})
}

type signOpts struct {
	*api.SignRequest
	Key          *Key
	InitiateOnly bool
}

type signer interface {
	Sign([]byte) ([]byte, error)
	SignTransaction(protocol.Signature, *protocol.Transaction) error
}

func sign(ledger LedgerManager, vault *model.Model, opts signOpts) (*api.SignResponse, error) {
	// Compare timestamp to the key's last used on value
	if opts.Timestamp <= opts.Key.model.LastUsedOn {
		opts.Timestamp = opts.Key.model.LastUsedOn + 1
	}

	// Store the new last used on
	opts.Key.model.LastUsedOn = opts.Timestamp
	err := vault.Address(opts.Key.model.LiteIdentity()).Put(opts.Key.model)
	if err != nil {
		return nil, err
	}

	// Construct the signature
	sig, err := protocol.NewSignature(opts.Key.KeyInfo.Type)
	if err != nil {
		panic(err)
	}
	var fieldSig *[]byte
	var fieldHash *[32]byte
	switch sig := sig.(type) {
	case *protocol.LegacyED25519Signature:
		sig.PublicKey = opts.PublicKey
		sig.Signer = opts.Signer
		sig.SignerVersion = opts.SignerVersion
		sig.Timestamp = opts.Timestamp
		sig.Vote = opts.Vote
		fieldSig = &sig.Signature
		fieldHash = &sig.TransactionHash
		if opts.Memo != "" {
			return nil, errors.BadRequest.With("legacy ed25519 signatures do not support memos")
		}
		if opts.Metadata != nil {
			return nil, errors.BadRequest.With("legacy ed25519 signatures do not support metadata")
		}
	case *protocol.ED25519Signature:
		sig.PublicKey = opts.PublicKey
		sig.Signer = opts.Signer
		sig.SignerVersion = opts.SignerVersion
		sig.Timestamp = opts.Timestamp
		sig.Vote = opts.Vote
		sig.Memo = opts.Memo
		sig.Data = opts.Metadata
		fieldSig = &sig.Signature
		fieldHash = &sig.TransactionHash
	case *protocol.RCD1Signature:
		sig.PublicKey = opts.PublicKey
		sig.Signer = opts.Signer
		sig.SignerVersion = opts.SignerVersion
		sig.Timestamp = opts.Timestamp
		sig.Vote = opts.Vote
		sig.Memo = opts.Memo
		sig.Data = opts.Metadata
		fieldSig = &sig.Signature
		fieldHash = &sig.TransactionHash
	case *protocol.BTCSignature:
		sig.PublicKey = opts.PublicKey
		sig.Signer = opts.Signer
		sig.SignerVersion = opts.SignerVersion
		sig.Timestamp = opts.Timestamp
		sig.Vote = opts.Vote
		sig.Memo = opts.Memo
		sig.Data = opts.Metadata
		fieldSig = &sig.Signature
		fieldHash = &sig.TransactionHash
	case *protocol.BTCLegacySignature:
		sig.PublicKey = opts.PublicKey
		sig.Signer = opts.Signer
		sig.SignerVersion = opts.SignerVersion
		sig.Timestamp = opts.Timestamp
		sig.Vote = opts.Vote
		sig.Memo = opts.Memo
		sig.Data = opts.Metadata
		fieldSig = &sig.Signature
		fieldHash = &sig.TransactionHash
	case *protocol.ETHSignature:
		sig.PublicKey = opts.PublicKey
		sig.Signer = opts.Signer
		sig.SignerVersion = opts.SignerVersion
		sig.Timestamp = opts.Timestamp
		sig.Vote = opts.Vote
		sig.Memo = opts.Memo
		sig.Data = opts.Metadata
		fieldSig = &sig.Signature
		fieldHash = &sig.TransactionHash
	case *protocol.RsaSha256Signature:
		sig.PublicKey = opts.PublicKey
		sig.Signer = opts.Signer
		sig.SignerVersion = opts.SignerVersion
		sig.Timestamp = opts.Timestamp
		sig.Vote = opts.Vote
		sig.Memo = opts.Memo
		sig.Data = opts.Metadata
		fieldSig = &sig.Signature
		fieldHash = &sig.TransactionHash
	case *protocol.EcdsaSha256Signature:
		sig.PublicKey = opts.PublicKey
		sig.Signer = opts.Signer
		sig.SignerVersion = opts.SignerVersion
		sig.Timestamp = opts.Timestamp
		sig.Vote = opts.Vote
		sig.Memo = opts.Memo
		sig.Data = opts.Metadata
		fieldSig = &sig.Signature
		fieldHash = &sig.TransactionHash
	default:
		panic("invalid key type")
	}

	// Configure the private key or external signer
	var signer signer
	if opts.InitiateOnly {
		signer = fakeSigner{}
	} else if opts.Key.KeyInfo.WalletId != nil && len(opts.Key.PrivateKey) == 0 {
		if ledger == nil {
			return nil, fmt.Errorf("Ledger support not available")
		}
		signer = &ledgerSigner{ledger, opts.Key}
	} else {
		signer = &walletSigner{key: opts.Key}
	}

	// Are we signing raw, without the signature metadata hash?
	if len(opts.Hash) != 0 {
		// The signature type doesn't really matter, but it seems like good form
		// to return a typed signature
		*fieldSig, err = signer.Sign(opts.Hash[:])
		if err != nil {
			return nil, err
		}
		return &api.SignResponse{Signature: sig}, nil
	}

	// Apply delegation
	for _, del := range opts.Delegators {
		sig = &protocol.DelegatedSignature{
			Signature: sig,
			Delegator: del,
		}
	}

	// Copy the transaction to flush a cached hash
	txn := opts.Transaction.Copy()

	// Initiate
	if txn.Header.Initiator == ([32]byte{}) && txn.Body.Type() != protocol.TransactionTypeRemote {
		txn.Header.Initiator = *(*[32]byte)(sig.Metadata().Hash())
	}

	// Set the transaction hash
	*fieldHash = txn.ID().Hash()

	// Sign
	err = signer.SignTransaction(sig, txn)
	if err != nil {
		return nil, err
	}

	return &api.SignResponse{
		Signature:   sig,
		Transaction: txn,
	}, nil
}

type walletSigner struct {
	key *Key
}

func (s *walletSigner) Sign(hash []byte) ([]byte, error) {
	switch s.key.KeyInfo.Type {
	case protocol.SignatureTypeLegacyED25519,
		protocol.SignatureTypeED25519,
		protocol.SignatureTypeRCD1:
		return ed25519.Sign(s.key.PrivateKey, hash[:]), nil

	case protocol.SignatureTypeBTC,
		protocol.SignatureTypeBTCLegacy,
		protocol.SignatureTypeETH:
		pvkey, _ := btc.PrivKeyFromBytes(btc.S256(), s.key.PrivateKey)
		sign, err := pvkey.Sign(hash[:])
		if err != nil {
			return nil, err
		}
		return sign.Serialize(), nil

	case protocol.SignatureTypeRsaSha256:
		//private key is expected to be in PKCS #1, ASN.1 DER format
		privateKey, err := x509.ParsePKCS1PrivateKey(s.key.PrivateKey)
		if err != nil {
			return nil, err
		}
		return rsa.SignPKCS1v15(rand.Reader, privateKey, crypto.SHA256, hash)

	case protocol.SignatureTypeEcdsaSha256:
		//private key is expected to be in SEC, ASN.1 DER format
		privateKey, err := x509.ParseECPrivateKey(s.key.PrivateKey)
		if err != nil {
			return nil, err
		}
		return ecdsa.SignASN1(rand.Reader, privateKey, hash)

	default:
		panic("unsupported signature type")
	}
}

func (s *walletSigner) SignTransaction(sig protocol.Signature, txn *protocol.Transaction) error {
	return s.signTransaction(sig, sig.Metadata().Hash(), txn.GetHash())
}

func (s *walletSigner) signTransaction(sig protocol.Signature, sigMdHash, message []byte) error {
	if sig, ok := sig.(*protocol.DelegatedSignature); ok {
		return s.signTransaction(sig.Signature, sigMdHash, message)
	}
	return signing.PrivateKey(s.key.PrivateKey).Sign(sig, sigMdHash, message)
}

type ledgerSigner struct {
	manager LedgerManager
	key     *Key
}

func (ls *ledgerSigner) SignTransaction(sig protocol.Signature, txn *protocol.Transaction) error {
	if ls.key.KeyInfo.WalletId == nil {
		return fmt.Errorf("the given key is not on a Ledger device")
	}

	path, err := bip44.ParseDerivationPath(ls.key.KeyInfo.Derivation)
	if err != nil {
		return err
	}

	_, err = ls.manager.Sign(ls.key.KeyInfo.WalletId, path, txn, sig)
	return err
}

func (ls *ledgerSigner) Sign([]byte) ([]byte, error) {
	return nil, fmt.Errorf("Ledger app does not support blind signing")
}

type fakeSigner struct{}

func (fakeSigner) SignTransaction(sig protocol.Signature, txn *protocol.Transaction) error {
	return nil
}

func (fakeSigner) Sign(hash []byte) ([]byte, error) {
	return nil, nil
}
