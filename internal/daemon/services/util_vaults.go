package services

import (
	"fmt"
	"path/filepath"
	"strings"

	"github.com/awnumar/memguard"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/model"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/vault"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

type WalletManagerWithKeys struct {
	InMemory bool
	Keys     KeyStore
	wallets  map[string]*vault.Wallet
}

func (m *WalletManagerWithKeys) UseMemDB() bool { return m.InMemory }

func (m *WalletManagerWithKeys) Each(fn func(Wallet) error) error {
	for _, w := range m.wallets {
		err := fn(&WalletWithKeys{w, m.Keys})
		if err != nil {
			return err
		}
	}
	return nil
}

func (m *WalletManagerWithKeys) Wallet(walletDir string) Wallet {
	if strings.HasSuffix(walletDir, ".db") {
		walletDir = filepath.Dir(walletDir)
	}
	walletDir = strings.TrimSuffix(walletDir, string(filepath.Separator))

	if m.wallets == nil {
		m.wallets = make(map[string]*vault.Wallet)
	}

	w, ok := m.wallets[walletDir]
	if ok {
		return &WalletWithKeys{w, m.Keys}
	}

	w = new(vault.Wallet)
	w.Directory = walletDir
	w.UseMemDB = m.UseMemDB()
	m.wallets[walletDir] = w
	return &WalletWithKeys{w, m.Keys}
}

type WalletWithKeys struct {
	*vault.Wallet
	Keys KeyStore
}

var _ Wallet = (*WalletWithKeys)(nil)

func (w *WalletWithKeys) Directory() string {
	return w.Wallet.Directory
}

func (w *WalletWithKeys) Index() Vault {
	return &VaultWithKeys{Vault: w.Wallet.Index(), Keys: w.Keys}
}

func (w *WalletWithKeys) Vault(name string) Vault {
	return &VaultWithKeys{Vault: w.Wallet.Vault(name), Keys: w.Keys, noMulti: name == ""}
}

func (w *WalletWithKeys) Each(fn func(Vault) error) error {
	return w.Wallet.Each(func(v *vault.Vault) error {
		return fn(&VaultWithKeys{Vault: v, Keys: w.Keys})
	})
}

type VaultWithKeys struct {
	*vault.Vault
	Keys    KeyStore
	noMulti bool
}

var _ Vault = (*VaultWithKeys)(nil)

func (v *VaultWithKeys) Unwrap() *vault.Vault { return v.Vault }

func (v *VaultWithKeys) Status(token []byte) (*api.VaultStatus, error) {
	return v.Vault.Status(&keysForToken{v.Keys, token})
}

func (v *VaultWithKeys) open(token []byte) (*model.Model, error) {
	db, err := v.Vault.Open(&keysForToken{v.Keys, token})
	if err != nil {
		return nil, err
	}

	ok, err := db.Multi().Enabled().Get()
	if err == nil && ok && v.noMulti {
		return nil, errors.VaultUnspecified.With("wallet is multi-vault but no vault name was specified")
	}

	return db, nil
}

func (v *VaultWithKeys) Open(token []byte) (*model.Model, error) {
	// Open it
	db, err := v.open(token)
	if err != nil {
		return nil, err
	}

	// Migrate
	out, err := RestoreAccounts(db.Unwrap())
	if err != nil {
		return nil, err
	}
	if len(out) > 0 {
		fmt.Println(out)
	}

	return db, nil
}

func (v *VaultWithKeys) OpenRaw(token []byte) (*model.Model, error) {
	return v.Vault.OpenRaw(&keysForToken{v.Keys, token})
}

func (v *VaultWithKeys) Create(token []byte) (*model.Model, error) {
	return v.Vault.Create(&keysForToken{v.Keys, token})
}

func (v *VaultWithKeys) With(token []byte, fn func(*model.Model) error) error {
	return v.Vault.With(&keysForToken{v.Keys, token}, func(m *model.Model) error {
		ok, err := m.Multi().Enabled().Get()
		if err == nil && ok && v.noMulti {
			return errors.VaultUnspecified.With("wallet is multi-vault but no vault name was specified")
		}

		return fn(m)
	})
}

type keysForToken struct {
	Keys  KeyStore
	token []byte
}

func (k *keysForToken) GetKey(v *vault.Vault) (*memguard.Enclave, error) {
	return k.Keys.GetKey(&VaultWithKeys{Vault: v, Keys: k.Keys}, k.token)
}
