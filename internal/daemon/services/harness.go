package services

import (
	daemon "gitlab.com/accumulatenetwork/core/wallet/pkg/wallet"
)

func NewSimpleDaemon(wallets WalletManager, keys KeyStore, ledger LedgerManager) *SimpleDaemon {
	s := new(SimpleDaemon)
	s.GeneralService = &GeneralService{Keys: keys}
	s.VaultService = &VaultService{Vaults: wallets, Keys: keys}
	s.TransactionService = &TransactionService{Vaults: wallets}
	s.SigningService = &SigningService{Vaults: wallets, Ledger: ledger}
	s.KeyService = &KeyService{Vaults: wallets}
	s.LedgerService = &LedgerService{Vaults: wallets, Ledger: ledger}
	s.AccountService = &AccountService{Vaults: wallets}
	return s
}

type SimpleDaemon struct {
	daemon.GeneralService
	daemon.VaultService
	daemon.TransactionService
	daemon.SigningService
	daemon.KeyService
	daemon.LedgerService
	daemon.AccountService
}
