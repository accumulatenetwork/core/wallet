package services_test

import (
	"context"
	"crypto/sha256"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	. "gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	. "gitlab.com/accumulatenetwork/core/wallet/internal/daemon/services"
)

func TestAccountRegistration(t *testing.T) {
	// Set up a vault
	w, _ := DaemonForTest(t, UseMemDB)
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Token: TestToken[:],
	})
	require.NoError(t, err)

	// Register a book
	_, err = w.RegisterBook(context.Background(), &api.RegisterBookRequest{
		Token: TestToken[:],
		Url:   url.MustParse("book"),
		Pages: []*KeyPage{{
			Url: url.MustParse("page"),
		}},
	})
	require.NoError(t, err)

	// Register a token account
	_, err = w.RegisterTokenAccount(context.Background(), &api.RegisterTokenAccountRequest{
		Token: TestToken[:],
		Url:   url.MustParse("tokens"),
	})
	require.NoError(t, err)

	// List registered accounts
	resp, err := w.ListAccounts(context.Background(), &api.ListAccountsRequest{
		Token: TestToken[:],
	})
	require.NoError(t, err)

	require.Len(t, resp.Books, 1)
	require.Equal(t, "acc://book", resp.Books[0].Url.String())
	require.Len(t, resp.Books[0].Pages, 1)
	require.Equal(t, "acc://page", resp.Books[0].Pages[0].Url.String())

	require.Len(t, resp.Tokens, 1)
	require.Equal(t, "acc://tokens", resp.Tokens[0].Url.String())

	// Unregister the token account
	_, err = w.UnregisterTokenAccount(context.Background(), &api.UnregisterTokenAccountRequest{
		Token: TestToken[:],
		Url:   url.MustParse("tokens"),
	})
	require.NoError(t, err)

	resp, err = w.ListAccounts(context.Background(), &api.ListAccountsRequest{
		Token: TestToken[:],
	})
	require.NoError(t, err)

	require.Len(t, resp.Tokens, 0)
}

func TestFindSignerPage(t *testing.T) {
	// Set up a vault
	w, _ := DaemonForTest(t, UseMemDB)
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Token: TestToken[:]})
	require.NoError(t, err)

	// Generate a key
	_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Token:    TestToken[:],
		Mnemonic: []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"},
	})
	require.NoError(t, err)

	r1, err := w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Token:  TestToken[:],
		Type:   SignatureTypeED25519,
		Labels: []string{"yellowkey"},
	})
	require.NoError(t, err)
	kh := sha256.Sum256(r1.Key.PublicKey)

	// Register a book with a page with the key
	_, err = w.RegisterBook(context.Background(), &api.RegisterBookRequest{
		Token: TestToken[:],
		Url:   url.MustParse("book"),
		Pages: []*KeyPage{{
			Url: url.MustParse("book/1"),
			Keys: []*KeySpec{{
				PublicKeyHash: kh[:],
			}},
		}},
	})
	require.NoError(t, err)

	// Find signers for the book
	r2, err := w.FindSigner(context.Background(), &api.FindSignerRequest{
		Token:       TestToken[:],
		Authorities: []*url.URL{url.MustParse("book")},
	})
	require.NoError(t, err)

	require.Len(t, r2.Paths, 1)
	require.Len(t, r2.Paths[0].Keys, 1)
	require.Equal(t, "yellowkey", r2.Paths[0].Keys[0].Labels.Names[0])
	require.Len(t, r2.Paths[0].Signers, 1)
	require.Equal(t, "acc://book/1", r2.Paths[0].Signers[0].String())
}

func TestFindSignerForPage(t *testing.T) {
	// Set up a vault
	w, _ := DaemonForTest(t, UseMemDB)
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Token: TestToken[:]})
	require.NoError(t, err)

	// Generate a key
	_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Token:    TestToken[:],
		Mnemonic: []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"},
	})
	require.NoError(t, err)

	r1, err := w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Token:  TestToken[:],
		Type:   SignatureTypeED25519,
		Labels: []string{"yellowkey"},
	})
	require.NoError(t, err)
	kh := sha256.Sum256(r1.Key.PublicKey)

	// Register a book with a page with the key
	_, err = w.RegisterBook(context.Background(), &api.RegisterBookRequest{
		Token: TestToken[:],
		Url:   url.MustParse("book"),
		Pages: []*KeyPage{{
			Url: url.MustParse("book/1"),
			Keys: []*KeySpec{{
				PublicKeyHash: kh[:],
			}},
		}},
	})
	require.NoError(t, err)

	// Find signers for the page
	r2, err := w.FindSigner(context.Background(), &api.FindSignerRequest{
		Token:       TestToken[:],
		Authorities: []*url.URL{url.MustParse("book/1")},
	})
	require.NoError(t, err)

	require.Len(t, r2.Paths, 1)
	require.Len(t, r2.Paths[0].Keys, 1)
	require.Equal(t, "yellowkey", r2.Paths[0].Keys[0].Labels.Names[0])
	require.Len(t, r2.Paths[0].Signers, 1)
	require.Equal(t, "acc://book/1", r2.Paths[0].Signers[0].String())
}

func TestFindSignerLiteNotInWallet(t *testing.T) {
	// Set up a vault
	w, _ := DaemonForTest(t, UseMemDB)
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Token: TestToken[:],
	})
	require.NoError(t, err)

	// Find signers for the book
	r, err := w.FindSigner(context.Background(), &api.FindSignerRequest{
		Token:       TestToken[:],
		Authorities: []*url.URL{url.MustParse("7117c50f04f1254d56b704dc05298912deeb25dbc1d26ef6")},
	})
	require.NoError(t, err)
	require.Empty(t, r.Paths)
}

func TestFindSignerLite(t *testing.T) {
	// Set up a vault
	w, _ := DaemonForTest(t, UseMemDB)
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Token: TestToken[:],
	})
	require.NoError(t, err)

	// Generate a key
	_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Token:    TestToken[:],
		Mnemonic: []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"},
	})
	require.NoError(t, err)

	r1, err := w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Token:  TestToken[:],
		Type:   SignatureTypeED25519,
		Labels: []string{"yellowkey"},
	})
	require.NoError(t, err)

	// Find signers for the book
	r, err := w.FindSigner(context.Background(), &api.FindSignerRequest{
		Token:       TestToken[:],
		Authorities: []*url.URL{url.MustParse(r1.Key.Labels.Lite)},
	})
	require.NoError(t, err)

	require.Len(t, r.Paths, 1)
	require.Len(t, r.Paths[0].Keys, 1)
	require.Equal(t, "yellowkey", r.Paths[0].Keys[0].Labels.Names[0])
	require.Len(t, r.Paths[0].Signers, 1)
	require.Equal(t, r1.Key.Labels.Lite, r.Paths[0].Signers[0].ShortString())
}

func TestFindSignerDelegated(t *testing.T) {
	// Set up a vault
	w, _ := DaemonForTest(t, UseMemDB)
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Token: TestToken[:],
	})
	require.NoError(t, err)

	// Generate a key
	_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Token:    TestToken[:],
		Mnemonic: []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"},
	})
	require.NoError(t, err)

	r1, err := w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Token:  TestToken[:],
		Type:   SignatureTypeED25519,
		Labels: []string{"yellowkey"},
	})
	require.NoError(t, err)
	kh := sha256.Sum256(r1.Key.PublicKey)

	// Register a book with a page with the key
	_, err = w.RegisterBook(context.Background(), &api.RegisterBookRequest{
		Token: TestToken[:],
		Url:   url.MustParse("charlie"),
		Pages: []*KeyPage{{
			Url: url.MustParse("charlie/1"),
			Keys: []*KeySpec{{
				PublicKeyHash: kh[:],
			}},
		}},
	})
	require.NoError(t, err)

	_, err = w.RegisterBook(context.Background(), &api.RegisterBookRequest{
		Token: TestToken[:],
		Url:   url.MustParse("bob"),
		Pages: []*KeyPage{{
			Url: url.MustParse("bob/1"),
			Keys: []*KeySpec{{
				Delegate: url.MustParse("charlie"),
			}},
		}},
	})
	require.NoError(t, err)

	_, err = w.RegisterBook(context.Background(), &api.RegisterBookRequest{
		Token: TestToken[:],
		Url:   url.MustParse("alice"),
		Pages: []*KeyPage{{
			Url: url.MustParse("alice/1"),
			Keys: []*KeySpec{
				{Delegate: url.MustParse("bob")},
				{Delegate: url.MustParse("charlie")},
			},
		}},
	})
	require.NoError(t, err)

	// Find signers for the book
	r2, err := w.FindSigner(context.Background(), &api.FindSignerRequest{
		Token:       TestToken[:],
		Authorities: []*url.URL{url.MustParse("alice")},
	})
	require.NoError(t, err)

	require.Len(t, r2.Paths, 2)
	require.Len(t, r2.Paths[0].Keys, 1)
	require.Equal(t, "yellowkey", r2.Paths[0].Keys[0].Labels.Names[0])

	require.Len(t, r2.Paths[0].Signers, 2)
	require.Equal(t, "acc://charlie/1", r2.Paths[0].Signers[0].String())
	require.Equal(t, "acc://alice/1", r2.Paths[0].Signers[1].String())

	require.Len(t, r2.Paths[1].Signers, 3)
	require.Equal(t, "acc://charlie/1", r2.Paths[1].Signers[0].String())
	require.Equal(t, "acc://bob/1", r2.Paths[1].Signers[1].String())
	require.Equal(t, "acc://alice/1", r2.Paths[1].Signers[2].String())
}

func TestFindSignerDelegatedCycle(t *testing.T) {
	// Set up a vault
	w, _ := DaemonForTest(t, UseMemDB)
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Token: TestToken[:],
	})
	require.NoError(t, err)

	// Generate a key
	_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Token:    TestToken[:],
		Mnemonic: []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"},
	})
	require.NoError(t, err)

	r1, err := w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Token:  TestToken[:],
		Type:   SignatureTypeED25519,
		Labels: []string{"yellowkey"},
	})
	require.NoError(t, err)
	kh := sha256.Sum256(r1.Key.PublicKey)

	// Register a book with a page with the key
	_, err = w.RegisterBook(context.Background(), &api.RegisterBookRequest{
		Token: TestToken[:],
		Url:   url.MustParse("bob"),
		Pages: []*KeyPage{{
			Url: url.MustParse("bob/1"),
			Keys: []*KeySpec{
				{PublicKeyHash: kh[:]},
				{Delegate: url.MustParse("alice")},
			},
		}},
	})
	require.NoError(t, err)

	_, err = w.RegisterBook(context.Background(), &api.RegisterBookRequest{
		Token: TestToken[:],
		Url:   url.MustParse("alice"),
		Pages: []*KeyPage{{
			Url: url.MustParse("alice/1"),
			Keys: []*KeySpec{
				{Delegate: url.MustParse("bob")},
			},
		}},
	})
	require.NoError(t, err)

	// Find signers for the book
	r2, err := w.FindSigner(context.Background(), &api.FindSignerRequest{
		Token:       TestToken[:],
		Authorities: []*url.URL{url.MustParse("alice")},
	})
	require.NoError(t, err)

	require.Len(t, r2.Paths, 1)
	require.Len(t, r2.Paths[0].Keys, 1)
	require.Equal(t, "yellowkey", r2.Paths[0].Keys[0].Labels.Names[0])
	require.Len(t, r2.Paths[0].Signers, 2)
	require.Equal(t, "acc://bob/1", r2.Paths[0].Signers[0].String())
	require.Equal(t, "acc://alice/1", r2.Paths[0].Signers[1].String())
}

// TestFindSignerDelegatedIdentityCycle verifies that FindSigner omits paths (by
// default) that include two pages from the same domain (root identity).
func TestFindSignerDelegatedIdentityCycle(t *testing.T) {
	// Set up a vault
	w, _ := DaemonForTest(t, UseMemDB)
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Token: TestToken[:],
	})
	require.NoError(t, err)

	// Generate a key
	_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Token:    TestToken[:],
		Mnemonic: []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"},
	})
	require.NoError(t, err)

	r1, err := w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Token:  TestToken[:],
		Type:   SignatureTypeED25519,
		Labels: []string{"yellowkey"},
	})
	require.NoError(t, err)
	kh := sha256.Sum256(r1.Key.PublicKey)

	// Register a book with a page with the key
	_, err = w.RegisterBook(context.Background(), &api.RegisterBookRequest{
		Token: TestToken[:],
		Url:   url.MustParse("id/bob"),
		Pages: []*KeyPage{{
			Url: url.MustParse("id/bob/1"),
			Keys: []*KeySpec{
				{PublicKeyHash: kh[:]},
				{Delegate: url.MustParse("id/alice")},
			},
		}},
	})
	require.NoError(t, err)

	_, err = w.RegisterBook(context.Background(), &api.RegisterBookRequest{
		Token: TestToken[:],
		Url:   url.MustParse("id/alice"),
		Pages: []*KeyPage{{
			Url: url.MustParse("id/alice/1"),
			Keys: []*KeySpec{
				{Delegate: url.MustParse("id/bob")},
			},
		}},
	})
	require.NoError(t, err)

	// Find signers for the book
	r2, err := w.FindSigner(context.Background(), &api.FindSignerRequest{
		Token:       TestToken[:],
		Authorities: []*url.URL{url.MustParse("id/alice")},
	})
	require.NoError(t, err)

	require.Len(t, r2.Paths, 1)
}
