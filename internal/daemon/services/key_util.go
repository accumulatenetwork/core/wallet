package services

import (
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"log"
	"runtime/debug"
	"time"

	"github.com/tyler-smith/go-bip32"
	"gitlab.com/accumulatenetwork/accumulate/pkg/database/values"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/address"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/bip44"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/model"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

type Key struct {
	api.Key
	Hash []byte

	model *model.Address
}

// Implement address.Address

func (k *Key) GetType() protocol.SignatureType { return k.KeyInfo.Type }
func (k *Key) GetPublicKey() ([]byte, bool)    { return k.PublicKey, k.PublicKey != nil }
func (k *Key) GetPrivateKey() ([]byte, bool)   { return k.PrivateKey, k.PrivateKey != nil }

func (k *Key) GetPublicKeyHash() ([]byte, bool) {
	return k.PublicKeyHash(), k.Hash != nil || k.PublicKey != nil
}

func (k *Key) pubKeyAddr() *address.PublicKey {
	return &address.PublicKey{Key: k.PublicKey, Type: k.KeyInfo.Type}
}

func (k *Key) String() string {
	// Print the public key hash in the appropriate format
	switch k.KeyInfo.Type {
	case protocol.SignatureTypeED25519,
		protocol.SignatureTypeLegacyED25519,
		protocol.SignatureTypeRCD1,
		protocol.SignatureTypeBTC,
		protocol.SignatureTypeBTCLegacy,
		protocol.SignatureTypeETH,
		protocol.SignatureTypeRsaSha256,
		protocol.SignatureTypeEcdsaSha256:
		return k.pubKeyAddr().String()

	default:
		debug.PrintStack()
		panic(fmt.Errorf("cannot hash key for unsupported signature type %v(%d)", k.KeyInfo.Type, k.KeyInfo.Type.GetEnumValue()))
	}
}

func (k *Key) PublicKeyHash() []byte {
	if k.Hash != nil {
		return k.Hash
	}

	hash, err := protocol.PublicKeyHash(k.PublicKey, k.KeyInfo.Type)
	if err != nil {
		debug.PrintStack()
		panic(fmt.Errorf("cannot hash key for unsupported signature type %v(%d)", k.KeyInfo.Type, k.KeyInfo.Type.GetEnumValue()))
	}

	k.Hash = hash
	return hash
}

func (k *Key) save(wallet db.DB, incrementAccountIndex bool, labels ...string) (err error) {
	if k.KeyInfo.Type == protocol.SignatureTypeUnknown {
		return fmt.Errorf("signature type is was not specified")
	}

	db := model.New(wallet)
	defer commitOnSuccess(&err, db)

	addr := &model.Address{
		Type:       k.KeyInfo.Type,
		Derivation: k.KeyInfo.Derivation,
		WalletId:   k.KeyInfo.WalletId,
		PublicKey:  k.PublicKey,
	}
	for _, label := range labels {
		addr.AddLabel(label)
	}
	if k.Labels != nil {
		for _, label := range k.Labels.Names {
			addr.AddLabel(label)
		}
	}
	store(&err, db.Address(addr.LiteIdentity()).Put, addr)
	if k.PrivateKey != nil {
		store(&err, db.PrivateKey(addr.LiteIdentity()).PutCopy, k.PrivateKey)
	}
	if err != nil {
		return err
	}

	if incrementAccountIndex {
		//increment the index for the account
		walletId := k.KeyInfo.WalletId
		if walletId == nil {
			walletId, err = getWalletIdFromSeed(wallet)
			if err != nil {
				return err
			}
		}
		err = incrementKeyIndex(wallet, walletId, k.KeyInfo.Type)
		if err != nil {
			return err
		}
	}

	return nil
}

// loadByLabel will populate the Key structure from the database using the label or lite label.
// Note: If the private key is not present (e.g. in the case of a hardware wallet), it is NOT
// an error, so it is the responsibility of the user to confirm the private key is present if
// the intention is to use the private key for signing.
func (k *Key) loadByLabel(wallet db.DB, label string, _, resolvePages bool) (err error) {
	var isLite bool
	label, isLite = LabelForLiteTokenAccount(label)
	if !isLite {
		//if not an account, test for simple identity label
		label, isLite = LabelForLiteIdentity(label)
	}

	var value values.Value[*model.Address]
	if isLite {
		//if is a lite account, pull from the lite bucket
		value = model.New(wallet).Address(url.MustParse(label).RootIdentity())
	} else {
		value = model.New(wallet).AddressByLabel(label)
	}

	return k.load(wallet, value, resolvePages)
}

// loadByPublicKey will populate the Key structure from the database.  Note: If the private key
// is not present (e.g. in the case of a hardware wallet), it is NOT an error, so it is the
// responsibility of the user to confirm the private key is present if the intention is to use
// the private key for signing.
func (k *Key) loadByPublicKey(wallet db.DB, publicKey []byte, _, resolvePages bool) error {
	value := model.New(wallet).AddressByPublicKey(publicKey)
	return k.load(wallet, value, resolvePages)
}

func (k *Key) load(wallet db.DB, value values.Value[*model.Address], resolvePages bool) error {
	addr, err := value.Get()
	if err != nil {
		return err
	}

	k.model = addr
	k.PublicKey = addr.PublicKey
	k.KeyInfo.Type = addr.Type
	k.KeyInfo.Derivation = addr.Derivation
	k.KeyInfo.WalletId = addr.WalletId
	k.LastUsedOn = addr.LastUsedOn
	k.Labels = new(api.KeyLabels)
	k.Labels.Names = addr.Labels
	k.Labels.Lite = addr.LiteIdentity().ShortString()
	k.Labels.Hash = addr.Hash()

	k.PrivateKey, err = model.New(wallet).PrivateKey(addr.LiteIdentity()).GetCopy()
	if err != nil && !errors.Is(err, errors.NotFound) {
		return err
	}

	if resolvePages {
		model := model.New(wallet)
		books, err := model.Accounts().Books().Get()
		if err != nil {
			return err
		}
		for _, u := range books {
			pages, err := model.Accounts().Book(u).Pages().Get()
			if err != nil {
				return err
			}
			for _, page := range pages {
				_, _, ok := page.EntryByKeyHash(k.PublicKeyHash())
				if ok {
					k.Pages = append(k.Pages, page.Url)
				}
			}
		}
	}

	return nil
}

func (k *Key) InitializeFromSeed(seed []byte, signatureType protocol.SignatureType, hdPath string) error {
	addr, err := address.FromPrivateKeyBytes(seed, signatureType)
	if err != nil {
		return err
	}

	k.KeyInfo.Type = signatureType
	k.KeyInfo.Derivation = hdPath
	k.PrivateKey = addr.Key
	k.PublicKey = addr.PublicKey.Key
	return nil
}

func (k *Key) NativeAddress() (address string, err error) {
	switch k.KeyInfo.Type {
	case protocol.SignatureTypeRCD1:
		address, err = protocol.GetFactoidAddressFromRCDHash(k.PublicKeyHash())
	case protocol.SignatureTypeBTC, protocol.SignatureTypeBTCLegacy:
		address = protocol.BTCaddress(k.PublicKey)
	case protocol.SignatureTypeETH:
		address, err = protocol.ETHaddress(k.PublicKey)
	default:
		u := protocol.LiteAuthorityForKey(k.PublicKey, protocol.SignatureTypeED25519)
		address = u.Hostname()
	}
	return address, err
}

func getWalletIdFromSeed(wallet db.DB) (*url.URL, error) {
	hdId, err := bip44.NewDerivationPath(protocol.SignatureTypeED25519)
	if err != nil {
		return nil, err
	}

	rootDerivation, err := hdId.ToPath()
	if err != nil {
		return nil, err
	}

	walletIdKey, err := generateKeyFromHDPath(wallet, rootDerivation, protocol.SignatureTypeED25519)
	if err != nil {
		return nil, err
	}

	return protocol.LiteAuthorityForKey(walletIdKey.PublicKey, protocol.SignatureTypeED25519), nil
}

func generateKeyNoSave(wallet db.DB, labels []string, sigType protocol.SignatureType, forceOverwrite bool) (*Key, error) {
	for _, label := range labels {
		err := LabelIsValid(label)
		if err != nil {
			return nil, err
		}

		if label != "" {
			_, _, _, errFs := protocol.GetFactoidAddressRcdHashPkeyFromPrivateFs(label)
			_, errFA := protocol.GetRCDFromFactoidAddress(label)
			if errFs == nil || errFA == nil {
				return nil, fmt.Errorf("key name cannot be a factoid address")
			}
			u, err := url.Parse(label)
			if err != nil {
				return nil, err
			}
			_, _, err = protocol.ParseLiteTokenAddress(u)
			if err != nil {
				return nil, fmt.Errorf("key name cannot look like an account")
			}
		}

		//make sure it doesn't exist
		_, err = lookupByLabel(wallet, label)
		if err == nil && !forceOverwrite {
			return nil, fmt.Errorf("key already exists for key name %s", label)
		}
	}

	key, err := generateKeyFromCurrentCount(wallet, sigType)
	if err != nil {
		return nil, err
	}

	// Check for a duplicate key
	if _, err = model.New(wallet).AddressByPublicKey(key.PublicKey).Get(); err == nil && !forceOverwrite {
		return nil, fmt.Errorf("key already exists: %s", key)
	}

	//derive a lite label if needed that will reference the Accumulate lite account
	keyHash := key.PublicKeyHash()
	lt, err := protocol.LiteTokenAddressFromHash(keyHash, protocol.ACME)
	if err != nil {
		return nil, fmt.Errorf("no label specified and cannot import as lite token account")
	}
	liteLabel, _ := LabelForLiteTokenAccount(lt.String())

	key.Labels = new(api.KeyLabels)
	key.Labels.Lite = liteLabel
	key.Labels.Hash = key.PublicKeyHash()
	key.Labels.Names = append(key.Labels.Names, labels...)
	return key, nil
}

func generateKey(wallet db.DB, labels []string, sigType protocol.SignatureType, forceOverwrite bool) (*Key, error) {
	key, err := generateKeyNoSave(wallet, labels, sigType, forceOverwrite)
	if err != nil {
		return nil, err
	}

	err = key.save(wallet, true, labels...)
	if err != nil {
		return nil, err
	}

	return key, nil
}

func generateKeyFromCurrentCount(wallet db.DB, sigtype protocol.SignatureType) (k *Key, err error) {

	hd, err := bip44.NewDerivationPath(sigtype)
	if err != nil {
		return nil, err
	}

	walletId, err := getWalletIdFromSeed(wallet)
	if err != nil {
		return nil, err
	}

	address, err := getKeyIndex(wallet, walletId, sigtype)
	if err != nil {
		return nil, err
	}

	hd = bip44.Derivation{hd.Purpose(), hd.CoinType(), hd.Account(), hd.Chain(), address}

	derivationPath, err := hd.ToPath()
	if err != nil {
		return nil, err
	}

	return generateKeyFromHDPath(wallet, derivationPath, sigtype)
}

func generateKeyFromHDPath(wallet db.DB, derivationPath string, sigtype protocol.SignatureType) (*Key, error) {
	hd := bip44.Derivation{}
	err := hd.FromPath(derivationPath)
	if err != nil {
		return nil, err
	}

	err = hd.Validate()
	if err != nil {
		return nil, err
	}

	seed, err := lookupSeed(wallet)
	if err != nil {
		return nil, fmt.Errorf("wallet not created, please create a seeded wallet \"accumulate wallet init\"")
	}

	curve := bip32.Bitcoin
	if hd.CoinType() == bip44.TypeAccumulate {
		curve = bip32.Ed25519
	}
	//if we do have a seed, then create a new key
	masterKey, _ := bip32.NewMasterKeyWithCurve(seed, curve)

	//create the derived key
	newKey, err := bip44.NewKeyFromMasterKey(masterKey, hd.CoinType(), hd.Account(), hd.Chain(), hd.Address())
	if err != nil {
		return nil, err
	}
	key := new(Key)
	err = key.InitializeFromSeed(newKey.Key, sigtype, derivationPath)
	if err != nil {
		return nil, err
	}
	return key, nil
}

func getKeyCountBucketKey(walletID *url.URL, sigtype protocol.SignatureType) []byte {
	if walletID == nil {
		walletID = new(url.URL)
	}
	return []byte(walletID.WithUserInfo(sigtype.String()).String())
}

func getKeyIndex(wallet db.DB, walletID *url.URL, sigtype protocol.SignatureType) (count uint32, err error) {
	bucketKey := getKeyCountBucketKey(walletID, sigtype)

	ct, _ := wallet.Get(BucketKeyCounter, bucketKey)
	if ct != nil {
		count = binary.LittleEndian.Uint32(ct)
	}

	if sigtype == protocol.SignatureTypeED25519 {
		count += uint32(0x80000000)
	}

	return count, nil
}

func incrementKeyIndex(wallet db.DB, walletID *url.URL, sigtype protocol.SignatureType) error {
	bucketKey := getKeyCountBucketKey(walletID, sigtype)
	count := uint32(0)
	ct, _ := wallet.Get(BucketKeyCounter, bucketKey)
	if ct != nil {
		count = binary.LittleEndian.Uint32(ct)
	}

	return storeKeyIndex(wallet, bucketKey, count+1)
}

func loadKeyIndex(wallet db.DB, accountIndexBucketKey []byte) (uint32, error) {
	ct, err := wallet.Get(BucketKeyCounter, accountIndexBucketKey)
	if err != nil {
		if errors.Is(err, errors.NotFound) {
			return 0, nil
		}
		return 0, err
	}
	return binary.LittleEndian.Uint32(ct), nil
}

func storeKeyIndex(wallet db.DB, accountIndexBucketKey []byte, count uint32) error {
	ct := make([]byte, 8)
	binary.LittleEndian.PutUint32(ct, count)
	return wallet.Put(BucketKeyCounter, accountIndexBucketKey, ct)
}

func lookupSeed(wallet db.DB) (seed []byte, err error) {
	seed, err = wallet.Get(BucketMnemonic, []byte("seed"))
	if err != nil {
		return nil, fmt.Errorf("mnemonic seed doesn't exist")
	}

	return seed, nil
}

func exportAccounts(wallet db.DB) (*api.Vault, error) {
	res := &api.Vault{}
	res.BackupDate = time.Now()

	walletVersion, err := wallet.Get(db.BucketConfig, []byte("version"))
	if err != nil {
		return nil, err
	}
	var v db.Version
	v.FromBytes(walletVersion)
	res.Version.Commit = uint64(v.Commit())
	res.Version.Major = uint64(v.Major())
	res.Version.Minor = uint64(v.Minor())
	res.Version.Revision = uint64(v.Revision())

	if b, err := wallet.GetBucket(BucketKeyCounter); err == nil {
		for i := range b.KeyValueList {
			dc := api.DerivationCount{}
			dc.Count = uint64(binary.LittleEndian.Uint32(b.KeyValueList[i].Value))
			u := url.MustParse(string(b.KeyValueList[i].Key))
			dc.WalletId = u.RootIdentity()
			//var ok bool
			dc.Type, _ = protocol.SignatureTypeByName(u.UserInfo)
			res.DerivationCounters = append(res.DerivationCounters, dc)
		}
	}

	keyz, err := listKeys(wallet, true)
	if err == nil {
		for _, k := range keyz {
			res.Keys = append(res.Keys, k.Key)
			if k.Labels == nil {
				continue
			}
			var ll api.LiteLabel
			ll.LiteName = k.Labels.Lite
			if len(k.Labels.Names) > 0 {
				ll.KeyName = k.Labels.Names[0]
			} else {
				ll.KeyName = ll.LiteName
			}
			res.LiteLabels = append(res.LiteLabels, ll)
			for _, name := range k.Labels.Names {
				res.KeyNames = append(res.KeyNames, api.KeyName{
					Name:      name,
					PublicKey: k.PublicKey,
				})
			}
		}
	}

	a, err := wallet.GetBucket(BucketAdi)
	if err == nil {
		for _, v := range a.KeyValueList {
			u, err := url.Parse(string(v.Key))
			if err != nil {
				a := api.Adi{}
				p := api.Page{}
				a.Url = u
				//page url's aren't currently stored
				a.Pages = append(a.Pages, p)
				p.KeyNames = append(p.KeyNames, hex.EncodeToString(v.Value))
				res.Adis = append(res.Adis, a)
			}
		}
	}

	bucket, err := wallet.GetBucket(BucketMnemonic)
	if err != nil {
		log.Println("mnemonic bucket doesn't exist")
	} else {
		for _, v := range bucket.KeyValueList {
			switch string(v.Key) {
			case "phrase":
				phrase, err := wallet.Get(BucketMnemonic, []byte("phrase"))
				if err != nil {
					log.Println("mnemonic seed doesn't exist, thus was not exported")
				}
				if phrase != nil {
					res.SeedInfo.Mnemonic = string(phrase)
				}
			case "seed":
				res.SeedInfo.Seed, err = wallet.Get(BucketMnemonic, []byte("seed"))
				if err != nil {
					log.Println("mnemonic seed doesn't exist, thus was not exported")
				}
			default:
				//these are the sig types
				dc := api.DerivationCount{}
				dc.Count = uint64(binary.LittleEndian.Uint32(v.Value))
				var found bool
				dc.Type, found = protocol.SignatureTypeByName(string(v.Key))
				if found {
					res.DerivationCounters = append(res.DerivationCounters, dc)
				}
			}
		}
	}
	return res, nil
}

func assignKeyLabel(wallet db.DB, currentKeyName string, newKeyName string, force bool) error {
	err := LabelIsValid(newKeyName)
	if err != nil {
		return err
	}

	key, err := lookupByLabel(wallet, currentKeyName)
	if err != nil {
		return fmt.Errorf("label provided does not map to an existing key")
	}

	_, err = lookupByLabel(wallet, newKeyName)
	if err == nil && !force {
		return fmt.Errorf("new key label already exists")
	}

	key.model.AddLabel(newKeyName)
	m := model.New(wallet)
	err = m.Address(key.model.LiteIdentity()).Put(key.model)
	if err != nil {
		return err
	}
	return m.Commit()
}

func renameKeyLabel(wallet db.DB, currentKeyName string, newKeyName string, force bool) error {
	err := assignKeyLabel(wallet, currentKeyName, newKeyName, force)
	if err != nil {
		return err
	}
	//if no errors, remove the orig label
	return removeKeyLabel(wallet, currentKeyName)
}

func removeKeyLabel(wallet db.DB, removeKeyName string) error {
	m := model.New(wallet)
	addr := m.AddressByLabel(removeKeyName)
	v, err := addr.Get()
	switch {
	case err == nil:
		// Ok
	case errors.Is(err, errors.NotFound):
		return nil
	default:
		return err
	}

	v.RemoveLabel(removeKeyName)
	err = addr.Put(v)
	if err != nil {
		return err
	}
	return m.Commit()
}
