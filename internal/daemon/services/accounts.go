package services

import (
	"context"
	"crypto/sha256"
	"strings"

	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"golang.org/x/exp/slog"
)

func (m *AccountService) ListAccounts(_ context.Context, req *api.ListAccountsRequest) (*api.ListAccountsResponse, error) {
	resp := new(api.ListAccountsResponse)
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	// Load books (and their pages)
	for _, book := range load(&err, wallet.Accounts().Books().Get) {
		resp.Books = append(resp.Books, &api.BookInfo{
			Url:   book,
			Pages: load(&err, wallet.Accounts().Book(book).Pages().Get),
		})
	}

	// Load token accounts
	for _, token := range load(&err, wallet.Accounts().Tokens().Get) {
		resp.Tokens = append(resp.Tokens, &api.TokenAccountInfo{
			Url: token,
		})
	}

	// Check for errors
	if err != nil {
		return nil, err
	}

	return resp, nil
}

func (m *AccountService) RegisterADI(_ context.Context, req *api.RegisterADIRequest) (*api.RegisterADIResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	// The only issue Registration worries about is the inability to register the ADI
	// Attempts to register an ADI multiple times may submit multiple transactions, but
	// such errors will fail, and cost the user only .01 credits per repeat.
	_, err = wallet.Unwrap().Get(BucketAdi, []byte(req.ADI.ShortString()))
	if err != nil && !errors.Is(err, errors.NotFound) {
		return nil, err
	}

	// TODO Is it OK to register with an empty key?
	if req.Key == nil {
		req.Key = []byte{}
	}
	err = wallet.Unwrap().Put(BucketAdi, []byte(req.ADI.ShortString()), req.Key)
	if err != nil {
		return nil, err
	}

	resp := api.RegisterADIResponse{}
	return &resp, nil
}

func (m *AccountService) AdiList(_ context.Context, req *api.AdiListRequest) (*api.AdiListResponse, error) {
	wallet, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	b, err := wallet.Unwrap().GetBucket(BucketAdi)
	if err != nil {
		return nil, err
	}

	resp := api.AdiListResponse{}
	for _, kv := range b.KeyValueList {
		info := new(api.AdiInfo)
		resp.ADIs = append(resp.ADIs, info)

		info.Url, err = url.Parse(string(kv.Key))
		if err != nil {
			return nil, err
		}
		if len(kv.Value) == 0 {
			continue
		}

		k := new(Key)
		err = k.loadByPublicKey(wallet.Unwrap(), kv.Value, true, true)
		if err != nil {
			slog.Error("Load key info", "error", err, "url", info.Url)
			continue
		}

		// Ignore the error and use whatever did get loaded
		info.Key = &k.Key
	}
	return &resp, nil
}

func (m *AccountService) RegisterBook(ctx context.Context, req *api.RegisterBookRequest) (_ *api.RegisterBookResponse, err error) {
	vault, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}
	defer commitOnSuccess(&err, vault)

	err = vault.Accounts().Book(req.Url).Pages().Put(req.Pages)
	if err != nil {
		return nil, err
	}

	err = vault.Accounts().Books().Add(req.Url)
	if err != nil {
		return nil, err
	}

	return &api.RegisterBookResponse{}, nil
}

func (m *AccountService) UnregisterBook(ctx context.Context, req *api.UnregisterBookRequest) (_ *api.UnregisterBookResponse, err error) {
	vault, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}
	defer commitOnSuccess(&err, vault)

	err = vault.Accounts().Book(req.Url).Pages().Put(nil)
	if err != nil {
		return nil, err
	}

	err = vault.Accounts().Books().Remove(req.Url)
	if err != nil {
		return nil, err
	}

	return &api.UnregisterBookResponse{}, nil
}

func (m *AccountService) RegisterTokenAccount(ctx context.Context, req *api.RegisterTokenAccountRequest) (*api.RegisterTokenAccountResponse, error) {
	vault, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}
	defer commitOnSuccess(&err, vault)

	err = vault.Accounts().Tokens().Add(req.Url)
	if err != nil {
		return nil, err
	}

	return &api.RegisterTokenAccountResponse{}, nil
}

func (m *AccountService) UnregisterTokenAccount(_ context.Context, req *api.UnregisterTokenAccountRequest) (*api.UnregisterTokenAccountResponse, error) {
	vault, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}
	defer commitOnSuccess(&err, vault)

	err = vault.Accounts().Tokens().Remove(req.Url)
	if err != nil {
		return nil, err
	}

	return &api.UnregisterTokenAccountResponse{}, nil
}

func (m *KeyService) FindSigner(_ context.Context, req *api.FindSignerRequest) (*api.FindSignerResponse, error) {
	// Load the vault
	db, err := m.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	// Do not expose private keys
	resp := new(api.FindSignerResponse)
	defer func() {
		for _, p := range resp.Paths {
			for _, k := range p.Keys {
				k.PrivateKey = nil
			}
		}
	}()

	// Check for lite identities
	vault := db
	lite := map[string]*Key{}
	var liteCount int
	for _, book := range req.Authorities {
		if key, _ := protocol.ParseLiteIdentity(book); key == nil {
			continue
		}
		liteCount++

		// Have we already seen this key?
		id := strings.ToLower(book.Authority)
		k, ok := lite[id]
		if !ok {
			// Load it
			k = new(Key)
			err = k.loadByLabel(db.Unwrap(), id, true, false)
			switch {
			case err == nil:
				lite[id] = k
			case errors.Is(err, errors.NotFound):
				continue
			default:
				return nil, err
			}
		}

		// Add it to the list
		resp.Paths = append(resp.Paths, &api.SigningPath{
			Signers: []*url.URL{book},
			Keys:    []*api.Key{&k.Key},
		})
		continue
	}
	if liteCount > 0 {
		if liteCount < len(req.Authorities) {
			return nil, errors.BadRequest.With("cannot mix lite identities and key books")
		}
		return resp, nil
	}

	// Scan registered pages
	keys := map[[32]byte]*Key{}               // Map key hash hash to Key
	pages := map[[32]byte]*protocol.KeyPage{} // Map page ID to KeyPage
	delegates := map[[32]byte][]*url.URL{}    // Map book ID to pages that delegate to that book
	var candidates [][]*url.URL
	for _, book := range load(&err, vault.Accounts().Books().Get) {
		for _, page := range load(&err, vault.Accounts().Book(book).Pages().Get) {
			pages[page.Url.AccountID32()] = page

			var hasKey bool
			for _, entry := range page.Keys {
				// If the page has a delegate, map that delegate to the page so
				// we can follow that path later
				if entry.Delegate != nil {
					id := entry.Delegate.AccountID32()
					delegates[id] = append(delegates[id], page.Url)
				}

				if entry.PublicKeyHash == nil {
					continue
				}

				// Have we loaded this key already?
				h := sha256.Sum256(entry.PublicKeyHash)
				_, ok := keys[h]
				if ok {
					hasKey = true
					continue
				}

				// Load it
				k := new(Key)
				err := k.loadByLabel(db.Unwrap(), protocol.LiteAuthorityForHash(entry.PublicKeyHash).String(), true, false)
				switch {
				case err == nil:
					keys[h] = k
					hasKey = true
				case !errors.Is(err, errors.NotFound):
					return nil, err
				}
			}

			// A page that has a key we own is a candidate
			if hasKey {
				candidates = append(candidates, []*url.URL{page.Url})
			}
		}
	}
	if err != nil {
		return nil, err
	}

	// Map ends
	end := map[[32]byte]bool{}
	for _, book := range req.Authorities {
		end[book.AccountID32()] = true
	}

	// Breadth-first search for all paths
	for c := candidates; len(c) > 0; c = candidates {
		candidates = nil
		for _, c := range c {
			// If the candidate ends in one of the requested authorities, add it
			// as a valid path
			page := pages[c[len(c)-1].AccountID32()]
			bookId := page.GetAuthority().AccountID32()
			if end[bookId] || end[page.Url.AccountID32()] {
				resp.Paths = append(resp.Paths, &api.SigningPath{Signers: c})
			}

			// Follow delegates
		outer:
			for _, delegator := range delegates[bookId] {
				// Prevent cycles
				for _, c := range c {
					if req.AllowBookLoops {
						// Detect a cycle where a page delegates to itself
						// (directly or indirectly)
						if c.Equal(delegator) {
							continue outer
						}
					} else {
						// Detect a cycle where a page delegates to another page
						// in the same book
						if c.Identity().Equal(delegator.Identity()) {
							continue outer
						}
					}
				}

				// Create a new candidate for each delegate
				var d []*url.URL
				d = append(d, c...)
				d = append(d, delegator)
				candidates = append(candidates, d)
			}
		}
	}

	// Attach keys to paths
	for _, path := range resp.Paths {
		for _, entry := range pages[path.Signers[0].AccountID32()].Keys {
			if entry.PublicKeyHash == nil {
				continue
			}

			h := sha256.Sum256(entry.PublicKeyHash)
			k, ok := keys[h]
			if ok {
				path.Keys = append(path.Keys, &k.Key)
			}
		}
	}

	return resp, nil
}
