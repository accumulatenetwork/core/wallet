package services

import (
	"fmt"
	"io/fs"
	"os"
	"path/filepath"

	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/vault"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

func encryptDatabase(dbu db.DB, name string, password string, UseMemDB bool) (_ *vault.Vault, _ string, err error) {
	oldPath := dbu.Name()
	if oldPath == "" {
		return nil, "", errors.BadRequest.With("vault does not have a path, cannot encrypt")
	}

	if filepath.Base(oldPath) == "wallet_encrypted.db" {
		return nil, "", errors.BadRequest.With("vault is already encrypted")
	}

	newPath := filepath.Join(filepath.Dir(oldPath), "wallet_encrypted.db")
	_, err = os.Stat(newPath)
	switch {
	case err == nil:
		return nil, "", errors.BadRequest.WithFormat("encrypted vault %s already exists", newPath)
	case !errors.Is(err, fs.ErrNotExist):
		return nil, "", err // Unknown error
	}

	//now create the new encrypted database
	vi := &api.VaultInfo{
		Name:     name,
		FilePath: newPath,
	}
	vi.FilePath = newPath
	dbe, err := vault.Open(vi, []byte(password), UseMemDB)
	if err != nil {
		return nil, "", err
	}
	defer func() {
		if err != nil {
			dbe.Close()
		}
	}()

	err = copyBucket(dbe, dbu, BucketLabel)
	if err != nil && err != db.ErrNoBucket {
		return nil, "", err
	}

	err = copyBucket(dbe, dbu, BucketAdi)
	if err != nil && err != db.ErrNoBucket {
		return nil, "", err
	}

	err = copyBucket(dbe, dbu, BucketKeys)
	if err != nil && err != db.ErrNoBucket {
		return nil, "", err
	}

	err = copyBucket(dbe, dbu, BucketLite)
	if err != nil && err != db.ErrNoBucket {
		return nil, "", err
	}

	err = copyBucket(dbe, dbu, BucketMnemonic)
	if err != nil && err != db.ErrNoBucket {
		return nil, "", err
	}

	err = copyBucket(dbe, dbu, BucketKeyInfo)
	if err != nil && err != db.ErrNoBucket {
		return nil, "", err
	}

	if !equalBucket(dbe, dbu, BucketMnemonic) ||
		!equalBucket(dbe, dbu, BucketAdi) ||
		!equalBucket(dbe, dbu, BucketKeys) ||
		!equalBucket(dbe, dbu, BucketLabel) ||
		!equalBucket(dbe, dbu, BucketLite) ||
		!equalBucket(dbe, dbu, BucketKeyInfo) {
		return nil, "", db.ErrMalformedEncryptedDatabase
	}

	fmt.Printf("\nSuccess:\tEncrypted wallet created at %s.\n", dbe.Name())
	fmt.Printf("\t\tImported unencrypted data from wallet %s.\n", dbu.Name())
	fmt.Printf("\t\tIt is advised you backup your unencrypted wallet and/or keys, then remove\n\t\t%s from your system.\n", dbu.Name())
	return dbe, newPath, nil
}
