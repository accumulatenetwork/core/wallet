package services

import (
	"context"
	"encoding/hex"
	"fmt"
	"strconv"
	"strings"

	"github.com/multiformats/go-multibase"
	"github.com/tyler-smith/go-bip32"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/address"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/bip44"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/model"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"golang.org/x/exp/slog"
)

func (s *KeyService) RestoreKeyCounters(ctx context.Context, req *api.RestoreKeyCountersRequest) (*api.RestoreKeyCountersResponse, error) {
	// Scan for keys
	wallet, err := s.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}

	mainWalletId, err := getWalletIdFromSeed(wallet.Unwrap())
	if err != nil {
		return nil, err
	}

	counts := make(map[string]uint32)
	for _, addr := range wallet.Addresses() {
		key, err := addr.Get()
		if err != nil {
			return nil, err
		}

		// Skip external keys
		if key.Derivation == "" || key.Derivation == "external" {
			continue
		}

		// Skip bad paths
		path, err := bip44.ParseDerivationPath(key.Derivation)
		if err != nil {
			slog.ErrorCtx(ctx, "Bad derivation path", "key", addr.Key(), "error", err, "path", path)
			continue
		}

		// Determine the index of this key
		addressIndex := path.Address()
		if path.Address() >= bip32.FirstHardenedChild {
			addressIndex -= bip32.FirstHardenedChild
		}

		// Compare that to the key counter
		id := mainWalletId
		if key.WalletId != nil {
			id = key.WalletId
		}
		bucketKey := string(getKeyCountBucketKey(id, key.Type))
		if _, ok := counts[bucketKey]; !ok {
			count, err := loadKeyIndex(wallet.Unwrap(), []byte(bucketKey))
			if err != nil {
				return nil, err
			}
			counts[bucketKey] = count
		}
		if addressIndex > counts[bucketKey] {
			counts[bucketKey] = addressIndex
		}
	}

	// Update the key counter(s)
	for key, count := range counts {
		err = storeKeyIndex(wallet.Unwrap(), []byte(key), count+1)
		if err != nil {
			return nil, err
		}
		slog.InfoCtx(ctx, "Restored count", "key", key, "count", count+1)
	}

	return &api.RestoreKeyCountersResponse{}, nil
}

func (s *KeyService) ImportKey(ctx context.Context, req *api.ImportKeyRequest) (*api.ResolveKeyResponse, error) {
	wallet, err := s.Vaults.Wallet(req.Wallet).Vault(req.Vault).Open(req.Token)
	if err != nil {
		return nil, err
	}
	root, err := wallet.Unwrap().Get(BucketMnemonic, []byte("seed"))
	if err != nil {
		return nil, err
	} else if len(root) == 0 {
		return nil, fmt.Errorf("the wallet has not been initialized; this can be resolved with accumulate wallet init create")
	}

	for _, l := range req.Labels {
		err = LabelIsValid(l)
		if err != nil {
			return nil, err
		}
	}

	pk := new(Key)
	if err := pk.InitializeFromSeed(req.Seed, req.Type, "external"); err != nil {
		return nil, err
	}

	_, err = protocol.LiteTokenAddress(pk.PublicKey, protocol.ACME, pk.KeyInfo.Type)
	if err != nil {
		return nil, fmt.Errorf("no label specified and cannot import as lite token account")
	}

	if len(req.Labels) == 0 {
		label, err := pk.NativeAddress()
		if err != nil {
			return nil, err
		}
		req.Labels = append(req.Labels, label)
	}

	//here will change the label if it is a lite account specified, otherwise just use the label
	for i, label := range req.Labels {
		req.Labels[i], _ = LabelForLiteTokenAccount(label)

		existing, err := lookupByLabel(wallet.Unwrap(), req.Labels[i])
		if err == nil {
			if !req.Force || existing.KeyInfo.Derivation != "external" {
				return nil, fmt.Errorf("key name is already being used")
			}
		}
	}

	existing, err := lookupByPubKey(wallet.Unwrap(), pk.PublicKey)
	if err == nil {
		if len(existing.Labels.Names) > 0 {
			return nil, fmt.Errorf("private key already exists in wallet by key name of %s", existing.Labels.Names[0])
		}
		return nil, fmt.Errorf("private key already exists in wallet")
	}

	err = pk.save(wallet.Unwrap(), false, req.Labels...)
	if err != nil {
		return nil, err
	}

	return &api.ResolveKeyResponse{
		Address: pk.String(),
		Key:     &pk.Key,
	}, nil
}

func lookupByLabel(wallet db.DB, label string) (*Key, error) {
	k := new(Key)
	return k, k.loadByLabel(wallet, label, false, false)
}

// LabelForLiteTokenAccount returns the identity of the token account if label
// is a valid token account URL. Otherwise, LabelForLiteTokenAccount returns the
// original value.
func LabelForLiteTokenAccount(label string) (string, bool) {
	u, err := url.Parse(label)
	if err != nil {
		return label, false
	}

	key, _, err := protocol.ParseLiteTokenAddress(u)
	if key == nil || err != nil {
		return label, false
	}

	return u.Hostname(), true
}

func LabelIsValid(label string) error {
	if _, err := strconv.ParseInt(label, 10, 64); err == nil {
		return fmt.Errorf("label cannot be a number")
	}

	if len(label) == 0 {
		return fmt.Errorf("label cannot be empty")
	}

	_, isLite := LabelForLiteIdentity(label)
	if !isLite {
		_, isLite = LabelForLiteTokenAccount(label)
	}
	if isLite {
		return fmt.Errorf("label cannot be a lite identity")
	}

	u, err := url.Parse(label)
	if err != nil {
		return nil // Is this really OK?
	}
	if !u.IsRootIdentity() {
		return fmt.Errorf("label cannot contain a slash")
	}
	if strings.HasSuffix(label, ".acme") {
		return fmt.Errorf("label cannot end with .acme")
	}

	return nil
}

func LabelIsLite(label string) bool {
	_, isLite := LabelForLiteIdentity(label)
	if !isLite {
		_, isLite = LabelForLiteTokenAccount(label)
	}
	return isLite
}

// LabelForLiteIdentity returns the label of the LiteIdentity if label
// is a valid LiteIdentity account URL. Otherwise, LabelForLiteIdentity returns the
// original value.
func LabelForLiteIdentity(label string) (string, bool) {
	u, err := url.Parse(label)
	if err != nil {
		return label, false
	}

	key, err := protocol.ParseLiteIdentity(u)
	if key == nil || err != nil {
		return label, false
	}

	return u.Hostname(), true
}

// ResolveAddress resolves a string to a known hash type or to a local key.
func resolveAddress(wallet db.DB, s string, resolveLabels, resolvePages bool) (address.Address, error) {
	// This is a hack for managing validator keys
	if bits := strings.Split(s, ":"); len(bits) == 3 && bits[0] == "raw" {
		typ, ok := protocol.SignatureTypeByName(bits[1])
		if !ok {
			return nil, errors.UnknownError.WithFormat("parse raw key: invalid type %q", bits[1])
		}
		b, err := hex.DecodeString(bits[2])
		if err != nil {
			return nil, errors.UnknownError.WithFormat("parse raw key: %w", err)
		}
		return &address.PublicKey{Type: typ, Key: b}, nil
	}

	// Is the string a label?
	k := new(Key)
	err := k.loadByLabel(wallet, s, resolveLabels, resolvePages)
	var errNotFound error
	switch {
	case err == nil:
		return k, nil
	case errors.Is(err, errors.NotFound):
		// Continue
		errNotFound = err
	default:
		return nil, err
	}

	// Parse address
	addr, err := address.Parse(s)
	if err != nil {
		if err.Error() == "unknown address format" {
			return nil, errNotFound
		}
		return nil, err
	}

	// Is the address an unprefixed BitCoin P2PKH address?
	const btcMin, btcMax = 1 + 20 + 4, 1 + 32 + 4
	if u, ok := addr.(*address.Unknown); ok && // Address type is unknown
		u.Encoding == multibase.Base58BTC && //   Base 58 encoded
		len(u.Value) >= btcMin && //              Length matches (prefix, hash, checksum)
		len(u.Value) <= btcMax && //
		u.Value[0] == 0x00 { //                   Prefix is 0x00

		// Add the prefix and parse it, replacing the Unknown address
		addr, err = address.Parse("BT" + s)
		if err != nil {
			return nil, err
		}
	}

	// Is the address a hash?
	hash, ok := addr.GetPublicKeyHash()
	if ok {
		// Address is a hash of a local key?
		lite := protocol.LiteAuthorityForHash(hash)
		k, err := maybeLoad(wallet, (*Key).loadByLabel, lite.Authority, resolveLabels, resolvePages)
		if err != nil {
			return nil, err
		}

		// Return the key, or the original address
		if k != nil {
			return k, nil
		}
		return addr, nil
	}

	// Address is unknown or lite - attempt to resolve it to a local key
	switch addr := addr.(type) {
	case *address.Unknown:
		// Is the address long enough to be a hash or lite identity?
		if len(addr.Value) < 20 {
			return nil, errors.BadRequest.WithFormat("cannot resolve address %q", s)
		}

		// Address is the hash of a local key?
		lite := protocol.LiteAuthorityForHash(addr.Value)
		k, err := maybeLoad(wallet, (*Key).loadByLabel, lite.Authority, resolveLabels, resolvePages)
		if k != nil || err != nil {
			return k, err
		}

		// Address is the public key of a local key?
		k, err = maybeLoad(wallet, (*Key).loadByPublicKey, addr.Value, resolveLabels, resolvePages)
		if k != nil || err != nil {
			return k, err
		}

		return nil, errors.BadRequest.WithFormat("cannot resolve address %q: value is ambiguous", s)

	case *address.Lite:
		k, err := lookupByLabel(wallet, addr.Url.Authority)
		if err != nil {
			return nil, err
		}
		return k, nil

	default:
		return nil, errors.BadRequest.WithFormat("cannot resolve address %q", s)
	}
}

func maybeLoad[T any](wallet db.DB, fn func(*Key, db.DB, T, bool, bool) error, value T, resolveLabels, resolvePages bool) (*Key, error) {
	k := new(Key)
	err := fn(k, wallet, value, resolveLabels, resolvePages)
	if err == nil {
		return k, nil
	}
	if errors.Is(err, errors.NotFound) {
		return nil, nil
	}
	return nil, err
}

func lookupByPubKey(wallet db.DB, pubKey []byte) (*Key, error) {
	k := new(Key)
	return k, k.loadByPublicKey(wallet, pubKey, false, false)
}

func listKeys(wallet db.DB, withPrivate bool) (ka []Key, err error) {
	for _, v := range model.New(wallet).Addresses() {
		//find all key names associated with the account
		k2 := Key{}
		err := k2.load(wallet, v, true)
		if err != nil {
			return nil, err
		}

		//if we don't want to expose private key, remove it
		if !withPrivate {
			k2.PrivateKey = nil
		}
		ka = append(ka, k2)
	}
	return ka, err
}
