package services

import (
	"context"
	"crypto/sha256"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/vault"
	"gitlab.com/accumulatenetwork/core/wallet/internal/ledger"
)

var TestToken = sha256.Sum256([]byte("token"))

type TestDaemonOptions struct {
	useMemDB bool
	dbPath   string
	ledger   LedgerManager
}

type TestDaemonOption = func(t *testing.T, o *TestDaemonOptions)

func UseMemDB(t *testing.T, o *TestDaemonOptions) {
	o.useMemDB = true
}

func WithBoltDB(path string) TestDaemonOption {
	return func(t *testing.T, o *TestDaemonOptions) {
		o.dbPath = path
	}
}

func WithLedger(ledger LedgerManager) TestDaemonOption {
	return func(_ *testing.T, o *TestDaemonOptions) {
		o.ledger = ledger
	}
}

func DaemonForTest(t *testing.T, opts ...TestDaemonOption) (*SimpleDaemon, WalletManager) {
	o := new(TestDaemonOptions)
	for _, opt := range opts {
		opt(t, o)
	}

	if o.ledger == nil {
		ledger, err := ledger.NewAPI()
		require.NoError(t, err)
		o.ledger = ledger
	}

	if o.dbPath == "" {
		o.dbPath = t.TempDir()
	}

	keys := new(TokenKeyStore)
	wallet := &vault.Wallet{UseMemDB: o.useMemDB, Directory: o.dbPath}
	manager := &testWallet{keys, wallet}
	s := NewSimpleDaemon(manager, keys, o.ledger)

	if o.useMemDB {
		return s, manager
	}

	t.Cleanup(func() {
		_, _ = s.LockVault(context.Background(), &api.LockVaultRequest{
			Close: true,
			Token: TestToken[:],
		})
	})
	return s, manager
}

type testWallet struct {
	keys   KeyStore
	wallet *vault.Wallet
}

func (m *testWallet) UseMemDB() bool { return m.wallet.UseMemDB }

func (m *testWallet) Wallet(string) Wallet {
	return &WalletWithKeys{m.wallet, m.keys}
}

func (m *testWallet) Each(fn func(Wallet) error) error {
	return fn(m.Wallet(""))
}
