package services

import (
	"context"
	"crypto/sha256"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
)

func TestSigning_IncrementTimestamp(t *testing.T) {
	// Set up a vault
	w, _ := DaemonForTest(t, UseMemDB)
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Token: TestToken[:],
	})
	require.NoError(t, err)

	// Generate a key
	_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Token:    TestToken[:],
		Mnemonic: []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"},
	})
	require.NoError(t, err)

	key, err := w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Token:  TestToken[:],
		Type:   protocol.SignatureTypeED25519,
		Labels: []string{"key"},
	})
	require.NoError(t, err)

	// Sign a transaction, verify the timestamp is the specified value
	sig, err := w.Sign(context.Background(), &api.SignRequest{
		Token:         TestToken[:],
		PublicKey:     key.Key.PublicKey,
		Signer:        url.MustParse("alice"),
		SignerVersion: 1,
		Timestamp:     5,
		Transaction: &protocol.Transaction{
			Body: &protocol.RemoteTransaction{
				Hash: sha256.Sum256([]byte("foo")),
			},
		},
	})
	require.NoError(t, err)
	require.Equal(t, 5, int(sig.Signature.(protocol.KeySignature).GetTimestamp()))

	// Sign another transaction, verify the timestamp is incremented
	sig, err = w.Sign(context.Background(), &api.SignRequest{
		Token:         TestToken[:],
		PublicKey:     key.Key.PublicKey,
		Signer:        url.MustParse("alice"),
		SignerVersion: 1,
		Transaction: &protocol.Transaction{
			Body: &protocol.RemoteTransaction{
				Hash: sha256.Sum256([]byte("bar")),
			},
		},
	})
	require.NoError(t, err)
	require.Equal(t, 6, int(sig.Signature.(protocol.KeySignature).GetTimestamp()))
}

func TestSigning_DefaultTimestamp(t *testing.T) {
	// Set up a vault
	w, _ := DaemonForTest(t, UseMemDB)
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Token: TestToken[:],
	})
	require.NoError(t, err)

	// Generate a key
	_, err = w.ImportMnemonic(context.Background(), &api.ImportMnemonicRequest{
		Token:    TestToken[:],
		Mnemonic: []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"},
	})
	require.NoError(t, err)

	key, err := w.GenerateAddress(context.Background(), &api.GenerateAddressRequest{
		Token:  TestToken[:],
		Type:   protocol.SignatureTypeED25519,
		Labels: []string{"key"},
	})
	require.NoError(t, err)

	// Sign a transaction, verify the timestamp is set
	sig, err := w.Sign(context.Background(), &api.SignRequest{
		Token:         TestToken[:],
		PublicKey:     key.Key.PublicKey,
		Signer:        url.MustParse("alice"),
		SignerVersion: 1,
		Transaction: &protocol.Transaction{
			Body: &protocol.RemoteTransaction{
				Hash: sha256.Sum256([]byte("foo")),
			},
		},
	})
	require.NoError(t, err)
	require.Equal(t, 1, int(sig.Signature.(protocol.KeySignature).GetTimestamp()))
}
