package services

import (
	"crypto/rand"
	"encoding/json"
	"time"

	"github.com/awnumar/memguard"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/model"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/vault"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

type TokenKeyStore struct {
	keys map[string]*tokenKeyStore
}

var _ KeyStore = (*TokenKeyStore)(nil)

func (s *TokenKeyStore) PutPassphrase(v Vault, token []byte, ttl time.Duration, passphrase *memguard.Enclave) (*time.Time, error) {
	// Open the database
	db, err := v.OpenRaw(token)
	if err != nil {
		return nil, err
	}

	// Derive the key
	key, err := deriveSymKey(db, passphrase)
	if err != nil {
		return nil, err
	}

	// Store the key
	return s.PutKey(v, token, ttl, key)
}

func (s *TokenKeyStore) PutKey(v Vault, token []byte, ttl time.Duration, key *memguard.Enclave) (*time.Time, error) {
	if len(token) != 32 {
		return nil, errors.BadRequest.WithFormat("invalid token")
	}

	// Verify the key
	db, err := v.OpenRaw(token)
	if err != nil {
		return nil, err
	}

	_, err = db.Decrypt(key)
	if err != nil {
		return nil, err
	}

	// Get the token store
	t := s.forToken(token, true)

	// Update deadline
	t.refresh(ttl)

	// Store the key
	err = t.put(v, string(token), key)
	if err != nil {
		return nil, err
	}

	return t.deadline, nil
}

func (s *TokenKeyStore) DeleteKey(v Vault, token []byte) error {
	t := s.forToken(token, false)
	if t == nil {
		return nil
	}

	err := t.delete(v)
	if err != nil {
		return err
	}

	if len(t.keys) == 0 {
		delete(s.keys, string(token))
	}
	return nil
}

func (s *TokenKeyStore) GetKey(v Vault, token []byte) (*memguard.Enclave, error) {
	if len(token) != 32 {
		return nil, errors.BadRequest.WithFormat("invalid token")
	}

	// Get the token store
	t := s.forToken(token, false)

	// Has the token expired?
	if t == nil {
		return nil, errAuthRequired(v)
	}

	return t.get(v, string(token))
}

func (s *TokenKeyStore) RefreshToken(oldToken, newToken []byte, ttl time.Duration) (*time.Time, error) {
	// Get the token store
	t := s.forToken(oldToken, true)

	// Change the map key
	delete(s.keys, string(oldToken))
	s.keys[string(newToken)] = t

	// Refresh the deadline
	t.refresh(ttl)

	// Re-encrypt the keys
	err := t.changeToken(string(oldToken), string(newToken))
	if err != nil {
		return nil, err
	}

	return t.deadline, nil
}

func (s *TokenKeyStore) forToken(token []byte, create bool) *tokenKeyStore {
	// If the store exists and is not expired, return it
	stoken := string(token)
	t, ok := s.keys[stoken]
	if ok && !t.expired() {
		return t
	}

	// If the store is expired, remove it
	delete(s.keys, stoken)
	if !create {
		return nil
	}

	// Ensure the map is not nil
	if s.keys == nil {
		s.keys = map[string]*tokenKeyStore{}
	}

	// Create a new store and return it
	t = new(tokenKeyStore)
	s.keys[stoken] = t
	return t
}

type tokenKeyStore struct {
	deadline *time.Time
	keys     map[*vault.Vault]*encryptedKey
}

func (s *tokenKeyStore) expired() bool {
	return s.deadline != nil && time.Now().After(*s.deadline)
}

func (s *tokenKeyStore) refresh(ttl time.Duration) {
	if ttl == 0 {
		return
	}

	dl := time.Now().Add(ttl)
	if s.deadline == nil || s.deadline.Before(dl) {
		s.deadline = &dl
	}
}

func (s *tokenKeyStore) changeToken(oldToken, newToken string) error {
	oldToken2 := memguard.NewEnclave([]byte(oldToken))
	newToken2 := memguard.NewEnclave([]byte(newToken))

	for _, k := range s.keys {
		plain, err := k.Decrypt(oldToken2)
		if err != nil {
			return err
		}
		cipher, err := newEncryptedKey(newToken2, plain)
		if err != nil {
			return err
		}
		k.Enclave = cipher.Enclave
	}
	return nil
}

func (k *tokenKeyStore) get(v Vault, token string) (*memguard.Enclave, error) {
	// Using a string lets us avoid copying

	// Unwrap the vault
	var u *vault.Vault
	if v, ok := v.(interface{ Unwrap() *vault.Vault }); !ok {
		return nil, errors.InternalError.With("not my vault")
	} else {
		u = v.Unwrap()
	}

	// Do we have a key?
	key, ok := k.keys[u]
	if !ok {
		return nil, errAuthRequired(v)
	}

	// Retrieve and decrypt the key
	return key.Decrypt(memguard.NewEnclave([]byte(token)))
}

func (k *tokenKeyStore) put(v Vault, token string, plain *memguard.Enclave) error {
	// Using a string lets us avoid copying

	// Encrypt the key
	cipher, err := newEncryptedKey(memguard.NewEnclave([]byte(token)), plain)
	if err != nil {
		return err
	}

	// Unwrap the vault
	var u *vault.Vault
	if v, ok := v.(interface{ Unwrap() *vault.Vault }); !ok {
		return errors.InternalError.With("not my vault")
	} else {
		u = v.Unwrap()
	}

	// Store the key
	if k.keys == nil {
		k.keys = map[*vault.Vault]*encryptedKey{}
	}

	k.keys[u] = cipher
	return nil
}

func (k *tokenKeyStore) delete(v Vault) error {
	// Unwrap the vault
	var u *vault.Vault
	if v, ok := v.(interface{ Unwrap() *vault.Vault }); !ok {
		return errors.InternalError.With("not my vault")
	} else {
		u = v.Unwrap()
	}

	delete(k.keys, u)
	return nil
}

type encryptedKey memguard.Enclave

func newEncryptedKey(key *memguard.Enclave, plain *memguard.Enclave) (*encryptedKey, error) {
	key2, err := key.Open()
	if err != nil {
		return nil, err
	}
	defer key2.Destroy()

	plain2, err := plain.Open()
	if err != nil {
		return nil, err
	}
	defer plain2.Destroy()

	cipher, err := db.SymEncrypt(plain2.Bytes(), key2.Bytes())
	if err != nil {
		return nil, err
	}

	return (*encryptedKey)(memguard.NewEnclave(cipher)), nil
}

func (e *encryptedKey) Decrypt(key *memguard.Enclave) (*memguard.Enclave, error) {
	key2, err := key.Open()
	if err != nil {
		return nil, err
	}
	defer key2.Destroy()

	cipher, err := (*memguard.Enclave)(e).Open()
	if err != nil {
		return nil, err
	}
	defer cipher.Destroy()

	plain, err := db.SymDecrypt(cipher.Bytes(), key2.Bytes())
	if err != nil {
		return nil, err
	}
	return memguard.NewEnclave(plain), nil
}

func (s *VaultService) createDb(v Vault, token []byte, ttl time.Duration, password *memguard.Enclave) (*time.Time, error) {
	// Create it
	m, err := v.Create(token)
	if err != nil {
		return nil, err
	}

	var key *memguard.Enclave
	var n *model.Model
	if password != nil {
		// Generate salt
		salt := make([]byte, 30)
		_, err = rand.Read(salt)
		if err != nil {
			return nil, err
		}
		err = m.Config().Salt().Put(salt)
		if err != nil {
			return nil, err
		}

		// Derive the key
		key, err = deriveSymKey(m, password)
		if err != nil {
			return nil, err
		}

		// Encrypt the magic
		n, m = m, m.AsEncrypted(key)
	}

	// Write the magic
	err = m.Config().Magic().Put()
	if err != nil {
		return nil, err
	}

	// Record the version
	err = m.Config().Version().Put(model.Version.Bytes())
	if err != nil {
		return nil, err
	}

	// Commit
	err = m.Commit()
	if err != nil {
		return nil, err
	}
	if n != nil {
		err = n.Commit()
		if err != nil {
			return nil, err
		}
	}

	if key == nil {
		return nil, nil
	}

	// Store the passphrase
	return s.Keys.PutKey(v, token, ttl, key)
}

func deriveSymKey(m *model.Model, password *memguard.Enclave) (*memguard.Enclave, error) {
	salt, err := m.Config().Salt().Get()
	if err != nil {
		return nil, err
	}

	pass, err := password.Open()
	if err != nil {
		return nil, err
	}
	defer pass.Destroy()

	key, err := db.DeriveSymKey(pass.Bytes(), salt)
	if err != nil {
		return nil, err
	}
	return memguard.NewEnclave(key), nil
}

func errAuthRequired(v Vault) error {
	b, _ := json.Marshal(v.Info())
	err := errors.NoPassword.With("no password specified for encrypted vault")
	err.Data = b
	return err
}
