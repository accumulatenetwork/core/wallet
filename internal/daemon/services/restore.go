package services

import (
	"bytes"
	"crypto/ed25519"
	"crypto/rand"
	"encoding"
	"encoding/hex"
	"fmt"
	"strings"

	"github.com/tyler-smith/go-bip32"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	url2 "gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/bip44"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/database/model"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"golang.org/x/exp/slog"
)

// oldBucket is a struct of deprecated bucket names.
//
// Deprecated: Only used for migration.
var oldBucket = struct {
	Anon    []byte
	SigType []byte
	Index   []byte
	Vaults  []byte
	Keys    []byte
	Lite    []byte
	KeyInfo []byte
	Label   []byte
}{
	Anon:    []byte("anon"),
	SigType: []byte("sigtype"),
	Index:   []byte("index"),
	Vaults:  []byte("vaults"),
	Keys:    []byte("keys"),
	Lite:    []byte("lite"),
	KeyInfo: []byte("keyinfo"),
	Label:   []byte("label"),
}

// modelBucket is a struct of buckets used by internal/database/model.
//
// Deprecated: Only used for migration.
var modelBucket = struct {
	Config     []byte
	Multi      []byte
	Accounts   []byte
	Address    []byte
	PrivateKey []byte
	Lookup     []byte
}{
	Config:     []byte("Config"),
	Multi:      []byte("Multi"),
	Accounts:   []byte("Accounts"),
	Address:    []byte("Address"),
	PrivateKey: []byte("PrivateKey"),
	Lookup:     []byte("Lookup"),
}

func RestoreAccounts(wallet db.DB) (out string, err error) {
	walletVersionData, err := wallet.Get(db.BucketConfig, []byte("version"))

	var walletVersion db.Version
	if err == nil {
		walletVersion.FromBytes(walletVersionData)
		//if there is no error getting version, check to see if it is the right version
		if model.Version.Compare(walletVersion) == 0 {
			//no need to update
			return "", nil
		}
		if model.Version.Compare(walletVersion) < 0 {
			return "", fmt.Errorf("cannot update wallet to an older version, wallet database version is %v, cli version is %v", walletVersion.String(), model.Version.String())
		}
	}

	m := model.New(wallet)
	defer commitOnSuccess(&err, m)

	// Vault list and other similar metadata
	vaultListVersion := db.NewVersion(0, 0, 3, 3)
	if walletVersion.Compare(vaultListVersion) <= 0 {
		list, err := getAs[api.VaultList](wallet, oldBucket.Index, []byte("wallet-list"))
		switch {
		case err == nil && len(list.Vaults) > 0:
			// Ok
		case errors.Is(err, errors.NotFound):
			list = new(api.VaultList)
		default:
			return "", err
		}

		if len(list.Vaults) > 0 {
			fmt.Println("Migrating old vault index")
			err = m.Multi().Names().Add(list.Vaults...)
			if err != nil {
				return "", err
			}

			err = m.Multi().Enabled().Put(true)
			if err != nil {
				return "", err
			}
		}

		for _, name := range list.Vaults {
			name = strings.ToLower(name)
			info, err := getAs[api.VaultInfo](wallet, oldBucket.Vaults, []byte(name))
			if err != nil {
				return "", err
			}
			err = m.Multi().Vault(name).Put(info)
			if err != nil {
				return "", err
			}
		}

		err = wallet.DeleteBucket(oldBucket.Index)
		if err != nil {
			return "", err
		}

		err = wallet.DeleteBucket(oldBucket.Vaults)
		if err != nil {
			return "", err
		}
	}

	// Fix up the salt
	err = model.With(wallet, func(db *model.Model) error {
		ok, err := db.Multi().Enabled().Get()
		if err != nil {
			return err
		}
		if !ok {
			return nil
		}

		salt, err := db.Multi().Salt().Get()
		switch {
		case err == nil:
			return nil
		case !errors.Is(err, errors.NotFound):
			return err
		}

		_, err = rand.Read(salt[:])
		if err != nil {
			return err
		}
		return db.Multi().Salt().Put(salt)
	})
	if err != nil {
		return "", err
	}

	for _, v := range readBucket(&err, wallet, oldBucket.Anon).KeyValueList {
		u, err := url2.Parse(string(v.Key))
		if err != nil {
			out += fmt.Sprintf("%q is not a valid URL\n", v.Key)
		}
		if u != nil {
			key, _, err := protocol.ParseLiteTokenAddress(u)
			if err != nil {
				out += fmt.Sprintf("%q is not a valid lite account: %v\n", v.Key, err)
			} else if key == nil {
				out += fmt.Sprintf("%q is not a lite account\n", v.Key)
			}
		}

		label, _ := LabelForLiteTokenAccount(string(v.Key))
		v.Key = []byte(label)

		privKey := ed25519.PrivateKey(v.Value)
		pubKey := privKey.Public().(ed25519.PublicKey)
		out += fmt.Sprintf("Converting %s : %x\n", v.Key, pubKey)

		err = wallet.Put(oldBucket.Label, v.Key, pubKey)
		if err != nil {
			return "", err
		}
		err = wallet.Put(oldBucket.Keys, pubKey, privKey)
		if err != nil {
			return "", err
		}
		err = wallet.DeleteBucket(oldBucket.Anon)
		if err != nil {
			return "", err
		}
	}
	if err != nil {
		return "", err
	}

	//fix the labels... there can be only one key one label.
	//should not have multiple labels to the same public key
	for _, v := range readBucket(&err, wallet, oldBucket.Label).KeyValueList {
		label, isLite := LabelForLiteTokenAccount(string(v.Key))
		if isLite {
			//if we get here, then that means we have a bogus label.
			bogusLiteLabel := string(v.Key)
			//so check to see if it is in our regular key bucket
			otherPubKey, err := wallet.Get(oldBucket.Label, []byte(label))
			if err != nil {
				//key isn't found, so let's add it
				out += fmt.Sprintf("Converting %s to %s : %x\n", v.Key, label, v.Value)
				//so it doesn't exist, map the good label to the public key
				err = wallet.Put(oldBucket.Label, []byte(label), v.Value)
				if err != nil {
					return "", err
				}

				//now delete the bogus label
				err = wallet.Delete(oldBucket.Label, []byte(bogusLiteLabel))
				if err != nil {
					return "", err
				}
			} else {
				//ok so it does exist, now need to know if public key is the same, it is
				//an error if they don't match so warn user
				if !bytes.Equal(v.Value, otherPubKey) {
					out += fmt.Sprintf("public key stored for %v, doesn't match what is expected for a lite account: %s (%x != %x)\n",
						bogusLiteLabel, label, v.Value, otherPubKey)
				} else {
					//key isn't found, so let's add it
					out += fmt.Sprintf("Removing duplicate %s / %s : %x\n", v.Key, label, v.Value)
					//now delete the bogus label
					err = wallet.Delete(oldBucket.Label, []byte(bogusLiteLabel))
					if err != nil {
						return "", err
					}
				}
			}
		}
	}
	if err != nil {
		return "", err
	}

	//build the map of lite accounts to keys
	for _, v := range readBucket(&err, wallet, oldBucket.Label).KeyValueList {
		k := new(Key)

		var err error
		k.PrivateKey, err = wallet.Get(oldBucket.Keys, v.Value)
		if err != nil {
			out += fmt.Sprintf("info: private key not found for label %s with public key %x\n", string(v.Key), v.Value)
		}

		k.PublicKey = v.Value

		//check to see if the key type has been assigned, if not set it to the ed25519Legacy...
		sigTypeData, err := wallet.Get(oldBucket.SigType, v.Value)
		if err != nil {
			//if it doesn't exist then 1) we are already using BucketKeyInfo, so we're golden, or 2)
			//we are an old wallet, so default it to ed25519 signature types

			//first, test for 1)
			kid, err := wallet.Get(oldBucket.KeyInfo, v.Value)
			if err != nil {
				//ok, so it is 2), wo we need to make the key info bucket
				k.KeyInfo.Type = protocol.SignatureTypeED25519
				k.KeyInfo.Derivation = "external"

				//add the default key type
				out += fmt.Sprintf("assigning default key type %s for key name %s\n", k.KeyInfo.Type, string(v.Key))
				kiData, err := k.KeyInfo.MarshalBinary()
				if err != nil {
					return "", err
				}

				err = wallet.Put(oldBucket.KeyInfo, v.Value, kiData)
				if err != nil {
					return "", err
				}
			} else {
				//we have key info, so, make sure it isn't a legacyEd25519 key and
				// assign it to key's key info.  If it is a legacyEd25519 then
				// update it to the regular accumulate ed25519 type
				err = k.KeyInfo.UnmarshalBinary(kid)
				if err != nil {
					return "", err
				}
				//if we hae the old key type, make it the new type.
				if k.KeyInfo.Type == protocol.SignatureTypeLegacyED25519 {
					k.KeyInfo.Type = protocol.SignatureTypeED25519

					kiData, err := k.KeyInfo.MarshalBinary()
					if err != nil {
						return "", err
					}

					err = wallet.Put(oldBucket.KeyInfo, v.Value, kiData)
					if err != nil {
						return "", err
					}
				}
			}
		} else {
			//we have some old data in the bucket, so move it to the new bucket.
			kt, err := uvarintUnmarshalBinary(sigTypeData)
			if err != nil {
				return "", err
			}

			k.KeyInfo.Type.SetEnumValue(kt)

			if k.KeyInfo.Type == protocol.SignatureTypeLegacyED25519 {
				k.KeyInfo.Type = protocol.SignatureTypeED25519
			}
			k.KeyInfo.Derivation = "external"

			//add the default key type
			out += fmt.Sprintf("assigning default key type %s for key name %v\n", k.KeyInfo.Type, string(v.Key))

			kiData, err := k.KeyInfo.MarshalBinary()
			if err != nil {
				return "", err
			}

			err = wallet.Put(oldBucket.KeyInfo, v.Value, kiData)
			if err != nil {
				return "", err
			}
		}
	}
	if err != nil {
		return "", err
	}

	//seed counter and remap lite accounts
	seedCountVersion := db.NewVersion(0, 0, 3, 2)
	if walletVersion.Compare(seedCountVersion) <= 0 {
		//if wallet version is less than the version where this change was made
		m := make(map[string]uint32)
		for _, v := range readBucket(&err, wallet, oldBucket.KeyInfo).KeyValueList {
			k := api.KeyInfo{}
			err = k.UnmarshalBinary(v.Value)
			if err != nil {
				out += fmt.Sprintf("cannot unmarshal key info for key %x: %v\n", v.Key, err)
				continue
			}

			//we also need to set the lite table to map to public key instead of label
			u := protocol.LiteAuthorityForKey(v.Key, k.Type)
			label, _ := LabelForLiteIdentity(u.String())

			out += fmt.Sprintf("remapping lite label %s to public key %x\n", label, v.Key)
			err = wallet.Put(oldBucket.Lite, []byte(label), v.Key)
			if err != nil {
				out += fmt.Sprintf("error mapping lite label %s for key %x, %v\n", label, v.Key, err)
			}

			if k.Derivation == "external" {
				continue
			}

			walletId := k.WalletId
			if walletId == nil {
				walletId, err = getWalletIdFromSeed(wallet)
				if err != nil {
					out += fmt.Sprintf("cannot get walletid from seed for key %x: %v\n", v.Key, err)
					continue
				}
			}
			der, err := bip44.ParseDerivationPath(k.Derivation)
			if err != nil {
				out += fmt.Sprintf("cannot get derivation from path %s for key %x: %v\n", k.Derivation, v.Key, err)
				continue
			}
			addressIndex := der.Address()

			if der.Address() >= bip32.FirstHardenedChild {
				addressIndex -= bip32.FirstHardenedChild
			}

			bucketKey := string(getKeyCountBucketKey(walletId, k.Type))
			if addressIndex > m[bucketKey] {
				m[bucketKey] = addressIndex
			}
		}
		//store items in map in account counter
		for k2, v2 := range m {
			err = storeKeyIndex(wallet, []byte(k2), v2+1)
			if err != nil {
				out += fmt.Sprintf("error storing key index for key %s, %v", k2, err)
				continue
			}
		}
		if err != nil {
			return "", err
		}
	}

	deleteBucket(&err, wallet, oldBucket.SigType)

	// Migrate keys to new structure
	keyDataModelVersion := db.NewVersion(0, 0, 5, 0)
	if walletVersion.Compare(keyDataModelVersion) <= 0 {
		// Skip the lite bucket - the data model does not have a place for it

		// Check for existing keys, just in case
		addrs := map[string]*model.Address{}
		for _, item := range m.Addresses() {
			addr := load(&err, item.Get)
			if addr != nil {
				addrs[string(addr.PublicKey)] = addr
			}
		}

		// Key info
		for _, item := range readBucket(&err, wallet, oldBucket.KeyInfo).KeyValueList {
			slog.Info("Migrating key info", "public-key", hex.EncodeToString(item.Key[:4]))
			_, ok := addrs[string(item.Key)]
			if ok {
				return "", fmt.Errorf("already have %x", item.Key)
			}

			info := new(api.KeyInfo)
			if !tryUnmarshal(&err, info, item.Value, "decode key info: %w") {
				continue
			}
			addrs[string(item.Key)] = &model.Address{
				Type:       info.Type,
				Derivation: info.Derivation,
				WalletId:   info.WalletId,
				PublicKey:  item.Key,
			}
		}

		// Private keys
		for _, item := range readBucket(&err, wallet, oldBucket.Keys).KeyValueList {
			slog.Info("Migrating private key", "public-key", hex.EncodeToString(item.Key[:4]))
			k, ok := addrs[string(item.Key)]
			if !ok {
				return "", fmt.Errorf("have private key for %x but no key info", item.Key)
			}
			if len(item.Value) == 0 {
				continue // Don't migrate empty private keys
			}
			store(&err, m.PrivateKey(k.LiteIdentity()).Put, item.Value)
		}

		// Labels
		for _, item := range readBucket(&err, wallet, oldBucket.Label).KeyValueList {
			slog.Info("Migrating label", "public-key", hex.EncodeToString(item.Value[:4]), "label", string(item.Key))
			k, ok := addrs[string(item.Value)]
			if !ok {
				return "", fmt.Errorf("have a label for %x but no key info", item.Value)
			}
			k.AddLabel(string(item.Key))
		}

		// Store the addresses
		for _, addr := range addrs {
			store(&err, m.Address(addr.LiteIdentity()).Put, addr)
		}

		// Delete the old buckets
		deleteBucket(&err, wallet, oldBucket.KeyInfo)
		deleteBucket(&err, wallet, oldBucket.Keys)
		deleteBucket(&err, wallet, oldBucket.Label)
		deleteBucket(&err, wallet, oldBucket.Lite)

		if err != nil {
			return "", err
		}
	}

	//update wallet version
	err = wallet.Put(db.BucketConfig, []byte("version"), model.Version.Bytes())
	if err != nil {
		return "", err
	}

	return out, nil
}

func SanitizedCopy(dst, src db.DB) error {
	var err error

	// Copy all of the buckets
	tryCopyBucket(&err, dst, src, BucketAdi, nil)
	tryCopyBucket(&err, dst, src, BucketKeyCounter, nil)
	tryCopyBucket(&err, dst, src, BucketTransactionCache, nil)

	// And the model buckets (TODO move to model package)
	tryCopyBucket(&err, dst, src, modelBucket.Multi, nil)
	tryCopyBucket(&err, dst, src, modelBucket.Accounts, nil)
	tryCopyBucket(&err, dst, src, modelBucket.Address, nil)
	tryCopyBucket(&err, dst, src, modelBucket.Lookup, nil)

	tryCopyBucket(&err, dst, src, modelBucket.Config, func(v db.KeyValue) db.KeyValue {
		if string(v.Key) == "salt" {
			v.Value = nil
		}
		return v
	})

	// And the deprecated buckets
	tryCopyBucket(&err, dst, src, oldBucket.Anon, nil)
	tryCopyBucket(&err, dst, src, oldBucket.SigType, nil)
	tryCopyBucket(&err, dst, src, oldBucket.Index, nil)
	tryCopyBucket(&err, dst, src, oldBucket.Vaults, nil)
	tryCopyBucket(&err, dst, src, oldBucket.Lite, nil)
	tryCopyBucket(&err, dst, src, oldBucket.KeyInfo, nil)
	tryCopyBucket(&err, dst, src, oldBucket.Label, nil)

	// Overwrite sensitive data with zeros
	zero := func(v db.KeyValue) db.KeyValue { v.Value = make([]byte, len(v.Value)); return v }
	tryCopyBucket(&err, dst, src, BucketMnemonic, zero)
	tryCopyBucket(&err, dst, src, modelBucket.PrivateKey, zero)
	tryCopyBucket(&err, dst, src, oldBucket.Keys, zero)

	return err
}

func tryUnmarshal(last *error, v encoding.BinaryUnmarshaler, b []byte, format string, args ...any) bool {
	if *last != nil {
		return false
	}

	err := v.UnmarshalBinary(b)
	if err != nil {
		*last = fmt.Errorf(format, append(args, err)...)
		return false
	}

	return true
}

func readBucket(last *error, wallet db.DB, bucket []byte) *db.Bucket {
	if *last != nil {
		return new(db.Bucket)
	}
	b, err := wallet.GetBucket(bucket)
	switch {
	case err == nil:
		return b
	case !errors.Is(err, errors.NotFound):
		*last = fmt.Errorf("load %s bucket: %w", bucket, err)
	}
	return new(db.Bucket)
}

func deleteBucket(last *error, wallet db.DB, bucket []byte) {
	if *last != nil {
		return
	}
	err := wallet.DeleteBucket(bucket)
	if err != nil {
		*last = fmt.Errorf("delete %s bucket: %w", bucket, err)
	}
}

func tryCopyBucket(err *error, dst, src db.DB, bucket []byte, mutate func(db.KeyValue) db.KeyValue) {
	if *err != nil {
		return
	}

	*err = copyBucket(dst, src, bucket, mutate)
	if errors.Is(*err, db.ErrNoBucket) {
		*err = nil
	}
}

func copyBucket(dst db.DB, src db.DB, bucket []byte, mutate func(db.KeyValue) db.KeyValue) error {
	bucketData, err := src.GetBucket(bucket)
	if err != nil {
		return err
	}

	for _, v := range bucketData.KeyValueList {
		if mutate != nil {
			v = mutate(v)
			if v.Value == nil {
				continue
			}
		}

		err = dst.Put(bucket, v.Key, v.Value)
		if err != nil {
			return err
		}
	}

	// Verify that it worked
	if !equalBucket(dst, src, bucket, mutate) {
		return fmt.Errorf("copying failed: bucket %s not equal", bucket)
	}

	return nil
}

func equalBucket(dst db.DB, src db.DB, bucket []byte, mutate func(db.KeyValue) db.KeyValue) bool {
	bucketDataDst, errDst := dst.GetBucket(bucket)
	bucketDataSrc, errSrc := src.GetBucket(bucket)
	if errDst != nil && errSrc != nil {
		return errDst == db.ErrNoBucket
	}

	srcVals := map[string][]byte{}
	for _, v := range bucketDataSrc.KeyValueList {
		if mutate != nil {
			v = mutate(v)
			if v.Value == nil {
				continue
			}
		}
		srcVals[string(v.Key)] = v.Value
	}

	for _, v := range bucketDataDst.KeyValueList {
		src, ok := srcVals[string(v.Key)]
		if !ok {
			return false
		}
		delete(srcVals, string(v.Key))

		if !bytes.Equal(v.Value, src) {
			return false
		}
	}

	return len(srcVals) == 0
}

func importAccounts(wallet db.DB, req *api.Vault) (msg string, err error) {
	mnemonics := strings.Split(req.SeedInfo.Mnemonic, " ")
	err = importMnemonic(wallet, mnemonics)
	if err != nil {
		return "", fmt.Errorf("failed importing mnemonic: %s", err)
	}
	msg = "mnemonic import successful"

	//import the sig type derivation address count
	for _, v := range req.DerivationCounters {
		bucketKey := getKeyCountBucketKey(v.WalletId, v.Type)
		err = storeKeyIndex(wallet, bucketKey, uint32(v.Count))
		if err != nil {
			msg += fmt.Sprintf("error storing key index for key %s, %v", bucketKey, err)
			continue
		}
	}

	lup := map[string]*api.Key{}
	for i := range req.Keys {
		key := &req.Keys[i]
		if key.Labels == nil {
			key.Labels = new(api.KeyLabels)
		}
		lite := protocol.LiteAuthorityForKey(key.PublicKey, key.KeyInfo.Type)
		lup[string(key.PublicKey)] = key
		lup[lite.String()] = key
	}

	//only used for importing wallet <= 0.3.0
	for _, v := range req.KeyNames {
		k := lup[string(v.PublicKey)]
		if k == nil {
			msg += fmt.Sprintf("cannot map name %s: key %x does not exist\n", v.Name, v.PublicKey)
			continue
		}
		k.Labels.Names = append(k.Labels.Names, v.Name)
	}

	//only used for importing wallet <= 0.3.0
	for _, v := range req.LiteLabels {
		lite, err := url.Parse(v.LiteName)
		if err != nil {
			msg += err.Error() + "\n"
		}
		k := lup[lite.String()]
		if k == nil {
			msg += fmt.Sprintf("cannot map name %s: key %v does not exist\n", v.KeyName, lite)
			continue
		}
		k.Labels.Names = append(k.Labels.Names, v.KeyName)
	}

	for _, v := range req.Keys {
		err := (&Key{Key: v}).save(wallet, false)
		if err != nil {
			msg += fmt.Sprintf("failed to store key %x, %v\n", v.PublicKey, err)
		}
	}

	for _, adi := range req.Adis {
		if len(adi.Pages) == 0 {
			msg += fmt.Sprintf("skipping adi import, %s, missing key page\n", adi.Url.String())
			continue
		}

		//pages for adi in version 1 is just a key since we don't actually store keys mapped to pages yet
		if len(adi.Pages[0].KeyNames) == 0 {
			msg += fmt.Sprintf("skipping adi import, %s, missing key in page\n", adi.Url.String())
			continue
		}

		k, err := lookupByLabel(wallet, adi.Pages[0].KeyNames[0])
		if err != nil {
			msg += fmt.Sprintf("skipping adi import, %s, cannot find key for name %s\n", adi.Url.String(), adi.Pages[0].KeyNames[0])
			continue
		}

		err = wallet.Put(BucketAdi, []byte(adi.Url.ShortString()), k.PublicKey)
		if err != nil {
			msg += fmt.Sprintf("skipping adi import for %s with key %s DB error: %v\n", adi.Url.String(), adi.Pages[0].KeyNames[0], err)
		}
	}

	version := db.NewVersion(int(req.Version.Commit), int(req.Version.Major), int(req.Version.Minor), int(req.Version.Revision))
	err = wallet.Put(db.BucketConfig, []byte("version"), version.Bytes())
	if err != nil {
		msg += fmt.Sprintf("failed to store version info %v\n", err)
	}

	out, err := RestoreAccounts(wallet)
	msg += out
	if err != nil {
		return msg, err
	}
	return msg, nil
}
