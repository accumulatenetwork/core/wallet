package services_test

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"math/big"
	"testing"
	"time"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	. "gitlab.com/accumulatenetwork/core/wallet/internal/daemon/services"
	db "gitlab.com/accumulatenetwork/core/wallet/internal/database"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

func callOk[Req, Res any](t *testing.T, fn func(context.Context, *Req) (Res, error), req Req) Res {
	t.Helper()
	res, err := fn(context.Background(), &req)
	require.NoError(t, err)
	return res
}

func TestEncryption(t *testing.T) {
	// Set up an encrypted vault
	w, wm := DaemonForTest(t, UseMemDB)
	_, err := w.CreateWallet(context.Background(), &api.CreateWalletRequest{
		Path:       wm.Wallet("").Directory(),
		Passphrase: "foo",
		Token:      TestToken[:],
	})
	require.NoError(t, err)

	// Open directly
	m, err := wm.Wallet("").Index().OpenRaw(TestToken[:])
	require.NoError(t, err)

	// This must fail
	err = m.Config().Magic().Get()
	require.Error(t, err)
	require.ErrorIs(t, err, db.ErrInvalidPassword)
}

func TestValidate(t *testing.T) {
	passphrase := "foo"
	w, wm := DaemonForTest(t)

	// Get version
	_, err := w.Version(context.Background())
	require.NoError(t, err)

	//*
	// https://gitlab.com/accumulatenetwork/sdk/test-data/-/blob/main/protocol.1.json
	testJson := json.RawMessage(`{ "signatures": [ { "type": "legacyED25519", "timestamp": 255288238, "publicKey": "79b2e21740c5dc73a59f580c6390cb93aebe58e158a4856264e0ba534f75bd05", "signature": "fa7668a581fc19b0ddb16b132e72c57e7029c5574b235c748a6c6ca7df40d747a8a9556c0f5e60c3ab464fd8ad73b8fcd7101df14b537e191c5e632a3a877301", "signer": "acc://lite-token-account.acme/ACME", "signerVersion": 1, "transactionHash": "c1e3541ab8ef58598131f8d2fc31af7bc45a977154482212dae3250df11fa730" } ], "transaction": [ { "header": { "principal": "acc://lite-token-account.acme/ACME", "initiator": "039b4ae4578e8c78ae2b1b316da136de40c63f6be3c480eeabb5091bbe09d225" }, "body": { "type": "createIdentity", "url": "acc://adi.acme", "keyHash": "79b2e21740c5dc73a59f580c6390cb93aebe58e158a4856264e0ba534f75bd05" } } ] }`)
	testBin, err := hex.DecodeString(`01b201010102aec7dd79032079b2e21740c5dc73a59f580c6390cb93aebe58e158a4856264e0ba534f75bd050440fa7668a581fc19b0ddb16b132e72c57e7029c5574b235c748a6c6ca7df40d747a8a9556c0f5e60c3ab464fd8ad73b8fcd7101df14b537e191c5e632a3a87730105226163633a2f2f6c6974652d746f6b656e2d6163636f756e742e61636d652f41434d45060108c1e3541ab8ef58598131f8d2fc31af7bc45a977154482212dae3250df11fa730037d014501226163633a2f2f6c6974652d746f6b656e2d6163636f756e742e61636d652f41434d4502039b4ae4578e8c78ae2b1b316da136de40c63f6be3c480eeabb5091bbe09d22502340101020e6163633a2f2f6164692e61636d65032079b2e21740c5dc73a59f580c6390cb93aebe58e158a4856264e0ba534f75bd05`)
	require.NoError(t, err)

	// Encode Transaction (no wallet required)
	res := callOk(t, w.Encode, api.EncodeRequest{
		DataJson: testJson,
	})
	encoded := new(api.EncodeResponse)
	require.NoError(t, Remarshal(res, encoded))
	require.Equal(t, testBin, encoded.DataBinary, "encoded result should match")

	// Normalize
	res = nil
	require.NoError(t, json.Unmarshal(testJson, &res))
	testJson, err = json.MarshalIndent(res, "", "  ")
	require.NoError(t, err)

	// Decode Transaction
	decoded := callOk(t, w.Decode, api.DecodeRequest{
		DataBinary: testBin,
	})
	res = nil
	require.NoError(t, json.Unmarshal(decoded.DataJson, &res))
	actual, err := json.MarshalIndent(res, "", "  ")
	require.NoError(t, err)
	require.Equal(t, string(testJson), string(actual), "decoded result should match")
	//*/

	// Create a wallet
	callOk(t, w.CreateWallet, api.CreateWalletRequest{
		Path:       wm.Wallet("").Directory(),
		Token:      TestToken[:],
		Passphrase: passphrase,
	})

	// Lock
	callOk(t, w.LockVault, api.LockVaultRequest{
		Token: TestToken[:],
		Close: true,
	})

	// Verify wallet is locked
	_, err = w.KeyList(context.Background(), &api.KeyListRequest{
		Token: TestToken[:],
	})
	require.Error(t, err)
	require.ErrorIs(t, err, errors.NoPassword)

	// Unlock
	callOk(t, w.UnlockVault, api.UnlockVaultRequest{
		Passphrase: passphrase,
		TTL:        time.Hour,
		Token:      TestToken[:],
	})

	// Import mnemonic
	callOk(t, w.ImportMnemonic, api.ImportMnemonicRequest{
		Token:    TestToken[:],
		Mnemonic: []string{"yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow", "yellow"},
	})

	// Generate key
	callOk(t, w.GenerateAddress, api.GenerateAddressRequest{
		Token:  TestToken[:],
		Labels: []string{"keytest"},
	})

	// Reload the wallet - fully close the wallet to ensure it will be reloaded
	// from scratch
	callOk(t, w.LockVault, api.LockVaultRequest{
		Token: TestToken[:],
		Close: true,
	})
	callOk(t, w.UnlockVault, api.UnlockVaultRequest{
		Passphrase: passphrase,
		TTL:        time.Hour,
		Token:      TestToken[:],
	})

	// List keys
	keys := callOk(t, w.KeyList, api.KeyListRequest{
		Token: TestToken[:],
	})
	require.NotEmpty(t, keys.KeyList, "list-keys should include the generated key")
	require.NotEmpty(t, keys.KeyList[0].Labels.Names, "list-keys should include the generated key")
	require.Equal(t, "keytest", keys.KeyList[0].Labels.Names[0], "list-keys should include the generated key")

	// Register ADI
	callOk(t, w.RegisterADI, api.RegisterADIRequest{
		Token: TestToken[:],
		ADI:   url.MustParse("RedWagon.acme"),
	})

	// Get ADI List
	adiList := callOk(t, w.AdiList, api.AdiListRequest{
		Token: TestToken[:],
	})
	require.NotEmpty(t, adiList.ADIs, "adi-list should include the registered ADI")
	require.Equal(t, "acc://RedWagon.acme", adiList.ADIs[0].Url.String(), "adi-list should include the registered ADI")

	// Create New Transaction
	callOk(t, w.CreateTransaction, api.CreateTransactionRequest{
		Token:     TestToken[:],
		Name:      "test-tx8",
		Principal: url.MustParse("test"),
	})

	// Add output to Transaction
	callOk(t, w.AddSendTokensOutput, api.AddSendTokensOutputRequest{
		Token: TestToken[:],
		Name:  "test-tx8",
		Recipient: &protocol.TokenRecipient{
			Url:    url.MustParse("test/token"),
			Amount: *big.NewInt(100),
		},
	})

	// Sign Transaction
	signTxn := callOk(t, w.SignTransaction, api.SignTransactionRequest{
		Token:         TestToken[:],
		Name:          "test-tx8",
		KeyName:       "keytest",
		Signer:        url.MustParse("test/book/1"),
		SignerVersion: 1,
	})
	require.NotEmpty(t, signTxn.Transaction, "the envelope should include one transaction")
	require.NotEmpty(t, signTxn.Signature, "the envelope should include one signature")
	sig := signTxn.Signature.(*protocol.ED25519Signature)

	// Get Transaction
	callOk(t, w.GetTransaction, api.GetTransactionRequest{
		Token: TestToken[:],
		Name:  "test-tx8",
	})

	// List Transactions
	listTxn := callOk(t, w.ListTransactions, api.ListTransactionsRequest{
		Token: TestToken[:],
	})
	require.NotEmpty(t, listTxn.Names, "list-transactions should include the created transaction")
	require.Equal(t, "test-tx8", listTxn.Names[0], "list-transactions should include the created transaction")

	// Delete Transaction
	callOk(t, w.DeleteTransaction, api.DeleteTransactionRequest{
		Token: TestToken[:],
		Name:  "test-tx8",
	})

	// No Transactions should be listed
	listTxn = callOk(t, w.ListTransactions, api.ListTransactionsRequest{
		Token: TestToken[:],
	})
	require.Empty(t, listTxn, "list-transactions should return an empty list")

	// Reset the key's timestamp to avoid incrementing it
	callOk(t, w.ResetLastUsedOn, api.ResetLastUsedOnRequest{
		Token: TestToken[:],
		Key:   "keytest",
	})

	// Sign message
	signMsg := callOk(t, w.SignMessage, api.SignMessageRequest{
		Token:         TestToken[:],
		KeyName:       "keytest",
		SignerVersion: 1,
		Timestamp:     sig.Timestamp,
		Signer:        sig.Signer,
		Message:       sig.TransactionHash[:],
		IsHash:        true,
	})
	sig2 := signMsg.Signature.(*protocol.ED25519Signature)
	require.Equal(t, sig.Signature, sig2.Signature, "expected signature does not match sign-message signature")

	// Export vault
	vault := callOk(t, w.ExportVault, api.ExportVaultRequest{
		Token: TestToken[:],
	})
	expect := "yellow yellow yellow yellow yellow yellow yellow yellow yellow yellow yellow yellow"
	require.Equal(t, expect, vault.Vault.SeedInfo.Mnemonic, "export-vault should export the correct mnemonic")
}

// Remarshal uses mapstructure to convert a generic JSON-decoded map into a struct.
func Remarshal(src interface{}, dst interface{}) error {
	data, err := json.Marshal(src)
	if err != nil {
		return err
	}
	return json.Unmarshal(data, dst)
}
