package errors

import (
	"errors"

	coreerr "gitlab.com/accumulatenetwork/accumulate/pkg/errors"
)

// As calls stdlib errors.As.
func As(err error, target interface{}) bool { return errors.As(err, target) }

// Is calls stdlib errors.Is.
func Is(err, target error) bool {
	// If the target is accumulate/pkg/errors.NotFound, check the error against
	// wallet/internal/errors.NotFound. This is a hack to deal with the fact
	// that the wallet has its own error codes but needs to treat both versions
	// of NotFound the same.
	if errors.Is(target, NotFound) && errors.Is(err, coreerr.NotFound) {
		return true
	}

	return errors.Is(err, target)
}

// Unwrap calls stdlib errors.Unwrap.
func Unwrap(err error) error { return errors.Unwrap(err) }

// Code returns the status code if the error is an [Error], or 0.
func Code(err error) Status {
	var err2 *Error
	if !As(err, &err2) {
		return 0
	}
	for err2.Code == UnknownError && err2.Cause != nil {
		err2 = err2.Cause
	}
	return err2.Code
}
