package errors

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/pkg/database"
	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
)

func TestNotFound(t *testing.T) {
	// database.NotFoundError ≃ wallet/internal/errors.NotFound
	require.ErrorIs(t, (*database.NotFoundError)(database.NewKey()), NotFound)

	// wallet/internal/errors.NotFound ≃ accumulate/pkg/errors.NotFound
	require.ErrorIs(t, NotFound, errors.NotFound)

	// accumulate/pkg/errors.NotFound ≃ wallet/internal/errors.NotFound, when using [Is].
	require.True(t, Is(errors.NotFound.With("foo"), NotFound))
}
