package errors

import "gitlab.com/accumulatenetwork/accumulate/pkg/errors"

//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-enum --package errors --short-names status.yml

type Error = errors.ErrorBase[Status]

// Status is a request status code.
type Status uint64

func (s Status) IsKnownError() bool { return s != UnknownError && s != 0 }

// Error implements error.
func (s Status) Error() string { return s.String() }

// Skip skips N frames when locating the call site.
func (s Status) Skip(n int) errors.Factory[Status] {
	return errors.Factory[Status]{Skip: n, Code: s, UnknownCode: UnknownError, EncodingCode: EncodingError}
}

func (s Status) Wrap(err error) error {
	return s.Skip(1).Wrap(err)
}

func (s Status) With(v ...interface{}) *errors.ErrorBase[Status] {
	return s.Skip(1).With(v...)
}

func (s Status) WithFormat(format string, args ...interface{}) *errors.ErrorBase[Status] {
	return s.Skip(1).WithFormat(format, args...)
}

func (s Status) WithCauseAndFormat(cause error, format string, args ...interface{}) *errors.ErrorBase[Status] {
	return s.Skip(1).WithCauseAndFormat(cause, format, args...)
}

func (s Status) Is(target error) bool {
	// If the other error is the same status -> true
	var t Status
	if As(target, &t) && t == s {
		return true
	}

	// If the other error is the core repo's not found and this error is not
	// found -> true
	if s == NotFound && Is(target, errors.NotFound) {
		return true
	}

	return false
}
