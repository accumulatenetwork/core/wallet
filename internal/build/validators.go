// Copyright 2022 The Accumulate Authors
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file or at
// https://opensource.org/licenses/MIT.

package build

import (
	"fmt"
	"strings"

	"gitlab.com/accumulatenetwork/accumulate/pkg/types/address"
	"gitlab.com/accumulatenetwork/accumulate/pkg/types/network"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
)

func AddOperator(values *network.GlobalValues, operatorCount int, newKey address.Address) (*protocol.Transaction, error) {
	hash, ok := newKey.GetPublicKeyHash()
	if !ok {
		return nil, fmt.Errorf("unknown public key hash: %v", newKey)
	}

	// Add the key hash to the page and update the threshold
	addKey := new(protocol.AddKeyOperation)
	addKey.Entry.KeyHash = hash
	setThreshold := new(protocol.SetThresholdKeyPageOperation)
	setThreshold.Threshold = values.Globals.OperatorAcceptThreshold.Threshold(operatorCount + 1)
	updatePage := new(protocol.UpdateKeyPage)
	updatePage.Operation = []protocol.KeyPageOperation{addKey, setThreshold}
	return initiateTransaction(protocol.DnUrl().JoinPath(protocol.Operators, "1"), updatePage)
}

func AddValidator(values *network.GlobalValues, operatorCount int, newKey address.Address, partition string, active bool) (*protocol.Transaction, error) {
	pubKey, ok := newKey.GetPublicKey()
	if !ok {
		return nil, fmt.Errorf("unknown public key: %v", newKey)
	}

	// Add the key to the network definition
	values.Network.AddValidator(pubKey, partition, active)
	return updateNetworkDefinition(values)
}

func RemoveOperator(values *network.GlobalValues, operatorCount int, oldKey address.Address) (*protocol.Transaction, error) {
	hash, ok := oldKey.GetPublicKeyHash()
	if !ok {
		return nil, fmt.Errorf("unknown public key hash: %v", oldKey)
	}

	// Remove the key hash from the page and update the threshold
	removeKey := new(protocol.RemoveKeyOperation)
	removeKey.Entry.KeyHash = hash
	setThreshold := new(protocol.SetThresholdKeyPageOperation)
	setThreshold.Threshold = values.Globals.OperatorAcceptThreshold.Threshold(operatorCount - 1)
	updatePage := new(protocol.UpdateKeyPage)
	updatePage.Operation = []protocol.KeyPageOperation{removeKey, setThreshold}
	return initiateTransaction(protocol.DnUrl().JoinPath(protocol.Operators, "1"), updatePage)
}

func RemoveValidatorFrom(values *network.GlobalValues, operatorCount int, oldKey address.Address, partition string) (*protocol.Transaction, error) {
	hash, ok := oldKey.GetPublicKeyHash()
	if !ok {
		return nil, fmt.Errorf("unknown public key hash: %v", oldKey)
	}
	// Remove the key from the network definition
	_, v, ok := values.Network.ValidatorByHash(hash)
	if !ok {
		return nil, errors.NotFound.WithFormat("validator %x not found", hash[:4])
	}
	for i, p := range v.Partitions {
		if strings.EqualFold(p.ID, partition) {
			copy(v.Partitions[i:], v.Partitions[i+1:])
			v.Partitions = v.Partitions[:len(v.Partitions)-1]
			return updateNetworkDefinition(values)
		}
	}
	return nil, errors.NotFound.WithFormat("validator %x not present on %s", hash[:4], partition)
}

func UpdateOperatorKey(oldKey, newKey address.Address) (*protocol.Transaction, error) {
	oldKeyHash, ok := oldKey.GetPublicKeyHash()
	if !ok {
		return nil, fmt.Errorf("unknown public key hash: %v", oldKey)
	}
	newKeyHash, ok := newKey.GetPublicKeyHash()
	if !ok {
		return nil, fmt.Errorf("unknown public key hash: %v", newKey)
	}
	// Update the key hash
	updateKey := new(protocol.UpdateKeyOperation)
	updateKey.OldEntry.KeyHash = oldKeyHash
	updateKey.NewEntry.KeyHash = newKeyHash
	updatePage := new(protocol.UpdateKeyPage)
	updatePage.Operation = []protocol.KeyPageOperation{updateKey}
	return initiateTransaction(protocol.DnUrl().JoinPath(protocol.Operators, "1"), updatePage)
}

func UpdateValidatorKey(values *network.GlobalValues, oldKey, newKey address.Address) (*protocol.Transaction, error) {
	oldPubKey, ok := oldKey.GetPublicKey()
	if !ok {
		return nil, fmt.Errorf("unknown public key: %v", oldKey)
	}
	newPubKey, ok := newKey.GetPublicKey()
	if !ok {
		return nil, fmt.Errorf("unknown public key: %v", newKey)
	}

	// Update the key in the network
	err := values.Network.UpdateValidatorKey(oldPubKey, newPubKey)
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}
	return updateNetworkDefinition(values)
}

func UpdateValidatorName(values *network.GlobalValues, key address.Address, name *url.URL) (*protocol.Transaction, error) {
	hash, ok := key.GetPublicKeyHash()
	if !ok {
		return nil, fmt.Errorf("unknown public key hash: %v", key)
	}

	// Update the key in the network
	_, v, ok := values.Network.ValidatorByHash(hash)
	if !ok {
		return nil, errors.NotFound.WithFormat("validator %x not found", hash[:4])
	}
	v.Operator = name
	return updateNetworkDefinition(values)
}

func updateNetworkDefinition(values *network.GlobalValues) (*protocol.Transaction, error) {
	values.Network.Version++
	writeData := new(protocol.WriteData)
	writeData.WriteToState = true
	writeData.Entry = &protocol.DoubleHashDataEntry{
		Data: values.FormatNetwork().GetData(),
	}
	env, err := initiateTransaction(protocol.DnUrl().JoinPath(protocol.Network), writeData)
	if err != nil {
		return nil, errors.UnknownError.Wrap(err)
	}

	return env, nil
}

func initiateTransaction(principal *url.URL, body protocol.TransactionBody) (*protocol.Transaction, error) {
	txn := new(protocol.Transaction)
	txn.Header.Principal = principal
	txn.Body = body
	return txn, nil
}
