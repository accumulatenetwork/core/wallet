package staking

import (
	"encoding/json"
	"sort"

	"gitlab.com/accumulatenetwork/accumulate/pkg/errors"
)

func FormatActions(actions ...Action) ([][]byte, error) {
	var parts [][]byte
	for _, act := range actions {
		parts = append(parts, []byte(act.ActionType().String()))

		// This is hacky but it avoids having to write a bunch of custom
		// marshalling code
		b, err := json.Marshal(act)
		if err != nil {
			return nil, errors.InternalError.WithFormat("cannot encode action")
		}

		// All of the (current) action fields are marshalled as strings
		// (including bigints)
		var fields map[string]string
		err = json.Unmarshal(b, &fields)
		if err != nil {
			return nil, errors.InternalError.WithFormat("cannot decode fields")
		}
		keys := make([]string, 0, len(fields)-1)
		for k := range fields {
			if k != "actionType" {
				keys = append(keys, k)
			}
		}
		sort.Strings(keys)

		for _, k := range keys {
			parts = append(parts, []byte(k+"="+fields[k]))
		}
	}

	return parts, nil
}
