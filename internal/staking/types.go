package staking

//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-enum --package staking --out types_enums_gen.go types_enums.yml
//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-types --package staking --out types_types_gen.go types_types.yml

//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-enum --package staking --out requests_enums_gen.go requests_enums.yml
//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-types --package staking --long-union-discriminator --out requests_types_gen.go requests_types.yml
//go:generate go run gitlab.com/accumulatenetwork/accumulate/tools/cmd/gen-types --package staking --long-union-discriminator --language go-union --out requests_unions_gen.go requests_types.yml

// AccountType is the stake type of a staking account.
type AccountType uint64

// AcceptingDelegates indicates if a full staker is accepting delegates.
type AcceptingDelegates uint64

// Status is the status of a staking account.
type Status uint64

// ActionType is the type of an [Action].
type ActionType uint64

type Action interface {
	CopyAsInterface() any
	ActionType() ActionType
}
