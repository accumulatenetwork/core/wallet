package util

import (
	"bytes"
	"io"
	"net/http"
)

/*
 * Copied from Accumulate's testing code
 *
 * TODO Use an API v3 style interface to avoid this
 */

func DirectHttpTransport(handler http.Handler) http.RoundTripper {
	return transportFunc(func(req *http.Request) (*http.Response, error) {
		wr := new(httpResponseWriter)
		wr.resp.Request = req
		wr.resp.Header = http.Header{}
		wr.resp.Body = io.NopCloser(&wr.body)
		handler.ServeHTTP(wr, req)
		wr.resp.ContentLength = int64(wr.body.Len())
		return &wr.resp, nil
	})
}

type transportFunc func(*http.Request) (*http.Response, error)

func (t transportFunc) RoundTrip(req *http.Request) (*http.Response, error) { return t(req) }

type httpResponseWriter struct {
	resp        http.Response
	body        bytes.Buffer
	wroteHeader bool
}

func (w *httpResponseWriter) Header() http.Header {
	return w.resp.Header
}

func (w *httpResponseWriter) WriteHeader(statusCode int) {
	w.resp.StatusCode = statusCode
	w.wroteHeader = true
}

func (w *httpResponseWriter) Write(b []byte) (int, error) {
	if !w.wroteHeader {
		w.WriteHeader(http.StatusOK)
	}

	return w.body.Write(b)
}
