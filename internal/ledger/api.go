package ledger

import (
	"bytes"
	"fmt"

	"gitlab.com/accumulatenetwork/accumulate/pkg/types/address"
	"gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/api"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/bip44"
	"gitlab.com/accumulatenetwork/core/wallet/internal/errors"
	"gitlab.com/accumulatenetwork/core/wallet/internal/ledger/accounts"
	"gitlab.com/accumulatenetwork/core/wallet/internal/ledger/usbwallet"
)

var Debug bool

type API struct {
	hub *usbwallet.Hub
}

func NewAPI() (*API, error) {
	hub, err := usbwallet.NewLedgerHub(Debug)
	if err != nil {
		return nil, err
	}

	return &API{
		hub: hub,
	}, nil
}

func (la *API) Wallets() ([]*api.LedgerWalletInfo, error) {
	var resp []*api.LedgerWalletInfo
	for _, wallet := range la.hub.Wallets() {
		info, err := la.queryLedgerInfo(wallet)
		if err != nil {
			return nil, err
		}
		resp = append(resp, info)
	}
	return resp, nil
}

func (la *API) queryLedgerInfo(wallet accounts.Wallet) (*api.LedgerWalletInfo, error) {
	walletOpen := false
	status := "ok"
	err := wallet.Open("")
	if err == nil {
		walletOpen = true
	} else {
		status = err.Error()
	}

	defer func() {
		if walletOpen {
			_ = wallet.Close()
		}
	}()

	info := wallet.Info()
	ledgerInfo := &api.LedgerWalletInfo{
		WalletID: info.WalletID,
		Version: api.LedgerVersion{
			Label: fmt.Sprintf("%d.%d.%d", info.AppVersion.Major, info.AppVersion.Minor, info.AppVersion.Patch),
			Major: uint64(info.AppVersion.Major),
			Minor: uint64(info.AppVersion.Minor),
			Patch: uint64(info.AppVersion.Patch),
		},
		VendorID:     uint64(info.DeviceInfo.VendorID),
		Manufacturer: info.DeviceInfo.Manufacturer,
		ProductID:    uint64(info.DeviceInfo.ProductID),
		Product:      info.DeviceInfo.Product,
		Status:       status,
	}
	return ledgerInfo, nil
}

func (la *API) Derive(id *url.URL, path bip44.Derivation, pin, display bool, labels ...string) (address.Address, error) {
	wallet, err := la.findWallet(id)
	if err != nil {
		return nil, err
	}

	err = wallet.Open("")
	if err != nil {
		return nil, err
	}
	defer func() { _ = wallet.Close() }()

	account, err := wallet.Derive(path, false, true, labels...)
	if err != nil {
		return nil, fmt.Errorf("derive function failed on ledger wallet: %v", err)
	}

	return &address.PublicKey{
		Key:  account.PubKey,
		Type: account.SignatureType,
	}, nil
}

func (la *API) Sign(id *url.URL, path bip44.Derivation, txn *protocol.Transaction, sig protocol.Signature) (protocol.Signature, error) {
	wallet, err := la.findWallet(id)
	if err != nil {
		return nil, err
	}

	err = wallet.Open("")
	if err != nil {
		return nil, err
	}
	defer func() {
		_ = wallet.Close()
	}()

	account, err := wallet.Derive(path, true, false)
	if err != nil {
		return nil, err
	}

	var pubKey []byte
	switch sig := sig.(type) {
	case protocol.KeySignature:
		pubKey = sig.GetPublicKey()
	default:
		return nil, fmt.Errorf("cannot sign using a %T, not supported", sig)
	}
	if !bytes.Equal(account.PubKey, pubKey) {
		return nil, fmt.Errorf("the request key is was not found in the wallet (the wallet ID is %s)", id)
	}

	return wallet.SignTx(&account, txn, sig)
}

func (la *API) findWallet(id *url.URL) (accounts.Wallet, error) {
	for _, wallet := range la.hub.Wallets() {
		wid, err := wallet.WalletID()
		if err != nil {
			return nil, err
		}
		if wid.Equal(id) {
			return wallet, nil
		}
	}
	return nil, errors.NotFound.WithFormat("cannot find Ledger %v", id)
}
