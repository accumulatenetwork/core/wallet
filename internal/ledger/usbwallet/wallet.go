// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

// Package usbwallet implements support for USB hardware wallets.
package usbwallet

import (
	"crypto/sha256"
	"io"
	"log"
	"net/url"
	"sync"
	"time"

	"github.com/karalabe/usb"
	url2 "gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/bip44"
	"gitlab.com/accumulatenetwork/core/wallet/internal/ledger/accounts"
)

// Maximum time between wallet health checks to detect USB unplugs.
const heartbeatCycle = time.Second

// Minimum time to wait between self derivation attempts, even it the user is
// requesting accounts like crazy.
//const selfDeriveThrottling = time.Second

// driver defines the vendor specific functionality hardware wallets instances
// must implement to allow using them with the wallet lifecycle management.
type driver interface {
	// Status returns a textual status to aid the user in the current state of the
	// wallet. It also returns an error indicating any failure the wallet might have
	// encountered.
	Status() (string, error)

	Version() accounts.Version

	// Open initializes access to a wallet instance. The passphrase parameter may
	// or may not be used by the implementation of a particular wallet instance.
	Open(device io.ReadWriter, passphrase string) error

	// Close releases any resources held by an open wallet instance.
	Close() error

	// Heartbeat performs a sanity check against the hardware wallet to see if it
	// is still online and healthy.
	Heartbeat() error

	// Derive sends a derivation request to the USB device and returns the public key on that path and key name
	Derive(path bip44.Derivation, display bool, alias ...string) ([]byte, string, [32]byte, error)

	// SignTx sends the transaction to the USB device and waits for the user to confirm
	// or deny the transaction.
	SignTx(path bip44.Derivation, tx *protocol.Transaction, signer protocol.Signature) (protocol.Signature, error)

	SignTypedMessage(path bip44.Derivation, messageHash []byte, domainHash []byte) ([]byte, error)
}

// wallet represents the common functionality shared by all USB hardware
// wallets to prevent reimplementing the same complex maintenance mechanisms
// for different vendors.
type wallet struct {
	hub    *Hub     // USB hub scanning
	driver driver   // Hardware implementation of the low level device operations
	url    *url.URL // Textual URL uniquely identifying this wallet

	info     usb.DeviceInfo // Known USB device infos about the wallet
	device   usb.Device     // USB device advertising itself as a hardware wallet
	walletID *url2.URL

	accounts []accounts.Account            // List of derive accounts pinned on the hardware wallet
	paths    map[[32]byte]bip44.Derivation // Known derivation paths for signing operations

	deriveNextPaths []bip44.Derivation // Next derivation paths for account auto-discovery (multiple bases supported)
	deriveNextAddrs []url2.URL         // Next derived account addresses for auto-discovery (multiple bases supported)
	deriveReq       chan chan struct{} // Channel to request a self-derivation on
	deriveQuit      chan chan error    // Channel to terminate the self-deriver with

	healthQuit chan chan error

	// Locking a hardware wallet is a bit special. Since hardware devices are lower
	// performing, any communication with them might take a non negligible amount of
	// time. Worse still, waiting for user confirmation can take arbitrarily long,
	// but exclusive communication must be upheld during. Locking the entire wallet
	// in the mean time however would stall any parts of the system that don't want
	// to communicate, just read some state (e.g. list the accounts).
	//
	// As such, a hardware wallet needs two locks to function correctly. A state
	// lock can be used to protect the wallet's software-side internal state, which
	// must not be held exclusively during hardware communication. A communication
	// lock can be used to achieve exclusive access to the device itself, this one
	// however should allow "skipping" waiting for operations that might want to
	// use the device, but can live without too (e.g. account self-derivation).
	//
	// Since we have two locks, it's important to know how to properly use them:
	//   - Communication requires the `device` to not change, so obtaining the
	//     commsLock should be done after having a stateLock.
	//   - Communication must not disable read access to the wallet state, so it
	//     must only ever hold a *read* lock to stateLock.
	commsLock chan struct{} // Mutex (buf=1) for the USB comms without keeping the state locked

	stateLock sync.RWMutex // Protects read and write access to the wallet struct fields
	log       *log.Logger  // Contextual logger to tag the base with its id
}

func (w *wallet) Info() accounts.WalletInfo {
	wid, _ := w.WalletID()
	return accounts.WalletInfo{
		WalletID:   wid,
		AppVersion: w.driver.Version(),
		DeviceInfo: w.info,
	}
}

func (w *wallet) WalletID() (*url2.URL, error) {
	if w.walletID == nil {
		derivation, err := bip44.NewDerivationPath(protocol.SignatureTypeED25519)
		if err == nil {
			chain := derivation.Chain()
			if chain&0x80000000 == 0 {
				chain ^= 0x80000000
			}

			derivationZero := bip44.Derivation{derivation.Purpose(), derivation.CoinType(), derivation.Account(), chain, 0x80000000}
			accountZero, err := w.Derive(derivationZero, false, false)
			if err != nil {
				return nil, err
			}
			w.walletID = accountZero.LiteAccount
		}
	}
	return w.walletID, nil
}

// URL implements accounts.Wallet, returning the URL of the USB hardware device.
func (w *wallet) URL() url.URL {
	return *w.url // Immutable, no need for a lock
}

// Status implements accounts.Wallet, returning a custom status message from the
// underlying vendor-specific hardware wallet implementation.
func (w *wallet) Status() (string, error) {
	w.stateLock.RLock() // No device communication, state lock is enough
	defer w.stateLock.RUnlock()

	status, failure := w.driver.Status()
	if w.device == nil {
		return "Closed", failure
	}
	return status, failure
}

// Open implements accounts.Wallet, attempting to open a USB connection to the
// hardware wallet.
func (w *wallet) Open(passphrase string) error {
	w.stateLock.Lock() // State lock is enough since there's no connection yet at this point
	defer w.stateLock.Unlock()

	// If the device was already opened once, refuse to try again
	if w.paths != nil {
		return accounts.ErrWalletAlreadyOpen
	}
	// Make sure the actual device connection is done only once
	if w.device == nil {
		device, err := w.info.Open()
		if err != nil {
			return err
		}
		w.device = device
		w.commsLock = make(chan struct{}, 1)
		w.commsLock <- struct{}{} // Enable lock
	}
	// Delegate device initialization to the underlying driver
	if err := w.driver.Open(w.device, passphrase); err != nil {
		return err
	}
	// Connection successful, start life-cycle management
	w.paths = make(map[[32]byte]bip44.Derivation)

	w.deriveReq = make(chan chan struct{})
	w.deriveQuit = make(chan chan error)
	w.healthQuit = make(chan chan error)

	go w.heartbeat()
	//go w.selfDerive()

	// Notify anyone listening for wallet events that a new device is accessible
	//go w.hub.updateFeed.Send(accounts.WalletEvent{Wallet: w, Kind: accounts.WalletOpened})

	return nil
}

// heartbeat is a health check loop for the USB wallets to periodically verify
// whether they are still present or if they malfunctioned.
func (w *wallet) heartbeat() {
	w.log.Println("USB wallet health-check started")
	defer w.log.Println("USB wallet health-check stopped")

	// Execute heartbeat checks until termination or error
	var (
		errc chan error
		err  error
	)
	for errc == nil && err == nil {
		// Wait until termination is requested or the heartbeat cycle arrives
		select {
		case errc = <-w.healthQuit:
			// Termination requested
			continue
		case <-time.After(heartbeatCycle):
			// Heartbeat time
		}
		// Execute a tiny data exchange to see responsiveness
		w.stateLock.RLock()
		if w.device == nil {
			// Terminated while waiting for the lock
			w.stateLock.RUnlock()
			continue
		}
		<-w.commsLock // Don't lock state while resolving version
		err = w.driver.Heartbeat()
		w.commsLock <- struct{}{}
		w.stateLock.RUnlock()

		if err != nil {
			w.stateLock.Lock() // Lock state to tear the wallet down
			w.close()
			w.stateLock.Unlock()
		}
		// Ignore non hardware related errors
		err = nil
	}
	// In case of error, wait for termination
	if err != nil {
		w.log.Println("USB wallet health-check failed", "err", err)
		errc = <-w.healthQuit
	}
	errc <- err
}

// Close implements accounts.Wallet, closing the USB connection to the device.
func (w *wallet) Close() error {
	// Ensure the wallet was opened
	w.stateLock.RLock()
	hQuit, _ /*dQuit*/ := w.healthQuit, w.deriveQuit
	w.stateLock.RUnlock()

	// Terminate the health checks
	var herr error
	if hQuit != nil {
		errc := make(chan error)
		hQuit <- errc
		herr = <-errc // Save for later, we *must* close the USB
	}

	// FIXME selfDerive is commented out so this will never trigger
	// Terminate the self-derivations
	/*	var derr error
		if dQuit != nil {
			errc := make(chan error)
			dQuit <- errc
			derr = <-errc // Save for later, we *must* close the USB
		}
	*/

	// Terminate the device connection
	w.stateLock.Lock()
	defer w.stateLock.Unlock()

	w.healthQuit = nil
	w.deriveQuit = nil
	w.deriveReq = nil

	if err := w.close(); err != nil {
		return err
	}
	if herr != nil {
		return herr
	}
	return nil //derr  TODO revert after above is fixed
}

// close is the internal wallet closer that terminates the USB connection and
// resets all the fields to their defaults.
//
// Note, close assumes the state lock is held!
func (w *wallet) close() error {
	// Allow duplicate closes, especially for health-check failures
	if w.device == nil {
		return nil
	}
	// Close the device, clear everything, then return
	w.device.Close()
	w.device = nil

	w.accounts, w.paths = nil, nil
	return w.driver.Close()
}

// Keys implements accounts.Wallet, returning the list of lite accounts pinned to
// the USB hardware wallet. If self-derivation was enabled, the account list is
// periodically expanded based on current chain state.
func (w *wallet) Keys() []accounts.Account {
	// Attempt self-derivation if it's running
	var ret []accounts.Account
	for _, base := range w.deriveNextPaths {
		a, err := w.Derive(base, false, false)
		if err != nil {
			return nil
		}
		ret = append(ret, a)
	}
	return ret
}

// selfDerive is an account derivation loop that upon request attempts to find
// new non-zero accounts.
//func (w *wallet) selfDerive() {
//	//unsupported for accumulate
//	w.log.Println("selfDeivation for accumulate not supported")
//}

// Contains implements accounts.Wallet, returning whether a particular account is
// or is not pinned into this wallet instance. Although we could attempt to resolve
// unpinned accounts, that would be an non-negligible hardware operation.
func (w *wallet) Contains(account *accounts.Account) bool {
	w.stateLock.RLock()
	defer w.stateLock.RUnlock()

	_, exists := w.paths[account.LiteAccount.Hash32()]
	return exists
}

// Derive implements accounts.Wallet, deriving a new account at the specific
// derivation path. If pin is set to true, the account will be added to the list
// of tracked accounts. Will return the populated key and key info
// if display is set to true, then the key info will be displayed on device if applicable
func (w *wallet) Derive(path bip44.Derivation, pin bool, display bool, alias ...string) (accounts.Account, error) {
	// Try to derive the actual account and update its URL if successful
	w.stateLock.RLock() // Avoid device disappearing during derivation

	if w.device == nil {
		w.stateLock.RUnlock()
		return accounts.Account{}, accounts.ErrWalletClosed
	}
	<-w.commsLock // Avoid concurrent hardware access
	pubKey, keyName, chainCode, err := w.driver.Derive(path, display, alias...)
	_ = chainCode

	w.commsLock <- struct{}{}

	w.stateLock.RUnlock()

	// If an error occurred or no pinning was requested, return
	if err != nil {
		return accounts.Account{}, err
	}
	liteAccount := protocol.LiteAuthorityForKey(pubKey, path.SignatureType())
	account := accounts.Account{
		LiteAccount:   liteAccount,
		PubKey:        pubKey,
		SignatureType: path.SignatureType(),
		KeyName:       keyName,
	}

	if !pin {
		return account, nil
	}
	//11 Pinning needs to modify the state
	w.stateLock.Lock()
	defer w.stateLock.Unlock()

	if _, ok := w.paths[liteAccount.Hash32()]; !ok {
		w.accounts = append(w.accounts, account)
		w.paths[liteAccount.Hash32()] = make(bip44.Derivation, len(path))
		copy(w.paths[liteAccount.Hash32()], path)
	}
	return account, nil
}

// SelfDerive sets a base account derivation path from which the wallet attempts
// to discover non zero accounts and automatically add them to list of tracked
// accounts.
//
// Note, self derivation will increment the last component of the specified path
// opposed to descending into a child path to allow discovering accounts starting
// from non zero components.
//
// Some hardware wallets switched derivation paths through their evolution, so
// this method supports providing multiple bases to discover old user accounts
// too. Only the last base will be used to derive the next empty account.
//
// You can disable automatic account discovery by calling SelfDerive with a nil
// chain state reader.
func (w *wallet) SelfDerive(bases []bip44.Derivation) {
	w.stateLock.Lock()
	defer w.stateLock.Unlock()

	w.deriveNextPaths = make([]bip44.Derivation, len(bases))
	for i, base := range bases {
		w.deriveNextPaths[i] = make(bip44.Derivation, len(base))
		copy(w.deriveNextPaths[i][:], bases[i][:])
	}
	w.deriveNextAddrs = make([]url2.URL, len(bases))
}

// signHash implements accounts.Wallet, however signing arbitrary data is not
// supported for hardware wallets, so this method will always return an error.
func (w *wallet) signHash(account *accounts.Account, hash []byte) ([]byte, error) {
	_ = account
	_ = hash
	return nil, accounts.ErrNotSupported
}

// SignData signs sha256(data). The mimetype parameter describes the type of data being signed
func (w *wallet) SignData(account *accounts.Account, mimeType string, data []byte) ([]byte, error) {
	// Unless we are doing 712 signing, simply dispatch to signHash
	if !(mimeType == accounts.MimetypeTypedData && len(data) == 66 && data[0] == 0x19 && data[1] == 0x01) {
		h := sha256.Sum256(data)
		return w.signHash(account, h[:])
	}

	// dispatch to 712 signing if the mimetype is TypedData and the format matches
	w.stateLock.RLock() // Comms have own mutex, this is for the state fields
	defer w.stateLock.RUnlock()

	// If the wallet is closed, abort
	if w.device == nil {
		return nil, accounts.ErrWalletClosed
	}
	// Make sure the requested account is contained within
	path, ok := w.paths[account.LiteAccount.Hash32()]
	if !ok {
		return nil, accounts.ErrUnknownAccount
	}
	// All infos gathered and metadata checks out, request signing
	<-w.commsLock
	defer func() { w.commsLock <- struct{}{} }()

	// Ensure the device isn't screwed with while user confirmation is pending
	// TODO(karalabe): remove if hotplug lands on Windows
	w.hub.commsLock.Lock()
	w.hub.commsPend++
	w.hub.commsLock.Unlock()

	defer func() {
		w.hub.commsLock.Lock()
		w.hub.commsPend--
		w.hub.commsLock.Unlock()
	}()
	// Sign the transaction
	signature, err := w.driver.SignTypedMessage(path, data[2:34], data[34:66])
	if err != nil {
		return nil, err
	}
	return signature, nil
}

// SignDataWithPassphrase implements accounts.Wallet, attempting to sign the given
// data with the given account using passphrase as extra authentication.
// Since USB wallets don't rely on passphrases, these are silently ignored.
func (w *wallet) SignDataWithPassphrase(account *accounts.Account, passphrase, mimeType string, data []byte) ([]byte, error) {
	_ = passphrase
	return w.SignData(account, mimeType, data)
}

func (w *wallet) SignText(account *accounts.Account, text []byte) ([]byte, error) {
	return w.signHash(account, accounts.TextHash(text))
}

// SignTx implements accounts.Wallet. It sends the transaction over to the Ledger
// wallet to request a confirmation from the user. It returns either the signed
// transaction or a failure if the user denied the transaction.
//
// Note, if the version of the Ethereum application running on the Ledger wallet is
// too old to sign EIP-155 transactions, but such is requested nonetheless, an error
// will be returned opposed to silently signing in Homestead mode.
func (w *wallet) SignTx(account *accounts.Account, tx *protocol.Transaction, preSig protocol.Signature) (protocol.Signature, error) {
	w.stateLock.RLock() // Comms have own mutex, this is for the state fields
	defer w.stateLock.RUnlock()

	// If the wallet is closed, abort
	if w.device == nil {
		return nil, accounts.ErrWalletClosed
	}
	// Make sure the requested account is contained within
	path, ok := w.paths[account.LiteAccount.Hash32()]
	if !ok {
		return nil, accounts.ErrUnknownAccount
	}
	// All infos gathered and metadata checks out, request signing
	<-w.commsLock
	defer func() { w.commsLock <- struct{}{} }()

	// Ensure the device isn't screwed with while user confirmation is pending
	// TODO(karalabe): remove if hotplug lands on Windows
	w.hub.commsLock.Lock()
	w.hub.commsPend++
	w.hub.commsLock.Unlock()

	defer func() {
		w.hub.commsLock.Lock()
		w.hub.commsPend--
		w.hub.commsLock.Unlock()
	}()

	signed, err := w.driver.SignTx(path, tx, preSig)
	if err != nil {
		return nil, err
	}

	return signed, nil
}

// SignTextWithPassphrase implements accounts.Wallet, however signing arbitrary
// data is not supported for Ledger wallets, so this method will always return
// an error.
func (w *wallet) SignTextWithPassphrase(account *accounts.Account, passphrase string, text []byte) ([]byte, error) {
	_ = passphrase
	return w.SignText(account, accounts.TextHash(text))
}

// SignTxWithPassphrase implements accounts.Wallet, attempting to sign the given
// transaction with the given account using passphrase as extra authentication.
// Since USB wallets don't rely on passphrases, these are silently ignored.
func (w *wallet) SignTxWithPassphrase(account *accounts.Account, passphrase string, tx *protocol.Transaction, preSig protocol.Signature) (protocol.Signature, error) {
	_ = passphrase
	return w.SignTx(account, tx, preSig)
}
