// Copyright 2017 The go-ethereum Authors
// This file is part of the go-ethereum library.
//
// The go-ethereum library is free software: you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// The go-ethereum library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Lesser General Public License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with the go-ethereum library. If not, see <http://www.gnu.org/licenses/>.

// This file contains the implementation for interacting with the Ledger hardware
// wallets. The wire protocol spec can be found in the Ledger Blue GitHub repo:
// https://raw.githubusercontent.com/LedgerHQ/blue-app-eth/master/doc/ethapp.asc

package usbwallet

import (
	"crypto/ed25519"
	"encoding/binary"
	"errors"
	"fmt"
	"io"
	"log"

	"gitlab.com/accumulatenetwork/accumulate/pkg/types/messaging"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/bip44"
	"gitlab.com/accumulatenetwork/core/wallet/internal/ledger/accounts"
	"gitlab.com/accumulatenetwork/core/wallet/internal/ledger/common/hexutil"
	"gitlab.com/accumulatenetwork/core/wallet/internal/ledger/crypto"
)

// ledgerOpcode is an enumeration encoding the supported Ledger opcodes.
type ledgerOpcode byte

// ledgerParam1 is an enumeration encoding the supported Ledger parameters for
// specific opcodes. The same parameter values may be reused between opcodes.
type ledgerParam1 byte

// ledgerParam2 is an enumeration encoding the supported Ledger parameters for
// specific opcodes. The same parameter values may be reused between opcodes.
type ledgerParam2 byte

const (
	ledgerOpGetApplicationVersion ledgerOpcode = 0x03 // Returns the application version
	ledgerOpGetAppName            ledgerOpcode = 0x04 // Signs a transaction after having the user validate the parameters
	ledgerOpGetPublicKey          ledgerOpcode = 0x05 // Returns the public key and chainCode for a given BIP 32 path
	ledgerOpSignTransaction       ledgerOpcode = 0x06 // Returns specific wallet application configuration

	ledgerP1Display                 ledgerParam1 = 0x01
	ledgerP1DirectlyFetchAddress    ledgerParam1 = 0x00 // Return address directly from the wallet
	ledgerP1InitTypedMessageData    ledgerParam1 = 0x00 // First chunk of Typed Message data
	ledgerP1InitTransactionData     ledgerParam1 = 0x00 // First transaction data block for signing
	ledgerP1ContTransactionData     ledgerParam1 = 0x01 // Subsequent transaction data block for signing
	ledgerP2MoreTransactionData     ledgerParam2 = 0x80 // More data to follow for transaction data
	ledgerP2LastTransactionData     ledgerParam2 = 0x00 // The last  data to follow for transaction data
	ledgerP2DiscardAddressChainCode ledgerParam2 = 0x00 // Do not return the chain code along with the address
)

// errLedgerReplyInvalidHeader is the error message returned by a Ledger data exchange
// if the device replies with a mismatching header. This usually means the device
// is in browser mode.
var errLedgerReplyInvalidHeader = errors.New("ledger: invalid reply header")
var errLedgerReplyNoAppStarted = errors.New("error 6d02")
var errLedgerReplyNoAppStarted2 = errors.New("error 6511")
var errLedgerReplyDeviceLocked = errors.New("error 530c")
var errLedgerReplyDeviceLocked2 = errors.New("error 6b0c")
var errLedgerReplyDeviceLocked3 = errors.New("error 5515") // Technically "unknown error"?
var errLedgerReplyTxNotSupported = errors.New("error ac04")

var errLedgerReplyDeny = errors.New("error 6985")

/**
 * Status word for incorrect P1 or P2.
 */
var errLedgerReplyWrongP1P2 = errors.New("error 6a86")

/**
 * Status word for either wrong Lc or length of APDU command less than 5.
 */
var errLedgerReplyWrongDataLength = errors.New("error 6a87")

/**
 * Status word for unknown command with this INS.
 */
var errLedgerReplyInsNotSupported = errors.New("error 6d00")

/**
 * Status word for instruction class is different than CLA.
 */
var errLedgerReplyClaNotSupported = errors.New("error 6e00")

/**
 * Status word for wrong response length (buffer too small or too big).
 */
var errLedgerReplyWrongResponseLength = errors.New("error b000")

/**
 * Status word for fail to display BIP32 path.
 */
var errLedgerReplyDisplayBIP32PathFail = errors.New("error b001")

/**
 * Status word for fail to display address.
 */
var errLedgerReplyDisplayAddressFail = errors.New("error b002")

/**
 * Status word for fail to display amount.
 */
var errLedgerReplyDisplayAmountFail = errors.New("error b003")

/**
 * Status word for wrong transaction length.
 */
var errLedgerReplyWrongTXLength = errors.New("error b004")

/**
 * Status word for fail of transaction parsing.
 */
var errLedgerReplyTXParsingFail = errors.New("error b005")

/**
 * Status word for fail of transaction hash.
 */
var errLedgerReplyTXHashFail = errors.New("error b006")

/**
 * Status word for bad state.
 */
var errLedgerReplyBadState = errors.New("error b007")

/**
 * Status word for signature fail.
 */
var errLedgerReplySignatureFail = errors.New("error b008")

// accumulate transaction parsing errors
var errLedgerReplyUnknown = errors.New("error ac01")
var errLedgerReplyInvalidEnum = errors.New("error ac02")
var errLedgerReplyInvalidObject = errors.New("error ac03")
var errLedgerReplyNotImplemented = errors.New("error ac04")
var errLedgerReplyTypeNotFound = errors.New("error ac05")
var errLedgerReplyParameterNil = errors.New("error ac06")
var errLedgerReplyParameterInsufficientData = errors.New("error ac07")
var errLedgerReplyBadCopy = errors.New("error ac08")
var errLedgerReplyBufferTooSmall = errors.New("error ac09")
var errLedgerReplyVarIntRead = errors.New("error ac0a")
var errLedgerReplyVarIntWrite = errors.New("error ac0b")
var errLedgerReplyResizeRequred = errors.New("error ac0c")
var errLedgerReplyInvalidBigInt = errors.New("error ac0d")
var errLedgerReplyInvalidString = errors.New("error ac0e")
var errLedgerReplyInvalidHashParameters = errors.New("error ac0f")
var errLedgerReplyUVarIntRead = errors.New("error ac10")
var errLedgerReplyUVarIntWrite = errors.New("error ac11")
var errLedgerReplyMempoolFull = errors.New("error ac012")
var errLedgerReplyInvalidOffset = errors.New("error ac13")
var errLedgerReplyInvalidData = errors.New("error ac14")
var errLedgerReplyBadKey = errors.New("error ac15")
var errLedgerReplyInvalidField = errors.New("error ac16")
var errLedgerReplyExpectingType = errors.New("error ac17")

// errLedgerInvalidVersionReply is the error message returned by a Ledger version retrieval
// when a response does arrive, but it does not contain the expected data.
var errLedgerInvalidVersionReply = errors.New("ledger: invalid version reply")
var errLedgerInvalidAppNameReply = errors.New("ledger: invalid app name reply")
var errAccumulateAppNotStarted = errors.New("ledger: the Accumulate app is not started")
var errAccumulateAppDeviceLocked = errors.New("ledger: the device is locked")
var errAccumulateAppTxNotSupported = errors.New("ledger: the transaction type is not supported")

// ledgerDriver implements the communication with a Ledger hardware wallet.
type ledgerDriver struct {
	device  io.ReadWriter // USB device connection to communicate through
	version [3]byte       // Current version of the Ledger firmware (zero if app is offline)
	browser bool          // Flag whether the Ledger is in browser mode (reply channel mismatch)
	failure error         // Any failure that would make the device unusable
	log     *log.Logger   // Contextual logger to tag the ledger with its id
}

// // newLedgerDriver creates a new instance of a Ledger USB protocol driver.
func newLedgerDriver(logger *log.Logger) driver {
	return &ledgerDriver{
		log: logger,
	}
}

// Version implements usbwallet.driver, returning the firmware app version
func (w *ledgerDriver) Version() accounts.Version {
	return accounts.Version{
		Major: w.version[0],
		Minor: w.version[1],
		Patch: w.version[2],
	}
}

// Status implements usbwallet.driver, returning various states the Ledger can
// currently be in.
func (w *ledgerDriver) Status() (string, error) {
	if w.failure != nil {
		return fmt.Sprintf("Failed: %v", w.failure), w.failure
	}
	if w.browser {
		return "Accumulate app in browser mode", w.failure
	}
	if w.offline() {
		return "Accumulate app offline", w.failure
	}
	return fmt.Sprintf("Accumulate app v%d.%d.%d online", w.version[0], w.version[1], w.version[2]), w.failure
}

// offline returns whether the wallet and the Ethereum app is offline or not.
//
// The method assumes that the state lock is held!
func (w *ledgerDriver) offline() bool {
	return w.version == [3]byte{0, 0, 0}
}

// Open implements usbwallet.driver, attempting to initialize the connection to the
// Ledger hardware wallet. The Ledger does not require a user passphrase, so that
// parameter is silently discarded.
func (w *ledgerDriver) Open(device io.ReadWriter, passphrase string) error {
	_ = passphrase
	w.device, w.failure = device, nil
	var err error

	// Check app name
	appName, err := w.ledgerAppName()
	if err != nil {
		return mapError(err)
	}
	if appName != "Accumulate" {
		return errAccumulateAppNotStarted
	}

	// Try to resolve the Accumulate ledger app's version
	w.version, err = w.ledgerAppVersion()
	if err != nil {
		// Accumulate app is not running or in browser mode, nothing more to do, return
		if err == errLedgerReplyInvalidHeader {
			w.browser = true
			return nil
		}
		w.version = [3]byte{1, 0, 0} // Assume worst case, can't verify if v1.0.0 or v1.0.1
		return err
	}
	return nil
}

// Close implements usbwallet.driver, cleaning up and metadata maintained within
// the Ledger driver.
func (w *ledgerDriver) Close() error {
	w.browser, w.version = false, [3]byte{}
	return nil
}

// Heartbeat implements usbwallet.driver, performing a sanity check against the
// Ledger to see if it's still online.
func (w *ledgerDriver) Heartbeat() error {
	if _, err := w.ledgerAppVersion(); err != nil && err != errLedgerInvalidVersionReply {
		w.failure = err
		return err
	}
	return nil
}

// Derive implements usbwallet.driver, sending a derivation request to the Ledger
// and returning the public key located on the derivation path and alias / key name
func (w *ledgerDriver) Derive(path bip44.Derivation, display bool, alias ...string) ([]byte, string, [32]byte, error) {
	return w.ledgerDerive(path, display, alias...)
}

// setKeyFromResponse
//
//	Description                             | Length
//	----------------------------------------+---------------------------
//	Public Key length                       | 1 byte
//	Public Key (compressed or uncompressed) | arbitrary (32, 33, or 65 bytes)
//	Key Name / Alias length                 | 1 byte
//	Key Name / Alias                        | arbitrary length key name / alias up to 64 bytes
func setKeyFromResponse(derivation bip44.Derivation, response []byte) (pubKey []byte, keyName string, chainCode [32]byte, err error) {
	if len(response) == 0 {
		return nil, "", chainCode, fmt.Errorf("no response from wallet")
	}

	if len(response) < int(response[0]) {
		return nil, "", chainCode, fmt.Errorf("invalid response")
	}

	keyLen := response[0]
	switch derivation.SignatureType() {
	case protocol.SignatureTypeBTC:
		if keyLen < 33 {
			return nil, "", chainCode, fmt.Errorf("invalid public key response")
		}
	case protocol.SignatureTypeRCD1:
	case protocol.SignatureTypeED25519:
		if keyLen < 32 {
			return nil, "", chainCode, fmt.Errorf("invalid public key response")
		}
	case protocol.SignatureTypeETH:
		if keyLen != 65 {
			return nil, "", chainCode, fmt.Errorf("invalid public key response")
		}
	default:
		return nil, "", chainCode, fmt.Errorf("unsupported key type")
	}

	offset := keyLen + 1
	pubKey = response[1:offset]

	//chain code
	if len(response[offset:]) > 1 {
		chainCodeLen := int(response[offset])
		offset++
		if len(response[offset:]) < chainCodeLen || chainCodeLen != 32 {
			return nil, "", [32]byte{}, fmt.Errorf("invalid chain code")
		}
		copy(chainCode[:], response[offset:offset+byte(chainCodeLen)])
	}
	//extract address name if available
	if len(response[offset:]) > 1 {
		keyNameLen := int(response[offset])
		offset++
		if len(response[offset:]) < keyNameLen {
			return nil, "", [32]byte{}, fmt.Errorf("invalid chain code")
		}
		keyName = string(response[offset : offset+byte(keyNameLen)])
	}

	return pubKey, keyName, chainCode, nil
}

func (w *ledgerDriver) setSignatureFromResponse(response []byte, tx *protocol.Transaction, pre protocol.Signature) error {
	if len(response) == 0 {
		return fmt.Errorf("no data in response")
	}
	if response[0] > byte(len(response)) {
		return fmt.Errorf("invalid response")
	}

	w.log.Printf("response:  %x", response)

	signatureLen := response[0]
	signature := response[1 : 1+signatureLen]

	switch sig := pre.(type) {
	case *protocol.ED25519Signature:
		if signatureLen < ed25519.SignatureSize {
			return fmt.Errorf("invalid signature length, received %d bytes, expected %d",
				len(signature), crypto.SignatureLength)
		}
		sig.Signature = signature
		if !sig.Verify(nil, tx) {
			return fmt.Errorf("invalid signature")
		}
	case *protocol.RCD1Signature:
		if signatureLen < ed25519.SignatureSize {
			return fmt.Errorf("invalid signature length, received %d bytes, expected %d",
				len(signature), crypto.SignatureLength)
		}

		sig.Signature = signature
		if !sig.Verify(nil, tx) {
			return fmt.Errorf("invalid signature")
		}
	case *protocol.BTCSignature:
		sig.Signature = signature
		if !sig.Verify(nil, tx) {
			return fmt.Errorf("invalid signature")
		}
	case *protocol.ETHSignature:
		sig.Signature = signature
		if !sig.Verify(nil, tx) {
			return fmt.Errorf("invalid signature")
		}
	default:
		return fmt.Errorf("cannot set the public key and signature on a %T", pre)
	}

	return nil
}

// SignTx implements usbwallet.driver, sending the transaction to the Ledger and
// waiting for the user to confirm or deny the transaction.
//
// Note, if the version of the Accumulate application running on the Ledger wallet is
// too old to sign transactions, but such is requested nonetheless, an error
// will be returned opposed to silently attempting to sign.
func (w *ledgerDriver) SignTx(path bip44.Derivation, tx *protocol.Transaction, signer protocol.Signature) (protocol.Signature, error) {
	// If the Accumulate app doesn't run, abort
	if w.offline() {
		return nil, accounts.ErrWalletClosed
	}

	err := path.Validate()
	if err != nil {
		return nil, err
	}

	// Ensure the wallet is capable of signing the given transaction
	if w.version[0] <= 1 && w.version[1] <= 0 && w.version[2] <= 2 {
		//lint:ignore ST1005 brand name displayed on the console
		return nil, fmt.Errorf("the Ledger v%d.%d.%d doesn't support signing this transaction (%s), please update firmware to v1.0.3 or greater",
			w.version[0], w.version[1], w.version[2], tx.Body.Type().String())
	}

	// All infos gathered and metadata checks out, request signing
	return w.ledgerSign(path, tx, signer)
}

// SignTypedMessage implements usbwallet.driver, sending the message to the Ledger and
// waiting for the user to sign or deny the transaction.
//
// Note: this was introduced in the ledger 1.5.0 firmware
func (w *ledgerDriver) SignTypedMessage(path bip44.Derivation, domainHash []byte, messageHash []byte) ([]byte, error) {
	// If the Ethereum app doesn't run, abort
	if w.offline() {
		return nil, accounts.ErrWalletClosed
	}
	// Ensure the wallet is capable of signing the given transaction
	if w.version[0] < 1 && w.version[1] < 5 {
		//lint:ignore ST1005 brand name displayed on the console
		return nil, fmt.Errorf("the Ledger version >= 1.5.0 required for EIP-712 signing (found version v%d.%d.%d)", w.version[0], w.version[1], w.version[2])
	}
	// All infos gathered and metadata checks out, request signing
	return w.ledgerSignTypedMessage(path, domainHash, messageHash)
}

// ledgerVersion retrieves the current version of the Accumulate wallet app running
// on the Ledger wallet.
//
// The version retrieval protocol is defined as follows:
//
//	CLA | INS | P1 | P2 | Lc | Le
//	----+-----+----+----+----+---
//	 E0 | 06  | 00 | 00 | 00 | 04
//
// With no input data, and the output data being:
//
//	Description                                        | Length
//	---------------------------------------------------+--------
//	Application major version                          | 1 byte
//	Application minor version                          | 1 byte
//	Application patch version                          | 1 byte
func (w *ledgerDriver) ledgerAppVersion() ([3]byte, error) {
	// Send the request and wait for the response
	reply, err := w.ledgerExchange(ledgerOpGetApplicationVersion, 0, 0, nil)
	if err != nil {
		return [3]byte{}, err
	}
	if len(reply) != 3 {
		return [3]byte{}, errLedgerInvalidVersionReply
	}
	// Cache the version for future reference
	var version [3]byte
	copy(version[:], reply[:])
	return version, nil
}

func (w *ledgerDriver) ledgerAppName() (string, error) {
	// Send the request and wait for the response
	reply, err := w.ledgerExchange(ledgerOpGetAppName, 0, 0, nil)
	if err != nil {
		return "", err
	}
	if len(reply) == 0 {
		return "", errLedgerInvalidAppNameReply
	}
	return string(reply[:]), nil
}

// ledgerDerive retrieves the currently active derived key from a Ledger
// wallet at the specified derivation path. Returns, public key, lite url, and key hash
//
// The address derivation protocol is defined as follows:
//
//	CLA | INS | P1 | P2 | Lc  | Le
//	----+-----+----+----+-----+---
//	 E0 | 02  | 00 return address
//	            01 display address and confirm before returning
//	               | 00: do not return the chain code
//	               | 01: return the chain code
//	                    | var | 00
//
// Where the input data is:
//
//	Description                                      | Length
//	-------------------------------------------------+--------
//	Number of BIP 32 derivations to perform (max 10) | 1 byte
//	First derivation index (big endian)              | 4 bytes
//	...                                              | 4 bytes
//	Last derivation index (big endian)               | 4 bytes
//
// And the output data is:
//
//			Description                             | Length
//			----------------------------------------+---------------------------
//			Public Key length                       | 1 byte
//			Public Key (compressed or uncompressed) | arbitrary (32, 33 or 65 bytes)
//	     Chain Code Len                          | 1 byte
//	     Chain Code                              | 32 bytes (will be zero if P2 == 0)
//		 	Key Name / Alias length                 | 1 byte
//		 	Key Name / Alias (string)               | arbitrary (up to 64 bytes)
func (w *ledgerDriver) ledgerDerive(derivationPath bip44.Derivation, display bool, alias ...string) ([]byte, string, [32]byte, error) {
	// Flatten the derivation path into the Ledger request
	path := make([]byte, 1+4*len(derivationPath))
	path[0] = byte(len(derivationPath))
	for i, v := range derivationPath {
		binary.BigEndian.PutUint32(path[1+i*4:], v)
	}

	p1 := ledgerP1DirectlyFetchAddress
	if display {
		p1 = ledgerP1Display
	}
	payload := path
	if len(alias) > 0 {
		payload = append(payload, byte(len(alias[0])))
		payload = append(payload, []byte(alias[0])...)
	}

	// Send the request and wait for the response
	reply, err := w.ledgerExchange(ledgerOpGetPublicKey, p1, ledgerP2DiscardAddressChainCode, payload)
	if err != nil {
		return nil, "", [32]byte{}, mapError(err)
	}

	return setKeyFromResponse(derivationPath, reply)
}

// ledgerSign sends the transaction to the Ledger wallet, and waits for the user
// to confirm or deny the transaction.
//
// The transaction signing protocol is defined as follows:
//
//	CLA | INS | P1 | P2 | Lc  | Le
//	----+-----+----+----+-----+---
//	 E0 | 04  | 00: first transaction data block
//	            80: subsequent transaction data block
//	               | 00 | variable | variable
//
// Where the input for the first transaction block (first 255 bytes) is:
//
//	Description                                      | Length
//	-------------------------------------------------+----------
//	Number of BIP 32 derivations to perform (max 10) | 1 byte
//	First derivation index (big endian)              | 4 bytes
//	...                                              | 4 bytes
//	Last derivation index (big endian)               | 4 bytes
//	accumulate transaction envelope                  | arbitrary
//
// And the input for subsequent transaction blocks (first 255 bytes) are:
//
//	Description           | Length
//	----------------------+----------
//	transaction chunk | arbitrary
//
// And the output data is:
//
//	Description | Length
//	------------+---------
//	public key Len  | 1 byte
//	public key      | 32 or 64 bytes
//	signature R     | 32 bytes
//	signature S     | 32 bytes
//	signature V     | 1 byte
func (w *ledgerDriver) ledgerSign(derivationPath bip44.Derivation, tx *protocol.Transaction,
	preSig protocol.Signature) (protocol.Signature, error) {

	// Flatten the derivation path into the Ledger request
	path := make([]byte, 1+4*len(derivationPath))
	path[0] = byte(len(derivationPath))
	for i, v := range derivationPath {
		binary.BigEndian.PutUint32(path[1+i*4:], v)
	}

	env := new(messaging.Envelope)
	env.Signatures = append(env.Signatures, preSig)
	env.Transaction = append(env.Transaction, tx)
	payload, err := env.MarshalBinary()
	if err != nil {
		return nil, err
	}
	w.log.Printf("tx payload: %x", payload)
	// Send the request and wait for the response
	var (
		p1    = ledgerP1InitTransactionData
		p2    = ledgerP2MoreTransactionData
		reply []byte
	)

	// Send the derivation path over, ensuring it's processed correctly
	reply, err = w.ledgerExchange(ledgerOpSignTransaction, p1, p2, path)
	if err != nil {
		return nil, err
	}
	//change state to continue with sending transaction data
	p1 = ledgerP1ContTransactionData

	for len(payload) > 0 {
		// Calculate the size of the next data chunk
		chunk := 255
		if chunk > len(payload) {
			chunk = len(payload)
			p1 = ledgerP1ContTransactionData
			p2 = ledgerP2LastTransactionData
		}

		// Send the chunk over, ensuring it's processed correctly
		reply, err = w.ledgerExchange(ledgerOpSignTransaction, p1, p2, payload[:chunk])
		if err != nil {
			return nil, mapError(err)
		}
		if reply != nil {
			w.log.Printf("%x", reply)
		}
		// Shift the payload and ensure subsequent chunks are marked as such
		payload = payload[chunk:]
		p2 = ledgerP2MoreTransactionData
	}

	err = w.setSignatureFromResponse(reply, tx, preSig)
	if err != nil {
		return nil, err
	}
	return preSig, nil
}

// ledgerSignTypedMessage sends the transaction to the Ledger wallet, and waits for the user
// to confirm or deny the transaction.
//
// The signing protocol is defined as follows:
//
//	CLA | INS | P1 | P2                          | Lc  | Le
//	----+-----+----+-----------------------------+-----+---
//	 E0 | 0C  | 00 | implementation version : 00 | variable | variable
//
// Where the input is:
//
//	Description                                      | Length
//	-------------------------------------------------+----------
//	Number of BIP 32 derivations to perform (max 10) | 1 byte
//	First derivation index (big endian)              | 4 bytes
//	...                                              | 4 bytes
//	Last derivation index (big endian)               | 4 bytes
//	domain hash                                      | 32 bytes
//	message hash                                     | 32 bytes
//
// And the output data is:
//
//	Description | Length
//	------------+---------
//	signature V | 1 byte
//	signature R | 32 bytes
//	signature S | 32 bytes
func (w *ledgerDriver) ledgerSignTypedMessage(derivationPath []uint32, domainHash []byte, messageHash []byte) ([]byte, error) {
	// Flatten the derivation path into the Ledger request
	path := make([]byte, 1+4*len(derivationPath))
	path[0] = byte(len(derivationPath))
	for i, component := range derivationPath {
		binary.BigEndian.PutUint32(path[1+4*i:], component)
	}
	// Create the 712 message
	payload := append(path, domainHash...)
	payload = append(payload, messageHash...)

	// Send the request and wait for the response
	var (
		op    = ledgerP1InitTypedMessageData
		reply []byte
		err   error
	)

	// Send the message over, ensuring it's processed correctly
	reply, err = w.ledgerExchange(ledgerOpSignTransaction, op, 0, payload)

	if err != nil {
		return nil, err
	}

	// Extract the Ethereum signature and do a sanity validation
	if len(reply) != crypto.SignatureLength {
		return nil, errors.New("reply lacks signature")
	}
	signature := append(reply[1:], reply[0])
	return signature, nil
}

// ledgerExchange performs a data exchange with the Ledger wallet, sending it a
// message and retrieving the response.
//
// The common transport header is defined as follows:
//
//	Description                           | Length
//	--------------------------------------+----------
//	Communication channel ID (big endian) | 2 bytes
//	Command tag                           | 1 byte
//	Packet sequence index (big endian)    | 2 bytes
//	Payload                               | arbitrary
//
// The Communication channel ID allows commands multiplexing over the same
// physical link. It is not used for the time being, and should be set to 0101
// to avoid compatibility issues with implementations ignoring a leading 00 byte.
//
// The Command tag describes the message content. Use TAG_APDU (0x05) for standard
// APDU payloads, or TAG_PING (0x02) for a simple link test.
//
// The Packet sequence index describes the current sequence for fragmented payloads.
// The first fragment index is 0x00.
//
// APDU Command payloads are encoded as follows:
//
//		Description              | Length
//		-----------------------------------
//	 APDU length (big endian) | 2 bytes
//		APDU CLA                 | 1 byte
//		APDU INS                 | 1 byte
//		APDU P1                  | 1 byte
//		APDU P2                  | 1 byte
//		APDU length              | 1 byte
//		Optional APDU data       | arbitrary
func (w *ledgerDriver) ledgerExchange(opcode ledgerOpcode, p1 ledgerParam1, p2 ledgerParam2, data []byte) ([]byte, error) {
	// Construct the message payload, possibly split into multiple chunks
	apdu := make([]byte, 0, 5+len(data))
	apdu = append(apdu, []byte{0xe0, byte(opcode), byte(p1), byte(p2), byte(len(data))}...)
	apdu = append(apdu, data...)
	w.log.Println("APDU", hexutil.Bytes(apdu))

	// Stream all the chunks to the device
	header := []byte{0x01, 0x01, 0x05, 0x00, 0x00, 0x00, 0x00}  // Channel ID, command tag appended ...
	binary.BigEndian.PutUint16(header[5:], uint16(5+len(data))) // .. and finally the APDU buffer length
	chunk := make([]byte, 64)
	space := len(chunk) - len(header)
	for i := 0; len(apdu) > 0; i++ {
		// Construct the new message to stream
		chunk = append(chunk[:0], header...)
		binary.BigEndian.PutUint16(chunk[3:], uint16(i))

		if len(apdu) > space {
			chunk = append(chunk, apdu[:space]...)
			apdu = apdu[space:]
			if i == 0 {
				//after the first packet, we dump the total bytes from the header
				header = header[0 : len(header)-2]
				//which gives us more space for data in the next chunk
				space += 2
			}
		} else {
			chunk = append(chunk, apdu...)
			apdu = nil
		}
		// Send over to the device
		w.log.Println("Data chunk sent to the Ledger", "chunk", hexutil.Bytes(chunk))
		if _, err := w.device.Write(chunk); err != nil {
			return nil, err
		}
	}
	// Stream the reply back from the wallet in 64 byte chunks
	var reply []byte
	chunk = chunk[:64] // Yeah, we surely have enough space
	for {
		// Read the next chunk from the Ledger wallet
		if _, err := io.ReadFull(w.device, chunk); err != nil {
			return nil, err
		}
		w.log.Println("Data chunk received from the Ledger", "chunk", hexutil.Bytes(chunk))

		// Make sure the transport header matches
		if chunk[0] != 0x01 || chunk[1] != 0x01 || chunk[2] != 0x05 {
			return nil, errLedgerReplyInvalidHeader
		}
		// If it's the first chunk, retrieve the total message length
		var payload []byte

		if chunk[3] == 0x00 && chunk[4] == 0x00 {
			reply = make([]byte, 0, int(binary.BigEndian.Uint16(chunk[5:7])))
			payload = chunk[7:]
		} else {
			payload = chunk[5:]
		}

		// Append to the reply and stop when filled up
		if left := cap(reply) - len(reply); left > len(payload) {
			reply = append(reply, payload...)
		} else {
			reply = append(reply, payload[:left]...)
			break
		}
	}
	if len(reply) > 0 {
		code := binary.BigEndian.Uint16(reply[len(reply)-2:])
		if code != 0x9000 {
			return nil, fmt.Errorf("error %x", code)
		}
	}
	return reply[:len(reply)-2], nil
}

func mapError(err error) error {
	switch err.Error() {
	case errLedgerReplyNoAppStarted.Error(), errLedgerReplyNoAppStarted2.Error(), errLedgerReplyInvalidHeader.Error():
		return errAccumulateAppNotStarted
	case errLedgerReplyDeviceLocked.Error(), errLedgerReplyDeviceLocked2.Error(), errLedgerReplyDeviceLocked3.Error():
		return errAccumulateAppDeviceLocked
	case errLedgerReplyTxNotSupported.Error():
		return errAccumulateAppTxNotSupported
	case errLedgerReplyDeny.Error():
		return errLedgerReplyDeny
	case errLedgerReplyWrongP1P2.Error():
		return errLedgerReplyWrongP1P2
	case errLedgerReplyWrongDataLength.Error():
		return errLedgerReplyWrongDataLength
	case errLedgerReplyInsNotSupported.Error():
		return errLedgerReplyInsNotSupported
	case errLedgerReplyClaNotSupported.Error():
		return errLedgerReplyClaNotSupported
	case errLedgerReplyWrongResponseLength.Error():
		return errLedgerReplyWrongResponseLength
	case errLedgerReplyDisplayBIP32PathFail.Error():
		return errLedgerReplyDisplayBIP32PathFail
	case errLedgerReplyDisplayAddressFail.Error():
		return errLedgerReplyDisplayAddressFail
	case errLedgerReplyDisplayAmountFail.Error():
		return errLedgerReplyDisplayAmountFail
	case errLedgerReplyWrongTXLength.Error():
		return errLedgerReplyWrongTXLength
	case errLedgerReplyTXParsingFail.Error():
		return errLedgerReplyTXParsingFail
	case errLedgerReplyTXHashFail.Error():
		return errLedgerReplyTXHashFail
	case errLedgerReplyBadState.Error():
		return errLedgerReplyBadState
	case errLedgerReplySignatureFail.Error():
		return errLedgerReplySignatureFail
	case errLedgerReplyUnknown.Error():
		return errLedgerReplyUnknown
	case errLedgerReplyInvalidEnum.Error():
		return errLedgerReplyInvalidEnum
	case errLedgerReplyInvalidObject.Error():
		return errLedgerReplyInvalidObject
	case errLedgerReplyNotImplemented.Error():
		return errLedgerReplyNotImplemented
	case errLedgerReplyTypeNotFound.Error():
		return errLedgerReplyTypeNotFound
	case errLedgerReplyParameterNil.Error():
		return errLedgerReplyParameterNil
	case errLedgerReplyParameterInsufficientData.Error():
		return errLedgerReplyParameterInsufficientData
	case errLedgerReplyBadCopy.Error():
		return errLedgerReplyBadCopy
	case errLedgerReplyBufferTooSmall.Error():
		return errLedgerReplyBufferTooSmall
	case errLedgerReplyVarIntRead.Error():
		return errLedgerReplyVarIntRead
	case errLedgerReplyVarIntWrite.Error():
		return errLedgerReplyVarIntWrite
	case errLedgerReplyResizeRequred.Error():
		return errLedgerReplyResizeRequred
	case errLedgerReplyInvalidBigInt.Error():
		return errLedgerReplyInvalidBigInt
	case errLedgerReplyInvalidString.Error():
		return errLedgerReplyInvalidString
	case errLedgerReplyInvalidHashParameters.Error():
		return errLedgerReplyInvalidHashParameters
	case errLedgerReplyUVarIntRead.Error():
		return errLedgerReplyUVarIntRead
	case errLedgerReplyUVarIntWrite.Error():
		return errLedgerReplyUVarIntWrite
	case errLedgerReplyMempoolFull.Error():
		return errLedgerReplyMempoolFull
	case errLedgerReplyInvalidOffset.Error():
		return errLedgerReplyInvalidOffset
	case errLedgerReplyInvalidData.Error():
		return errLedgerReplyInvalidData
	case errLedgerReplyBadKey.Error():
		return errLedgerReplyBadKey
	case errLedgerReplyInvalidField.Error():
		return errLedgerReplyInvalidField
	case errLedgerReplyExpectingType.Error():
		return errLedgerReplyExpectingType
	default:
		return err
	}
}
