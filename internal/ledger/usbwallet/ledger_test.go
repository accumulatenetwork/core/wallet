package usbwallet_test

import (
	"encoding/hex"
	"errors"
	"log"
	"math/big"
	"os"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/accumulatenetwork/accumulate/pkg/client/signing"
	url2 "gitlab.com/accumulatenetwork/accumulate/pkg/url"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
	"gitlab.com/accumulatenetwork/core/wallet/cmd/accumulate/walletd/bip44"
	"gitlab.com/accumulatenetwork/core/wallet/internal/ledger/usbwallet"
)

func TestLedgerDriver(t *testing.T) {
	if os.Getenv("CI") != "" {
		t.Skip("Skipping testing in CI environment")
	}

	pk, _ := hex.DecodeString("")
	u2, _ := url2.Parse("")
	ls2 := new(usbwallet.LedgerSignerTester)
	ls2.PubKey = pk

	bb2 := signing.Builder{}
	bb2.Type = protocol.SignatureTypeBTC
	bb2.SetTimestamp(1234)
	bb2.SetVersion(1)
	bb2.SetSigner(ls2)
	bb2.SetUrl(u2)
	//tSig, err := bb2.Initiate(tx)

	hub, err := usbwallet.NewLedgerHub(true)
	if errors.Is(err, usbwallet.ErrUnsupported) {
		t.Skip("Platform not supported")
	}
	require.NoError(t, err)
	w := hub.Wallets()
	if len(w) == 0 {
		t.Skip("No ledger wallets found")
	}

	//open the device
	require.NoError(t, w[0].Open(""))
	defer w[0].Close()

	display := false
	pinAccountToPath := true
	//now get the key info
	hd := bip44.Derivation{bip44.Purpose, bip44.TypeAccumulate, 0x80000000 + 0, 0x80000000 + 0, 0x80000000 + 0} //bip44.DefaultAccumulateBaseDerivationPath

	a, err := w[0].Derive(hd, pinAccountToPath, display)
	require.NoError(t, err)

	ua, err := protocol.LiteTokenAddress(a.PubKey, protocol.ACME, hd.SignatureType())
	require.NoError(t, err)
	t.Log(ua.String())
	require.NoError(t, err)

	//display bitcoin
	hd = bip44.Derivation{bip44.Purpose, bip44.TypeEther, 0x80000000 + 0, 0, 0} //bip44.DefaultAccumulateBaseDerivationPath
	e, err := w[0].Derive(hd, pinAccountToPath, display)
	require.NoError(t, err)
	ue, err := protocol.LiteTokenAddress(e.PubKey, protocol.ACME, hd.SignatureType())
	require.NoError(t, err)
	t.Log(protocol.ETHaddress(e.PubKey))
	t.Log(ue.String())
	require.NoError(t, err)
	//display eth
	hd = bip44.Derivation{bip44.Purpose, bip44.TypeBitcoin, 0x80000000 + 0, 0, 0} //bip44.DefaultAccumulateBaseDerivationPath
	b, err := w[0].Derive(hd, pinAccountToPath, display)
	require.NoError(t, err)
	ub, err := protocol.LiteTokenAddress(b.PubKey, protocol.ACME, hd.SignatureType())
	require.NoError(t, err)
	t.Log(protocol.BTCaddress(b.PubKey))
	t.Log(ub.String())

	require.NoError(t, err)
	////display fct
	hd = bip44.Derivation{bip44.Purpose, bip44.TypeFactomFactoids, 0x80000000 + 0, 0, 0} //bip44.DefaultAccumulateBaseDerivationPath

	uf, err := protocol.LiteTokenAddress(a.PubKey, protocol.ACME, hd.SignatureType())
	require.NoError(t, err)
	f, err := w[0].Derive(hd, pinAccountToPath, display, "tfa rules")
	require.NoError(t, err)
	rcd := protocol.GetRCDHashFromPublicKey(f.PubKey, 0)
	fct, err := protocol.GetFactoidAddressFromRCDHash(rcd)
	t.Log(uf.String())
	t.Log(fct)
	require.NoError(t, err)

	//make a send tokens transaction
	//
	//we are building up the signer so that we can generate the tx hash here to compare against what wallet will do
	//for production, all we need is the timestamp, version, url for the signer, and transaction body.
	ls := new(usbwallet.LedgerSignerTester)
	ls.PubKey = a.PubKey

	bb := signing.Builder{}
	bb.Type = a.SignatureType
	bb.SetTimestampToNow()
	bb.SetVersion(1)
	bb.SetSigner(ls)
	bb.SetUrl(ua)

	tx := new(protocol.Transaction)
	tx.Header = protocol.TransactionHeader{}
	tx.Header.Principal = ua
	tx.Header.Memo = "ledger add credits test"

	credits := new(protocol.AddCredits)
	credits.Recipient = ue
	credits.Oracle = 1234
	credits.Amount = *big.NewInt(1000000000)
	tx.Body = credits
	preSig, err := bb.Initiate(tx)

	if !errors.Is(err, usbwallet.UseHardwareError) {
		require.NoError(t, err)
	}
	sig, err := w[0].SignTx(&a, tx, preSig)
	_ = sig
	require.NoError(t, err)

	tx = new(protocol.Transaction)
	tx.Header = protocol.TransactionHeader{}
	tx.Header.Principal = ua
	tx.Header.Memo = "acme send tokens test"

	send := new(protocol.SendTokens)
	amt := big.NewInt(1000000000)
	send.AddRecipient(ue, amt)
	tx.Body = send

	preSig, err = bb.Initiate(tx)

	if !errors.Is(err, usbwallet.UseHardwareError) {
		require.NoError(t, err)
	}
	sig, err = w[0].SignTx(&a, tx, preSig)
	require.NoError(t, err)

	t.Logf("%x", sig.Metadata())

	bb = signing.Builder{}
	tx = new(protocol.Transaction)
	tx.Header = protocol.TransactionHeader{}
	tx.Header.Principal = ub
	tx.Header.Memo = "btc send tokens test"
	tx.Body = send
	ls.PubKey = b.PubKey
	bb.Type = b.SignatureType
	log.Println(ub.String())
	bb.SetSigner(ls)
	bb.SetUrl(ub)
	bb.SetTimestamp(12345)
	bb.SetVersion(1)

	preSig, err = bb.Initiate(tx)
	if !errors.Is(err, usbwallet.UseHardwareError) {
		require.NoError(t, err)
	}
	log.Printf("metadata hash %x\n", preSig.Metadata().Hash())

	sig, err = w[0].SignTx(&b, tx, preSig)
	_ = sig
	require.NoError(t, err)

	bb = signing.Builder{}
	tx = new(protocol.Transaction)
	tx.Header = protocol.TransactionHeader{}
	tx.Header.Principal = e.LiteAccount
	tx.Header.Memo = "eth send tokens test"
	tx.Body = send
	ls.PubKey = e.PubKey
	bb.Type = e.SignatureType
	bb.SetSigner(ls)
	bb.SetUrl(e.LiteAccount)
	bb.SetTimestampToNow()
	bb.SetVersion(1)

	preSig, err = bb.Initiate(tx)
	if !errors.Is(err, usbwallet.UseHardwareError) {
		require.NoError(t, err)
	}
	sig, err = w[0].SignTx(&e, tx, preSig)
	_ = sig
	require.NoError(t, err)

}
