package usbwallet

import (
	"crypto/sha256"
	"errors"
	"fmt"

	"gitlab.com/accumulatenetwork/accumulate/pkg/client/signing"
	"gitlab.com/accumulatenetwork/accumulate/protocol"
)

type LedgerSignature struct {
}

type LedgerSignerTester struct {
	signing.Signer
	PubKey       []byte
	SignThis     []byte
	preSignature protocol.Signature
}

var (
	UseHardwareError = errors.New("use the hardware")
)

func (l *LedgerSignerTester) SetPublicKey(sig protocol.Signature) error {
	l.preSignature = sig
	if l.PubKey == nil {
		return fmt.Errorf("public key not set")
	}
	switch sig := sig.(type) {
	case *protocol.LegacyED25519Signature:
		sig.PublicKey = l.PubKey
	case *protocol.ED25519Signature:
		sig.PublicKey = l.PubKey
	case *protocol.RCD1Signature:
		sig.PublicKey = l.PubKey
	case *protocol.BTCSignature:
		sig.PublicKey = l.PubKey
	case *protocol.BTCLegacySignature:
		sig.PublicKey = l.PubKey
	case *protocol.ETHSignature:
		sig.PublicKey = l.PubKey
	case *protocol.DelegatedSignature:
		//do nothing
	default:
		return fmt.Errorf("unsupported signature type %v", sig.Type())
	}
	return nil
}

func (l *LedgerSignerTester) Sign(sig protocol.Signature, sigMdHash []byte, txnHash []byte) error {
	if sigMdHash == nil {
		sigMdHash = sig.Metadata().Hash()
	}
	data := sigMdHash
	data = append(data, txnHash...)
	hash := sha256.Sum256(data)
	l.SignThis = hash[:]
	return UseHardwareError
}

func (l *LedgerSignerTester) SignTransaction(sig protocol.Signature, txn *protocol.Transaction) error {
	return l.Sign(sig, nil, txn.GetHash())
}

func (l *LedgerSignerTester) PreSignature() protocol.Signature {
	return l.preSignature
}
