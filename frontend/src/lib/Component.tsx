import React from "react";

interface Component<P = {}, S = {}> {
    propsDidChange?(prevProps?: Readonly<P>)
}

function equal(a, b) {
    if (a === b) return true;
    if (typeof a !== typeof b) return false;
    if (typeof a !== 'object') return false;
    if (a == null || b == null) return false;

    if (Symbol.iterator in a || typeof a.length === 'number') {
        a = Array.from(a)
        b = Array.from(b)
        if (a.length !== b.length) return false;
    }

    const checked = new Set();
    for (const i in a) {
        checked.add(i);
        if (i === 'children') continue; // Avoid trying to compare react nodes
        if (!(i in b)) return false;
        if (!equal(a[i], b[i])) return false;
    }
    for (const i in b) {
        if (!checked.has(i)) return false;
    }
    return true;
}

abstract class Component<P, S> extends React.Component<P, S> {
    protected updateState(s: Partial<S>) {
        this.setState(Object.assign(this.state || {}, s));
    }

    async componentDidMount() {
        this.propsDidChange && await this.propsDidChange()
    }

    async componentDidUpdate(prevProps: Readonly<P>) {
        if (equal(this.props, prevProps)) return;
        this.propsDidChange && await this.propsDidChange(prevProps)
    }
}

export default Component