import { Key, PublicKeyAddress, SignOptions, Signer, TxID, URL, URLArgs, api_v3, core, messaging } from "accumulate.js";
import { Buffer } from "accumulate.js/lib/common";
import React from "react";
import { api } from "../../wailsjs/go/models";
import { AccountRecord } from "accumulate.js/lib/api_v3";
import { MainContext } from "./MainContext"
import { GoAPI } from "./Go";
import { notification } from "antd";
import { Account, AccountType } from "accumulate.js/lib/core";

export function validateTokenAmount(account: core.TokenAccount | core.LiteTokenAccount, issuer: core.TokenIssuer, value: string) {
    if (!/^([0-9]+(\.[0-9]*)?|[0-9]*\.[0-9]+)$/.test(value)) {
        throw new Error('Amount must be a number')
    }

    const v = parseTokenBalance(value, issuer)
    if (v > account.balance!) {
        throw new Error(`Insufficient balance`)
    }
}

export async function validateAccount(network: api_v3.JsonRpcClient, value: URLArgs, ...types: AccountType[]) {
    const url = URL.parse(value)

    // Does the account exist?
    const { account } = (await tryQuery(network, url) as AccountRecord) || {};
    if (!account) throw new Error(`${url} does not exist`)

    // Is it the right type?
    if (!types.includes(account.type)) {
        throw new Error(`${url} must be a ${types.join(' or ')}`)
    }

    return account;
}

export function formatTokenBalance(account: core.TokenAccount | core.LiteTokenAccount, issuer: core.TokenIssuer) {
    if (!account.balance) {
        return <span style={{ color: 'grey' }}>0 {issuer.symbol!}</span>
    }

    return <span>{formatTokenBalanceStr(account, issuer)}</span>
}

export function formatTokenBalanceStr(account: core.TokenAccount | core.LiteTokenAccount, issuer: core.TokenIssuer) {
    if (!account.balance) {
        return `0 ${issuer.symbol!}`
    }

    const { precision } = issuer;
    let balance = account.balance?.toString();
    if (balance.length <= precision!) {
        balance = '0'.repeat(precision! - balance.length + 1) + balance
    }
    balance = balance.substring(0, balance.length - precision!) + '.' + balance.substring(balance.length - precision!)
    balance = balance.replace(/\.?0+$/, '')
    return `${balance} ${issuer.symbol!}`
}

export function parseTokenBalance(str: string, issuer: core.TokenIssuer) {
    let [int, frac] = str.split('.', 2)
    if (!int?.length) {
        int = '0'
    }
    if (!frac) {
        frac = ''
    }
    if (frac.length > issuer.precision!) {
        throw new Error(`${issuer.symbol} precision is ${issuer.precision}`)
    }
    if (frac.length < issuer.precision!) {
        frac += '0'.repeat(issuer.precision! - frac.length)
    }

    return BigInt(int+frac)
}

export class UnsupportedKeyType extends Error {
    constructor(public readonly key: KeyAndVault) {
        super(`${key.keyInfo!.type} keys are not supported`)
    }
}

export class FailedSubmission extends Error {
    constructor(public readonly submission: api_v3.Submission) {
        super(submission.message)
    }
}

export type KeyAndVault = Omit<api.Key, 'convertValues'> & { vault: string };

class WalletKey implements Key {
    private constructor(
        private readonly go: GoAPI,
        private readonly key: KeyAndVault,
        public readonly address: PublicKeyAddress,
    ){}

    static async from(go: GoAPI, key: KeyAndVault) {
        const type = core.SignatureType.byName(key.keyInfo!.type as any)
        const pubKey = Buffer.from(key.publicKey as any, 'hex')
        const address = await PublicKeyAddress.from(type, pubKey)
        return new this(go, key, address);
    }

    async sign(message: Uint8Array | core.Transaction, args: SignOptions): Promise<core.UserSignature> {
        if (!(message instanceof core.Transaction)) {
            throw new Error(`Can't sign bare hash`)
        }

        const isLedger = !!this.key.keyInfo?.walletId
        notification.info({
            key: "sign-with-ledger",
            message: "Sign the transaction with your Ledger",
            duration: 0,
        })

        const req: Omit<api.SignRequest, 'convertValues'> = {
            vault: this.key.vault,
            publicKey: this.key.publicKey as any,
            transaction: message.asObject() as any,
            delegators: args.delegators?.map(x => x.toString()),
            signer: args.signer.toString(),
            signerVersion: args.signerVersion,
            timestamp: args.timestamp,
        }
        try {
            const resp = await this.go.Sign(req as any, isLedger)
            message.header!.initiator = Buffer.from(resp.transaction!.header!.initiator as any, 'hex')
            return core.UserSignature.fromObject(resp.signature!)
        } finally {
            notification.destroy("sign-with-ledger")
        }
    }
}

export type SubmitResult = Awaited<ReturnType<typeof submitTransaction>>;

export async function submitTransaction(context: React.ContextType<typeof MainContext>, transaction: core.Transaction, key: KeyAndVault) {
    // Set up the signer
    const page = (await context.network!.query(key.pages![0]) as AccountRecord).account as core.KeyPage;
    const walletKey = await WalletKey.from(context.go, key);
    const signer = new Signer(walletKey, page.url!).withVersion(page.version! || 1); // Use 1 if it's a LID

    // Sign the transaction
    const sig = await signer.sign(transaction, { timestamp: Date.now() }) as core.KeySignature;
    const envelope = new messaging.Envelope({ transaction: [transaction], signatures: [sig] });

    if (Buffer.from(await transaction.hash()).toString('hex') !== Buffer.from(sig.transactionHash!).toString('hex')) {
        throw new Error('Internal error: signature and transaction do not match')
    }

    // Submit the envelope
    const submissions = await context.network!.submit(envelope);
    for (const sub of submissions) {
        if (!sub.success) {
            throw new FailedSubmission(sub);
        }
    }

    return { envelope, submissions };
}

export async function tryQuery(network: api_v3.JsonRpcClient, scope: URLArgs | TxID) {
    try {
        return await network.query(scope)
    } catch (error) {
        if (typeof error.data === 'object' && error.data.code === 'notFound')
            return;
        throw error;
    }
}