import { Form, FormInstance, FormProps, Modal, ModalProps } from "antd";
import Component from "./Component";
import React from "react";
import { Store } from "antd/es/form/interface";

interface Props<V> {
    children: React.ReactNode | ((_: State) => React.ReactNode);
    onFinish: NonNullable<FormProps<V>['onFinish']>;
    close: () => void,

    modal: Pick<ModalProps, 'open' | 'onCancel' | 'title'>;
    form?: Pick<FormProps<V>, 'layout' | 'requiredMark' | 'disabled'> & {
        initialValues?: Partial<V>;
        ref?: React.Ref<FormInstance<V>> | undefined;
    };
}

interface State {
    confirming: boolean;
}

const { Provider, Consumer } = React.createContext<State>({ confirming: false });

export type FormModalProps<V = any> = Props<V>;

export class FormModal<V extends Record<string, any> = any> extends Component<Props<V>, State> {
    static Context = Consumer;
    state: State;

    constructor(props: Props<V>) {
        super(props)

        this.state = {
            confirming: false,
        }
    }

    private async onFinish(v: V) {
        this.updateState({ confirming: true })
        await this.props.onFinish(v)
        this.updateState({ confirming: false })
        this.props.close()
    }

    render() {
        let { children } = this.props
        if (typeof children === 'function')
            children = children(this.state)

        return (
            <Modal
                footer={null}
                confirmLoading={this.state.confirming}
                onCancel={() => this.props.close()} // May be overridden
                {...this.props.modal}>
                <Form
                    disabled={this.state.confirming || this.props.form?.disabled}
                    onFinish={v => this.onFinish(v)}
                    {...this.props.form}>
                    <Provider value={this.state}>
                        {children}
                    </Provider>
                </Form>
            </Modal>
        )
    }
}