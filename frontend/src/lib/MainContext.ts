import React from "react";
import { GoAPI } from "./Go";
import { JsonRpcClient } from "accumulate.js/lib/api_v3";

interface Context {
    onError: (err: any) => void;
    go: GoAPI;
    network?: JsonRpcClient;
}

export const MainContext = React.createContext<Context>({
    onError(err) { throw err },
    go: null as any,
});