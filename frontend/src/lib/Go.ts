import * as go from "../../wailsjs/go/main/App";

type BaseAPI = Omit<typeof go, 'Start'>;

type Wrapper = (key: keyof BaseAPI, fn: Function) => Function | void;

export type GoAPI = BaseAPI & { wrap(fn: Wrapper): GoAPI };

const { Start: _, ...fns } = go;
const baseGo = {
    wrap(wrapper: Wrapper): GoAPI {
        const { wrap, ...newGo } = this;
        for (const key in newGo) {
            const k = key as keyof BaseAPI;
            const fn = wrapper(k, newGo[k])
            if (fn) newGo[k] = fn as any;
        }
        return { wrap, ...newGo};
    },
    ...fns,
}

async function Go(wallet: string): Promise<GoAPI> {
    await go.Start(wallet);

    return baseGo
        .wrap((key, fn) => {
            // Inject the wallet path
            return (...args) => {
                args[0].wallet = wallet;
                return fn(...args);
            }
        });
}

export default Go;