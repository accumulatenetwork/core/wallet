import Component from "../lib/Component";
import { Button, Typography } from "antd";
import React from "react";
import { LinkOutlined } from '@ant-design/icons';
import { SubmitResult } from "../lib/util";

const { Title, Link } = Typography;

interface Props extends SubmitResult {
    onClose(): void;
}

export class Submitted extends Component<Props, {}> {
    render() {
        const kind =
            this.props.envelope.transaction?.length ?
                this.props.envelope.transaction?.length > 1 ? 'transactions' : 'transaction' :
            this.props.envelope.signatures?.length ?
                this.props.envelope.signatures?.length > 1 ? 'signatures' : 'signature' :
            'messages';

        return (
            <div>
                <Title level={2}>Successfully submitted {kind}</Title>
                <ul>
                    {this.props.submissions.map(s => {
                        const id = s.status!.txID!.toString().replace(/^acc:\/\//, '')
                        return (
                            <li key={id}>
                                <Link href={`https://explorer.accumulatenetwork.io/acc/${id}`} target="_blank">
                                    {id} <LinkOutlined />
                                </Link>
                            </li>
                        )
                    })}
                </ul>
                <Button onClick={() => this.props.onClose()}>Close</Button>
            </div>
        )
    }
}