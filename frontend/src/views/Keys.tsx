import Component from "../lib/Component";
import React from "react";
import { Button, ConfigProvider, Descriptions, Empty, Form, Input, List, Modal, Select, Switch, Tabs, Tooltip, Typography, notification } from "antd";
import { api } from "../../wailsjs/go/models";
import { Buffer } from "accumulate.js/lib/common";
import { SignatureType } from "accumulate.js/lib/core";
import { MainContext } from "../lib/MainContext";
import { Unlocked } from "../components/Unlocked";
import { PlusOutlined } from '@ant-design/icons';
import { Link } from "react-router-dom";
import { Address, PrivateKeyAddress } from "accumulate.js";

const { Title } = Typography;

type Key = Omit<api.Key, 'convertValues'> & { vault: string, name: string }

interface State {
    status?: api.StatusResponse;
    keys?: Key[]
    modal: M;
    confirmingNewKey?: boolean;
    importedAddress?: PrivateKeyAddress;
}

type GenerateValues = {
    type: SignatureType,
    vault?: string,
    label?: string,
    ledger: boolean,
}

type ImportValues = {
    vault?: string,
    label?: string,
}

function keyName(key: Omit<api.Key, 'convertValues'>) {
    if (!key.labels) return 'unknown key'
    if (!key.labels.names?.length) return key.labels.lite || 'unknown key';
    return key.labels.names[0]
}

enum M {
    None,
    NewKey,
}

export default class Keys extends Component<{}, State> {
    static contextType = MainContext;
    context: React.ContextType<typeof MainContext>;
    private unlocked = React.createRef<Unlocked>();

    state: State = {
        modal: M.None,
    };

    private async refresh(status: api.StatusResponse) {
        try {
            const names = ['index', ...(status.vaults || []).map(x => x.name!)]
            const resp = await Promise.all(names.map(vault => this.unlocked.current!.go.KeyList({ vault })));

            const keys: Key[] = [];
            for (const i in resp) {
                for (const k of resp[i].keyList || []) {
                    const vault = names[i];
                    const name = keyName(k);
                    const key = { vault, name, ...k };
                    keys.push(key);
                }
            }
            this.updateState({ status, keys });
        } catch (error) {
            this.context.onError(error);
        }
    }

    private async generateKey(values: GenerateValues) {
        try {
            const key = await this.context.go.GenerateAddress({
                type: SignatureType.getName(values.type) as any,
                labels: values.label?.length ? [values.label] : [],
            })
            const typ = SignatureType.byName(key.keyInfo!.type! as any)
            const pub = Buffer.from(key.publicKey as any, 'hex')
            const addr = await Address.fromKey(typ, pub)
            notification.info({
                message: `Generated key ${await addr.format()}`
            })
            this.refresh(this.state.status!)
        } catch (error) {
            this.context.onError(error);
        }
    }

    private async generateLedgerKey(values: GenerateValues) {
        try {
            const key = await this.context.go.GenerateLedgerKey({
                sigType: SignatureType.getName(values.type) as any,
                labels: values.label?.length ? [values.label] : [],
            })
            const typ = SignatureType.byName(key.keyInfo!.type! as any)
            const pub = Buffer.from(key.publicKey as any, 'hex')
            const addr = await Address.fromKey(typ, pub)
            notification.info({
                message: `Generated ledger key ${await addr.format()}`
            })
            this.refresh(this.state.status!)
        } catch (error) {
            this.context.onError(error);
        }
    }

    private async importKey(values: ImportValues) {
        try {
            const { importedAddress: key } = this.state
            await this.context.go.ImportKey({
                vault: values.vault,
                labels: values.label?.length ? [values.label] : [],
                seed: Buffer.from(key!.privateKey).toString('hex') as any,
                type: SignatureType.getName(key!.type) as any,
            })

            this.refresh(this.state.status!)
        } catch (error) {
            this.context.onError(error);
        }
    }

    render() {
        const newKeyButton =
            <Tooltip title="New key" placement="bottom">
                <Button
                    shape="circle"
                    icon={<PlusOutlined />}
                    onClick={() => this.updateState({ modal: M.NewKey }) } />
            </Tooltip>

        const keysTable =
            <List
                bordered
                itemLayout="horizontal"
                dataSource={this.state.keys}
                loading={!this.state.keys}
                footer={newKeyButton}
                renderItem={(r) => {
                    let link: HTMLAnchorElement;
                    return (
                        <List.Item key={`${r.vault}.${r.name}`} onClick={() => link.click()} style={{ cursor: 'pointer' }}>
                            <Link ref={el => link = el!} to={`/keys/${r.vault || 'index'}/${r.name}`}>
                                <span>{r.name}</span>
                                {r.vault !== 'index' && <span style={{ color: 'grey' }}> ({r.vault})</span>}
                            </Link>
                        </List.Item>
                    )
                }}
            />

        const keyType =
            <Select>
                <Select.Option value={SignatureType.ED25519}>ED25519</Select.Option>
                <Select.Option value={SignatureType.RCD1}>RCD1</Select.Option>
                <Select.Option value={SignatureType.ETH}>Ethereum</Select.Option>
            </Select>

        const generateKeyForm =
            <Form
                layout="vertical"
                requiredMark="optional"
                disabled={this.state.confirmingNewKey}
                initialValues={{ type: SignatureType.ED25519, ledger: false }}
                onFinish={async (v: GenerateValues) => {
                    this.updateState({ confirmingNewKey: true })

                    if (v.ledger) await this.generateLedgerKey(v)
                    else await this.generateKey(v)

                    this.updateState({ modal: M.None, confirmingNewKey: false })
                }}>
                <Title level={5}>Generate a key</Title>

                <Form.Item
                    label="Type"
                    name="type"
                    rules={[{ required: true }]}
                    children={keyType} />

                {this.state.status?.wallet?.multi && (
                    <Form.Item
                        label="Vault"
                        name="vault"
                        rules={[{ required: true }]}>
                        <Select>
                            {this.state.status.vaults?.map(vault =>
                                <Select.Option key={vault.name} value={vault.name}>{vault.name}</Select.Option>
                            )}
                        </Select>
                    </Form.Item>
                )}

                <Form.Item
                    label="Label"
                    name="label"
                    children={<Input />} />

                <Tooltip title="Use your Ledger Nano to generate the key">
                    <Form.Item
                        label="Ledger"
                        name="ledger"
                        rules={[{ required: true }]}
                        // The switch must be a direct child of the form item,
                        // so the tooltip must be outermost
                        children={<Switch />} />
                </Tooltip>

                <div style={{ display: 'flex', justifyContent: 'right', marginTop: '20px'}}>
                    <Form.Item style={{ marginBottom: 0 }}>
                        <Button htmlType="submit" type="primary" loading={this.state.confirmingNewKey}>Generate</Button>
                    </Form.Item>
                </div>
            </Form>

        const importKeyForm =
            <Form
                layout="vertical"
                requiredMark="optional"
                disabled={this.state.confirmingNewKey}
                initialValues={{ type: SignatureType.ED25519 }}
                onFinish={async (v: ImportValues) => {
                    this.updateState({ confirmingNewKey: true })
                    await this.importKey(v)
                    this.updateState({ modal: M.None, confirmingNewKey: false })
                }}>
                <Title level={5}>Import a key</Title>

                {this.state.status?.wallet?.multi && (
                    <Form.Item
                        label="Vault"
                        name="vault"
                        rules={[{ required: true }]}>
                        <Select>
                            {this.state.status.vaults?.map(vault =>
                                <Select.Option key={vault.name} value={vault.name}>{vault.name}</Select.Option>
                            )}
                        </Select>
                    </Form.Item>
                )}

                <Form.Item
                    label="Label"
                    name="label"
                    children={<Input />}
                />

                <Form.Item
                    label="Address"
                    name="address"
                    rules={[{
                        required: true,
                        validator: async (_, value) => {
                            const { privateKey, publicKey, type } = await this.context.go.ParseKey({
                                address: value,
                            })
                            this.updateState({
                                importedAddress: await PrivateKeyAddress.from(
                                    SignatureType.byName(type as any),
                                    Buffer.from(publicKey, 'hex'),
                                    Buffer.from(privateKey, 'hex'),
                                ),
                            })
                        }
                    }]}
                    children={<Input.Password />}
                />
                <div style={{ display: 'flex', justifyContent: 'right', marginTop: '20px' }}>
                    <Form.Item style={{ marginBottom: 0 }}>
                        <Button htmlType="submit" type="primary" loading={this.state.confirmingNewKey}>Import</Button>
                    </Form.Item>
                </div>
            </Form>

        const newKeyModal =
            <Tabs
                defaultActiveKey="generate"
                items={[
                    {
                        key: "generate",
                        label: <span>Generate</span>,
                        children: generateKeyForm
                    },
                    {
                        key: "import",
                        label: <span>Import</span>,
                        children: importKeyForm,
                    },
                ]} />

        return (
            <Unlocked ref={this.unlocked} onUnlock={(s) => this.refresh(s)}>
                <ConfigProvider
                    children={keysTable}
                    renderEmpty={() => {
                        const noVaults = this.state.status?.wallet?.multi && !this.state.status.vaults?.length
                        return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={noVaults ? "No open vaults" : "No keys"} />
                    }} />
                <Modal
                    open={this.state.modal == M.NewKey}
                    onCancel={() => this.updateState({ modal: M.None })}
                    footer={null}
                    confirmLoading={this.state.confirmingNewKey}
                    children={newKeyModal} />
            </Unlocked>
        )
    }
}