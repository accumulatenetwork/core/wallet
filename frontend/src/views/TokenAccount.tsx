import { useParams } from "react-router-dom";
import Component from "../lib/Component";
import React from "react";
import { core } from "accumulate.js";
import { MainContext } from "../lib/MainContext";
import { AccountRecord } from "accumulate.js/lib/api_v3";
import { Button, Descriptions, Modal, Spin, Typography } from "antd";
import { Buffer } from "accumulate.js/lib/common";
import { SubmitResult, formatTokenBalance } from "../lib/util";
import { Submitted } from "./Submitted";
import { SendTokensModal } from "../modals/SendTokensModal";

const { Title } = Typography;

interface DynamicProps {
    account: string
}

interface State {
    account?: core.TokenAccount | core.LiteTokenAccount
    issuer?: core.TokenIssuer;
    showSend?: boolean;
    sendResult?: SubmitResult | null;
}

class TokenAccount extends Component<{} & DynamicProps, State> {
    static contextType = MainContext;
    context: React.ContextType<typeof MainContext>;

    state: State = {}

    async propsDidChange() {
        try {
            const url = Buffer.from(this.props.account, 'base64').toString('utf-8')
            const account = (await this.context.network!.query(url) as AccountRecord).account as core.TokenAccount | core.LiteTokenAccount;
            const issuer = (await this.context.network!.query(account.tokenUrl!) as AccountRecord).account as core.TokenIssuer;
            this.updateState({ account, issuer });
        } catch (error) {
            this.context.onError(error);
        }
    }

    render() {
        const { account, issuer } = this.state
        if (!account || !issuer) {
            return <Spin tip="Loading" />
        }

        const main =
            <div>
                <Title level={2} className="break-all">Account</Title>
                <Title level={4} type="secondary" style={{ marginTop: "-10px" }} className="break-all" copyable={{text: account.url!.toString()}}>{account.url!.toString()}</Title>

                <Descriptions bordered column={1} size="middle">
                    <Descriptions.Item label="Balance">{account && formatTokenBalance(account, issuer)}</Descriptions.Item>
                </Descriptions>

                <Button onClick={() => this.updateState({ showSend: true })}>Send Tokens</Button>
            </div>

        const sendModal =
            <SendTokensModal
                account={account}
                issuer={issuer}
                close={() => this.updateState({ showSend: false })}
                modal={{
                    title: "Send tokens",
                    open: this.state.showSend,
                }}
                onFinish={(r) => this.updateState({ sendResult: r })}
            />

        const submittedModal =
            <Modal
                open={!!this.state.sendResult}
                onCancel={() => this.updateState({ sendResult: null })}
                footer={null}>
                {this.state.sendResult && (
                    <Submitted
                        {...this.state.sendResult}
                        onClose={() => this.updateState({ sendResult: null })}
                    />
                )}
            </Modal>

        return (
            <div>
                {main}
                {sendModal}
                {submittedModal}
            </div>
        )
    }
}

export default function(props: {}) {
    return <TokenAccount {...props} {...(useParams() as any as DynamicProps)} />
}