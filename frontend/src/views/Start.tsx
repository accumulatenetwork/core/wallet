import React from "react";
import Component from "../lib/Component";
import { MainContext } from "../lib/MainContext";
import { Button, List, Select, Tooltip, Typography } from "antd";
import * as go from "../../wailsjs/go/main/App";
import { PlusOutlined, WalletOutlined, MinusOutlined } from '@ant-design/icons';

const { Title } = Typography;

export const MainNet = 'https://mainnet.accumulatenetwork.io'
const Kermit = 'https://kermit.accumulatenetwork.io'
const Fozzie = 'https://fozzie.accumulatenetwork.io'

interface Props {
    first: boolean;
    path?: string;
    didOpen(wallet: Wallet);
}

interface State {
    homeDir?: string;
    slash?: string;
    wallets?: Wallet[]
}

export interface Wallet {
    path: string;
    network: string;
    networkName?: string;
}

export class Start extends Component<Props, State> {
    static contextType = MainContext;
    context: React.ContextType<typeof MainContext>;

    async componentDidMount() {
        try {
            // Get the user's home directory
            const homeDir = await go.UserHomeDir();
            const slash = homeDir.startsWith('/') ? '/' : '\\'
            this.updateState({ homeDir, slash })

            // Load stored wallet paths
            const s = localStorage.getItem('wallets')

            // If this is the first time the app is launched, add the default wallet
            const defaultPath = [homeDir, '.accumulate', 'wallet'].join(slash)
            const wallets: Wallet[] = s ? JSON.parse(s) : [{
                path: defaultPath,
                network: MainNet,
            }]
            this.setWallets(wallets)

            // If a path is already open, open that wallet
            const w = this.props.path && wallets.find(w => w.path === this.props.path)
            if (w) {
                this.props.didOpen(w)
                return
            }

            // If this is the initial app launch and there's only one wallet and it's the default, open it
            if (this.props.first && wallets.length === 1 && wallets[0].path == defaultPath) {
                this.props.didOpen(wallets[0])
                return
            }
        } catch (error) {
            this.context.onError(error)
        }
    }

    private setWallets(wallets: Wallet[]) {
        this.updateState({ wallets })
        localStorage.setItem('wallets', JSON.stringify(wallets))
    }

    private addWallet(wallet: Wallet) {
        const { wallets = [] } = this.state || {}
        wallets.push(wallet)
        this.setWallets(wallets)
    }

    private removeWallet(wallet: Wallet) {
        const { wallets = [] } = this.state || {}
        const i = wallets.indexOf(wallet)
        if (i < 0) return
        wallets.splice(i, 1)
        this.setWallets(wallets)
    }

    private didUpdateWallets() {
        if (this.state.wallets) {
            this.setWallets(this.state.wallets);
        }
    }

    private niceName(w: Wallet) {
        const { path } = w
        if (this.state?.homeDir && path.startsWith(this.state.homeDir))
            return '~' + path.substring(this.state.homeDir.length)
        return path
    }

    private async chooseWallet() {
        try {
            const path = await go.ChooseWallet();
            if (path) {
                this.addWallet({ path, network: MainNet, networkName: 'MainNet' })
            }
        } catch (error) {
            this.context.onError(error)
        }
    }

    render() {
        const options = [
            { id: 'mainnet', value: MainNet, name: 'MainNet' },
            { id: 'kermit', value: Kermit, name: 'Kermit TestNet' },
            { id: 'fozzie', value: Fozzie, name: 'Fozzie TestNet' },
        ]

        return (
            <div>
                <Title level={2}>Open a wallet</Title>

                <List
                    itemLayout="horizontal"
                    bordered
                    dataSource={this.state?.wallets || []}
                    renderItem={w => (
                        <List.Item
                            key={w.path}
                            className="start-wallet-item"
                            onClick={() => {
                                w.networkName = options.find(x => x.value == w.network)?.name
                                this.props.didOpen(w)
                            }}>
                            <span>
                                <WalletOutlined /> {this.niceName(w)}
                            </span>

                            <div>
                                <Select
                                    style={{ minWidth: '150px'}}
                                    defaultValue={w.network}
                                    onClick={(e) => {
                                        e.stopPropagation()
                                        e.nativeEvent.stopImmediatePropagation()
                                    }}
                                    onChange={(value) => {
                                        w.network = value
                                        this.didUpdateWallets()
                                    }}>
                                    {options.map(({ id, value, name }) => (
                                        <Select.Option id={id} value={value}>{name}</Select.Option>
                                    ))}
                                </Select>

                                <Tooltip title="Remove" placement="bottom">
                                    <Button
                                        shape="circle"
                                        icon={<MinusOutlined />}
                                        style={{ marginLeft: '15px'}}
                                        onClick={(e) => {
                                            e.stopPropagation()
                                            e.nativeEvent.stopImmediatePropagation()
                                            this.removeWallet(w)
                                        }}/>
                                </Tooltip>
                            </div>

                        </List.Item>
                    )}
                    footer={(
                        <div style={{ display: 'flex', justifyContent: 'center'}}>
                            <Tooltip title="Browse for a wallet" placement="bottom">
                                <Button
                                    shape="circle"
                                    icon={<PlusOutlined />}
                                    onClick={() => this.chooseWallet()}/>
                            </Tooltip>
                        </div>
                    )}
                    />
            </div>
        )
    }
}