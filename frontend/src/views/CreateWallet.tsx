import React from "react";
import Component from "../lib/Component";
import { MainContext } from "../lib/MainContext";
import { Alert, Button, Descriptions, Form, FormInstance, Input, Radio, Space, Switch, Typography } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';

const { Title } = Typography;

interface Props {
    path: string;
    didCreate();
}

interface State {
    multi: boolean;
    encrypted: boolean;
    passphrase?: string;
    mnemonic?: string;
    newMnemonic: boolean;
}

export class CreateWallet extends Component<Props, State> {
    static contextType = MainContext;
    context: React.ContextType<typeof MainContext>;

    constructor(props: Props) {
        super(props);
    }

    state: State = {
        multi: false,
        encrypted: true,
        newMnemonic: false,
    };

    private form = React.createRef<FormInstance<State>>();

    private async create({ multi, passphrase, mnemonic }: State) {
        try {
            // TODO Confirm mnemonic

            await this.context.go.CreateWallet({
                multiVault: multi,
                passphrase: passphrase,
                path: this.props.path,
            })

            if (!multi) {
                await this.context.go.ImportMnemonic({
                    mnemonic: mnemonic!.split(' ')
                })
            }

            this.props.didCreate();
        } catch (error) {
            this.context.onError(error);
        }
    }

    private async generateMnemonic() {
        try {
            const { mnemonic } = await this.context.go.GenerateMnemonic({ entropy: 128 })
            this.form.current!.setFieldsValue({
                mnemonic: mnemonic!.join(' '),
                newMnemonic: true,
            })
        } catch (error) {
            this.context.onError(error);
        }
    }

    render() {
        return (
            <div>
                <Title level={2}>Create a wallet</Title>

                <Form
                    ref={this.form}
                    initialValues={this.state}
                    onValuesChange={(v: Partial<State>) => this.updateState(v)}
                    onFinish={(v: State) => this.create(v)}>
                    <Descriptions bordered column={1} size="middle">
                        <Descriptions.Item label="Multi-vault">
                            <Form.Item name="multi" valuePropName="checked" style={{ marginBottom: 0 }}><Switch /></Form.Item>
                            In single-vault mode, the wallet behaves like a traditional crypto wallet. Single-vault mode is the best choice for most users.<br />
                            In multi-vault mode, keys are stored in named vaults, each encrypted (optionally) with a different passphrase and each with a different mnemonic. Multi-vault mode is not recommended for beginners. Multi-vault mode is most useful for users who want to segregate different keys used for different purposes.
                            {this.state.multi && <Alert type="warning" message="Multi-vault support is currently limited - the only way to create vaults is via the CLI" />}
                        </Descriptions.Item>

                        <Descriptions.Item label="Encrypted">
                            <div style={{ display: 'flex' }}>
                                <Form.Item name="encrypted" valuePropName="checked" style={{ marginBottom: 0, marginRight: '10px' }}>
                                    <Switch defaultChecked />
                                </Form.Item>

                                <Form.Item name="passphrase" rules={[{ required: this.state?.encrypted }]} style={{ marginBottom: 0, flex: 1 }}>
                                    <Input.Password
                                        placeholder="Password"
                                        disabled={!this.state?.encrypted}
                                        iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                                        />
                                </Form.Item>
                            </div>
                            Encrypting your wallet is highly recommended
                        </Descriptions.Item>

                        {!this.state.multi && <Descriptions.Item label="Mnemonic">
                            <Space.Compact style={{ width: '100%' }}>
                                <Button onClick={() => this.generateMnemonic()}>Generate</Button>

                                <Form.Item name="mnemonic" rules={[{ required: true }]} style={{ marginBottom: 0, width: '100%' }}>
                                    <Input.Password
                                        placeholder="Mnemonic"
                                        disabled={this.state.newMnemonic}
                                        iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)} />
                                </Form.Item>
                            </Space.Compact>
                        </Descriptions.Item>}

                        <Descriptions.Item>
                            <Form.Item style={{ marginBottom: 0 }}>
                                <Button htmlType="submit" type="primary">Create</Button>
                            </Form.Item>
                        </Descriptions.Item>
                    </Descriptions>
                </Form>
            </div>
        )
    }
}