import React from "react";
import { api } from "../../wailsjs/go/models";
import Component from "../lib/Component";
import { MainContext } from "../lib/MainContext";
import { Unlocked } from "../components/Unlocked";
import { Button, ConfigProvider, Empty, Form, Input, Select, Table, TableProps, Tooltip } from "antd";
import { AccountRecord } from "accumulate.js/lib/api_v3";
import { AccountType, LiteTokenAccount, TokenAccount, TokenIssuer } from "accumulate.js/lib/core";
import { Link } from "react-router-dom";
import { Buffer } from "accumulate.js/lib/common";
import { PlusOutlined } from '@ant-design/icons';
import { FormModal } from "../lib/FormModal";
import { validateAccount } from "../lib/util";

interface Account {
    vault: string;
    url: string;
    issuer: TokenIssuer;
    balance?: bigint;
}

interface State {
    status?: api.StatusResponse;
    accounts?: Account[];
    showRegister?: boolean;
}

interface RegisterValues {
    account: string
    vault: string
}

export default class TokenAccounts extends Component<{}, State> {
    static contextType = MainContext;
    context: React.ContextType<typeof MainContext>;
    private unlocked = React.createRef<Unlocked>();

    state: State = {}

    private async getRegistered(status: api.StatusResponse) {
        const accounts: Account[] = [];
        const load = async (url: string, vault: string) => {
            const account = (await this.context.network!.query(url) as AccountRecord).account as TokenAccount | LiteTokenAccount;
            const issuer = (await this.context.network!.query(account.tokenUrl!) as AccountRecord).account as TokenIssuer;

            accounts.push({
                vault,
                url,
                issuer,
                balance: account.balance,
            });
        }

        await Promise.all([
            'index',
            ...(status.vaults || []).map(x => x.name!)
        ].map(async vault => {
            const { tokens } = await this.unlocked.current!.go.ListAccounts({ vault })
            if (!tokens) return;
            return Promise.all(tokens?.map(({ url }) => load(url, vault)));
        }))
        return accounts;
    }

    private async getLite(status: api.StatusResponse) {
        const accounts: Account[] = [];
        const load = async (key: api.Key, vault: string) => {
            const r = await this.context.network!.query(key.labels!.lite!, { queryType: 'directory', range: {} })
            for (const { value: url } of r?.records || []) {
                const account = (await this.context.network!.query(url!) as AccountRecord).account as LiteTokenAccount;
                const issuer = (await this.context.network!.query(account.tokenUrl!) as AccountRecord).account as TokenIssuer;

                accounts.push({
                    vault,
                    url: url!.toString(),
                    issuer,
                    balance: account.balance,
                });
            }
        }

        await Promise.all([
            'index',
            ...(status.vaults || []).map(x => x.name!)
        ].map(async vault => {
            const { keyList } = await this.unlocked.current!.go.KeyList({ vault })
            if (!keyList) return;
            return Promise.all(keyList?.map(k => load(k, vault)));
        }))
        return accounts;
    }

    private async refresh(status: api.StatusResponse) {
        try {
            const accounts = [
                ...await this.getRegistered(status),
                ...await this.getLite(status),
            ]
            this.updateState({ status, accounts });
        } catch (error) {
            this.context.onError(error);
        }
    }

    render() {
        const registerButton =
            <Tooltip title="Register an account" placement="bottom">
                <Button
                    shape="circle"
                    icon={<PlusOutlined />}
                    onClick={() => this.updateState({ showRegister: true }) }/>
            </Tooltip>

        const registerModal =
            <FormModal
                modal={{
                    open: this.state.showRegister,
                    title: "Register an account",
                }}
                form={{
                    layout: "vertical",
                    requiredMark: "optional",
                }}
                close={() => this.updateState({ showRegister: false })}
                onFinish={async ({ account, vault }: RegisterValues) => {
                    try {
                        await this.context.go.RegisterTokenAccount({ url: account, vault } as any)
                        this.refresh(this.state.status!);
                    } catch (error) {
                        this.context.onError(error);
                    }
                }}>
                {this.state.status?.wallet?.multi && (
                    <Form.Item
                        label="Vault"
                        name="vault"
                        rules={[{ required: true }]}>
                        <Select>{this.state.status.vaults?.map(vault =>
                            <Select.Option key={vault.name} value={vault.name}>{vault.name}</Select.Option>
                        )}</Select>
                    </Form.Item>
                )}
                <Form.Item
                    label="Account"
                    name="account"
                    rules={[{
                        required: true,
                        validator: async (_, value) => {
                            await validateAccount(this.context.network!, value, AccountType.LiteTokenAccount, AccountType.TokenAccount)
                        },
                    }]}
                    children={<Input />} />
                <div style={{ display: 'flex', justifyContent: 'right' }}>
                    <Form.Item style={{ marginBottom: 0 }}>
                        <FormModal.Context children={state =>
                            <Button htmlType="submit" type="primary" loading={state.confirming}>Register</Button>
                        } />
                    </Form.Item>
                </div>
            </FormModal>

        const columns: TableProps<Account>['columns'] = [
            {
                title: registerButton,
                key: 'actions',
                width: '0px',
                render: () => null,
            },
            {
                title: 'Account',
                key: 'url',
                dataIndex: 'url',
                render(value: string) {
                    value = value.replace(/^acc:\/\//, '')
                    return <Link to={`/tokens/${Buffer.from(value, 'utf-8').toString('base64')}`}>{value}</Link>
                },
            },
            {
                title: 'Balance',
                key: 'balance',
                render(_, record) {
                    if (!record.balance) {
                        return <span style={{ color: 'grey' }}>0 {record.issuer.symbol!}</span>
                    }

                    const { precision } = record.issuer;
                    let balance = record.balance?.toString();
                    if (balance.length <= precision!) {
                        balance = '0'.repeat(precision! - balance.length + 1) + balance
                    }
                    balance = balance.substring(0, balance.length - precision!) + '.' + balance.substring(balance.length - precision!)
                    balance = balance.replace(/\.?0+$/, '')
                    return <span>{balance} {record.issuer.symbol!}</span>
                }
            },
        ]
        const accountsTable =
            <ConfigProvider renderEmpty={() => {
                const noVaults = this.state?.status?.wallet?.multi && !this.state.status.vaults?.length
                return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={noVaults ? "No open vaults" : "No registered accounts"} />
            }}>
                <Table
                    columns={columns}
                    dataSource={this.state?.accounts}
                    loading={!this.state?.accounts}
                    rowKey='url'
                />
            </ConfigProvider>

        return (
            <Unlocked
                ref={this.unlocked}
                onUnlock={async (s) => this.refresh(s)}>
                {accountsTable}
                {registerModal}
            </Unlocked>
        )
    }
}