import React from "react";
import { api } from "../../wailsjs/go/models";
import Component from "../lib/Component";
import { MainContext } from "../lib/MainContext";
import { Descriptions, Spin, Typography } from "antd";
import Address from "../components/Address";
import { useParams } from "react-router-dom";

const { Title } = Typography;

interface DynamicProps {
    vault: string;
    name: string;
}

interface State {
    key?: Omit<api.Key, 'convertValues'>;
}

class Key extends Component<DynamicProps, State> {
    static contextType = MainContext;
    context: React.ContextType<typeof MainContext>;

    state: State = {};

    async propsDidChange() {
        try {
            const { key } = await this.context.go.ResolveKey({
                vault: this.props.vault,
                value: this.props.name,
            })
            this.updateState({ key });
        } catch (error) {
            this.context.onError(error);
        }
    }

    render() {
        if (!this.state.key) return <Spin />

        return (
            <div>
                <Descriptions bordered column={1} size="middle">
                    <Descriptions.Item label="Key"><Address theKey={this.state.key} /></Descriptions.Item>
                    <Descriptions.Item label="Lite Identity">{this.state.key.labels!.lite}</Descriptions.Item>
                    {this.props.vault !== "index" && <Descriptions.Item label="Vault">{this.props.vault}</Descriptions.Item>}
                    {this.state.key.labels?.names?.map(name => (
                        <Descriptions.Item key={name} label="Name">{name}</Descriptions.Item>
                    ))}
                    {this.state.key.pages?.map(page => (
                        <Descriptions.Item key={page} label="Page">{page}</Descriptions.Item>
                    ))}
                </Descriptions>
            </div>
        )
    }
}
export default function(props: {}) {
    return <Key {...props} {...(useParams() as any as DynamicProps)} />
}