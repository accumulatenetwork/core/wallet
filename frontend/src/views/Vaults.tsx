import React from "react";
import { api } from "../../wailsjs/go/models";
import Component from "../lib/Component";
import { CloseCircleOutlined, UnlockTwoTone, LockOutlined, LoginOutlined, StopOutlined } from '@ant-design/icons';
import { Button, ConfigProvider, Empty, List, Row, Tooltip } from "antd";
import { MainContext } from "../lib/MainContext";
import { Unlocked } from "../components/Unlocked";
import { Unlock } from "../components/Unlock";

interface Props {
}

interface State {
    status?: api.StatusResponse
    vaults?: api.ListVaultsResponse
    vaultToUnlock: string | null;
}

export default class Vaults extends Component<Props, State> {
    static contextType = MainContext;
    context: React.ContextType<typeof MainContext>;
    private unlocked = React.createRef<Unlocked>();
    private escapeCallback: ((this: Document, ev: KeyboardEvent) => any) | null;

    private async open(vault: string) {
        try {
            await this.unlocked.current!.go.OpenVault({ vault });
            this.unlock(vault);
        } catch (error) {
            this.context.onError(error);
        }
    }

    private async unlock(vault: string) {
        this.clearUnlock();

        this.updateState({ vaultToUnlock: vault });
        this.escapeCallback = (event) => {
            if (event.key !== 'Escape') return;
            this.clearUnlock();
        }

        document.addEventListener("keydown", this.escapeCallback);
    }

    private async lock(vault: string) {
        try {
            await this.unlocked.current!.go.LockVault({ vault });
        } catch (error) {
            this.context.onError(error);
        }
    }

    private async close(vault: string) {
        try {
            await this.unlocked.current!.go.LockVault({ vault, close: true });
        } catch (error) {
            this.context.onError(error);
        }
    }

    private async clearUnlock() {
        this.updateState({ vaultToUnlock: null });
        if (this.escapeCallback) {
            document.removeEventListener("keydown", this.escapeCallback);
            this.escapeCallback = null;
        }
    }

    componentWillUnmount(): void {
        this.clearUnlock();
    }

    render() {
        return (
            <Unlocked indexOnly ref={this.unlocked} onUnlock={(status) => {
                if (!status.wallet?.multi) {
                    this.updateState({ status })
                    return
                }
                this.context.go.ListVaults({}).then((vaults) =>{
                    this.updateState({ status, vaults })
                });
            }}>
                <ConfigProvider renderEmpty={() => {
                    if (!this.state?.status?.wallet?.multi) {
                        return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description={
                            <span>This is a single-vault wallet. It can be converted to a multi-vault wallet with <code>accumulate vault migrate</code>.</span>
                        } />
                    }
                    return <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} description="No vaults" />
                }}>
                    <List
                        itemLayout="horizontal"
                        dataSource={this.state?.vaults?.vaults}
                        loading={!this.state?.status}
                        renderItem={name => {
                            const status = this.state?.status?.vaults?.find(x => x.name === name);

                            if (!status?.open) {
                                return (
                                    <List.Item key={name} actions={[
                                        <Tooltip title="open" placement="bottom">
                                            <LoginOutlined style={{ cursor: 'pointer' }} onClick={() => this.open(name)} />
                                        </Tooltip>
                                    ]}>
                                        <Button type="link" block onClick={() => this.open(name)}>{name}</Button>
                                    </List.Item>
                                )
                            }

                            const closeAction = (
                                <Tooltip title="close" placement="bottom">
                                    <CloseCircleOutlined style={{ cursor: 'pointer' }} onClick={() => this.close(name)} />
                                </Tooltip>
                            );

                            if (status.unlocked) {
                                return (
                                    <List.Item key={name} actions={[
                                        <Tooltip title="lock" placement="bottom">
                                            <UnlockTwoTone style={{ cursor: 'pointer' }} onClick={() => this.lock(name)} />
                                        </Tooltip>,
                                        closeAction,
                                    ]}>
                                        <div style={{ width: '100%' }}>
                                            <Row justify='center'>{name}</Row>
                                        </div>
                                    </List.Item>
                                )
                            }

                            if (this.state.vaultToUnlock != name) {
                                return (
                                    <List.Item key={name} actions={[
                                        <Tooltip title="unlock" placement="bottom">
                                            <LockOutlined style={{ cursor: 'pointer' }} onClick={() => this.unlock(name)} />
                                        </Tooltip>,
                                        closeAction,
                                    ]}>
                                        <Button type="link" block onClick={() => this.unlock(name)}>{name}</Button>
                                    </List.Item>
                                )
                            }

                            const { go: _, ...restContext } = this.context;
                            return (
                                <List.Item key={name} actions={[
                                    <Tooltip title="cancel" placement="bottom">
                                        <StopOutlined style={{ cursor: 'pointer' }} onClick={() => this.clearUnlock()} />
                                    </Tooltip>,
                                    closeAction,
                                ]}>
                                    <div style={{ width: '100%' }}>
                                        <Row justify='center'>{name}</Row>
                                        <MainContext.Provider value={{ go: this.unlocked.current!.go, ...restContext }}>
                                            <Unlock vault={this.state?.vaultToUnlock} onUnlock={() => this.clearUnlock()} />
                                        </MainContext.Provider>
                                    </div>
                                </List.Item>
                            )
                        }}
                        />
                </ConfigProvider>
            </Unlocked>
        )
    }
}