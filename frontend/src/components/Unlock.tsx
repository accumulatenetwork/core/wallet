import React from "react";
import Component from "../lib/Component";
import { MainContext } from "../lib/MainContext";
import { Form, FormInstance, Input, Tooltip, notification } from "antd";
import { EyeInvisibleOutlined, EyeTwoTone, UnlockTwoTone } from '@ant-design/icons';

interface Props {
    vault: string;
    onUnlock: () => void;
}

interface Values {
    password?: string;
}

export class Unlock extends Component<Props, {}> {
    static contextType = MainContext;
    context: React.ContextType<typeof MainContext>;
    private form = React.createRef<FormInstance<Values>>();

    private async unlock() {
        try {
            const { password } = this.form.current?.getFieldsValue() || {}
            if (!password) return;

            await this.context.go.UnlockVault({
                vault: this.props.vault,
                passphrase: password,
                timeout: '10m' as any,
            });

            this.props.onUnlock();

        } catch (error) {
            if (typeof error === 'object' && typeof error.data === 'string' && error.data.endsWith(': cipher: message authentication failed')) {
                this.form.current?.setFields([{
                    name: 'password',
                    value: null,
                    errors: ['Incorrect password'],
                }]);
                return;
            }

            this.context.onError(error);
        }
    }

    async componentDidMount() {
        try {
            const status = await this.context.go.Status({});
            const myStatus = status.vaults?.find(v => v.name === this.props.vault);
            if (!myStatus) return;

            if (myStatus.unlocked) {
                this.props.onUnlock();
                return;
            }

            // Attempt to unlock with 1Password
            if (myStatus.onePasswordRef) {
                const { success } = await this.context.go.UnlockVault({
                    vault: this.props.vault,
                    use1Password: true,
                    timeout: '10m' as any,
                });
                if (success) {
                    notification.success({
                        message: `Unlocked ${this.props.vault} vault with 1Password`,
                    })
                    this.props.onUnlock();
                    return;
                }
            }
        } catch (error) {
            this.context.onError(error);
        }
    }

    render() {
        return (
            <Form ref={this.form} onSubmitCapture={() => this.unlock()}>
                <Form.Item name="password" rules={[{ required: true }]} style={{ marginBottom: 0 }}>
                    <Input.Password
                        placeholder="Password"
                        iconRender={(visible) => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                        addonBefore={
                            <Tooltip placement="bottom" title="Unlock">
                                <UnlockTwoTone onClick={() => this.form.current?.submit()} />
                            </Tooltip>
                        }
                        />
                </Form.Item>
            </Form>
        )
    }
}