import Component from "../lib/Component";
import { MainContext } from "../lib/MainContext";
import { Button, Form, FormInstance, Input, Modal, Select, Space, Tooltip } from "antd";
import React from "react";
import { api } from "../../wailsjs/go/models";
import { KeyAndVault, SubmitResult, validateAccount } from "../lib/util";
import { AddCreditsModal } from "../modals/AddCreditsModal";
import { Account, AccountType, KeyPage, LiteIdentity } from "accumulate.js/lib/core";
import { Submitted } from "../views/Submitted";
import Label from "./Label";
import { ReloadOutlined } from '@ant-design/icons';
import { URLArgs } from "accumulate.js";

interface Props {
    principal?: Account;
    status: api.StatusResponse;
    form: React.RefObject<FormInstance<any>>;
    creditsRequired: number;
    onChange: (_: KeyAndVault | undefined) => void;
}

interface State {
    insufficientCredits: boolean;
    showBuyCredits: boolean;
    signers?: KeyAndVault[];
    signer?: LiteIdentity | KeyPage;
    buyResult?: SubmitResult | null;
}

export class GetSigner extends Component<Props, State> {
    static contextType = MainContext;
    context: React.ContextType<typeof MainContext>;

    state: State = {
        insufficientCredits: false,
        showBuyCredits: false,
    }

    private get authorities() {
        const { principal } = this.props
        if (!principal) return
        switch (principal.type) {
            case AccountType.Identity:
            case AccountType.TokenIssuer:
            case AccountType.TokenAccount:
            case AccountType.KeyBook:
            case AccountType.DataAccount:
                return principal.authorities!.map(x => x.url!.toString())

            case AccountType.KeyPage:
                return [principal.url!.toString().replace(/\/\d+$/, '')]

            case AccountType.LiteIdentity:
                return [principal.url!.toString()]

            case AccountType.LiteTokenAccount:
                return [principal.url!.authority]

            case AccountType.LiteDataAccount:
                // TODO
            default:
                throw new Error(`Unsupported account type ${AccountType.getName(principal.type)}`)
        }
    }

    async propsDidChange() {
        try {
            if (!this.props.principal) return
            const { authorities, props: { status } } = this;
            const signers: KeyAndVault[] = [];
            await Promise.all([
                'index',
                ...(status.vaults || []).map(x => x.name!)
            ].map(async vault => {
                const { paths } = await this.context.go.FindSigner({ vault, authorities })

                // Skip empty
                if (!paths) return

                for (const path of paths) {
                    // Skip delegated
                    if (path.signers!.length > 1)
                        continue

                    for (const key of path.keys!) {
                        signers.push({ vault, pages: path.signers, ...key })
                    }
                }
            }))
            this.updateState({ signers })

            const key = signers[0].publicKey! as any
            if (this.props.form.current!.getFieldValue('signer') !== key) {
                this.props.form.current!.setFieldValue('signer', signers[0].publicKey! as any)
                this.onChange(signers[0].publicKey! as any)
            }
        } catch (error) {
            this.context.onError(error);
        }
    }

    private async onChange(key: string) {
        try {
            const s = this.state!.signers!.find(s => s.publicKey as any === key)
            this.props.onChange(s)
            this.props.form.current!.setFieldsValue({ credits: null })
            if (!s) return;

            this.refresh(s.pages![0])
        } catch (error) {
            this.context.onError(error);
        }
    }

    private async refresh(url: URLArgs) {
        try {
            const signer = await validateAccount(this.context.network!, url, AccountType.KeyPage, AccountType.LiteIdentity) as KeyPage | LiteIdentity
            this.updateState({ signer })

            const credits = (signer.creditBalance || 0) / 100
            const insufficientCredits = credits < this.props.creditsRequired
            this.updateState({ insufficientCredits })
            this.props.form.current!.setFields([
                {
                    name: 'credits',
                    value: `${credits}`,
                    errors: insufficientCredits ? ['Insufficient balance'] : [],
                }
            ]);
        } catch (error) {
            this.context.onError(error);
        }
    }

    render() {
        return [
            <Form.Item
                label="Signer"
                name="signer"
                rules={[{ required: true }]}>
                <Select
                    disabled={!this.props.principal}
                    onChange={(key) => this.onChange(key)}
                    children={(this.state.signers || []).map(s => {
                        const v = `${s.labels!.names![0]} @ ${s.pages![0].replace(/^acc:\/\//, '')}`
                        return <Select.Option key={s.publicKey as any} value={s.publicKey}>{v}</Select.Option>
                    })}/>
            </Form.Item>,

            this.props.creditsRequired > 0 ? (
                <Form.Item
                    label="Credits"
                    requiredMark={false}>
                    <Space.Compact style={{ width: '100%' }}>
                        {this.state.insufficientCredits ? (
                            <Button
                                children="Buy credits"
                                onClick={() => this.updateState({ showBuyCredits: true })} />
                        ) : (
                            <Label value="Balance" />
                        )}
                        <Form.Item
                            name="credits"
                            noStyle
                            rules={[{ required: true }]}>
                            <Input
                                readOnly
                                disabled={!this.props.principal}
                                suffix={this.state.signer ? (
                                  <Tooltip title="Refresh">
                                        <ReloadOutlined
                                            style={{ color: 'hsla(0, 0%, 0%, .45)', cursor: 'pointer' }}
                                            onClick={() => this.refresh(this.state.signer!.url!)}
                                            />
                                  </Tooltip>
                                ) : null}/>
                        </Form.Item>
                        <Label value={`of ${this.props.creditsRequired}`} />
                    </Space.Compact>
                </Form.Item>
            ) : null,

            <AddCreditsModal
                recipient={this.state.signer}
                amount={this.props.creditsRequired}
                close={() => this.updateState({ showBuyCredits: false })}
                modal={{
                    title: "Buy credits",
                    open: this.state.showBuyCredits,
                }}
                onFinish={(r) => this.updateState({ buyResult: r })}
            />,

            <Modal
                open={!!this.state.buyResult}
                onCancel={() => this.updateState({ buyResult: null })}
                footer={null}>
                {this.state.buyResult && (
                    <Submitted
                        {...this.state.buyResult}
                        onClose={() => this.updateState({ buyResult: null })}
                    />
                )}
            </Modal>
        ]
    }
}