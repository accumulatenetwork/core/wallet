import React from "react";
import Component from "../lib/Component";
import { MainContext } from "../lib/MainContext";
import { api } from "../../wailsjs/go/models";
import { Spin } from "antd";
import { GoAPI } from "../lib/Go";
import { Unlock } from "./Unlock";

interface Props {
    children: React.ReactNode | React.ReactNode[];
    indexOnly?: boolean;
    onUnlock?: (status: api.StatusResponse) => void;
}

interface State {
    status: api.StatusResponse | null;
}

export class Unlocked extends Component<Props, State> {
    static contextType = MainContext;
    context: React.ContextType<typeof MainContext>;
    public go: GoAPI;

    async refresh() {
        try {
            // Get the status
            let status = await this.context.go.Status({});

            // Try unlock vaults, in case they are unencrypted
            let didUnlock = false;
            if (status.wallet?.exists && !status.wallet.open) {
                await this.context.go.OpenVault({ vault: 'index' })
                didUnlock = true;
            }
            if (!this.props.indexOnly) {
                for (const vault of this.state?.status?.vaults || []) {
                    if (vault.name && !vault.open) {
                        await this.context.go.OpenVault({ vault: vault.name })
                        didUnlock = true;
                    }
                }
            }
            if (didUnlock) {
                status = await this.context.go.Status({});
            }

            this.updateState({ status });
            this.shouldNotify = true;
        } catch (error) {
            this.updateState({ status: null });
            this.context.onError(error);
        }
    }

    async componentDidMount() {
        this.go = this.context.go.wrap((key, fn) => this.interceptAPI(key, fn));
        this.refresh();
    }

    private interceptAPI(key: keyof Omit<GoAPI, 'wrap'>, fn: Function): Function | void {
        // Intercept calls that change the vault state
        switch (key) {
        case 'OpenVault':
        case 'UnlockVault':
        case 'LockVault':
            return async (...args) => {
                const r = await fn(...args);
                this.refresh();
                return r;
            }
        }
    }

    private shouldNotify = true;

    render() {
        if (!this.state?.status) {
            return (
                <Spin tip="Loading" />
            )
        }

        // Is the wallet unlocked?
        if (!this.state?.status?.wallet?.unlocked) {
            this.shouldNotify = true;
            return (
                <div>
                    <h3>Unlock wallet</h3>
                    <Unlock vault="index" onUnlock={() => this.refresh()} />
                </div>
            );
        }

        // Are all of the open vaults unlocked?
        if (!this.props.indexOnly) {
            for (const vault of this.state?.status?.vaults || []) {
                if (vault.name && !vault.unlocked) {
                    this.shouldNotify = true;
                    return (
                        <div>
                            <h3>Unlock {vault.name} vault</h3>
                            <Unlock vault={vault.name} onUnlock={() => this.refresh()} />
                        </div>
                    );
                }
            }
        }

        if (this.shouldNotify && this.props.onUnlock) {
            this.shouldNotify = false;
            this.props.onUnlock(this.state.status);
        }

        return this.props.children;
    }
}