import * as acc from "accumulate.js";
import { api } from "../../wailsjs/go/models";
import Component from "../lib/Component";
import { MainContext } from "../lib/MainContext";
import { SignatureType } from "accumulate.js/lib/core";
import { Buffer } from "accumulate.js/lib/common";
import { Spin } from "antd";
import React from "react";

interface Props {
    theKey: Omit<api.Key, 'convertValues'>;
}

interface State {
    address?: string;
}

export default class Address extends Component<Props, State> {
    static contextType = MainContext;
    context: React.ContextType<typeof MainContext>;

    state: State = {};

    async propsDidChange() {
        try {
            const type = SignatureType.byName(this.props.theKey.keyInfo!.type! as any)
            const pub = Buffer.from(this.props.theKey.publicKey! as any, 'hex')
            const address = await acc.Address.fromKey(type, pub)
            this.updateState({ address: await address.format() })
        } catch (error) {
            this.context.onError(error);
        }
    }

    render() {
        if (!this.state.address) return <Spin />
        return <code>{this.state.address}</code>
    }
}