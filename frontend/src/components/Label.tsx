import { Input } from "antd";
import React from "react";

interface Props {
    value: JSX.Element | string;
}
    // border-start-start-radius: 0;
    // border-end-start-radius: 0;
export default function Label({ value }: Props) {
    // This is a hack for approximating the behavior of input addons in a way
    // that is compatible with Space.Compact
    return (
        <div className="input-label">
            <span>{value}</span>
        </div>
    )
        // <Input
        //     readOnly
        //     value={value}
        //     ref={el => el?.input?.setAttribute('size', '1')}
        //     style={{
        //         width: 'auto'
        //     }}
        // />
}