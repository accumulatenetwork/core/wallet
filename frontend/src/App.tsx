import './App.css';
import 'antd/dist/reset.css';
import Vaults from './views/Vaults';
import Component from './lib/Component';
import React from "react";
import { Empty, Layout, Menu, MenuProps, MenuRef, Spin, notification } from 'antd';
import { Link, Location, NavigateFunction, Route, RouteProps, Routes, useLocation, useNavigate } from 'react-router-dom';
import { MainContext } from './lib/MainContext';
import Go, { GoAPI } from './lib/Go';
import * as go from "../wailsjs/go/main/App";
import { BankOutlined, KeyOutlined, DollarOutlined, WalletOutlined } from '@ant-design/icons';
import Keys from './views/Keys';
import { JsonRpcClient } from 'accumulate.js/lib/api_v3';
import TokenAccounts from './views/TokenAccounts';
import TokenAccount from './views/TokenAccount';
import { MainNet, Start, Wallet } from './views/Start';
import { CreateWallet } from './views/CreateWallet';
import Key from './views/Key';

interface Props {}

interface DynamicProps extends Props {
    navigate: NavigateFunction
    location: Location
}

interface State {
    go: GoAPI
    wallet?: Wallet;
    modal?: JSX.Element;
}

type Elem<A> = A extends readonly (infer T)[] ? T : never;

export class App extends Component<DynamicProps, State> {
    private setError(err) {
        let message;
        if (typeof err !== 'object')
            message = err;
        else if (!err.data)
            message = err.message;
        else if (typeof err.data === 'string')
            message = err.data;
        else if (typeof err.data.message === 'string')
            message = err.data.message;
        else
            message = err.message;
        notification.error({ message });
        console.trace(message);
        console.info(err.stack);
    }

    private el = {
        sider: React.createRef<HTMLDivElement>(),
        menu: React.createRef<MenuRef>(),
    };

    private closeModal() {
        this.updateState({ modal: undefined })
    }

    private async openStart(first: boolean, path?: string) {
        if (!first) {
            try {
                await go.Close()
            } catch (error) {
                this.setError(error);
            }
        }

        const done = async (wallet: Wallet) => {
            try {
                this.closeModal()
                const go = await Go(wallet.path);
                this.updateState({ go, wallet });

                const status = await go.Status({});
                if (!status.wallet?.exists) {
                    this.openCreate();
                    return;
                }

            } catch (error) {
                this.setError(error);
            }
        }

        const modal = <Start first={first} didOpen={done} path={path} />
        this.updateState({ modal, go: undefined, wallet: undefined })
    }

    private async openCreate() {
        const done = async () => {
            try {
                this.closeModal();
            } catch (error) {
                this.setError(error);
            }
        }

        const modal = <CreateWallet path={this.state!.wallet!.path} didCreate={done} />
        this.updateState({ modal })
    }

    async componentDidMount() {
        let path: string | undefined;
        try {
            path = await go.Started();
        } catch (error) {
            this.setError(error);
        }

        this.openStart(true, path);
    }

    componentDidCatch(error: Error, errorInfo: React.ErrorInfo): void {
        this.setError(error)
    }

    render() {
        const routes: (Elem<MenuProps['items']> & Omit<RouteProps, 'path'> & { paths: string[] })[] = [
            {
                label: <Link to="/">Keys</Link>,
                key: 'keys',
                icon: <KeyOutlined />,
                paths: ['/', '/keys'],
                element: <Keys />,
            },
            {
                label: <Link to="/tokens">Tokens</Link>,
                key: 'list-tokens',
                icon: <DollarOutlined />,
                paths: ['/tokens'],
                element: <TokenAccounts />,
            },
            {
                label: <Link to="/vaults">Vaults</Link>,
                key: 'vaults',
                icon: <BankOutlined />,
                paths: ['/vaults'],
                element: <Vaults />,
                style: { marginTop: 'auto' },
            },
        ]
        const path = '/' + this.props.location.pathname.split('/')[1] || ''
        const route = routes.find(r => r.paths.includes(path))

        return (
            <div id="app">
                <Layout className="layout">
                    {this.state?.wallet && this.state.wallet.network != MainNet && <Layout.Header id='testnet-header'><span>Connected to {this.state.wallet.networkName || this.state.wallet.network}</span></Layout.Header>}
                    <Layout hasSider>
                        <Layout.Sider ref={this.el.sider}>
                            <Menu
                                ref={this.el.menu}
                                theme='dark'
                                mode='inline'
                                selectable={false}
                                style={{ display: 'flex', flexFlow: 'column', height: '100%' }}
                                items={[
                                    ...routes,
                                    {
                                        label: 'Close wallet',
                                        key: 'close-wallet',
                                        icon: <WalletOutlined />,
                                        onClick: () => { this.openStart(false) },
                                    }
                                ]}
                                selectedKeys={route ? [`${route.key}`] : []}
                                />
                        </Layout.Sider>
                        <Layout.Content style={{ padding: '1em', overflow: 'auto' }}>
                            <MainContext.Provider value={{
                                onError: e => this.setError(e),
                                go: this.state?.go,
                                network: this.state?.wallet && new JsonRpcClient(`${this.state.wallet.network}/v3`),
                            }}>
                                { this.state?.modal ?
                                    this.state.modal
                                : !this.state?.go ?
                                    <Spin tip="Loading" />
                                :
                                    <Routes>
                                        {routes.flatMap(r => r.paths.map(path => ({ ...r, path }))).map(r => <Route key={r.key} path={r.path} element={r.element} />)}
                                        <Route path="/tokens/:account" element={<TokenAccount />} />
                                        <Route path="/keys/:vault/:name" element={<Key />} />
                                        <Route path="/*" element={<Empty />} />
                                    </Routes>
                                }
                            </MainContext.Provider>
                        </Layout.Content>
                    </Layout>
                </Layout>
            </div>
        )
    }
}

export default function(props: Props) {
    return <App {...props} navigate={useNavigate()} location={useLocation()} />
}