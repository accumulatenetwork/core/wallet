import { core } from "accumulate.js";
import { KeyAndVault, SubmitResult, submitTransaction, validateAccount } from "../lib/util";
import { FormModal, FormModalProps } from "../lib/FormModal";
import { api } from "../../wailsjs/go/models";
import Component from "../lib/Component";
import { MainContext } from "../lib/MainContext";
import { Button, Form, FormInstance, Input } from "antd";
import React from "react";
import { Unlocked } from "../components/Unlocked";
import { RiAccountBoxLine } from 'react-icons/ri';
import { DollarOutlined } from '@ant-design/icons';
import { GetSigner } from "../components/GetSigner";
import { AccountType, AddCredits, KeyPage, LiteIdentity, LiteTokenAccount, TokenAccount, Transaction } from "accumulate.js/lib/core";

interface Props {
    sender?: core.LiteTokenAccount | core.TokenAccount;
    recipient?: core.LiteIdentity | core.KeyPage;
    amount: number;
    onFinish: (_: SubmitResult) => void;

    modal: FormModalProps['modal'];
    close: FormModalProps['close'];
}

interface State {
    status?: api.StatusResponse
    oracle?: number
    signer?: KeyAndVault
    sender?: core.LiteTokenAccount | core.TokenAccount
    recipient?: core.LiteIdentity | core.KeyPage
}

interface Values {
    sender: string;
    recipient: string;
    amount: string;
}

export class AddCreditsModal extends Component<Props, State> {
    static contextType = MainContext;
    context: React.ContextType<typeof MainContext>;

    state: State = {};

    async propsDidChange() {
        try {
            const ns = await this.context.network!.networkStatus({ })
            this.updateState({ oracle: ns.oracle!.price! })
        } catch (error) {
            this.context.onError(error)
        }
    }

    private async onFinish(v: Values) {
        const txn = new Transaction({
            header: {
                principal: v.sender,
            },
            body: new AddCredits({
                // The oracle's precision is 2 and ACME's precision is 8 so the
                // net is 10
                amount: 1e10 * Number(v.amount) / this.state.oracle!,
                oracle: this.state.oracle,
                recipient: v.recipient,
            })
        })

        const r = await submitTransaction(this.context, txn, this.state.signer!);
        this.props.onFinish(r);
    }

    render() {
        const form = React.createRef<FormInstance<Values>>();
        return (
            <FormModal
                modal={this.props.modal}
                close={this.props.close}
                form={{
                    layout: "vertical",
                    requiredMark: "optional",
                    ref: form,
                    disabled: !this.state.oracle,
                    initialValues: {
                        sender: this.props.sender?.url?.toString(),
                        recipient: this.props.recipient?.url?.toString(),
                        amount: this.props.amount.toString(),
                    }
                }}
                onFinish={async (v: Values) => {
                    try {
                        await this.onFinish(v);
                    } catch (error) {
                        this.context.onError(error);
                    }
                }}>
                <Unlocked
                    onUnlock={status => this.updateState({ status })}>

                    <Form.Item
                        label="Sender"
                        name="sender"
                        rules={[{
                            required: true,
                            validator: async (_, value) => {
                                try {
                                    const sender = await validateAccount(this.context.network!, value, AccountType.LiteTokenAccount, AccountType.TokenAccount) as LiteTokenAccount | TokenAccount
                                    this.updateState({ sender })
                                } catch (error) {
                                    this.updateState({ sender: undefined })
                                    throw error
                                }
                            },
                        }]}>
                        <Input
                            readOnly={!!this.props.sender}
                            prefix={<RiAccountBoxLine />} />
                    </Form.Item>

                    <Form.Item
                        label="Recipient"
                        name="recipient"
                        rules={[{
                            required: true,
                            validator: async (_, value) => {
                                try {
                                    const recipient = await validateAccount(this.context.network!, value, AccountType.LiteIdentity, AccountType.KeyPage) as LiteIdentity | KeyPage
                                    this.updateState({ recipient })
                                } catch (error) {
                                    this.updateState({ recipient: undefined })
                                    throw error
                                }
                            },
                        }]}>
                        <Input
                            readOnly={!!this.props.recipient}
                            prefix={<RiAccountBoxLine />} />
                    </Form.Item>

                    <Form.Item
                        label="Amount"
                        name="amount"
                        rules={[{
                            required: true,
                            validator: async (_, value: string) => {
                                if (!/^([0-9]+(\.[0-9]*)?|[0-9]*\.[0-9]+)$/.test(value)) {
                                    throw new Error('Amount must be a number')
                                }
                            },
                        }]}>
                        <Input prefix={<DollarOutlined />}/>
                    </Form.Item>

                    {this.state.status && (
                        <GetSigner
                            form={form}
                            principal={this.state.sender}
                            status={this.state.status}
                            creditsRequired={0}
                            onChange={signer => this.updateState({ signer })} />
                    )}

                    <Form.Item>
                        <FormModal.Context children={state =>
                            <Button htmlType="submit" type="primary" loading={state.confirming}>Buy</Button>
                        } />
                    </Form.Item>
                </Unlocked>
            </FormModal>
        )
    }
}