import { api_v3, core } from "accumulate.js";
import { api } from "../../wailsjs/go/models";
import Component from "../lib/Component";
import { MainContext } from "../lib/MainContext";
import { FormModal, FormModalProps } from "../lib/FormModal";
import React from "react";
import { Unlocked } from "../components/Unlocked";
import { Button, Form, FormInstance, Input, Space } from "antd";
import { KeyAndVault, SubmitResult, formatTokenBalanceStr, parseTokenBalance, submitTransaction, validateAccount, validateTokenAmount } from "../lib/util";
import { RiAccountBoxLine } from 'react-icons/ri';
import { DollarOutlined } from '@ant-design/icons';
import { GetSigner } from "../components/GetSigner";
import { AccountType } from "accumulate.js/lib/core";

interface Props {
    account: core.TokenAccount | core.LiteTokenAccount
    issuer: core.TokenIssuer;
    onFinish: (_: SubmitResult) => void;

    modal: FormModalProps['modal'];
    close: FormModalProps['close'];
}

interface State {
    status?: api.StatusResponse
    signer?: KeyAndVault;
}

interface Values {
    amount: string;
    account: string;
    recipient: string;
    balance: string;
}

export class SendTokensModal extends Component<Props, State> {
    static contextType = MainContext;
    context: React.ContextType<typeof MainContext>;

    state: State = {};

    private async onFinish(v: Values) {
        const recipient = (await this.context.network!.query(v.recipient) as api_v3.AccountRecord).account as core.TokenAccount | core.LiteTokenAccount
        const amount = parseTokenBalance(v.amount, this.props.issuer);

        const txn = new core.Transaction({
            header: {
                principal: this.props.account.url,
            },
            body: new core.SendTokens({
                to: [{ amount, url: recipient.url, }],
            })
        })

        const r = await submitTransaction(this.context, txn, this.state.signer!);
        this.props.onFinish(r);
    }

    render() {
        const form = React.createRef<FormInstance<Values>>();
        return (
            <FormModal
                modal={this.props.modal}
                close={this.props.close}
                form={{
                    layout: "vertical",
                    requiredMark: "optional",
                    ref: form,
                    initialValues: {
                        account: this.props.account.url?.toString()!,
                        balance: formatTokenBalanceStr(this.props.account, this.props.issuer),
                    }
                }}
                onFinish={async (v: Values) => {
                    try {
                        await this.onFinish(v);
                    } catch (error) {
                        this.context.onError(error);
                    }
                }}>
                <Unlocked
                    onUnlock={status => this.updateState({ status })}>

                    <Form.Item
                        label="Account"
                        name="account"
                        rules={[{ required: true }]}
                        children={<Input readOnly />} />

                    <Form.Item
                        label="Balance"
                        name="balance"
                        rules={[{ required: true }]}
                        children={<Input readOnly />} />

                    <Form.Item
                        label="Recipient"
                        name="recipient"
                        rules={[{
                            required: true,
                            validator: async (_, value) => {
                                await validateAccount(this.context.network!, value, AccountType.LiteTokenAccount, AccountType.TokenAccount)
                            },
                        }]}>
                        <Input prefix={<RiAccountBoxLine />}/>
                    </Form.Item>

                    <Form.Item
                        label="Amount"
                        name="amount"
                        rules={[{
                            required: true,
                            validator: async (_, value: string) => {
                                validateTokenAmount(this.props.account, this.props.issuer, value)
                            },
                        }]}>
                        <Input prefix={<DollarOutlined />}/>
                    </Form.Item>

                    {this.state.status && (
                        <GetSigner
                            form={form}
                            principal={this.props.account}
                            status={this.state.status}
                            creditsRequired={3}
                            onChange={signer => this.updateState({ signer })} />
                    )}

                    <Form.Item>
                        <FormModal.Context children={state =>
                            <Button htmlType="submit" type="primary" loading={state.confirming}>Send</Button>
                        } />
                    </Form.Item>
                </Unlocked>
            </FormModal>
        )
    }
}