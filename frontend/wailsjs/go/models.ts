export namespace api {
	
	export class CreateWalletRequest {
	    path?: string;
	    multiVault?: boolean;
	    passphrase?: string;
	
	    static createFrom(source: any = {}) {
	        return new CreateWalletRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.path = source["path"];
	        this.multiVault = source["multiVault"];
	        this.passphrase = source["passphrase"];
	    }
	}
	export class CreateWalletResponse {
	
	
	    static createFrom(source: any = {}) {
	        return new CreateWalletResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	
	    }
	}
	export class KeyLabels {
	    lite?: string;
	    hash?: number[];
	    names?: string[];
	
	    static createFrom(source: any = {}) {
	        return new KeyLabels(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.lite = source["lite"];
	        this.hash = source["hash"];
	        this.names = source["names"];
	    }
	}
	export class KeyInfo {
	    type?: number;
	    derivation?: string;
	    // Go type: url
	    walletId?: any;
	
	    static createFrom(source: any = {}) {
	        return new KeyInfo(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.type = source["type"];
	        this.derivation = source["derivation"];
	        this.walletId = this.convertValues(source["walletId"], null);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class Key {
	    privateKey?: number[];
	    publicKey?: number[];
	    keyInfo?: KeyInfo;
	    lastUsedOn?: number;
	    labels?: KeyLabels;
	    pages?: url.URL[];
	
	    static createFrom(source: any = {}) {
	        return new Key(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.privateKey = source["privateKey"];
	        this.publicKey = source["publicKey"];
	        this.keyInfo = this.convertValues(source["keyInfo"], KeyInfo);
	        this.lastUsedOn = source["lastUsedOn"];
	        this.labels = this.convertValues(source["labels"], KeyLabels);
	        this.pages = this.convertValues(source["pages"], url.URL);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class SigningPath {
	    signers?: url.URL[];
	    keys?: Key[];
	
	    static createFrom(source: any = {}) {
	        return new SigningPath(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.signers = this.convertValues(source["signers"], url.URL);
	        this.keys = this.convertValues(source["keys"], Key);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class FindSignerResponse {
	    paths?: SigningPath[];
	
	    static createFrom(source: any = {}) {
	        return new FindSignerResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.paths = this.convertValues(source["paths"], SigningPath);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class GenerateAddressRequest {
	    wallet?: string;
	    vault?: string;
	    type?: number;
	    labels?: string[];
	    force?: boolean;
	
	    static createFrom(source: any = {}) {
	        return new GenerateAddressRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	        this.vault = source["vault"];
	        this.type = source["type"];
	        this.labels = source["labels"];
	        this.force = source["force"];
	    }
	}
	export class GenerateLedgerKeyRequest {
	    wallet?: string;
	    vault?: string;
	    labels?: string[];
	    walletID?: string;
	    sigType?: number;
	
	    static createFrom(source: any = {}) {
	        return new GenerateLedgerKeyRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	        this.vault = source["vault"];
	        this.labels = source["labels"];
	        this.walletID = source["walletID"];
	        this.sigType = source["sigType"];
	    }
	}
	export class GenerateMnemonicRequest {
	    entropy?: number;
	
	    static createFrom(source: any = {}) {
	        return new GenerateMnemonicRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.entropy = source["entropy"];
	    }
	}
	export class GenerateMnemonicResponse {
	    mnemonic?: string[];
	
	    static createFrom(source: any = {}) {
	        return new GenerateMnemonicResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.mnemonic = source["mnemonic"];
	    }
	}
	export class ImportKeyRequest {
	    wallet?: string;
	    vault?: string;
	    seed?: number[];
	    labels?: string[];
	    force?: boolean;
	    type?: number;
	
	    static createFrom(source: any = {}) {
	        return new ImportKeyRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	        this.vault = source["vault"];
	        this.seed = source["seed"];
	        this.labels = source["labels"];
	        this.force = source["force"];
	        this.type = source["type"];
	    }
	}
	export class ImportMnemonicRequest {
	    wallet?: string;
	    vault?: string;
	    mnemonic?: string[];
	
	    static createFrom(source: any = {}) {
	        return new ImportMnemonicRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	        this.vault = source["vault"];
	        this.mnemonic = source["mnemonic"];
	    }
	}
	export class ImportMnemonicResponse {
	
	
	    static createFrom(source: any = {}) {
	        return new ImportMnemonicResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	
	    }
	}
	
	
	
	export class KeyListRequest {
	    wallet?: string;
	    vault?: string;
	
	    static createFrom(source: any = {}) {
	        return new KeyListRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	        this.vault = source["vault"];
	    }
	}
	export class KeyListResponse {
	    vault?: string;
	    keyList?: Key[];
	
	    static createFrom(source: any = {}) {
	        return new KeyListResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.vault = source["vault"];
	        this.keyList = this.convertValues(source["keyList"], Key);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class ListAccountsRequest {
	    wallet?: string;
	    vault?: string;
	
	    static createFrom(source: any = {}) {
	        return new ListAccountsRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	        this.vault = source["vault"];
	    }
	}
	export class TokenAccountInfo {
	    // Go type: url
	    url?: any;
	
	    static createFrom(source: any = {}) {
	        return new TokenAccountInfo(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.url = this.convertValues(source["url"], null);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class BookInfo {
	    // Go type: url
	    url?: any;
	    pages?: protocol.KeyPage[];
	
	    static createFrom(source: any = {}) {
	        return new BookInfo(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.url = this.convertValues(source["url"], null);
	        this.pages = this.convertValues(source["pages"], protocol.KeyPage);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class ListAccountsResponse {
	    books?: BookInfo[];
	    tokens?: TokenAccountInfo[];
	
	    static createFrom(source: any = {}) {
	        return new ListAccountsResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.books = this.convertValues(source["books"], BookInfo);
	        this.tokens = this.convertValues(source["tokens"], TokenAccountInfo);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class ListVaultsRequest {
	    wallet?: string;
	
	    static createFrom(source: any = {}) {
	        return new ListVaultsRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	    }
	}
	export class ListVaultsResponse {
	    vaults?: string[];
	
	    static createFrom(source: any = {}) {
	        return new ListVaultsResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.vaults = source["vaults"];
	    }
	}
	export class LockVaultRequest {
	    wallet?: string;
	    vault?: string;
	    close?: boolean;
	
	    static createFrom(source: any = {}) {
	        return new LockVaultRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	        this.vault = source["vault"];
	        this.close = source["close"];
	    }
	}
	export class LockVaultResponse {
	
	
	    static createFrom(source: any = {}) {
	        return new LockVaultResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	
	    }
	}
	export class OpenVaultRequest {
	    wallet?: string;
	    vault?: string;
	
	    static createFrom(source: any = {}) {
	        return new OpenVaultRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	        this.vault = source["vault"];
	    }
	}
	export class OpenVaultResponse {
	
	
	    static createFrom(source: any = {}) {
	        return new OpenVaultResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	
	    }
	}
	export class RegisterTokenAccountRequest {
	    wallet?: string;
	    vault?: string;
	    // Go type: url
	    url?: any;
	
	    static createFrom(source: any = {}) {
	        return new RegisterTokenAccountRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	        this.vault = source["vault"];
	        this.url = this.convertValues(source["url"], null);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class RegisterTokenAccountResponse {
	
	
	    static createFrom(source: any = {}) {
	        return new RegisterTokenAccountResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	
	    }
	}
	export class ResolveKeyRequest {
	    wallet?: string;
	    vault?: string;
	    value?: string;
	    includePrivateKey?: boolean;
	
	    static createFrom(source: any = {}) {
	        return new ResolveKeyRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	        this.vault = source["vault"];
	        this.value = source["value"];
	        this.includePrivateKey = source["includePrivateKey"];
	    }
	}
	export class ResolveKeyResponse {
	    address?: string;
	    publicKeyHash?: number[];
	    key?: Key;
	
	    static createFrom(source: any = {}) {
	        return new ResolveKeyResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.address = source["address"];
	        this.publicKeyHash = source["publicKeyHash"];
	        this.key = this.convertValues(source["key"], Key);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class SignRequest {
	    wallet?: string;
	    vault?: string;
	    publicKey?: number[];
	    // Go type: url
	    signer?: any;
	    signerVersion?: number;
	    timestamp?: number;
	    vote?: number;
	    delegators?: url.URL[];
	    hash?: number[];
	    transactionName?: string;
	    transaction?: protocol.Transaction;
	
	    static createFrom(source: any = {}) {
	        return new SignRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	        this.vault = source["vault"];
	        this.publicKey = source["publicKey"];
	        this.signer = this.convertValues(source["signer"], null);
	        this.signerVersion = source["signerVersion"];
	        this.timestamp = source["timestamp"];
	        this.vote = source["vote"];
	        this.delegators = this.convertValues(source["delegators"], url.URL);
	        this.hash = source["hash"];
	        this.transactionName = source["transactionName"];
	        this.transaction = this.convertValues(source["transaction"], protocol.Transaction);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class SignResponse {
	    signature?: any;
	    transaction?: protocol.Transaction;
	
	    static createFrom(source: any = {}) {
	        return new SignResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.signature = source["signature"];
	        this.transaction = this.convertValues(source["transaction"], protocol.Transaction);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class StatusRequest {
	    wallet?: string;
	
	    static createFrom(source: any = {}) {
	        return new StatusRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	    }
	}
	export class VaultStatus {
	    name?: string;
	    filePath?: string;
	    onePasswordRef?: string;
	    exists?: boolean;
	    open?: boolean;
	    unlocked?: boolean;
	
	    static createFrom(source: any = {}) {
	        return new VaultStatus(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.name = source["name"];
	        this.filePath = source["filePath"];
	        this.onePasswordRef = source["onePasswordRef"];
	        this.exists = source["exists"];
	        this.open = source["open"];
	        this.unlocked = source["unlocked"];
	    }
	}
	export class WalletStatus {
	    name?: string;
	    filePath?: string;
	    onePasswordRef?: string;
	    exists?: boolean;
	    open?: boolean;
	    unlocked?: boolean;
	    multi?: boolean;
	
	    static createFrom(source: any = {}) {
	        return new WalletStatus(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.name = source["name"];
	        this.filePath = source["filePath"];
	        this.onePasswordRef = source["onePasswordRef"];
	        this.exists = source["exists"];
	        this.open = source["open"];
	        this.unlocked = source["unlocked"];
	        this.multi = source["multi"];
	    }
	}
	export class StatusResponse {
	    wallet?: WalletStatus;
	    vaults?: VaultStatus[];
	
	    static createFrom(source: any = {}) {
	        return new StatusResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = this.convertValues(source["wallet"], WalletStatus);
	        this.vaults = this.convertValues(source["vaults"], VaultStatus);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class UnlockVaultRequest {
	    wallet?: string;
	    vault?: string;
	    use1Password?: boolean;
	    passphrase?: string;
	    timeout?: number;
	
	    static createFrom(source: any = {}) {
	        return new UnlockVaultRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	        this.vault = source["vault"];
	        this.use1Password = source["use1Password"];
	        this.passphrase = source["passphrase"];
	        this.timeout = source["timeout"];
	    }
	}
	export class UnlockVaultResponse {
	    success?: boolean;
	    // Go type: time
	    unlockedUntil?: any;
	
	    static createFrom(source: any = {}) {
	        return new UnlockVaultResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.success = source["success"];
	        this.unlockedUntil = this.convertValues(source["unlockedUntil"], null);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}

}

export namespace main {
	
	export class FindSignerRequest {
	    wallet?: string;
	    vault?: string;
	    authorities?: string[];
	
	    static createFrom(source: any = {}) {
	        return new FindSignerRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.wallet = source["wallet"];
	        this.vault = source["vault"];
	        this.authorities = source["authorities"];
	    }
	}
	export class ParseKeyRequest {
	    address?: string;
	
	    static createFrom(source: any = {}) {
	        return new ParseKeyRequest(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.address = source["address"];
	    }
	}
	export class ParseKeyResponse {
	    privateKey: string;
	    publicKey: string;
	    type: number;
	
	    static createFrom(source: any = {}) {
	        return new ParseKeyResponse(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.privateKey = source["privateKey"];
	        this.publicKey = source["publicKey"];
	        this.type = source["type"];
	    }
	}

}

export namespace protocol {
	
	export class BlockThreshold {
	    minorBlock?: number;
	
	    static createFrom(source: any = {}) {
	        return new BlockThreshold(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.minorBlock = source["minorBlock"];
	    }
	}
	export class TransactionHeader {
	    // Go type: url
	    principal?: any;
	    initiator?: number[];
	    memo?: string;
	    metadata?: number[];
	    holdUntil?: BlockThreshold;
	
	    static createFrom(source: any = {}) {
	        return new TransactionHeader(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.principal = this.convertValues(source["principal"], null);
	        this.initiator = source["initiator"];
	        this.memo = source["memo"];
	        this.metadata = source["metadata"];
	        this.holdUntil = this.convertValues(source["holdUntil"], BlockThreshold);
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}
	export class Transaction {
	    header?: TransactionHeader;
	    body?: any;
	
	    static createFrom(source: any = {}) {
	        return new Transaction(source);
	    }
	
	    constructor(source: any = {}) {
	        if ('string' === typeof source) source = JSON.parse(source);
	        this.header = this.convertValues(source["header"], TransactionHeader);
	        this.body = source["body"];
	    }
	
		convertValues(a: any, classs: any, asMap: boolean = false): any {
		    if (!a) {
		        return a;
		    }
		    if (a.slice) {
		        return (a as any[]).map(elem => this.convertValues(elem, classs));
		    } else if ("object" === typeof a) {
		        if (asMap) {
		            for (const key of Object.keys(a)) {
		                a[key] = new classs(a[key]);
		            }
		            return a;
		        }
		        return new classs(a);
		    }
		    return a;
		}
	}

}

