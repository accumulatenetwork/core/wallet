# Changelog

# 0.4.0

- Updates minimum Go version to 1.19.
- Implements [AIP-001][AIP-001], Accumulate's universal address format.
- Fixes incompatibilities with Accumulate v1.0.2.

[AIP-001]: https://gitlab.com/accumulatenetwork/governance/aip/-/blob/main/AIP/001-universal-address-format.md