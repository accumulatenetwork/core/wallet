all: build

# Go handles build caching, so Go targets should always be marked phony.
.PHONY: all build tags

GIT_DESCRIBE = $(shell git fetch --tags -q ; git describe --dirty)
GIT_COMMIT = $(shell git rev-parse HEAD)
VERSION = gitlab.com/accumulatenetwork/core/wallet/pkg.Version=$(GIT_DESCRIBE)
COMMIT = gitlab.com/accumulatenetwork/core/wallet/pkg.Commit=$(GIT_COMMIT)

# TODO Add mainnet to tags
LDFLAGS = '-X "$(VERSION)" -X "$(COMMIT)"'
FLAGS = $(BUILDFLAGS) -ldflags $(LDFLAGS) -trimpath

build:
	go build $(FLAGS) ./cmd/accumulate

install: build
	sudo cp -f accumulate /usr/local/bin/accumulate